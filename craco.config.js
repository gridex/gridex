const CracoLessPlugin = require('craco-less');
const CracoEsbuildPlugin = require('craco-esbuild');
// const CracoAntDesignPlugin = require('craco-antd');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { '@primary-color': '#2abdd2' },
            javascriptEnabled: true,
          },
        },
      },
    },
    { plugin: CracoEsbuildPlugin },
    // { plugin: CracoAntDesignPlugin },
  ],
  module: {
    rules: [
      {
        test: /\.ts.?$/,
        exclude: ['src/api/', 'src/processor/'],
      },
    ],
  },
};
