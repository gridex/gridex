const { Connection, Transaction, SystemProgram, Account, PublicKey } = require('@solana/web3.js');

const connection = new Connection('https://api.mainnet-beta.solana.com');
acc = [
  172,
  45,
  112,
  194,
  48,
  68,
  190,
  100,
  150,
  217,
  57,
  40,
  169,
  93,
  162,
  203,
  191,
  58,
  28,
  145,
  198,
  249,
  18,
  112,
  205,
  186,
  247,
  199,
  73,
  36,
  96,
  250,
  199,
  119,
  34,
  218,
  74,
  180,
  125,
  100,
  18,
  36,
  230,
  17,
  74,
  80,
  41,
  178,
  24,
  173,
  205,
  172,
  28,
  155,
  184,
  8,
  31,
  119,
  160,
  71,
  24,
  246,
  227,
  236,
];
account = new Account(acc);

const SEND_OPTIONS = {
  skipPreflight: false,
  preflightCommitment: 'max',
};

async function main() {
  const transaction = new Transaction().add(
    SystemProgram.transfer({
      fromPubkey: account.publicKey,
      toPubkey: new PublicKey('2z2sb2pCMYDdEX8cMZDRFjAxGreVuaLjDNYkLN3f47qN'),
      lamports: 4000000000,
    }),
  );
  const { blockhash } = await connection.getRecentBlockhash('max');
  transaction.recentBlockhash = blockhash;
  transaction.sign(account);

  transaction.feePayer = account.publicKey;

  tx = await connection.sendRawTransaction(transaction.serialize(), SEND_OPTIONS);
  console.log(tx);
}

main();
