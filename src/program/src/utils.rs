use core::{
    result::{
        Result,
        Result::{Err, Ok}
    }
};
use solana_program::{
    program_error::ProgramError,
    pubkey::{Pubkey},
};


/// create address wit seed
pub fn create_address_with_seed(
    base: &Pubkey,
    seed: &str,
    program_id: &Pubkey,
) -> (Pubkey, u8) {
    return Pubkey::find_program_address(&[base.as_ref(), seed.as_ref()], &program_id);
}

/// unpack Pubkey from byte array
pub fn unpack_pubkey(input: &[u8]) -> Result<(Pubkey, &[u8]), ProgramError> {
    if input.len() >= 32 {
        let (key, rest) = input.split_at(32);
        let pk = Pubkey::new(key);
        Ok((pk, rest))
    } else {
        Err(ProgramError::InvalidAccountData.into())
    }
}
