//! Program state processor

use byteorder::{ByteOrder, LittleEndian};
use solana_program::{
    account_info::{AccountInfo, next_account_info},
    entrypoint::ProgramResult,
    // log::*,
    // trace,
    // msg,
    program::invoke_signed,
    program_error::{ProgramError},
    pubkey::Pubkey,
};
use solana_program::{
    instruction::{AccountMeta, Instruction},
    system_program,
    system_instruction::{
        create_account,
        transfer,
    },
};

use crate::{
    utils::{create_address_with_seed, /* unpack_pubkey */},
};

/// Program state handler.
pub struct Processor {}

/// balance seed
pub static SEED: &str = "balance";

impl Processor {
    /// Processes an [Instruction](enum.Instruction.html).
    pub fn process(program_id: &Pubkey, accounts: &[AccountInfo], input: &[u8]) -> ProgramResult {
        let (instruction, input) = input.split_last().unwrap();

        return match instruction {
            0 => {
                proxy_instruction(
                    program_id,
                    accounts,
                    input,
                )
            }
            1 => {
                let lamports = LittleEndian::read_u64(input);
                deposit(
                    program_id,
                    accounts,
                    lamports,
                )
            }
            2 => {
                let lamports = LittleEndian::read_u64(input);
                withdraw(
                    program_id,
                    accounts,
                    lamports,
                )
            }
            _ => {
                Err(ProgramError::Custom(0x222))
            }
        };
    }
}


/// proxy transaction in behalf of inner program account
fn proxy_instruction(
    program_id: &Pubkey,
    accounts: &[AccountInfo],
    input: &[u8],
) -> ProgramResult {
    let (gridex_account, accounts) = accounts.split_last().unwrap(); // owning_account
    let (program_account, accounts) = accounts.split_last().unwrap();
    let (program_to_call, accounts) = accounts.split_last().unwrap();
    let (inner_derived_account, accounts) = accounts.split_last().unwrap();
    let (user_account, accounts) = accounts.split_last().unwrap();

    if program_account.key != program_id {
        return Err(ProgramError::IncorrectProgramId.into());
    }

    // check user and derived address
    let (derived_check_address, seed_add) = create_address_with_seed(
        user_account.key,
        &SEED,
        &program_id,
    );
    if inner_derived_account.key != &derived_check_address {
        return Err(ProgramError::InvalidSeeds.into());
    }

    // TODO check our gridex account calling us
    // let (_owning_key, _) = unpack_pubkey(*own_program.data.borrow())?;
    if /* owning_account.key != &owning_key ||*/ !gridex_account.is_signer {
        return Err(ProgramError::MissingRequiredSignature.into());
    }

    // set our derived address as signer account
    let mut accounts_vec = accounts.to_vec();
    accounts_vec.iter_mut().for_each(|acc| {
        acc.is_signer = acc.key == &derived_check_address;
    });
    let accounts = accounts_vec.as_slice();

    let account_metas = accounts.into_iter().map(|acc| {
        AccountMeta {
            pubkey: acc.key.clone(),
            is_signer: acc.is_signer,
            is_writable: acc.is_writable,
        }
    }).collect();
    let accounts = &[accounts, &[program_to_call.clone()]].concat();

    invoke_signed(
        &Instruction {
            program_id: *program_to_call.key,
            accounts: account_metas,
            data: input.to_vec(),
        },
        accounts,
        &[&[user_account.key.as_ref(), SEED.as_ref(), &[seed_add]]],
    )?;

    Ok(())
}


/// instruction deposit
pub fn deposit(
    program_id: &Pubkey,
    accounts: &[AccountInfo],
    lamports: u64,
) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let source_account = next_account_info(account_info_iter)?;
    let destination_account = next_account_info(account_info_iter)?;

    let (derived_address, seed_add) = create_address_with_seed(
        source_account.key,
        &SEED,
        &program_id,
    );

    if destination_account.key != &derived_address {
        return Err(ProgramError::InvalidSeeds.into());
    }

    let instruction = if **destination_account.lamports.borrow() == 0 {
        create_account(
            &source_account.key,
            &destination_account.key,
            lamports,
            0,
            &program_id,
        )
    } else {
        transfer(&source_account.key, &destination_account.key, lamports)
    };

    invoke_signed(
        &instruction,
        &accounts,
        &[&[source_account.key.as_ref(), SEED.as_ref(), &[seed_add]]],
    )?;

    Ok(())
}

/// instruction test withdraw
pub fn withdraw(
    program_id: &Pubkey,
    accounts: &[AccountInfo],
    lamports: u64,
) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let source_account = next_account_info(account_info_iter)?;
    let user_account = next_account_info(account_info_iter)?;

    let (derived_address, seed_add) = create_address_with_seed(
        user_account.key,
        SEED,
        &program_id,
    );
    if source_account.key != &derived_address {
        return Err(ProgramError::InvalidSeeds.into());
    }
    if !user_account.is_signer {
        return Err(ProgramError::MissingRequiredSignature.into());
    }

    **source_account.try_borrow_mut_lamports()? -= lamports;
    **user_account.try_borrow_mut_lamports()? += lamports;

    Ok(())
}
