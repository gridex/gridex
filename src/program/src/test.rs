#[cfg(test)]
mod test {
    use byteorder::{ByteOrder, LittleEndian};
    use std::{mem};
    use solana_program::{
        system_program,
        system_instruction,
        instruction::{Instruction, AccountMeta},
        pubkey::Pubkey,
    };

    use crate::{
        entrypoint::process_instruction,
        processor::{SEED},
        utils::create_address_with_seed,
    };
    use solana_program_test::{processor, ProgramTest};
    use solana_sdk::transaction::Transaction;
    use solana_sdk::signature::{Signer, Keypair};
    use solana_sdk::account::Account;

    #[tokio::test]
    async fn deposit() {
        let program_id = Pubkey::new_unique();
        let mut test = ProgramTest::new(
            "solana_gridex1",
            program_id,
            processor!(process_instruction),
        );


        // create wallet
        let wallet = Keypair::new();
        test.add_account(wallet.pubkey(), Account::new(
            100000,
            0,
            &system_program::id(),
        ));

        let (mut banks_client, _payer, recent_blockhash) = test.start().await;

        let (acc_key, _) = create_address_with_seed(&wallet.pubkey(), &SEED, &program_id);


        let lamports = 20000;
        {
            let mut instruction_data = vec![0; mem::size_of::<u64>()];
            LittleEndian::write_u64(&mut instruction_data, lamports);
            instruction_data.push(1); // instruction Deposit

            let accounts = vec![
                AccountMeta::new(wallet.pubkey(), true),
                AccountMeta::new(acc_key, false),
                AccountMeta::new_readonly(system_program::id(), false),
            ];

            let instruction = Instruction { program_id, data: instruction_data, accounts };
            let mut transaction = Transaction::new_with_payer(
                &[instruction.clone(), instruction],
                Some(&wallet.pubkey()),
            );
            transaction.sign(&[&wallet], recent_blockhash);
            banks_client.process_transactions(vec![transaction.clone(), transaction]).await.unwrap();

            let wallet_result = banks_client.get_account(wallet.pubkey()).await.unwrap().unwrap();
            let inner_acc = banks_client.get_account(acc_key).await.unwrap().unwrap();
            // Processor::process(&program_id, &accounts, &instruction_data).unwrap();
            assert_eq!(wallet_result.lamports, 55000);
            assert_eq!(inner_acc.lamports, lamports * 2);
        }
    }

    #[tokio::test]
    async fn widthdraw() {
        let program_id = Pubkey::new_unique();
        let mut test = ProgramTest::new(
            "solana_gridex1",
            program_id,
            processor!(process_instruction),
        );

        // create wallet
        let gridex_account = Keypair::new();
        let wallet = Keypair::new();
        test.add_account(wallet.pubkey(), Account::new(
            100000,
            0,
            &system_program::id(),
        ));
        test.add_account(gridex_account.pubkey(), Account::new(
            100000,
            0,
            &system_program::id(),
        ));

        let (acc_key, _) = create_address_with_seed(&wallet.pubkey(), &SEED, &program_id);
        test.add_account(acc_key.clone(), Account::new(
            100000,
            0,
            &program_id,
        ));

        let (mut banks_client, _payer, recent_blockhash) = test.start().await;


        let lamports = 20000;
        {
            let mut instruction_data = vec![0; mem::size_of::<u64>()];
            LittleEndian::write_u64(&mut instruction_data, lamports);
            instruction_data.push(2); // instruction Deposit

            let accounts = vec![
                AccountMeta::new(acc_key, false),
                AccountMeta::new(wallet.pubkey(), true),
                AccountMeta::new_readonly(system_program::id(), false),
            ];

            let instruction = Instruction { program_id, data: instruction_data, accounts };
            let mut transaction = Transaction::new_with_payer(
                &[instruction.clone(), instruction],
                Some(&wallet.pubkey()),
            );
            transaction.sign(&[&wallet], recent_blockhash);
            banks_client.process_transactions(vec![transaction.clone(), transaction]).await.unwrap();

            let wallet_result = banks_client.get_account(wallet.pubkey()).await.unwrap().unwrap();
            let inner_acc = banks_client.get_account(acc_key).await.unwrap().unwrap();
            // Processor::process(&program_id, &accounts, &instruction_data).unwrap();
            assert_eq!(wallet_result.lamports, 135000);
            assert_eq!(inner_acc.lamports, 100000 - lamports * 2);
        }
    }

    // #[tokio::test]
    async fn proxy() {
        let program_id = Pubkey::new_unique();
        let mut test = ProgramTest::new(
            "solana_gridex1",
            program_id,
            processor!(process_instruction),
        );

        // create wallet
        let gridex_account = Keypair::new();
        let wallet = Keypair::new();
        test.add_account(wallet.pubkey(), Account::new(
            100000,
            0,
            &system_program::id(),
        ));
        test.add_account(gridex_account.pubkey(), Account::new(
            100000,
            0,
            &system_program::id(),
        ));

        let (acc_key, _) = create_address_with_seed(&wallet.pubkey(), &SEED, &program_id);
        test.add_account(acc_key.clone(), Account::new(
            100000,
            0,
            &program_id,
        ));

        let (mut banks_client, _payer, recent_blockhash) = test.start().await;


        {
            // let lamports = 20000;
            // let mut instruction_data = vec![0; mem::size_of::<u64>()];
            // LittleEndian::write_u64(&mut instruction_data, lamports);
            // instruction_data.push(1); // instruction Deposit

            // let accounts = vec![
            //     AccountMeta::new(wallet.pubkey(), true),
            //     AccountMeta::new(acc_key, false),
            //     AccountMeta::new_readonly(system_program::id(), false),
            // ];

            // let deposit_instruction = Instruction {
            //     program_id,
            //     data: instruction_data,
            //     accounts: accounts.clone(),
            // };

            let mut proxy_transfer_instruction = system_instruction::transfer(
                &acc_key,
                &wallet.pubkey(),
                10000,
            );
            {
                // update instruction to proxy it thru our program.
                proxy_transfer_instruction.data.push(0); // instruction 0
                proxy_transfer_instruction.program_id = program_id.clone(); // change program_id to proxy
                let accs = &mut proxy_transfer_instruction.accounts;
                accs[0].is_signer = false; // remove signer-accounts, we will be signer

                accs.push(AccountMeta::new_readonly(wallet.pubkey(), false)); // add all the keys,
                accs.push(AccountMeta::new(acc_key, false)); // we need to run transasction thru
                accs.push(AccountMeta::new_readonly(system_program::id(), false)); // proxy in this order
                accs.push(AccountMeta::new_readonly(program_id, false));
                accs.push(AccountMeta::new_readonly(gridex_account.pubkey(), true));
            }


            let mut transaction = Transaction::new_with_payer(
                &[proxy_transfer_instruction],
                Some(&gridex_account.pubkey()),
            );
            transaction.sign(&[&gridex_account], recent_blockhash);
            banks_client.process_transaction(transaction).await.unwrap();

            let wallet_result = banks_client.get_account(wallet.pubkey()).await.unwrap().unwrap();
            let gridex_result = banks_client.get_account(gridex_account.pubkey()).await.unwrap().unwrap();
            let inner_acc = banks_client.get_account(acc_key).await.unwrap().unwrap();

            assert_eq!(inner_acc.owner, program_id);
            // Processor::process(&program_id, &accounts, &instruction_data).unwrap();
            assert_eq!(wallet_result.lamports, 110000);
            assert_eq!(gridex_result.lamports, 95000);
            assert_eq!(inner_acc.lamports, 90000);
        }
    }
}
