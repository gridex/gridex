//! Serum DEX Grid Bot
#![deny(missing_docs)]
#![forbid(unsafe_code)]

pub mod error;
pub mod processor;
mod test;
mod utils;
// pub mod instruction;

#[cfg(not(feature = "no-entrypoint"))]
mod entrypoint;

// Export current sdk types for downstream users building with a different sdk version
pub use solana_program;

