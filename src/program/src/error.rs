//! Error types

use num_derive::FromPrimitive;
use solana_program::{
    decode_error::DecodeError,
    program_error::{PrintProgramError, ProgramError},
    msg,
};
use thiserror::Error;
use num_traits::FromPrimitive;

/// Errors that may be returned by the TokenSwap program.
#[derive(Clone, Debug, Eq, Error, FromPrimitive, PartialEq)]
pub enum GridexError {
    /// The account cannot be initialized because it is already being used.
    #[error("Test error")]
    TestError,
    /// Greeted account does not have the correct program id
    #[error("Greeted account does not have the correct program id")]
    IncorrectProgramId,
    /// Greeted account data length too small for u32
    #[error("Greeted account data length too small for u32")]
    InvalidAccountData
}
impl From<GridexError> for ProgramError {
    fn from(e: GridexError) -> Self {
        ProgramError::Custom(e as u32)
    }
}
impl<T> DecodeError<T> for GridexError {
    fn type_of() -> &'static str {
        "Swap Error"
    }
}

impl PrintProgramError for GridexError {
    fn print<E>(&self)
        where
            E: 'static + std::error::Error + DecodeError<E> + PrintProgramError + FromPrimitive,
    {
        match self {
            GridexError::TestError => msg!("Test Error"),
            GridexError::IncorrectProgramId => msg!("Greeted account does not have the correct program id"),
            GridexError::InvalidAccountData => msg!("Greeted account data length too small for u32")
        }
    }
}
