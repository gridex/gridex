import {
  getBot,
  getNewMessages,
  checkInvitedStatus,
  checkJoinPending,
  checkJoinByCodePending
} from './functions';
import wait from '../common/utils/wait';

export async function telegramBotLoop() {
  const bot = getBot();

  async function checkInvite(botKey, ctx) {
    let text;
    const action = await checkInvitedStatus(botKey, ctx.update.message.from.id);
    if (action === "no_account") {
      text = "Sorry we don' t find your account.\n\nPlease visit <a href='https://gridex.io/app/grid-bot/'>gridex.io/app/grid-bot</a>. And click 'Connect' button";
    } else if (action === "saved") {
      text = "Thank you for your attention.\n\nKeep calm and wait your invite.";
    } else {
      text = "Hey! You are already invited!\n\nCheck your account and start your first Grid Bot!\n\n<a href='https://gridex.io/app/grid-bot/'>gridex.io/app/grid-bot</a>";
    }
    return text;
  }

  bot.start(async (ctx) => {
    if (ctx.update.message.chat.type === "private") {
    let text;
      if (ctx.startPayload) {
        const { startPayload } = ctx;
        if (startPayload.startsWith('join_')) {
          text = await checkJoinPending(startPayload, ctx);
        } else {
          text = await checkInvite(startPayload, ctx);
        }
      } else {
        text = 'Hello. I am GridEx telegram bot. If you want to be invited to GridEx Testing Program - send me your wallet/bot public key. Thank you.';
      }
      ctx.reply(text, { parse_mode: "HTML" });
    }
  });

  bot.on('text', async (ctx) => {
    if (ctx.update.message.chat.type === "private") {
    const message = ctx.message.text;
    let text;
    if (message.startsWith("join_")) {
      text = await checkJoinByCodePending(message, ctx);
    } else {
      text = await checkInvite(message, ctx);
    }
    ctx.reply(text, { parse_mode: "HTML" });
    }
  })

  bot.launch();

  async function loop() {
    const messages = await getNewMessages();
    if (messages && messages[0]) {
      for (const message of messages) {
        await message.send();
      }
    }
  }

  while(true) {
    loop();
    await wait(1000);
  }
}
