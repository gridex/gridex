import { Telegraf } from 'telegraf';
import model from '../api/utils/model';
import { TelegramBotMessage } from '../common/models/telegram_bot_message';

let bot;

type SimpleMessage = {
  to: number,
  text: string,
};

export function getBot() {
  if (!bot) {
    console.log('TELEGRAM_BOT_TOKEN', process.env.TELEGRAM_BOT_TOKEN);
    console.log('TELEGRAM_SUPPORT_CHAT_ID', process.env.TELEGRAM_SUPPORT_CHAT_ID);
    console.log('TELEGRAM_BOT_NAME', process.env.TELEGRAM_BOT_NAME);

    bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN || "");
  }

  return bot;
}

export async function sendMessage({ to, text }: SimpleMessage) {
  const bot = getBot();
  try {
    return await bot.telegram.sendMessage(to, text);
  } catch (err) {
    return err
  }
};

export async function addMessage({ to, text }: SimpleMessage) {
  const botMessages = await model("telegram_messages");
  await botMessages.add({
    to, text, date: new Date(), result: null,
  });
}

class Message {
  private message: TelegramBotMessage;
  constructor(data: TelegramBotMessage) {
    this.message = data;
  }

  async send() {
    const result = await sendMessage({
      to: this.message.to,
      text: this.message.text,
    });
    const botMessages = await model("telegram_messages");
    await botMessages.updateOne(this.message._id, {
      ...this.message,
      send_date: new Date(),
      result
    });
    return true;
  }
}

export async function getNewMessages(): Promise<Message[]> {
  const botMessages = await model("telegram_messages");
  return (await botMessages.find({
    result: null,
    text: { $ne: null }
  })).map(m => new Message(m));
}

export async function checkInvitedStatus(publicKeyString, telegramId): Promise<string | "saved" | "no_account"> {
  const invitesWaitModel = await model("invite_wait_list");
  const keysModel = await model("bot_keys");

  const account = await keysModel.findOne({
    $or: [
      { botAccountPublicKey: publicKeyString },
      { publicKey: publicKeyString },
    ]
  });

  if (account) {
    await keysModel.updateOne(account._id, {
      telegramId
    });
  }

  if (account && account.activator) {
    return account.botAccountPublicKey;
  } else if (account) {
    if (!(await invitesWaitModel.findOne({ botAccountPublicKey: account.botAccountPublicKey }))) {
      await invitesWaitModel.add({
        createdAt: new Date(),
        botAccountPublicKey: account.botAccountPublicKey,
        telegramId
      });
    }
    return "saved";
  }

  if (!(await invitesWaitModel.findOne({ telegramId }))) {
    await invitesWaitModel.add({
      createdAt: new Date(),
      botAccountPublicKey: "",
      telegramId
    });
  }

  return "no_account";
}

export const checkJoinPending = async (payload: string, ctx) => {
  try {
    const publicKey = payload.replace('join_', '');
    const botKeys = model('bot_keys');
    const currentBotKeys = await botKeys.findOne({ publicKey });
    const id = currentBotKeys._id;
    const tgBotJoinPending = model('tg_bot_join_pending');
    const tgBotPendingData = await tgBotJoinPending.find({
      publicKey
    });
    const pendingItem = tgBotPendingData.find((item) => {
      return item.expiresIn > Date.now();
    });
    if (pendingItem) {
      await botKeys.updateOne(id, {
        telegramId:  ctx.update.message.from.id
      });
      return 'Hello and welcome to grid bot!';
    } else {
      return 'Go to gridex account and generate new link!';
    }
  } catch (err) {
    return JSON.stringify(err);
  }
}

export const checkJoinByCodePending = async (codeText: string, ctx) => {
  try {
    const botKeys = model('bot_keys');
    const tgBotJoinPending = model('tg_bot_join_pending');
    const tgBotPendingData = await tgBotJoinPending.find({
      botCode: codeText
    });
    const pendingItem = tgBotPendingData.find((item) => item.expiresIn > Date.now());
    if (pendingItem) {
      const currentBotKeys = await botKeys.findOne({ publicKey: pendingItem.publicKey });
      const id = currentBotKeys._id;
      await botKeys.updateOne(id, {
        telegramId:  ctx.update.message.from.id
      });
      return 'Hello and welcome to grid bot!';
    } else {
      return `Sorry but code \`${codeText}\` has been expired. Go to gridex.io and generate new code!`;
    }
  } catch (err) {
    return JSON.stringify(err);
  }
}
