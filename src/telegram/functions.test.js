import { addMessage, getNewMessages, sendMessage } from './functions';
import dotenv from 'dotenv';
import testsBase from '../processor/utils/testsBase';
import model from '../api/utils/model';
dotenv.config();

testsBase();

const TEST_TELEGRAM_ID = +process.env.TEST_TELEGRAM_ID;

describe('test send message', function () {
  let result = null;
  beforeEach(async (done) => {
    result = await sendMessage({
      to: TEST_TELEGRAM_ID,
      text: 'test',
    });
    done();
  });

  it('test sending result', () => {
    expect(typeof result.message_id).toEqual('number');
  });
});

describe('test add message to db', function () {
  let messages;
  const text = `Hello, ${Math.random()}`;

  beforeEach(async (done) => {
    await addMessage({
      to: TEST_TELEGRAM_ID,
      text,
    });
    const telegramMessagesModel = await model('telegram_messages');
    messages = await telegramMessagesModel.get(1);
    done();
  });

  it('test last message in db', () => {
    const [message] = messages;
    const { _id, date, ...rest } = message;
    expect(rest).toEqual({
      to: TEST_TELEGRAM_ID,
      text,
      result: null,
    });
    expect(date.constructor.name === 'Date').toBeTruthy();
    expect(_id.constructor.name === 'ObjectID').toBeTruthy();
  });

  describe('get new messages', () => {
    let newMessages;

    beforeEach(async (done) => {
      newMessages = await getNewMessages();
      done();
    });

    it('check last message by method', () => {
      const [message] = newMessages;

      const { _id, date, to, result, text: _text } = message.message;
      expect(to).toEqual(TEST_TELEGRAM_ID);
      expect(text).toEqual(_text);
      expect(result).toEqual(null);
      expect(date.constructor.name === 'Date').toBeTruthy();
      expect(_id.constructor.name === 'ObjectID').toBeTruthy();

      expect(typeof message.send).toEqual('function');
    });
  });
});
