import React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';

import { Header, Content, Footer } from '../components/landing';

const StyledLayout = styled(Layout)`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  background-color: #10161d;
`;

export default function GridBorPage() {
  return (
    <StyledLayout>
      <Header />
      <Content />
      <Footer />
    </StyledLayout>
  );
}
