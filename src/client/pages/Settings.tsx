import React from 'react';
import { Col, Row } from 'antd';
import ExplorerSettings from '../components/ExplorerSetting';
import TelegramLinking from '../components/TelegramLinking';
import BalanceWidgetSettings from '../components/BalanceWidgetSettings';

const Settings = () => {
  return (
    <>
      <Row>
        <Col span={24}>
          <TelegramLinking />
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <ExplorerSettings />
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <BalanceWidgetSettings />
        </Col>
      </Row>
    </>
  );
};

export default Settings;
