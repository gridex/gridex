import React, { useState } from 'react';
import { Tabs } from 'antd';

import FloatingElement from '../components/layout/FloatingElement';
import Trades from '../components/Trades';
import { UrlMarketProvider } from '../utils/markets';

export default function TradePage() {
  const [tabKey, setTabKey] = useState('trade');

  return (
    <FloatingElement style={{ flex: 1 }}>
      <UrlMarketProvider>
        <Tabs activeKey={tabKey} onTabClick={(key) => setTabKey(key)}>
          <Tabs.TabPane tab="Trade" key="trade">
            <Trades />
          </Tabs.TabPane>
        </Tabs>
      </UrlMarketProvider>
    </FloatingElement>
  );
}
