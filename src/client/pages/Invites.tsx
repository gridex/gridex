import React, { useEffect, useState } from 'react';
import { dateFormat } from '../utils/dateFormat';
import { useBackend } from '../utils/backend';
import DataTable from '../components/layout/DataTable';
import { Col, Row } from 'antd';
import Text from 'antd/es/typography/Text';

const Invites = () => {
  const { backendRequest } = useBackend();
  const [usedInvites, setUsedInvites] = useState<any[]>([]);
  const [newInvites, setNewInvites] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);
  const tableColumns = [
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'coin',
    },
    {
      title: 'Invited account',
      dataIndex: 'activated_account',
      key: 'activated_account',
    },
    {
      title: 'Activation date',
      key: 'activation_date',
      render: (record) => record.activation_date ? dateFormat(record.activation_date) : ""
    },
  ];

  const loadInvites = async () => {
    setLoading(true)
    const data = await backendRequest("/invites", 'get');
    setUsedInvites(data.invites.filter(({ activated_account }) => Boolean(activated_account)));
    setNewInvites(data.invites.filter(({ activated_account }) => !Boolean(activated_account)));
    setLoading(false);
  }

  useEffect(() => {
    loadInvites();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
   <>
     <Row>
       <Col span={24}>
         <h3>New codes</h3>
       </Col>
       <Col span={24}>
         { newInvites.map(({ code }) => <Text code key={code}>{ code }</Text>)}
       </Col>
       <Col span={24}>
         <h3>You will have +1 invite for every started bot every day. Bot musts work more than one day.</h3>
       </Col>
     </Row>
     <Row>
       <Col span={24}>
         <h3>Used codes</h3>
       </Col>
       <Col span={24}>
         <DataTable
           rowKey="_id"
           dataSource={usedInvites}
           columns={tableColumns}
           loading={loading}
         />
       </Col>
     </Row>
   </>
  );
};

export default Invites;