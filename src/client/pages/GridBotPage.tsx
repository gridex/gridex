import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { Tabs } from 'antd';
import { useConnection } from '../utils/connection';
import FloatingElement from '../components/layout/FloatingElement';
import CreateBot from '../components/GridBot/CreateBot';
import BotsTable from '../components/GridBot/BotsTable';
import { notify } from '../utils/notifications';
import { UrlMarketProvider } from '../utils/markets';
import { useBackend } from '../utils/backend';
import { PublicKey } from '@solana/web3.js';
import { useWallet } from '../utils/wallet';
import { TabWallet } from '../components/GridUtils/TabWallet';
import { TabBot } from '../components/GridUtils/TabBot';
import { TabPaperBot } from '../components/GridUtils/TabPaperBot';
import Invites from './Invites';
import Settings from './Settings';
import Dependent from '../components/Dependent';

const { TabPane } = Tabs;

export default function GridBotPage({ tab }: { tab: string }) {
  const [botAccountPublicKey, setBotAccountPublicKey] = React.useState<null | PublicKey>(null);
  const connection = useConnection();
  const { backEndConnected, backendRequest, invited } = useBackend();
  const { connected } = useWallet();

  const history = useHistory();
  const setTabKey = useCallback(
    (tab) => {
      history.push(`/grid-bot/${tab === 'create_bot' ? '' : tab}`);
    },
    [history],
  );
  const onCreated = useCallback(
    ({ _id }) => {
      notify({ message: 'Bot created', description: `id: ${_id}` });
      setTabKey('bots');
    },
    [setTabKey],
  );

  React.useEffect(() => {
    if (backEndConnected && connected) {
      backendRequest('/bot/public_key', 'get').then((data) => {
        if (data.success && data.botAccountPublicKey) {
          setBotAccountPublicKey(new PublicKey(data.botAccountPublicKey));
        }
      });
    } else {
      setBotAccountPublicKey(null);
    }
  }, [connected, backEndConnected, backendRequest]);

  return (
    <FloatingElement style={{ flex: 1 }}>
      <Dependent dependencies={[Boolean(connection)]}>
        <UrlMarketProvider>
          <Tabs activeKey={tab} onTabClick={(key) => setTabKey(key)}>
            <TabPane tab="Create bot" key="create_bot">
              <CreateBot onCreated={onCreated} onChange={() => ({})} />
            </TabPane>
            {backEndConnected && (
              <TabPane tab="Bots List" key="bots">
                {tab === 'bots' && <BotsTable />}
              </TabPane>
            )}
            {botAccountPublicKey && (
              <TabPane tab="Bot info" key="bot">
                <TabBot activeTab={tab} botAccountPublicKey={botAccountPublicKey} />
              </TabPane>
            )}
            { botAccountPublicKey && <TabPane tab="Paper Bot info" key="paper-bot">
              <TabPaperBot activeTab={tab} botAccountPublicKey={botAccountPublicKey} />
            </TabPane>}
            {botAccountPublicKey && (
              <TabPane tab="Account info" key="wallet">
                <TabWallet activeTab={tab} botAccountPublicKey={botAccountPublicKey} />
              </TabPane>
            )}
            {backEndConnected && invited && (
              <TabPane tab="My invites" key="invites">
                <Invites />
              </TabPane>
            )}
            {backEndConnected && invited && (
              <TabPane tab="Settings" key="settings">
                <Settings />
              </TabPane>
            )}
          </Tabs>
        </UrlMarketProvider>
      </Dependent>
    </FloatingElement>
  );
}
