import { createGlobalStyle } from 'styled-components';

export const mainColor = `#0080FF`;
export const secondaryColor = `#0080ff`;
export const buyColor = 'rgba(65, 199, 122, 0.6)';
export const sellColor = 'rgba(242, 60, 105, 0.6)';
export const waitColor = 'rgba(232, 172, 101, 0.6)';


export const GlobalStyle = createGlobalStyle`
html {
  --antd-wave-shadow-color: ${secondaryColor};
}
html,body{
  background: #11161D;
}
input[type=number]::-webkit-inner-spin-button {
  opacity: 0;
}
input[type=number]:hover::-webkit-inner-spin-button,
input[type=number]:focus::-webkit-inner-spin-button {
  opacity: 0.25;
}
/* width */
::-webkit-scrollbar {
  width: 15px;
}
/* Track */
::-webkit-scrollbar-track {
  background: #2d313c;
}
/* Handle */
::-webkit-scrollbar-thumb {
  background: #5b5f67;
}
/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #5b5f67;
}
.ant-slider-track, .ant-slider:hover .ant-slider-track {
  background-color: #0080FF;
  opacity: 0.75;
}
.ant-slider-track,
.ant-slider ant-slider-track:hover {
  background-color: #0080FF;
  opacity: 0.75;
}
.ant-slider-dot-active,
.ant-slider-handle,
.ant-slider-handle-click-focused,
.ant-slider:hover .ant-slider-handle:not(.ant-tooltip-open)  {
  border: 2px solid #0080FF;
}
.ant-table-tbody > tr.ant-table-row:hover > td {
  background: #273043;
}
.ant-table-tbody > tr > td {
  border-bottom: 8px solid #1A2029;
}
.ant-table-container table > thead > tr:first-child th {
  border-bottom: none;
}
.ant-divider-horizontal.ant-divider-with-text::before, .ant-divider-horizontal.ant-divider-with-text::after {
  border-top: 1px solid #434a59 !important;
}
.ant-layout {
    background: #141414;
  }
  .ant-table {
    background: #212734;
  }
  .ant-table-thead > tr > th {
    background: #1A2029;
  }
.ant-select-item-option-content {
  img {
    margin-right: 4px;
  }
}
.ant-modal-content {
  background-color: #212734;
}

.ant-btn {
  :not(.ant-btn-primary) {
    :hover, :focus {
      color: ${mainColor};
      border-color: ${mainColor};
    }
  }

  &.ant-btn-primary {
    background-color: ${secondaryColor};
    border-color: ${secondaryColor};

    :not(:disabled) {
      :hover, :focus {
        background-color: ${mainColor};
        border-color: ${mainColor};
      }
    }
    :disabled {
      color: rgba(255, 255, 255, 0.3);
      background: rgba(255, 255, 255, 0.08);
      border-color: #434343;
      text-shadow: none;
      box-shadow: none;
    }
  }
}

.ant-switch-checked {
  background: ${secondaryColor};
}

.ant-tabs-ink-bar {
  background: ${secondaryColor};
}

.ant-tabs-tab {
  &.ant-tabs-tab-active {
    .ant-tabs-tab-btn {
      color: ${secondaryColor};
      font-weight: 300;
    }
  }
  .ant-tabs-tab-btn {
    :active {
      color: ${secondaryColor};
    }
  }
  :hover {
    color: ${secondaryColor};
  }
}

.ant-select:not(.ant-select-disabled):hover .ant-select-selector {
  border-color: ${secondaryColor};
}
.ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input) .ant-select-selector {
  border-color: ${secondaryColor};
}

.ant-menu-horizontal:not(.ant-menu-dark) {
 > .ant-menu-item-selected, .ant-menu-item:hover, .ant-menu-submenu:hover, .ant-menu-submenu-title:hover, .ant-menu-submenu-open, .ant-menu-submenu-selected {
    color: ${mainColor};
    border-color: ${mainColor};
  }
}

.ant-menu-item-selected, .ant-menu-vertical .ant-menu-submenu-selected, .ant-menu-submenu:hover > .ant-menu-submenu-title,
.ant-menu-submenu:hover > .ant-menu-submenu-title > .ant-menu-submenu-arrow {
  &, :hover {
    color: ${mainColor};
    background: transparent;
  }
}

.ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {
  background: transparent;
}

.ant-input:focus, .ant-input-focused {
  border-color: ${mainColor};
  box-shadow: 0 0 0 2px rgba(1, 128, 255, 0.2);
}

.ant-input-affix-wrapper:focus, .ant-input-affix-wrapper-focused, .ant-input-affix-wrapper:hover {
  border-color: ${mainColor};
  box-shadow: 0 0 0 2px rgba(1, 128, 255, 0.2);
}

.ant-input:hover {
  border-color: ${mainColor};
}

::selection {
  color: #fff;
  background: ${mainColor};
}

.ant-menu-item a:hover, .ant-menu-item:hover {
  color: ${mainColor};
}

a, a.ant-typography, .ant-typography a {
  color: ${secondaryColor};
}

.ant-table-tbody > tr {
  &:first-child {
    > td {
      border-bottom: none;
    }
  }
  &:nth-child(2) {
    > td {
      border-top: 8px solid #1A2029;
    }
  }
}

@-webkit-keyframes highlight {
  from { background-color: #0080FF;}
  to {background-color: #1A2029;}
}
@-moz-keyframes highlight {
  from { background-color: #0080FF;}
  to {background-color: #1A2029;}
}
@-keyframes highlight {
  from { background-color: #0080FF;}
  to {background-color: #1A2029;}
}
.flash {
  -moz-animation: highlight 0.5s ease 0s 1 alternate ;
  -webkit-animation: highlight 0.5s ease 0s 1 alternate;
  animation: highlight 0.5s ease 0s 1 alternate;
}`;
