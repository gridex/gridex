let id = 0;

// @ts-ignore
export default function objectId(o: any) {
  if (typeof o.__uniqueid == 'undefined') {
    Object.defineProperty(o, '__uniqueid', {
      value: ++id,
      enumerable: false,
      // This could go either way, depending on your
      // interpretation of what an "id" is
      writable: false,
    });
  }

  return o.__uniqueid;
}
