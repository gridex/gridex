import { useBackend } from './backend';
import { useCallback, useEffect, useState } from 'react';

export function useBackendRequest(url: string, method: string = 'get'): [any, boolean, () => void] {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const backend = useBackend();
  const update = useCallback(() => {
    setLoading(true);
    backend.backendRequest(url, method).then(
      data => setData(data),
      err => console.error('useBackendRequest', err),
    ).finally(() => setLoading(false));
  }, [setLoading, setData, backend, url, method]);

  useEffect(update, []);

  return [data, loading, update];
}
