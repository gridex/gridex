import { useAsyncData } from './fetch-loop';
import { useBackend } from './backend';


export function useBotKeys() {
  const { backendRequest } = useBackend();
  return useAsyncData(() => backendRequest('/bot/public_key', 'get'), 'botPublicKey');
}
