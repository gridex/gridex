import { useCallback, useState, ChangeEvent } from 'react';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';


function useInputValue<T>(defValue: T): [T, ((evt: ChangeEvent<any>|CheckboxChangeEvent) => void)] {
  const [value, setValue] = useState<T>(defValue);
  const setEvent = useCallback((evt: ChangeEvent<any>|CheckboxChangeEvent) => {
    const newValue = evt.target.value ?? evt.target.checked;
    setValue(newValue)
  }, [setValue]);

  return [value, setEvent];
}

export default useInputValue;
