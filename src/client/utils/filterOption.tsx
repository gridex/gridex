export const filterOption = (input, option) => option?.name?.toLowerCase().indexOf(input.toLowerCase()) >= 0;
