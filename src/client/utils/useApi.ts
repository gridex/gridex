import { useMemo } from 'react';
import { useBackend } from './backend';
import { GridBot } from '../../common/models/grid-bot';


const createGridexApi = (backendRequest, connected) => ({
  connected,
  bots: {
    load(account: string = 'default'): Promise<GridBot[]> {
      return backendRequest(`/grid-bot?account=${account}`);
    },
    botKeys(account: string = 'default') {
      return backendRequest(`/bot/public_key?account=${account}`);
    },
  },
  paperBot: {
    changeBalance(mintAddress: string, amount: number, name?: string) {
      return backendRequest('/paper-bot/change-balance', 'POST', { mintAddress, amount, name });
    },
    exchange(fromMint: string, fromAmount: number, toMint: string) {
      return backendRequest('/paper-bot/exchange', 'POST', { fromMint, fromAmount, toMint });
    },
    async getBalances() {
      const account = await backendRequest(`/bot/public_key?account=paper`);
      return account.balances;
    },
    backtest(bot: Partial<GridBot>) {
      return backendRequest('/paper-bot/backtest', 'POST', { bot });
    },
  },
});


function useApi() {
  const { backendRequest, backEndConnected } = useBackend();
  return useMemo(
    () => createGridexApi(backendRequest, backEndConnected),
    [backendRequest, backEndConnected],
  );
};


export default useApi;
