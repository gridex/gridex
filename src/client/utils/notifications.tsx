import React from 'react';
import { notification } from 'antd';
import Link from '../components/Link';
import { EXPLORER_TX_KEY } from '../APP_SETTINGS';
import { getExplorerUrl } from '../utils/utils';

type Message = {
  message: string;
  type: string;
  description?: string | JSX.Element;
};

let previous: Message = {
  message: '',
  type: '',
  description: '',
};

let notifyTimeout;

export function notify({
  message,
  description,
  txid,
  type = 'info',
  placement = 'bottomLeft',
  duration = 3,
  closeIcon = false,
  options,
}: {
  message: string;
  description?: string | JSX.Element;
  txid?: string;
  type?: string;
  placement?: string;
  duration?: number;
  closeIcon?: boolean;
  options?: any;
}) {
  if (txid) {
    description = (
      <Link external to={getExplorerUrl(txid, EXPLORER_TX_KEY)} style={{ color: '#0000ff' }}>
        View transaction {txid.slice(0, 8)}...{txid.slice(txid.length - 8)}
      </Link>
    );
  }
  (window as any).notify = notify;

  if (message === 'Error loading all market' && previous.message === 'Error loading all market') {
    return;
  }

  if (message !== previous.message || description !== previous.description) {
    previous = {
      message,
      type,
      description,
    };
    notification[type]({
      message: <span style={{ color: 'black' }}>{message}</span>,
      description: <span style={{ color: 'black', opacity: 0.5 }}>{description}</span>,
      placement,
      style: {
        backgroundColor: 'white',
      },
      duration,
      closeIcon,
      ...options,
    });
  }

  if (!notifyTimeout) {
    previous = {
      message,
      type,
      description,
    };

    notifyTimeout = setTimeout(() => {
      notifyTimeout = undefined;
      previous = {
        message: '',
        type: '',
        description: '',
      };
    }, 100);
  }
}

export const message = (message: string, description?: string|JSX.Element, options?: any) =>
  notify({
    message,
    description,
    options,
  });

export const error = (message: string, description: string|JSX.Element, options?: any) =>
  notify({
    message,
    description,
    type: 'error',
    options,
  });
