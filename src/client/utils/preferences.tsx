import React, { useContext } from 'react';
import { useLocalStorageState } from './utils';
import { PreferencesContextValues } from './types';

const PreferencesContext = React.createContext<PreferencesContextValues | null>(
  null,
);

export function PreferencesProvider({ children }) {
  const [autoSettleEnabled, setAutoSettleEnabled] = useLocalStorageState(
    'autoSettleEnabled',
    true,
  );

  const [autoConnectEnabled, setAutoConnectEnabled] = useLocalStorageState(
    "AUTO_CONNECT_ENABLED_STORAGE_KEY",
    false,
  );

  const [loggedIn, setLoggedIn] = useLocalStorageState(
    "LOGGED_IN_STORAGE_KEY",
    false,
  );

  return (
    <PreferencesContext.Provider
      value={{
        autoSettleEnabled,
        setAutoSettleEnabled,
        autoConnectEnabled,
        setAutoConnectEnabled,
        loggedIn,
        setLoggedIn,
      }}
    >
      {children}
    </PreferencesContext.Provider>
  );
}

export function usePreferences(): PreferencesContextValues {
  const context = useContext(PreferencesContext);
  if (!context) {
    throw new Error('Missing preferences context');
  }
  return context;
}
