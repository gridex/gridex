import { useInterval } from './useInterval';
import { settleAllFunds } from './send';
import { useWallet } from './wallet';
import { useAllMarkets, useSelectedTokenAccounts, useTokenAccounts } from './markets';
import { useConnection } from './connection';
import { usePreferences } from './preferences';

export function ServicesProvider({ children }) {
  const { connected, wallet } = useWallet();
  const { autoSettleEnabled } = usePreferences();
  const [tokenAccounts] = useTokenAccounts();
  const [marketList] = useAllMarkets();
  const connection = useConnection();
  const [selectedTokenAccounts] = useSelectedTokenAccounts();

  useInterval(() => {
    const autoSettle = async () => {
      const markets = (marketList || []).map((m) => m.market);
      try {
        console.log('Auto settling');
        await settleAllFunds({
          connection,
          wallet,
          tokenAccounts: tokenAccounts || [],
          markets,
          selectedTokenAccounts,
        });
      } catch (e) {
        console.log('Error auto settling funds: ' + e.message);
      }
    };

    if (connected && wallet?.autoApprove && autoSettleEnabled) autoSettle();
  }, 10000);

  return children;
}

