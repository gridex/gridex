import React, { useContext, useEffect, useState } from 'react';
import { useConnection, useConnectionConfig } from './connection';
import { useOriginalWallet } from './wallet';
import { SystemProgram, Transaction } from '@solana/web3.js';
import { message, notify, error } from './notifications';
import axios, { Method } from 'axios';
import { signTransaction } from './send';
// import set = Reflect.set;

const BackendContext = React.createContext<any>(null);

// @ts-ignore
const apiUrl = '/api';

let refreshing = false;

export const simpleBackendRequest = (url, method: Method = 'GET', params) => {
  return axios(`${apiUrl}${url}`, {
    method,
    data: params,
  }).then(res => {
    console.log('BR', url, method, params, res.data);
    return res.data;
  }).catch(err => { console.error(err); throw err });
}

export function BackendProvider({ children }) {
  const connection = useConnection();
  const { endpointInfo } = useConnectionConfig();
  const { wallet, connected, impersonatePublicKey } = useOriginalWallet();
  const [showInviteModal, setShowInviteModal] = useState(false);
  const [invited, setInvited] = useState<boolean>(false);
  const [backEndConnected, setBackEndConnected] = React.useState(false);
  const [
    backEndConnectionLoading,
    setBackEndConnectionLoading,
  ] = React.useState(false);

  const checkInvited = async () => {
    try {
      const result = await backendRequest('/invited_status', 'get');
      if (result?.invited) {
        setInvited(true);
      }
    } catch (e) {
      // do nothing
    }
  }

  const connectBackEnd = async () => {
    try {
      if (!connected) {
        notify({
          message: 'Wallet not connected',
          description: 'Not connected',
          type: 'error',
        });
        return;
      }
      setBackEndConnectionLoading(true);

      // get secret lamports count to auth the transaction
      const accountPublicKey = wallet.publicKey.toBase58();
      const loginReq: any = await backendRequest('/login-req', 'post', {
        accountPublicKey,
        impersonatePublicKey,
      });
      if (!loginReq.success) {
        notify({
          message: 'Authorization error',
          description: loginReq.error,
          type: 'error',
        });
        return;
      }

      const instruction = SystemProgram.assign({
        accountPubkey: wallet.publicKey,
        basePubkey: wallet.publicKey,
        programId: wallet.publicKey,
        seed: loginReq.secret,
      });

      const { blockhash } = await connection.getRecentBlockhash();
      const transaction = new Transaction();
      transaction.add(instruction);
      transaction.recentBlockhash = blockhash;
      transaction.feePayer = wallet.publicKey;

      let signedTransaction;

      try {
        signedTransaction = await signTransaction({
          transaction,
          wallet,
          connection,
        });
      } catch (e) {
        wallet.disconnect();
        setBackEndConnectionLoading(false);
        return;
      }

      const result: any = await backendRequest('/login', 'post', {
        transaction: signedTransaction.serialize(),
        endpoint: endpointInfo?.name,
        accountPublicKey,
        impersonatePublicKey,
      });

      if (result.success) {
        setBackEndConnected(true);
        checkInvited();
        notify({
          message: 'Backend connected',
        });
      } else {
        notify({
          message: 'Authorization error',
          description: result.error,
          type: 'error',
        });
      }
    } catch (e) {
      console.trace(e);
      notify({
        message: 'Wallet update',
        description: e?.error ?? `${e}`,
        type: 'error',
      });
    }
    setBackEndConnectionLoading(false);
  };

  const disconnectBackEnd = async () => {
    try {
      await backendRequest('/logout', 'post', {
        save_session: refreshing
      });
      setBackEndConnected(false);
      message('Backend disconnected');
    } catch (e) {
      console.error(e);
      error("Logout error", e.message);
    }
  };

  const allowedUnauthorizedRoutes = ['/login-req', '/login', '/logout', '/ping', '/check-klines'];
  const backendRequest = (url: string, method: Method, params: any = {}): Promise<any> => {
    if (!backEndConnected && !allowedUnauthorizedRoutes.includes(url)) {
      const message = `Backend not connected`;
      error('Backend connect', message);
      throw new Error(message);
    }
    return simpleBackendRequest(url, method, params);
  };

  useEffect(() => {
    (async () => {
      if (connected && wallet) {
        try {
          await backendRequest('/ping', 'post');
          setBackEndConnected(true);
          checkInvited();
        } catch (e) {
          if (e.request?.status === 401) {
            await connectBackEnd();
          }
        }
      } else if (backEndConnected) {
        console.log('disconnectBackEnd');
        await disconnectBackEnd();
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [backEndConnected, connected, wallet?.publicKey?.toBase58() ?? null]);

  useEffect(() => {
    if (!connected) {
      setBackEndConnectionLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connected]);

  useEffect(() => {
    const onUnload = () => {
      refreshing = true;
    };
    window.addEventListener("beforeunload", onUnload);
    return () => window.removeEventListener("beforeunload", onUnload);
  }, []);

  return (
    <BackendContext.Provider
      value={{
        backEndConnected,
        backEndConnectionLoading,
        connectBackEnd,
        disconnectBackEnd,
        backendCanBeConnected: connected,
        backendRequest,
        invited,
        checkInvited,
        setShowInviteModal,
        showInviteModal
      }}
    >
      {children}
    </BackendContext.Provider>
  );
}

export function useBackend() {
  const context = useContext(BackendContext);

  if (!context) {
    throw new Error('Missing backend context');
  }

  return {
    backEndConnected: context.backEndConnected,
    backEndConnectionLoading: context.backEndConnectionLoading,
    connectBackEnd: context.connectBackEnd,
    disconnectBackEnd: context.disconnectBackEnd,
    backendCanBeConnected: context.backendCanBeConnected,
    backendRequest: context.backendRequest,
    invited: context.invited,
    setShowInviteModal: context.setShowInviteModal,
    showInviteModal: context.showInviteModal,
    checkInvited: context.checkInvited,
  };
}
