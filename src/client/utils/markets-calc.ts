import { Market, MARKETS, Orderbook, TokenInstructions } from '@project-serum/serum';
import { Connection, PublicKey } from '@solana/web3.js';
import BN from 'bn.js';

import DEVNET_MARKETS_JSON from '../../common/devnet/markets.json';
import DEVNET_TOKEN_MINTS_JSON from '../../common/devnet/tokens.json';

import { CustomMarketInfo, MarketBalance, MarketInfo, TokenAccount } from './types';
import { ENDPOINTS } from './connection';
import { getTokenAccountInfo } from '../../processor/utils/balance/getTokenAccountInfo';
import TOKEN_MINTS from '../../common/utils/tokenMints';

// Used in debugging, should be false in production
const _IGNORE_DEPRECATED = false;

export const MARKETS_MAP: { [key: string]: {
    address: PublicKey;
    name: string;
    programId: PublicKey;
    deprecated: boolean;
  } } = MARKETS.reduce((acc, market) => {
    acc[market.programId.toBase58()] = market;
    return acc;
}, {});

export const DEVNET_MARKETS: typeof MARKETS = DEVNET_MARKETS_JSON.map(
  (market: any) => ({
    ...market,
    address: new PublicKey(market.address),
    programId: new PublicKey(market.programId),
  }),
);
export const DEVNET_TOKEN_MINTS: typeof TOKEN_MINTS = DEVNET_TOKEN_MINTS_JSON.map(
  (token: any) => ({
    ...token,
    address: new PublicKey(token.address),
  }),
);

export const USE_MARKETS: MarketInfo[] = _IGNORE_DEPRECATED
  ? MARKETS.map((m) => ({ ...m, deprecated: false }))
  : MARKETS;

export function calcMarketPrice(
  bids: Orderbook,
  asks: Orderbook,
  trades,
): number | null {
  const bb = bids.getL2(1)[0]?.[0];
  const ba = asks.getL2(1)[0]?.[0];

  if (!bb || !ba) {
    return null;
  }

  const last = trades?.[0]?.price;

  const markPrice = last
    ? [bb, ba, last].sort((a, b) => a - b)[1]
    : (bb + ba) / 2;

  return markPrice;
}

export async function loadMarketPrice(
  connection: Connection,
  market: Market,
): Promise<number | null> {
  const [asksOrderbook, bidsOrderbook, trades] = await Promise.all([
    market.loadAsks(connection),
    market.loadBids(connection),
    market.loadFills(connection, 1),
  ]);
  const marketPrice = calcMarketPrice(bidsOrderbook, asksOrderbook, trades);

  return marketPrice;
}

export function getMarketInfos(
  customMarkets: CustomMarketInfo[],
  endpoint: string = ENDPOINTS[0].endpoint,
): MarketInfo[] {
  const customMarketsInfo = customMarkets.map((m) => ({
    ...m,
    address: new PublicKey(m.address),
    programId: new PublicKey(m.programId),
    deprecated: false,
  }));

  let markets: MarketInfo[] = [];
  if (endpoint === ENDPOINTS[0]?.endpoint) { // main-net
    markets = USE_MARKETS;
  } else if (endpoint === ENDPOINTS[1]?.endpoint) {
    markets = DEVNET_MARKETS;
  } else {
    markets = USE_MARKETS;
  }

  return [...customMarketsInfo, ...markets];
}

export function getTokens(endpoint: string): typeof TOKEN_MINTS {
  if (endpoint === ENDPOINTS[0]?.endpoint) {
    // main-net
    return TOKEN_MINTS;
  } else if (endpoint === ENDPOINTS[1]?.endpoint) {
    // devnet
    return DEVNET_MARKETS;
  }

  return [];
}

export function getDefaultMarket(endpoint: string): MarketInfo | undefined {
  return getMarketInfos([], endpoint).find(m => !m.deprecated);
}

export function getDexProgramId(endpoint: string) {
  const info = getMarketInfos([], endpoint).find(({ deprecated }) => !deprecated);

  return info?.programId.toBase58();
}

export async function loadMarket(
  connection: Connection,
  marketAddress: string,
  programId: string,
): Promise<Market | null> {
  const market = await Market.load(
    connection,
    new PublicKey(marketAddress),
    {},
    new PublicKey(programId),
  );

  return market;
}

export function getSelectedTokenAccountForMint(
  accounts: TokenAccount[] | undefined | null,
  mint: PublicKey | undefined,
  selectedPubKey?: string | PublicKey | null,
) {
  if (!accounts || !mint) {
    return null;
  }
  const filtered = accounts.find(
    ({ effectiveMint, pubkey }) =>
      mint.equals(effectiveMint) &&
      (!selectedPubKey ||
        (typeof selectedPubKey === 'string'
          ? selectedPubKey
          : selectedPubKey.toBase58()) === pubkey.toBase58()),
  );
  return filtered;
}

export async function loadlMarketBalances(
  connection: Connection,
  walletAddress: PublicKey,
  market: Market,
): Promise<MarketBalance> {
  const accounts = await getTokenAccountInfo(connection, walletAddress);
  const selectedTokenAccounts = JSON.parse(
    localStorage.getItem('selectedTokenAccounts') || '{}',
  );
  const getBalance = async (type: 'base' | 'quote'): Promise<number> => {
    const mintAddress = market[`${type}MintAddress`];
    const currencyAccount = getSelectedTokenAccountForMint(
      accounts,
      mintAddress,
      selectedTokenAccounts[mintAddress.toBase58()],
    );
    if (!currencyAccount) {
      return 0;
    }

    const accountInfo = await connection.getAccountInfo(currencyAccount.pubkey);
    if (!accountInfo) {
      return 0;
    }
    if (mintAddress.equals(TokenInstructions.WRAPPED_SOL_MINT)) {
      return accountInfo.lamports / 1e9 ?? 0;
    }
    return market[`${type}SplSizeToNumber`](
      new BN(accountInfo.data.slice(64, 72), 10, 'le'),
    );
  };

  const [base, quote] = await Promise.all([
    await getBalance('base'),
    await getBalance('quote'),
  ]);
  return { base, quote };
}
