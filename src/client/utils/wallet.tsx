import React, { useContext, useEffect, useMemo, useState } from 'react';
import { PublicKey } from '@solana/web3.js';
import { notify } from './notifications';
import { useConnectionConfig } from './connection';
import { useLocalStorageState } from './utils';
// import { WalletContextValues } from './types';
import { usePreferences } from './preferences';
import { EVENT_SIGNING_CANCEL_TRANSACTION } from './send';
import { IWallet } from '../../common/models/wallet';

// wallet adapters
import Wallet from '@project-serum/sol-wallet-adapter';
import { SolongAdapter } from './wallet-adapters/solong_adapter';
import { MathWalletAdapter } from './wallet-adapters/math_wallet_adapter';
import { SolletExtensionAdapter } from './wallet-adapters/sollet_extention';
import { PhantomWalletAdapter } from './wallet-adapters/phantom';
import { LedgerWalletAdapter } from './wallet-adapters/ledger';

type WalletContextType = {
  wallet: IWallet;
  connected: boolean;
  providerUrl: string;
  setProviderUrl: (url: string) => void;
  providerName: string;
  impersonatePublicKey: string;
  setPublicKeyAddress: (key: string) => void;
};
const WalletContext = React.createContext<WalletContextType | null>(null);

interface WalletProvider {
  name: string;
  url: string;
  useInjectedInterface: boolean;
  processed: boolean;
  disabled?: string[];
}

export function useWalletProviders(providerUrl: string = '') {
  const [WALLET_PROVIDERS, setWalletProviders] = React.useState<WalletProvider[]>(() => [
    {
      name: 'Sollet.io',
      url: 'https://www.sollet.io',
      useInjectedInterface: false,
      processed: true,
    },
    {
      name: 'Sollet Extension',
      url: 'https://www.sollet.io/extension',
      useInjectedInterface: false,
      processed: true,
    },
    {
      name: 'Solong Wallet',
      url: 'http://solongwallet.com',
      useInjectedInterface: false,
      processed: true,
    },
    {
      name: 'Math Wallet',
      url: 'https://www.mathwallet.org',
      useInjectedInterface: false,
      processed: false,
    },
    {
      name: 'Bonfida',
      url: 'https://bonfida.com/wallet/',
      useInjectedInterface: false,
      processed: false,
    },
    {
      name: 'Phantom',
      url: 'https://www.phantom.app',
      useInjectedInterface: false,
      processed: false,
    },
    {
      name: 'Ledger',
      url: 'https://www.ledger.com',
      useInjectedInterface: false,
      processed: false,
    },
  ]);

  React.useEffect(() => {
    let timer = 100;
    let iteration = 0;

    function updateProviders() {
      if ((window as any)?.solana?.isMathWallet) {
        setWalletProviders(
          WALLET_PROVIDERS.map((provider) => {
            if (provider.name === 'Math Wallet') {
              provider.useInjectedInterface = true;
              provider.processed = true;
            }
            return provider;
          }),
        );
      } else if (iteration < 100) {
        iteration++;
        timer = iteration < 20 ? 100 : 300;
        setTimeout(updateProviders, timer);
      }
    }

    if (providerUrl === 'https://www.mathwallet.org') {
      updateProviders();
    }
    return () => {
      iteration = 100;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [providerUrl]);

  return {
    WALLET_PROVIDERS,
  };
}

export function useEndpoints(providerUrl: string) {
  const { availableEndpoints } = useConnectionConfig();
  const { WALLET_PROVIDERS } = useWalletProviders();
  const endpoints = availableEndpoints.filter((e) => {
    return (
      e.custom ||
      WALLET_PROVIDERS.some((wp) => wp.url === providerUrl && !wp.disabled?.includes(e.name))
    );
  });

  return endpoints;
}

export function WalletProvider({ children }) {
  const [connected, setConnected] = useState(false);
  const { endpoint } = useConnectionConfig();

  const { autoConnectEnabled, loggedIn } = usePreferences();

  const [providerUrl, setProviderUrl] = useLocalStorageState(
    'walletProvider',
    'http://solongwallet.com',
  );
  const [impersonatePublicKey, setPublicKeyAddress] = useLocalStorageState(
    'impersonatePublicKey',
    '',
  );
  const { WALLET_PROVIDERS } = useWalletProviders(providerUrl);

  const wallet = useMemo(() => {
    let adapter;
    if (providerUrl === 'http://solongwallet.com') {
      (window as any).SolongAdapter = SolongAdapter;
      adapter = new SolongAdapter(providerUrl, endpoint);
    } else if (providerUrl === 'https://www.mathwallet.org') {
      (window as any).MathWalletAdapter = MathWalletAdapter;
      adapter = new MathWalletAdapter();
    } else if (providerUrl === 'https://www.phantom.app') {
      (window as any).solana = PhantomWalletAdapter;
      adapter = new PhantomWalletAdapter();
    } else if (providerUrl === 'https://www.ledger.com') {
      (window as any).ledger = LedgerWalletAdapter;
      adapter = new LedgerWalletAdapter();
    } else if (providerUrl === 'https://www.sollet.io/extension') {
      adapter = SolletExtensionAdapter(providerUrl, endpoint);
      const walletProvider = WALLET_PROVIDERS.find(({ url }) => url === providerUrl);

      if (walletProvider?.useInjectedInterface) {
        adapter._useInjectedInterface = true;
      }
    } else {
      adapter = new Wallet(providerUrl, endpoint);
      const walletProvider = WALLET_PROVIDERS.find(({ url }) => url === providerUrl);

      if (walletProvider?.useInjectedInterface) {
        adapter._useInjectedInterface = true;
      }
    }

    adapter.processed = true;

    return adapter;
  }, [providerUrl, endpoint, WALLET_PROVIDERS]);

  useEffect(() => {
    console.log('use new provider:', providerUrl, ' endpoint:', endpoint, WALLET_PROVIDERS);

    wallet.on('connect', () => {
      localStorage.removeItem('feeDiscountKey');
      setConnected(true);
      const walletPublicKey = wallet.publicKey.toBase58();
      const keyToDisplay =
        walletPublicKey.length > 20
          ? `${walletPublicKey.substring(0, 7)}.....${walletPublicKey.substring(
              walletPublicKey.length - 7,
              walletPublicKey.length,
            )}`
          : walletPublicKey;
      notify({
        message: 'Wallet update',
        description: 'Connected to wallet ' + keyToDisplay,
      });
    });

    wallet.on('disconnect', () => {
      setConnected(false);
      notify({
        message: 'Wallet update',
        description: 'Disconnected from wallet',
      });
      localStorage.removeItem('feeDiscountKey');
    });

    if (wallet && wallet.processed && autoConnectEnabled && loggedIn) {
      setTimeout(() => {
        wallet.connect();
      }, 2000);
    }

    return () => {
      wallet.disconnect();
      setConnected(false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [wallet]);

  useEffect(() => {
    if (!connected) {
      window.dispatchEvent(new CustomEvent(EVENT_SIGNING_CANCEL_TRANSACTION));
    }
  }, [connected]);

  const providerName = WALLET_PROVIDERS.find(({ url }) => url === providerUrl)?.name ?? providerUrl;

  return (
    <WalletContext.Provider
      value={{
        wallet,
        impersonatePublicKey,
        setPublicKeyAddress,
        connected,
        providerUrl,
        setProviderUrl,
        providerName,
      }}
    >
      {children}
    </WalletContext.Provider>
  );
}

export function useWallet(): WalletContextType {
  const context = useContext(WalletContext);
  if (!context) {
    throw new Error('Missing wallet context');
  }

  return useMemo(() => {
    if (context.impersonatePublicKey) {
      const wallet = Object.create(context.wallet, {
        publicKey: {
          get() {
            return (
              this.impersonatePublicKey ||
              (this.impersonatePublicKey = new PublicKey(context.impersonatePublicKey))
            );
          },
        },
      });
      return {
        ...context,
        wallet,
      };
    }

    return context;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context.impersonatePublicKey, context]);
}

export function useOriginalWallet(): WalletContextType {
  const context = useContext(WalletContext);
  if (!context) {
    throw new Error('Missing wallet context');
  }

  return context;
}
