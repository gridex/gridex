import { USE_MARKETS } from './markets-calc';

export const findTVMarketFromAddress = (marketAddressString: string) => {
  return (USE_MARKETS.find(({ address }) => address.toBase58() === marketAddressString) || { name: '' }).name;
};
