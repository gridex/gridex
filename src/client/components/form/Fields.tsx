import * as React from 'react';
import { ReactElement, useCallback, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { Field, useField } from 'react-final-form';
import { FieldSubscription } from 'final-form';
import { Form, Input, Select, Switch } from 'antd';
import { InputProps } from 'antd/lib/input';
import { SelectProps } from 'antd/lib/select';
import { SwitchProps } from 'antd/lib/switch';

// connection, spl-token(solong wallet usdt), bot-account usdt, size -> transaction

interface InputFieldProps<T = any> {
  name: string;
  label?: string|ReactElement;
  meta?: any;
  onChange?: (T) => void;
}

interface NumberFieldProps extends InputFieldProps {
  precision?: number;
}

interface InputInputProps<T = any> extends InputFieldProps<T> {
  precision?: number;
  value: any;
}

// final-form field level subscriptions
const FIELD_SUB: FieldSubscription = { touched: true, error: true, submitError: true, value: true };

export const InputField = ({ name, label, ...inputProps }: InputFieldProps & InputProps) => {
  return (
    <Field
      name={name}
      subscription={FIELD_SUB}
      render={({ input, meta }) => (
        <Form.Item
          validateStatus={meta.error && 'error'}
          help={meta.error}
          // label={label}
        >
          <Input
            {...inputProps}
            {...input}
            addonBefore={label}
          />
        </Form.Item>
      )}
    />
  );
};

export const NumberField = ({ name, label, ...numberProps }: NumberFieldProps & InputProps) => {
  const { input, meta } = useField(name, { subscription: FIELD_SUB });
  return <NumberInput {...input} meta={meta} {...numberProps} label={label} />;
};

export const NumberInput = ({ label,  value: inputValue, onChange: inputOnChange, precision = 3, meta = {}, ...numberProps }: InputInputProps<number> & InputProps) => {
  const [selfValue, setValue] = React.useState(`${inputValue}`);

  const onChange = (evt) => {
    const value = evt.target.value;
    if (!value || new RegExp(`^(-)?[0-9]{1,6}${precision ? `(\\.\\d{0,${precision}})?` : ''}$`).test(value)) {
      let numberValue = parseFloat(value || '0');
      if (numberProps.min && numberValue < numberProps.min) {
        // numberValue = +numberProps.min;
      } else if (numberProps.max && numberValue > numberProps.max) {
        numberValue = +numberProps.max;
      } else if (!Number.isNaN(numberValue)) {
        inputOnChange?.(numberValue);
      }
      setValue(value);
    }
  };
  useEffect(() => { // update value on external change (new initial values)
    if (+inputValue !== +selfValue) {
      setValue(inputValue);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inputValue]);

  return (
    <>
      <Form.Item
        validateStatus={(meta.error || meta.submitError) && 'error'}
        help={(meta.error || meta.submitError)}
      >
        <Input
          {...numberProps}
          value={selfValue}
          onChange={onChange}
          type="text"
          addonBefore={label}
        />
      </Form.Item>
    </>
  );
};

type OptionsType1 = ({ label: string, value: any } | string)[];

interface SelectorProps extends Omit<SelectProps<any>, 'options'> {
  onChange: (value: any) => void;
  options: OptionsType1;
  search?: boolean;
}

export const Selector = ({ options = [], value, onChange, search, ...props }: SelectorProps) => {
  const opt = useMemo(
    () => options.map(o => (typeof o === 'object' ? o : { value: o, label: o })),
    [options],
  );
  const change = useCallback(i => onChange(opt[i].value), [onChange, opt]);

  let val;
  if (value != null) {
    const valueIdx = opt.findIndex(o => o.value === value);
    val = valueIdx < 0 ? '' : valueIdx;
  }

  let searchProps;
  if (search) {
    searchProps = {
      showSearch: true,
      filterOption: (input, option) => {
        return option!.children.toLowerCase().includes(input.toLowerCase());
      },
    };
  }

  return (
    <Select {...props} {...searchProps} value={val} onChange={change}>
      {opt.map((o, i) => (
        <Select.Option value={i} key={i}>
          {o.label}
        </Select.Option>
      ))}
    </Select>
  );
};

interface SelectFieldProps extends InputFieldProps {
  options: OptionsType1;
}

export const SelectField = ({ name, label, ...selectorProps }: SelectFieldProps) => {
  return (
    <Field
      name={name}
      subscription={FIELD_SUB}
      render={({ input, meta }) => (
        <Form.Item
          validateStatus={meta.error && 'error'}
          help={meta.error}
          label={label}
        >
          <Selector {...selectorProps} {...input} />
        </Form.Item>
      )}
    />
  );
};

const ItemRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const ItemText = styled.span`
  max-width: 50%;
  font-weight: 500;
`;
const ItemValue = styled.span`
`;

interface LabelItemProps {
  label: string | ReactElement;
  value: string | ReactElement;
  addonAfter?: string | ReactElement;
}

export const LabelField = ({ label, value, addonAfter }: LabelItemProps) => {
  return (
    <ItemRow>
      <ItemText>{label}</ItemText>
      <div>
        <ItemValue>{value}</ItemValue>
        {addonAfter && <ItemText>{addonAfter}</ItemText>}
      </div>
    </ItemRow>
  );
};


export const SwitchField = ({ name, label, ...inputProps }: SwitchProps & InputFieldProps) => {
  return (
    <Field
      name={name}
      subscription={FIELD_SUB}
      render={({ input, meta }) => (
        <Form.Item
          validateStatus={meta.error && 'error'}
          help={meta.error}
          label={label}
          colon={false}
        >
          <Switch
            {...inputProps}
            {...input}
          />
        </Form.Item>
      )}
    />
  );
};
