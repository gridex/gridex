import React from 'react';
import { Select, Typography, Space } from 'antd';
import APP_SETTINGS from '../APP_SETTINGS';
import { useLocalStorageStringState } from '../utils/utils';
import styled from 'styled-components';

const { walletExplorers } = APP_SETTINGS;
const options = walletExplorers.map((explorer) => explorer.name);

const Container = styled.div`
  margin-top: 25px;
`;

const StyledSelect = styled(Select)`
  min-width: 150px;
`;

const ExplorerSetting = () => {
  const [currentExplorer, setCurrentExplorer] = useLocalStorageStringState(
    'currentExplorer',
    options[0],
  );

  return (
    <Container>
      <h3>Explorer settings</h3>
      <Space direction="vertical">
        <Typography.Text>Select preferred blockchain explorer</Typography.Text>
        <StyledSelect value={currentExplorer || options[0]} onChange={setCurrentExplorer}>
          {options.map((option) => (
            <Select.Option name={option} value={option} key={option}>
              {walletExplorers.find((explorer) => explorer.name === option)?.displayName}
            </Select.Option>
          ))}
        </StyledSelect>
      </Space>
    </Container>
  );
};

export default ExplorerSetting;
