import React, { useCallback, useMemo, useState } from 'react';
import { Button, Col, Form, Input, Row } from 'antd';

import DataTable from '../layout/DataTable';
import { useTokenMints } from '../../utils/markets';
import {
  parseTokenMintData,
} from '../../utils/tokens';
import { useWallet } from '../../utils/wallet';
import { useConnection } from '../../utils/connection';
import { refreshCache, useAsyncData } from '../../utils/fetch-loop';
import {
  createTokenAccountTransaction,
  sendTransaction,
} from '../../utils/send';
import { getOwnedTokenAccounts } from '../../../processor/utils/balance/getOwnedTokenAccounts';
import { parseTokenAccountData } from '../../../processor/utils/tokens/parseTokenAccountData';

function useInputValue(initialValue) {
  const [value, setValue] = useState(initialValue);
  const onChange = useCallback(event => {
    setValue(event.currentTarget.value);
  }, []);

  return {
    value,
    onChange,
  };
}

export default function TokensTable() {
  const search = useInputValue('');
  const tokens = useTokenMints();
  const [loading, setLoading] = useState({});

  const { wallet } = useWallet();
  const connection = useConnection();
  const [accountTokens, loaded] = useAsyncData(async () => {
    const ownedTokens = await getOwnedTokenAccounts(
      connection,
      wallet.publicKey,
    );
    return ownedTokens.map((token) => ({
      ...token,
      ...parseTokenAccountData(token.accountInfo.data),
      ...parseTokenMintData(token.accountInfo.data),
    }));
  }, 'tokenInfo');

  const balanceTokens = useMemo(() => {
    return tokens.map((token) => {
      const info = accountTokens?.find((owned) =>
        owned.mint.equals(token.address),
      );
      if (!info) return token;
      const decimals = info.decimals <= 64 ? info.decimals : 6;
      return {
        ...token,
        balance: (info.amount / Math.pow(10, decimals)).toFixed(decimals),
        publicKey: info.publicKey,
      };
    });
  }, [tokens, accountTokens]);

  const searchValue = search.value.toLowerCase();
  const filteredTokens = useMemo(() => {
    return searchValue
      ? balanceTokens.filter((c) => c.name.toLowerCase().includes(searchValue))
      : balanceTokens;
  }, [balanceTokens, searchValue]);

  const createTokenAccount =
    async (token) => {
      try {
        setLoading({
          ...loading,
          [token.address.toBase58()]: true,
        });
        const {
          transaction,
          signer,
        } = await createTokenAccountTransaction({
          connection,
          wallet,
          mintPublicKey: token.address,
        });

        await sendTransaction({
          transaction,
          wallet,
          connection,
          signers: [signer],
        });
        await refreshCache('tokenInfo', true);
      } catch (e) {
        console.log(e);
        setLoading({
          ...loading,
          [token.address.toBase58()]: false,
        });
      }
    };

  const columns = [
    {
      title: 'Coin',
      dataIndex: 'name',
      filtered: true,
    },
    {
      title: 'Address',
      dataIndex: 'publicKey',
      render: (publicKey, token) => {
        if (publicKey) return publicKey.toBase58();
        const tokenAddressPublicKey = token.address.toBase58()

        return (
          <Button size="small" onClick={() => createTokenAccount(token)} disabled={loading[tokenAddressPublicKey]} loading={loading[tokenAddressPublicKey]}>
            Create account
          </Button>
        );
      },
    },
    {
      title: 'Balance',
      dataIndex: 'balance',
    },
  ];

  return (
    <Row>
      <Col span={24}>
        <Form.Item>
          <Input.Search style={{ width: 240 }} {...search} />
        </Form.Item>
        <DataTable
          emptyLabel="No bots"
          dataSource={filteredTokens}
          columns={columns}
          pagination={true}
          loading={!loaded}
          rowKey="address"
        />
      </Col>
    </Row>
  );
}
