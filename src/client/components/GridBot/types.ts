import { GridBot } from '../../../common/models/grid-bot';

export interface CreateBotProps {
  onCreated: (data: any) => void;
  onChange: (bot: GridBot, marketPrice: number) => void;
}
