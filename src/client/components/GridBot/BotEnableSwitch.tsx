import { Col, Modal, Row, Switch, Tooltip, Button } from 'antd';
import {
  Account,
  Connection,
  PublicKey,
  SystemProgram,
  Transaction,
  TransactionInstruction,
} from '@solana/web3.js';
import { QuestionCircleOutlined } from '@ant-design/icons';
import styled from 'styled-components';

import { useWallet } from '../../utils/wallet';
import { useConnection } from '../../utils/connection';
import React, { useCallback, useState } from 'react';
import { loadMarket, loadMarketPrice } from '../../utils/markets-calc';
import { error, message } from '../../utils/notifications';
import { GridBot } from '../../../common/models/grid-bot';
import calculateBalanceForGridBot from '../../../processor/utils/balance/calculateBalanceForGridBot';
import { useBackend } from '../../utils/backend';
import { getTokenAccountInfo } from '../../../processor/utils/balance/getTokenAccountInfo';
import { createTokenTransferInstruction } from '../../../processor/utils/tokens/transferTokens';
import { MintInfo, parseTokenMintData } from '../../utils/tokens';
import { signTransaction } from '../../utils/send';
import wait from '../../../common/utils/wait';
import { calculateCoinAmounts } from '../../../common/grid-bot/grids';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';
import { useHistory } from 'react-router';

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

const Icon = styled(QuestionCircleOutlined)`
  margin-left: 8px;
`;

function createTransferInstruction(
  source: PublicKey,
  destination: PublicKey,
  amount: number,
  accountPublicKey: PublicKey,
  mintData: MintInfo,
): TransactionInstruction {
  return mintData.mintAddress!.toBase58() === WRAPPED_SOL_MINT.toBase58()
    ? SystemProgram.transfer({
      fromPubkey: source,
      toPubkey: destination,
      lamports: +amount * Math.pow(10, mintData.decimals),
    })
    : createTokenTransferInstruction(
      source,
      destination,
      +amount * Math.pow(10, mintData.decimals),
      accountPublicKey,
    );
}

function showModal(connection: Connection, bot: GridBot, botAccount, gridCalc, wallet) {
  async function transfer() {
    const walletAccounts = await getTokenAccountInfo(connection, wallet.publicKey);
    const tokenAccounts = await getTokenAccountInfo(
      connection,
      new PublicKey(botAccount.botAccountPublicKey),
    );
    const transaction = new Transaction();
    const A_BIT_MORE = 1.05; // add some money to bot account for fee and other.
    if (gridCalc.base.lacking > 0) {
      const token = tokenAccounts.find(
        (token) => token.effectiveMint.toBase58() === bot.baseMintKey,
      );
      const walletToken = walletAccounts.find(
        (token) => token.effectiveMint.toBase58() === bot.baseMintKey,
      );
      if (!token || !walletToken) {
        return error('Token account not found', '');
      }
      const mintAccount = (await connection.getAccountInfo(token.effectiveMint))!;
      const mintData = parseTokenMintData(mintAccount.data);
      mintData.mintAddress = token.effectiveMint;

      transaction.add(
        createTransferInstruction(
          walletToken.pubkey,
          token.pubkey,
          gridCalc.base.lacking * A_BIT_MORE,
          wallet.publicKey,
          mintData,
        ),
      );
    }

    if (gridCalc.quote.lacking > 0) {
      const token = tokenAccounts.find(
        (token) => token.effectiveMint.toBase58() === bot.quoteMintKey,
      );
      const walletToken = walletAccounts.find(
        (token) => token.effectiveMint.toBase58() === bot.quoteMintKey,
      );
      if (!token || !walletToken) {
        return error('Token account not found', '');
      }
      const mintAccount = (await connection.getAccountInfo(token.effectiveMint))!;
      const mintData = parseTokenMintData(mintAccount.data);
      mintData.mintAddress = token.effectiveMint;

      transaction.add(
        createTransferInstruction(
          walletToken.pubkey,
          token.pubkey,
          gridCalc.quote.lacking * A_BIT_MORE,
          wallet.publicKey,
          mintData,
        ),
      );
    }
    transaction.feePayer = wallet.publicKey;
    transaction.recentBlockhash = (await connection.getRecentBlockhash('max')).blockhash;
    try {
      const tx = await wallet.signTransaction(transaction);
      const signature = await connection.sendRawTransaction(tx.serialize(), {
        skipPreflight: false,
        preflightCommitment: 'singleGossip',
      });
      message('Transfer transaction sent', signature);
    } catch (e) {
      let message = e.toString();
      if (message.includes('Error processing Instruction 0: custom program error: 0x1')) {
        message = 'Insufficient funds';
      }
      error('Error', message);
    }
  }

  Modal.warning({
    title: 'Low balance',
    onOk: transfer,
    okText: 'Transfer',
    cancelText: 'Cancel',
    closable: true,
    content: (
      <>
        <Row>
          <Col md={24}>
            <h3>Bot balance is less than needed to create all bot orders</h3>
          </Col>
        </Row>
        <Row>
          <Col md={8}>{bot.baseCurrency}</Col>
          <Col md={16}>{gridCalc.base.lacking.toFixed(4)}</Col>
        </Row>
        <Row>
          <Col md={8}>{bot.quoteCurrency}</Col>
          <Col md={16}>{gridCalc.quote.lacking.toFixed(4)}</Col>
        </Row>
      </>
    ),
  });
}

async function signAndSend(connection: Connection, wallet: Account, transaction: Transaction) {
  const tx = await signTransaction({ transaction, wallet, signers: [], connection });
  return connection.sendRawTransaction(tx.serialize(), {
    skipPreflight: false,
    preflightCommitment: 'singleGossip',
  });
}

async function depositLamportsToBotTransaction(
  connection: Connection,
  wallet: Account,
  toKey: PublicKey,
  lamports: number,
) {
  return await signAndSend(
    connection,
    wallet,
    new Transaction().add(
      SystemProgram.transfer({
        fromPubkey: wallet.publicKey,
        toPubkey: toKey,
        lamports,
      }),
    ),
  );
}

type BotSwitchProps = {
  title?: string;
  value: boolean;
  bot: GridBot;
  disabled?: boolean;
  onChange: (boolean) => void;
};

export const BotEnableSwitch = ({ bot, value, disabled, onChange, title }: BotSwitchProps) => {
  if (bot.account === 'paper') {
    return <BotEnablePaperSwitch bot={bot} value={value} disabled={disabled} onChange={onChange} />;
  }
  /* eslint-disable react-hooks/rules-of-hooks */
  const { wallet, connected } = useWallet();
  const { backendRequest } = useBackend();
  const connection = useConnection();
  const [loading, setLoading] = useState(false);

  const transferToBotAccount = useCallback(
    async (botAccount) => {
      const accInfo = (await connection.getAccountInfo(
        new PublicKey(botAccount.botAccountPublicKey),
      ))!;
      // 0.2 sol
      const lamports = 200_000_000;
      const accLamports = accInfo?.lamports || 0;
      if (accLamports < lamports / 2) {
        await depositLamportsToBotTransaction(
          connection,
          wallet,
          botAccount.botAccountPublicKey,
          lamports,
        );
        return true;
      }
    },
    [connection, wallet],
  );

  const createTokenAccounts = useCallback(
    async (botAccount, bot) => {
      const tokenAccounts = await getTokenAccountInfo(
        connection,
        new PublicKey(botAccount.botAccountPublicKey),
      );
      if (
        tokenAccounts.find((a) => a.effectiveMint.toBase58() === bot.baseMintKey) &&
        tokenAccounts.find((a) => a.effectiveMint.toBase58() === bot.quoteMintKey)
      ) {
        return false;
      }

      await backendRequest(`/bot/create_token_accounts`, 'post', { botId: bot._id });
      return true;
    },
    [backendRequest, connection],
  );

  const change = useCallback(
    async (enabled) => {
      if (!connected) {
        return;
      }
      setLoading(true);
      try {
        if (enabled) {
          const market = await loadMarket(connection, bot.marketAddress, bot.marketProgramId);
          if (!market) {
            error('Enable bot', 'Can\'t enable bot, market not found');
            return;
          }
          const marketPrice = await loadMarketPrice(connection, market);
          if (!marketPrice) {
            error('Enable bot', 'Can\'t enable bot, market not found');
            return;
          }

          const botAccount = await backendRequest('/bot/public_key', 'get');

          if (await transferToBotAccount(botAccount)) {
            message('Transfering funds', 'Transferring funds to your account, please wait...');
            await wait(15_000);
            return await change(enabled);
          }
          if (await createTokenAccounts(botAccount, bot)) {
            message('Creating accounts', 'Creating token accounts, please wait...');
            await wait(15_000);
            return await change(enabled);
          }

          const gridCalc = await calculateBalanceForGridBot(
            connection,
            new PublicKey(botAccount.botAccountPublicKey),
            market,
            bot,
          );

          if (gridCalc.base.lacking > 0 || gridCalc.quote.lacking > 0) {
            showModal(connection, bot, botAccount, gridCalc, wallet);
            return;
          }
        }
        await onChange(enabled);
      } catch (err) {
        if (err === null) {
          error('Error enabling bot', 'rejected by user');
        } else {
          error('Error enabling bot', err.message);
        }
      } finally {
        setLoading(false);
      }
    },
    [
      wallet,
      connected,
      onChange,
      connection,
      bot,
      backendRequest,
      createTokenAccounts,
      transferToBotAccount,
    ],
  );

  return (
    <Container>
      <Switch checked={value} disabled={disabled} onChange={change} loading={loading} />
      {title && (
        <Tooltip title={title}>
          <Icon />
        </Tooltip>
      )}
    </Container>
  );
};

const BotEnablePaperSwitch = ({ bot, value, disabled, onChange }: BotSwitchProps) => {
  const [loading, setLoading] = useState(false);
  const connection = useConnection();
  const { backendRequest } = useBackend();
  const history = useHistory();
  const change = useCallback(
    async (enabled: boolean) => {
      setLoading(true);
      try {
        if (enabled) {
          const account = await backendRequest('/bot/public_key?account=paper');
          const getBalance = (mint) => account.balances.find((b) => b.mint === mint)?.amount || 0;

          const market = await loadMarket(connection, bot.marketAddress, bot.marketProgramId);
          const marketPrice = market && (await loadMarketPrice(connection, market));
          if (!marketPrice) {
            throw new Error('Can\'t enable bot, market not found');
          }

          const grid = calculateCoinAmounts(bot, marketPrice);

          if (
            grid.base > getBalance(bot.baseMintKey) ||
            grid.quote > getBalance(bot.quoteMintKey)
          ) {
            return error('Error enabling bot',
              <span>Insufficient funds, need at least
                <div>{grid.base} {bot.baseCurrency}</div>
                <div>{grid.quote} ${bot.quoteCurrency}</div>
              </span>,
              { btn: <Button type="primary" onClick={() => history.push("paper-bot")}>Go to Transfer tab</Button> });
          }
        }

        await onChange(enabled);
      } catch (err) {
        error('Error enabling bot', err.message);
      } finally {
        setLoading(false);
      }
    },
    [onChange, backendRequest, bot, connection, history],
  );

  return <Switch checked={value} disabled={disabled} onChange={change} loading={loading} />;
};
