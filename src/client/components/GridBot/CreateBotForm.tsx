import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import { Form as FinalForm, FormSpy } from 'react-final-form';
import styled from 'styled-components';
import { Button, Divider, Form, Modal, Tooltip } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { CreateBotProps } from './types';

import { useMarket, useMarkPrice } from '../../utils/markets';
import { useBackend } from '../../utils/backend';
import { notify } from '../../utils/notifications';
import { useWallet } from '../../utils/wallet';
import { LabelField, NumberField, SwitchField } from '../form';
import { secondaryColor } from '../../global_style';
import { BotState, GridBot } from '../../../common/models/grid-bot';
import { calculateCoinAmounts } from '../../../common/grid-bot/grids';
import { MarketBalance } from '../../utils/types';
import Link from '../Link';

const DEFAULT_VALUES = {
  upperLimitPrice: 0,
  lowerLimitPrice: 0,
  gridQuantity: 5,
  quantityPerGrid: 3,
};

const Title = styled.div``;

function fround(n: number, precision: number) {
  const mult = 10 ** precision;
  return Math.round(n * mult) / mult;
}

const CreateBotForm = ({ onCreated, onChange }: CreateBotProps) => {
  const { backendRequest, backEndConnected, invited, setShowInviteModal } = useBackend();
  const { quoteCurrency, baseCurrency, market } = useMarket();
  const { connected } = useWallet();

  const [isCreating, setCreating] = useState(false);
  const [showWarningModal, setShowWarningModal] = useState(false);

  const marketPrice = useMarkPrice() || 0;

  const createBotValue = useCallback(
    ({ isPaper, ...values }): GridBot => ({
      account: isPaper ? 'paper' : 'default',
      marketAddress: market?.address.toBase58(),
      marketProgramId: market?.programId.toBase58(),
      marketTickSize: market?.tickSize,
      orders: [],
      closedOrders: [],
      state: BotState.STOPPED,
      name: `${baseCurrency}/${quoteCurrency}`,
      baseCurrency,
      baseMintKey: market?.baseMintAddress.toBase58(),
      quoteCurrency,
      quoteMintKey: market?.quoteMintAddress.toBase58(),
      ...values,
      quantityPerGrid: ((dig) => Math.floor(values.quantityPerGrid * dig) / dig)(
        1 / market?.minOrderSize!,
      ),
    }),
    [market, quoteCurrency, baseCurrency],
  );

  const createBotRequest = useCallback(
    async (values) => {
      setCreating(true);
      setShowWarningModal(false);
      try {
        console.log('onSubmit', values);

        const bot: GridBot = createBotValue(values);
        const result: any = await backendRequest('/grid-bot', 'post', bot);
        bot._id = result.botId;
        onCreated(bot);
      } catch (e) {
        console.error(e);
        notify({
          message: 'Error creating grid bot',
          description: e.message,
          type: 'error',
        });
      } finally {
        setCreating(false);
      }
    },
    [backendRequest, onCreated, createBotValue],
  );

  const onSubmit = (values) => {
    const { upperLimitPrice, lowerLimitPrice } = values;
    const isMarketPriceInBotRange = lowerLimitPrice < marketPrice && upperLimitPrice > marketPrice;
    if (isMarketPriceInBotRange || showWarningModal) {
      createBotRequest(values);
    } else {
      setShowWarningModal(true);
    }
  };

  // calculate estimated balance
  const [botData, setBotData] = useState<{
    bot: GridBot;
    balances: MarketBalance;
  } | null>(null);
  const recalcEstimatedBalance = useCallback(
    ({ values }) => {
      if (marketPrice) {
        const bot = createBotValue(values);
        setBotData({
          bot,
          balances: calculateCoinAmounts(bot, marketPrice),
        });
        onChange(bot, marketPrice);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [setBotData, marketPrice, createBotValue],
  );

  // set upper and lower to +10% and -10% from current price
  const enableRecalc = marketPrice > 0;
  const initialValues = {
    ...DEFAULT_VALUES,
    upperLimitPrice: fround(marketPrice * 1.1, 4),
    lowerLimitPrice: fround(marketPrice * 0.9, 4),
  };

  useEffect(
    () => recalcEstimatedBalance({ values: initialValues }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [/* dont enable: recalcEstimatedBalance */ enableRecalc, market],
  );

  let submit; // closure to handle form submit from modal
  return (
    <FinalForm onSubmit={onSubmit} initialValues={initialValues} keepDirtyOnReinitialize>
      {({ handleSubmit }) => {
        submit = handleSubmit;
        return (
          <Form layout="horizontal" onFinish={handleSubmit}>
            <FormSpy onChange={recalcEstimatedBalance} subscription={{ values: true }} />
            <Title>Grid bot creation form</Title>
            <>
              <Divider />
              <NumberField name="upperLimitPrice" label="Upper Limit Price" min={0} />
              <NumberField name="lowerLimitPrice" label="Lower Limit Price" min={0} />
              <NumberField name="gridQuantity" label="Grid Quantity" precision={0} min={2} />
              <NumberField name="quantityPerGrid" label="Quantity Per Grid" min={0} />
              {botData && (
                <>
                  <LabelField
                    label="Estimated required balance"
                    value={
                      <span>
                        {!isNaN(+botData.balances.base) && !isNaN(+botData.balances.quote) ? (
                          <>
                            {`${+botData.balances.base.toFixed(4)} ${baseCurrency}`}
                            <br />
                            {`${+botData.balances.quote.toFixed(4)} ${quoteCurrency}`}
                          </>
                        ) : (
                          ''
                        )}
                      </span>
                    }
                  />
                </>
              )}
              {/*<SwitchField*/}
              {/*  name="isPaper"*/}
              {/*  label="Paper account"*/}
              {/*/>*/}
              <SwitchField
                name="isPaper"
                label={
                  <span>Paper account
                    <Tooltip
                      placement="top"
                      title={<span>
                        Paper allows you to safely try paper-bots without any real assets involved.
                        All the trading will happen on virtual account, accessible
                        at <Link to="/grid-bot/paper-bot" >Paper Bot tab</Link>
                      </span>}
                    > <QuestionCircleOutlined style={{ color: secondaryColor }} />
                    </Tooltip>
                  </span>
                }
              />
              <Divider />
            </>
            {backEndConnected && !invited && (
              <>
                <span>
                  You can not create bot because you account is not invited.{' '}
                  <Link to="#" onClick={() => setShowInviteModal(true)}>
                    Get invite
                  </Link>
                </span>
                <Divider />
              </>
            )}
            <Button
              loading={isCreating}
              disabled={isCreating || !connected || !backEndConnected || !invited}
              type="primary"
              htmlType="submit"
              block
            >
              Create
            </Button>
            <Modal
              visible={showWarningModal}
              title="Market price is outside the bot grid"
              onCancel={() => setShowWarningModal(false)}
              onOk={(event) => {
                submit(event);
              }}
              okText={'Create'}
              width="30vw"
            >
              <p>Are you sure you want to create bot with this grids?</p>
            </Modal>
          </Form>
        );
      }}
    </FinalForm>
  );
};

export default CreateBotForm;
