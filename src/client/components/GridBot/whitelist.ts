import { MarketContextValues } from '../../utils/types';

export const PAIR_WHITELIST: string[] = [
  ''
];

export const BASE_WHITELIST: string[] = [
  // 'SOL'
];

export function isWhitelisted(market: MarketContextValues) {
  return (market.baseCurrency && BASE_WHITELIST.includes(market.baseCurrency)) ||
    PAIR_WHITELIST.includes(`${market.baseCurrency}${market.quoteCurrency}`);
}
