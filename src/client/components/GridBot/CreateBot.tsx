import React, { useCallback, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { Alert, Button, Col, Row, Tooltip } from 'antd';
import Orderbook from '../Orderbook';
import TradesTable from '../TradesTable';
import { MarketSelector } from '../Trades';
import { useMarket, useMarketsList } from '../../utils/markets';
import CreateBotForm from './CreateBotForm';
import { isWhitelisted } from './whitelist';
import { CreateBotProps } from './types';
import { TVChartContainer as TradingView } from '../TradingView';
import { calculateLines } from '../../../common/grid-bot/grids';
import { useBackend } from '../../utils/backend';
import { GridBot, Order } from '../../../common/models/grid-bot';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { secondaryColor } from '../../global_style';
import useApi from '../../utils/useApi';

const ComponentsWrapper = styled.div`
  background-color: #212834;
  padding: 20px;

  & + & {
    margin-top: 8px;
  }
`;

const Rectangle = styled.div`
  width: 100%;
  height: 0;
  padding: 40% 0;
  position: relative;
`;

const ChartContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const DisabledOverlay = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 400px;
`;

const BalanceItem = styled.div`
  display: flex;
  justify-content: space-between;
`;

const TVCol = styled(Col)`
  margin-bottom: 8px;
  @media screen and (min-width: 992px) {
    margin-bottom: 0;
  }
`;

const FormCol = styled(Col)`
  margin-bottom: 8px;
  @media screen and (min-width: 992px) {
    margin-bottom: 0;
  }
`;

const Margin = styled.div`
  margin-bottom: ${({ bottom }) => bottom}px;
`;

export default function CreateBot({ onCreated }: CreateBotProps) {
  const [lines, setLines] = useState<any[]>([]);
  const [botData, setBot] = useState<GridBot | undefined>();
  const [backtest, setBacktest] = useState<{ trades: Order[], gains: { base: number, quote: number }, isLoading: boolean } | null>(null);

  const api = useApi();
  const { backEndConnected, backEndConnectionLoading } = useBackend();
  const markets = useMarketsList();
  const [, setHandleDeprecated] = useState(false);
  const market = useMarket();
  const { marketName } = market;

  const changeOrderRef = useRef<({ size, price }: { size?: number; price?: number }) => void>();

  const onPrice = useCallback(
    (price) => changeOrderRef.current && changeOrderRef.current({ price }),
    [],
  );
  const onSize = useCallback(
    (size) => changeOrderRef.current && changeOrderRef.current({ size }),
    [],
  );

  const runBacktest = async () => {
    if (!botData) return;

    setBacktest({ trades: [], gains: { base: 0, quote: 0 }, isLoading: true });
    const res = await api.paperBot.backtest(botData);
    setBacktest({ ...res, isLoading: false });
  };

  useEffect(() => {
    document.title = marketName
      ? `${marketName} — create Grid bot`
      : 'Create Grid bot';
  }, [marketName]);

  const onChange = (bot, marketPrice) => {
    const lines = calculateLines(
      bot.upperLimitPrice, bot.lowerLimitPrice, bot.gridQuantity, marketPrice, bot.marketTickSize,
    );
    setBot(bot);
    setLines(lines);
  };

  return (
    <Row gutter={8}>
      <Col
        lg={{ order: 1, span: 6 }}
        md={{ order: 2, span: 12 }}
        xs={{ order: 3, span: 24 }}
      >
        <Orderbook smallScreen={false} onPrice={onPrice} onSize={onSize} />
        <TradesTable smallScreen={false} />
      </Col>
      <TVCol lg={{ order: 2, span: 12 }} xs={{ order: 1, span: 24 }}>
        <ComponentsWrapper>
          <Rectangle>
            <ChartContainer>
              <TradingView lines={lines} backtest={backtest?.trades} />
            </ChartContainer>
          </Rectangle>
          {!backEndConnectionLoading && !backEndConnected &&
          <Alert message={'Connect wallet for using native trading view'} type={'warning'} />}
        </ComponentsWrapper>
      </TVCol>
      <FormCol
        lg={{ order: 3, span: 6 }}
        md={{ order: 3, span: 12 }}
        xs={{ order: 2, span: 24 }}
      >
        <ComponentsWrapper>
          <MarketSelector
            markets={markets}
            setHandleDeprecated={setHandleDeprecated}
            placeholder={'Select market'}
            customMarkets={false}
            onDeleteCustomMarket={false}
            width="100%"
          />
        </ComponentsWrapper>
        <ComponentsWrapper>
          {isWhitelisted(market) ? (
            <DisabledOverlay>Coming soon</DisabledOverlay>
          ) : (
            <CreateBotForm onCreated={onCreated} onChange={onChange} />
          )}
        </ComponentsWrapper>
        { (backEndConnected) && <ComponentsWrapper>
          <Margin bottom={16}>
            {'Backtesting '}
            <Tooltip placement="top"
                     title="Push button to backtest current bot settings for 15 last days, look for trading marks on chart">
              <QuestionCircleOutlined style={{ color: secondaryColor }} />
            </Tooltip>
          </Margin>

          <Margin bottom={16}>
            <Button onClick={runBacktest} block loading={backtest?.isLoading}>Backtest</Button>
          </Margin>
          <div>
            Balance changes:
            <BalanceItem><span>{ botData?.baseCurrency }</span> {backtest?.gains.base.toFixed(3) || '-'}</BalanceItem>
            <BalanceItem><span>{ botData?.quoteCurrency }</span> {backtest?.gains.quote.toFixed(3) || '-'}</BalanceItem>
          </div>
        </ComponentsWrapper>}
      </FormCol>
    </Row>
  );
}
