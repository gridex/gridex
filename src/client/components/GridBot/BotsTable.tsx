import React, { useCallback, useEffect, useState } from 'react';
import { Button, Col, Modal, Row, Tag, Tooltip } from 'antd';
import { DeleteOutlined, QuestionCircleOutlined, ReloadOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import moment from 'moment';
import Jdenticon from 'react-jdenticon';

import { error, message } from '../../utils/notifications';
import { useBackend } from '../../utils/backend';
import DataTable from '../layout/DataTable';
import { useBackendRequest } from '../../utils/useBackend';
import { BotEnableSwitch } from './BotEnableSwitch';
import { BotState, BotStateNames, Order } from '../../../common/models/grid-bot';
import ButtonGroup from 'antd/lib/button/button-group';
import { calculateLines, Line } from '../../../common/grid-bot/grids';
import { TVChartContainer } from '../TradingView';
import { loadMarketPrice, MARKETS_MAP } from '../../utils/markets-calc';
import { Market } from '@project-serum/serum';
import { useConnection } from '../../utils/connection';
import { Connection, PublicKey } from '@solana/web3.js';
import { buyColor, sellColor } from '../../global_style';
import BotErrors from './BotErrors';
import { dateFormat } from '../../utils/dateFormat';
import { NumberInput } from '../form/Fields';
import { useBotKeys } from '../../utils/useBotKeys';
import calcProfit, { calcProfitByCurrentPrice } from '../../../common/utils/calcProfit';

const ChartContainer = styled.div`
  width: 100%;
  height: 80vh;
`;

const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

const IconContainer = styled.div`
  margin-right: 8px;

  > div {
    display: flex;
  }
`;

const TagWithPointer = styled(Tag)`
  cursor: pointer;
`;

const ProfitLine = styled.p`
  margin: 0;
`;

const Icon = ({ value, size }: { value: string; size: string }) => (
  <IconContainer>
    <Jdenticon value={value} size={size} />
  </IconContainer>
);

const ChangeMarketPrice = ({ marketAddress }) => {
  const { backendRequest } = useBackend();
  const [isModal, setModal] = useState(false);
  const [price, setPrice] = useState(0);

  const changePrice = async () => {
    await backendRequest('/paper-bot/set-price', 'POST', { marketAddress, price });
    setModal(false);
  };

  return (
    <>
      <Modal title={`Set market ${marketAddress} price`} visible={isModal} onOk={changePrice}>
        <NumberInput value={price} name="price" onChange={(n) => setPrice(n)} />
      </Modal>

      <Button onClick={() => setModal(true)}>Price</Button>
    </>
  );
};

const Profit = ({
  baseCurrency,
  quoteCurrency,
  closedOrders,
  marketAddress,
  marketProgramId,
  connection,
}: {
  baseCurrency: string;
  quoteCurrency: string;
  closedOrders: Order[];
  marketAddress: string;
  marketProgramId: string;
  connection: Connection;
}): JSX.Element => {
  const [marketPrice, setMarketPrice] = useState<number>(0);

  const loadMarket = async () => {
    const market = await Market.load(
      connection,
      new PublicKey(marketAddress),
      {},
      new PublicKey(marketProgramId),
    );

    const marketPrice = await loadMarketPrice(connection, market);

    if (marketPrice) {
      setMarketPrice(marketPrice);
    }
  }

  const profit = calcProfit(closedOrders);
  const { base: baseRealProfit, quote: quoteRealProfit } = profit;
  const baseRealProfitString = baseRealProfit > 0 ? `+${baseRealProfit.toFixed(2)}` : `${baseRealProfit.toFixed(2)}`;
  const quoteRealProfitString = quoteRealProfit > 0 ? `+${quoteRealProfit.toFixed(2)}` : `${quoteRealProfit.toFixed(2)}`;

  const marketProfit = calcProfitByCurrentPrice(profit, marketPrice);
  const { base: baseMarketProfit, quote: quoteMarketProfit } = marketProfit;
  const baseMarketProfitString = baseMarketProfit > 0 ? `+${baseMarketProfit.toFixed(2)}` : `${baseMarketProfit.toFixed(2)}`;
  const quoteMarketProfitString = quoteMarketProfit > 0 ? `+${quoteMarketProfit.toFixed(2)}` : `${quoteMarketProfit.toFixed(2)}`;

  useEffect(() => {
    loadMarket();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <div>
    <ProfitLine>Real profit: {baseRealProfitString} {baseCurrency} / {quoteRealProfitString} {quoteCurrency}</ProfitLine>
    {
      marketPrice && <ProfitLine>By current price: {baseMarketProfitString} {baseCurrency} / {quoteMarketProfitString} {quoteCurrency}</ProfitLine>
    }
  </div>
}

export default function BotsTable() {
  const connection = useConnection();
  const [errorsBotId, setErrorsBotId] = useState<string>('');
  const [showTVModal, setShowTVModal] = useState(false);
  const [marketAddress, setMarketAddress] = useState<undefined | string>(undefined);
  const [lines, setLines] = useState<null | Line[]>(null);
  const [linesLoading, setLinesLoading] = React.useState<null | string>(null);
  const [list, setList] = useState<null | any[]>(null);
  const [bots, loading, update] = useBackendRequest('/grid-bot');
  const backend = useBackend();
  const removeBot = useCallback(
    async (bot) => {
      if (bot.state && ![BotState.ERROR, BotState.STOPPED].includes(bot.state)) {
        alert('Cant remove enabled bot');
        return;
      }
      const botId = bot._id;
      backend.backendRequest(`/grid-bot/${botId}`, 'delete').then(
        () => {
          message('Bot deleted', `id: ${botId}`);
          update();
        },
        (err) => {
          console.error(err);
          error("Can't delete bot", `id: ${botId}`);
        },
      );
    },
    [backend, update],
  );

  const enableBot = useCallback(
    async (bot: any, enabled: boolean) => {
      await backend.backendRequest(`/grid-bot/${bot._id}/enable`, 'post', { enabled });
      update();
    },
    [backend, update],
  );

  const showChart = async (bot) => {
    setLinesLoading(bot._id);
    const {
      upperLimitPrice,
      lowerLimitPrice,
      gridQuantity,
      marketTickSize,
      marketAddress,
      marketProgramId,
    } = bot;
    const market = await Market.load(
      connection,
      new PublicKey(marketAddress),
      {},
      new PublicKey(marketProgramId),
    );
    const currentPrice = await loadMarketPrice(connection, market);
    const lines = calculateLines(
      upperLimitPrice,
      lowerLimitPrice,
      gridQuantity,
      currentPrice,
      marketTickSize,
    );
    setShowTVModal(true);
    setMarketAddress(marketAddress);
    setLines(lines);
    setLinesLoading(null);
  };

  const closeChart = () => {
    setMarketAddress(undefined);
    setLines(null);
    setTimeout(setShowTVModal, 300, false);
  };

  const showList = (bot) => {
    setList(
      (bot.closedOrders || [])
        .map((order) => ({
          ...order,
          createdAt: order.createdAt ? dateFormat(order.createdAt) : '-',
          closedAt: order.closedAt ? dateFormat(order.closedAt) : '-',
          date: +moment(order.createdAt || order.closedAt).toDate(),
        }))
        .reverse(),
    );
  };

  const closeList = () => {
    setList(null);
  };

  const [{ isAdmin = false } = {}] = useBotKeys();

  const ordersColumns = [
    {
      title: 'Created at',
      dataIndex: 'createdAt',
    },
    {
      title: 'Closed at',
      dataIndex: 'closedAt',
    },
    {
      title: 'Price',
      dataIndex: 'price',
    },
    {
      title: 'Size',
      dataIndex: 'size',
    },
    {
      title: 'Side',
      render: (_, bot) => <Tag color={bot.side === 'sell' ? sellColor : buyColor}>{bot.side}</Tag>,
    },
  ];

  // @ts-ignore
  const columns = [
    {
      title: () => (
        <>
          Name{' '}
          <Tooltip title={'Bot name generates automatically'}>
            <QuestionCircleOutlined />
          </Tooltip>
        </>
      ),
      dataIndex: 'botName',
      render: (_, bot) => (
        <TitleContainer>
          <Icon size="32" value={bot.botName} />
          {bot.botName}
        </TitleContainer>
      ),
    },
    {
      title: 'Market',
      render: (_, bot) => `${bot.baseCurrency}/${bot.quoteCurrency}`,
    },
    {
      title: 'Grid',
      render: (_, bot) => `${bot.gridQuantity} x ${bot.quantityPerGrid} ${bot.baseCurrency}`,
    },
    {
      title: 'Limit price',
      render: (_, bot) => `${bot.lowerLimitPrice} - ${bot.upperLimitPrice}`,
    },
    {
      title: 'Profit',
      render: (_, bot) => {
        return <Profit
          baseCurrency={bot.baseCurrency}
          quoteCurrency={bot.quoteCurrency}
          closedOrders={bot.closedOrders}
          marketProgramId={bot.marketProgramId}
          marketAddress={bot.marketAddress}
          connection={connection}
        />
      },
    },
    {
      title: 'Status',
      render: (_, bot) => `${BotStateNames[bot.state]}`,
    },
    {
      title: 'Errros',
      render: (_, bot) => (
        <TagWithPointer
          color={bot.errorsCount > 0 ? sellColor : buyColor}
          onClick={() => setErrorsBotId(bot._id)}
        >
          {bot.errorsCount} {bot.newErrorsCount > 0 ? `(+${bot.newErrorsCount})` : ''}
        </TagWithPointer>
      ),
    },
    {
      title: 'Enabled',
      render: (_, bot) => {
        const disabled =
          ![BotState.STOPPED, BotState.RUNNING, BotState.ERROR].includes(bot.state) ||
          MARKETS_MAP[bot.marketProgramId]?.deprecated;
        const showTitle = MARKETS_MAP[bot.marketProgramId]?.deprecated
          ? 'Market is deprecated.'
          : bot.state === BotState.ERROR
          ? 'Check error info.'
          : undefined;
        return (
          <BotEnableSwitch
            bot={bot}
            value={bot.state === BotState.RUNNING}
            disabled={disabled}
            onChange={(enabled) => enableBot(bot, enabled)}
            title={showTitle ? `You can not enable bot. ${showTitle}` : undefined}
          />
        );
      },
    },
    {
      title: 'Actions',
      render: (_, bot) => (<ButtonGroup>
        {bot.account === 'paper' && isAdmin && <ChangeMarketPrice marketAddress={bot.marketAddress} />}
        <Button disabled={!!linesLoading} loading={linesLoading === bot._id} onClick={() => showChart(bot)}>Trading view</Button>
        <Button onClick={() => showList(bot)}>Closed orders{(bot.closedOrders || []).length > 0 ? ` (${bot.closedOrders.length})` : ""}</Button>
        <Button danger onClick={() => removeBot(bot)} icon={<DeleteOutlined color="red" />} />
      </ButtonGroup>),
    },
    {
      title: 'Account',
      dataIndex: 'account',
    },
  ];

  useEffect(() => {
    document.title = 'Bots list';
  }, []);

  const dropNewErrors = () => {
    update();
  };

  return (
    <Row>
      <BotErrors
        botId={errorsBotId}
        onClose={() => setErrorsBotId('')}
        dropNewErrors={dropNewErrors}
      />
      <Modal
        title="Closed orders"
        width="800px"
        visible={list !== null}
        footer={null}
        closeIcon={null}
        onCancel={closeList}
      >
        <DataTable
          emptyLabel="No closed orders"
          dataSource={list || []}
          columns={ordersColumns}
          rowKey="_id"
        />
      </Modal>
      {showTVModal && (
        <Modal
          width="90vw"
          visible={marketAddress !== undefined}
          onOk={closeChart}
          onCancel={closeChart}
          footer={null}
          closeIcon={null}
        >
          {marketAddress !== undefined ? (
            <ChartContainer>
              <TVChartContainer lines={lines || []} marketAddress={marketAddress} />
            </ChartContainer>
          ) : null}
        </Modal>
      )}
      <Col span={24}>
        <Button onClick={() => update()} icon={<ReloadOutlined />} />
        <DataTable
          emptyLabel="No bots"
          dataSource={bots}
          columns={columns}
          pagination={true}
          // pageSize={pageSize ? pageSize : 5}
          loading={loading}
          rowKey="_id"
        />
      </Col>
    </Row>
  );
}
