import React, { useEffect, useState } from 'react';
import { Col, Modal, Row } from 'antd';
import { useBackend } from '../../utils/backend';
import DataTable from '../layout/DataTable';
import { dateFormat } from '../../utils/dateFormat';
import { getErrorReason } from '../../../common/utils/get-error-reason';

const BotErrors = ({ botId, onClose, dropNewErrors } : { botId: string, onClose: () => void, dropNewErrors: () => void }) => {
  const { backendRequest } = useBackend();
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [errors, setErrors] = useState([]);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  const [total, setTotal] = useState(1);
  const [reloadId, setReloadId] = useState(0);

  const loadErrors = async () => {
    setLoading(true);
    const { errors, total } = await backendRequest(`/grid-bot/${botId}/errors`, 'post', { page, pageSize });
    setErrors(errors);
    setTotal(total);
    setLoading(false);
  };

  const markErrorsAsOld = async () => {
    await backendRequest(`/grid-bot/${botId}/errors`, 'delete');
  }

  useEffect(() => {
    setVisible(!!botId);
    if (botId) {
      loadErrors();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [botId, reloadId]);

  useEffect(() => {
    if (visible) {
      dropNewErrors();
      markErrorsAsOld();
    } else {
      onPaginationChange(1, 20);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visible]);

  const onPaginationChange = (page, pageSize) => {
    setPage(page);
    setPageSize(pageSize);
    setReloadId(Math.random());
  }

  const columns = [
    {
      title: 'First error date',
      render: (e) => dateFormat(e.date),
    },
    {
      title: "Last error date",
      render: (e) => dateFormat(e.lastError || e.date),
    },
    {
      title: "Repeats count",
      dataIndex: 'count'
    },
    {
      title: 'Message',
      render: (e) => <Row>
        <Col span={24}>
          <b>Message:</b> { e.message }
        </Col>
        <Col span={24}>
          <b>Reason:</b> { getErrorReason(e.message) }
        </Col>
      </Row>
    }
  ];

  return (
    <Modal visible={visible} title="Errors" onCancel={onClose} footer={null} width="90vw">
      <DataTable
        loading={loading}
        columns={columns}
        rowKey="_id"
        dataSource={errors}
        pagination={{ page, pageSize, total, showSizeChanger: true, onChange: onPaginationChange }}
      />
    </Modal>
  );
};

export default BotErrors;
