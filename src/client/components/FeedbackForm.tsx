import React, { useRef, useState } from 'react';
import { useBackend } from '../utils/backend';
import { Modal, Button, Input, Form, Typography } from 'antd';
import styled from 'styled-components';
import { useWallet } from '../utils/wallet';
import { notify } from '../utils/notifications';

const { Text } = Typography;

const StyledText = styled(Text)`
  margin-top: 20px;
`;

const StyledForm = styled(Form)`
  display: flex;
  align-items: stretch;
  flex-direction: column;
`;

const FeedbackForm = () => {
  const [isVisible, setIsVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');
  const [isSuccessed, setIsSuccessed] = useState(false);
  const { connected } = useWallet();
  const { backendRequest } = useBackend();

  const formRef = useRef();

  const clearForm = () => {
    if (formRef && formRef.current) {
      try {
        // @ts-ignore
        formRef.current.resetFields();
      } catch (e) {
        // do nothing
      }
    }
  }

  const sendFeedback = async (formData) => {
    setError('');
    setIsLoading(true);
    try {
      await backendRequest('/feedback', 'POST', formData);
      setIsLoading(false);
      setIsSuccessed(true);
      setIsVisible(false);
      clearForm();
      notify({
        message: "Thank you for your feedback!",
      });
    } catch (err) {
      setError(err.toString());
      setIsLoading(false);
      notify({
        type: "error",
        message: "Error! Something went wrong!",
      });
    }
  };

  const resetMessages = () => {
    setError('');
    setIsSuccessed(false);
  };

  const showModal = () => setIsVisible(true);

  const hideModal = () => {
    setIsVisible(false);
    resetMessages();
  };

  const onSubmit = (formData) => sendFeedback(formData);

  return connected ? (
    <>
      <Button type="default" onClick={showModal} size="large">
        Give us feedback
      </Button>
      <Modal
        title="Feedback form"
        visible={isVisible}
        footer={null}
        onCancel={hideModal}
        destroyOnClose
      >
        <StyledForm onFinish={onSubmit} onChange={resetMessages} layout="vertical" ref={formRef}>
          <Form.Item name={['title']} label="Title" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name={['message']} label="Message" rules={[{ required: true }]}>
            <Input.TextArea />
          </Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Submit
          </Button>
          {error && <StyledText type="danger">{error}</StyledText>}
          {isSuccessed && <StyledText type="success">Successfully sent!</StyledText>}
        </StyledForm>
      </Modal>
    </>
  ) : null;
};

export { FeedbackForm };
