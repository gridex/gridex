import React, { Component } from 'react';
import { Button, Col as ACol, Row, Typography } from 'antd';
import styled from 'styled-components';
import ButtonGroup from 'antd/es/button/button-group';

const Btn = styled(Button)`
  & + & {
    margin-left: 8px;
  }
`;

const Col = styled(ACol)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  text-align: center;
`;

const Title = styled(Typography.Title)`
  width: 100%;
  margin: 0 0 16px 0 !important;
`;

const BtnGroup = styled(ButtonGroup)`
  width: 100%;
  margin-bottom: 16px;
  display: flex;
  justify-content: center;
`;

const P = styled(Typography.Paragraph)`
  margin: 0;
`;

export default class ErrorBoundary extends Component {
  state = {
    hasError: false,
  };

  static getDerivedStateFromError(error) {
    console.log(error);
    return { hasError: true };
  }

  reload = () => {
    window.location.reload();
  };

  clearAndReload = () => {
    localStorage.clear();
    this.reload();
  };

  render() {
    if (this.state.hasError) {
      return (
        <Row align="center" justify="middle" style={{ height: '100vh' }}>
          <Col span={12}>
            <Title level={2}>Something went wrong</Title>
            <Title level={4}>Try one of actions</Title>
            <BtnGroup>
              <Btn onClick={this.reload}>Reload</Btn>
              <Btn onClick={this.clearAndReload} type="primary">
                Clear data*
              </Btn>
            </BtnGroup>
            <P>* clear local data and reload page</P>
          </Col>
        </Row>
      );
    }

    return this.props.children;
  }
}
