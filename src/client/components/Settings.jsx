import React from 'react';
import { Button, Select, Switch } from 'antd';
import { usePreferences } from '../utils/preferences';
import { useBotKeys } from '../utils/useBotKeys';
import { useWallet, useWalletProviders, useEndpoints } from '../utils/wallet';
import { useConnection, useConnectionConfig } from '../utils/connection';
import { refreshCache } from '../utils/fetch-loop';
import tuple from 'immutable-tuple';

// @ts-ignore
export default function Settings({ setAddEndpointVisible }) {
  const [airdropLoading, setAirdropLoading] = React.useState(false);
  const connection = useConnection();
  const { WALLET_PROVIDERS } = useWalletProviders();
  const {
    connected,
    wallet,
    setProviderUrl,
    providerUrl,
    setPublicKeyAddress,
    impersonatePublicKey,
  } = useWallet();
  const { autoConnectEnabled, setAutoConnectEnabled } = usePreferences();
  const [botKeys] = useBotKeys();
  const isAdmin = !!botKeys?.isAdmin;

  const { endpoint, setEndpoint, endpointInfo } = useConnectionConfig();
  const endpoints = useEndpoints(providerUrl);

  const requestAirdrop = async () => {
    setAirdropLoading(true);
    const lamports = await connection.getBalance(wallet.publicKey);
    let new_lamports = lamports;
    await connection.requestAirdrop(wallet.publicKey, 1000000000);
    while (lamports === new_lamports) {
      new_lamports = await connection.getBalance(wallet.publicKey);
    }
    setAirdropLoading(false);
    refreshCache(tuple('getTokenAccounts', wallet, connected));
  };

  const setCustomPublicKey = () => {
    const address = prompt('enter custom address');
    setPublicKeyAddress(address);
  };

  return (
    <>
      {/*{connected && (*/}
      {/*  <div style={{ marginBottom: 8 }}>*/}
      {/*    <Switch*/}
      {/*      style={{ marginRight: 8 }}*/}
      {/*      disabled={!autoApprove}*/}
      {/*      checked={autoApprove && autoSettleEnabled}*/}
      {/*      onChange={setAutoSettleEnabled}*/}
      {/*    />{' '}*/}
      {/*    Auto settle*/}
      {/*    {!autoApprove && (*/}
      {/*      <Paragraph*/}
      {/*        style={{ color: 'rgba(255,255,255,0.5)', marginTop: 10 }}*/}
      {/*      >*/}
      {/*        To use auto settle, first enable auto approval in your wallet*/}
      {/*      </Paragraph>*/}
      {/*    )}*/}
      {/*  </div>*/}
      {/*)}*/}
      {/*<div style={{ marginBottom: 8 }}>*/}
      {/*  <Button onClick={() => setAddEndpointVisible(true)}>*/}
      {/*    Add custom endpoint*/}
      {/*  </Button>*/}
      {/*</div>*/}
      {connected && endpointInfo?.name !== 'mainnet-beta' && (
        <div style={{ marginBottom: 8 }}>
          <Button onClick={requestAirdrop} disabled={airdropLoading} loading={airdropLoading}>
            Request airdrop
          </Button>
        </div>
      )}
      {(isAdmin || impersonatePublicKey) && (
        <Button onClick={setCustomPublicKey} style={{ marginBottom: 8 }}>
          Set address
        </Button>
      )}
      <div style={{ marginBottom: 8 }}>
        <Switch
          style={{ marginRight: 8 }}
          checked={autoConnectEnabled}
          onChange={setAutoConnectEnabled}
        />
        Auto connect
      </div>
      {connected && (
        <>
          <div style={{ marginBottom: 8 }}>
            <Select onSelect={setEndpoint} value={endpoint} style={{ width: '100%' }}>
              {endpoints.map(({ name, endpoint: _endpoint }) => (
                <Select.Option value={_endpoint} key={_endpoint}>
                  {name}
                </Select.Option>
              ))}
            </Select>
          </div>
          <div>
            <Select onSelect={setProviderUrl} value={providerUrl} style={{ width: '100%' }}>
              {WALLET_PROVIDERS.map(({ name, url }) => (
                <Select.Option value={url} key={url}>
                  {name}
                </Select.Option>
              ))}
            </Select>
          </div>
        </>
      )}
    </>
  );
}
