import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Button, Modal, Popover } from 'antd';
import { InfoCircleOutlined, UserOutlined } from '@ant-design/icons';
import { useWallet } from '../utils/wallet';
import LinkAddress from './LinkAddress';
import { usePreferences } from '../utils/preferences';
import { useBackend } from '../utils/backend';
import { useLocalStorageState } from '../utils/utils';

const text = "We authorize users by signing a transaction created by the server. The wallet gets authorized only if the valid signature is provided. None of the data is connected to a specific user, but to the wallet, thereby authorizing the transactions.";

const AuthorizationPopoverContainer = styled.div`
  width: 300px;
  max-width: 100%;
`;

const AuthorizationPopover = () => (<AuthorizationPopoverContainer>{ text }</AuthorizationPopoverContainer>)

export default function WalletConnect() {
  const ref = React.useRef() as React.MutableRefObject<HTMLInputElement>;
  const { connected, wallet } = useWallet();
  const publicKey = wallet?.publicKey?.toBase58();
  const { loggedIn, setLoggedIn } = usePreferences();
  const [userWasConnected, setUserWasConnected] = useLocalStorageState(
    "USER_WAS_CONNECTED",
    false,
  );
  const [showAuthInfoModal, setShowAuthInfoModal] = React.useState(false);

  const {
    backEndConnected,
    disconnectBackEnd,
    backEndConnectionLoading,
  } = useBackend();

  useEffect(() => {
    wallet.on('disconnect', disconnectBackEnd);
    return () => wallet.off('disconnect', disconnectBackEnd);
  }, [wallet, disconnectBackEnd]);

  const disconnect = async () => {
    if (loggedIn) {
      setLoggedIn(false);
    }
    disconnectBackEnd();
    wallet.disconnect();
  };

  const connect = (isModal) => () => {
    if (userWasConnected || isModal) {
      setLoggedIn(true)
      setUserWasConnected(true);
      setShowAuthInfoModal(false);
      console.log(wallet);
      wallet.connect();
    } else {
      setShowAuthInfoModal(true);
    }
  }

  return (
    <React.Fragment>
      <Modal
        title="Authorization info"
        closable
        visible={showAuthInfoModal}
        onOk={connect(true)}
        okText="Connect"
        cancelText="Cancel"
        onCancel={() => setShowAuthInfoModal(false)}
      >
        { text }
      </Modal>
      <Button
        type="text"
        onClick={connected ? disconnect : connect(false)}
        style={{ color: '#0080FF', marginRight: 8 }}
        ref={ref}
        loading={backEndConnectionLoading}
      >
        <UserOutlined />
        {backEndConnected ? 'Disconnect' : 'Connect wallet'}
      </Button>
      {!backEndConnected && (
        <Popover
          content={<AuthorizationPopover />}
          placement="bottomRight"
          title="Authorization"
          trigger="click"
        >
          <InfoCircleOutlined style={{ color: '#0080FF' }} />
        </Popover>
      )}
      {backEndConnected && (
        <Popover
          content={<LinkAddress address={publicKey} />}
          placement="bottomRight"
          title="Wallet public key"
          trigger="click"
        >
          <InfoCircleOutlined style={{ color: '#0080FF' }} />
        </Popover>
      )}
    </React.Fragment>
  );
}
