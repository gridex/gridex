import React from 'react';

const Dependent = ({ children, dependencies } : { children: React.ReactNode, dependencies: boolean[]}) => {
  for (const dependency of dependencies) {
    if (!dependency) {
      return null;
    }
  }

  return <>{ children }</>;
};

export default Dependent;