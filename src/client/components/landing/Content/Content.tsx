import React from 'react';

import { Content as LayoutContent } from '../styles';

import { Main, Wallets, GridBot, Roadmap, Security } from '.';

const Content = () => {
  return (
    <LayoutContent>
      <Main />
      <GridBot />
      <Wallets />
      <Roadmap />
      <Security />
    </LayoutContent>
  );
};

export { Content };
