import React from 'react';
import styled from 'styled-components';

import { Section, Container, SectionTitle, Text, Button } from '../styles';

const StyledButton = styled(Button)`
  margin-top: 56px;

  @media (min-width: 768px) {
    margin-top: 72px;
  }
`;

const Security = () => {
  return (
    <Section id="security-section" withBackground>
      <Container column>
        <SectionTitle>Security</SectionTitle>

        <Text>
          Users authorize by signing a transaction created by the server. The wallet gets authorized only if the valid signature is provided. None of the data is connected to a specific user, but to the wallet, thereby authorizing the transactions.
        </Text>

        <StyledButton size="large" href="/gridex.pdf" target="_blank">Download presentation</StyledButton>
      </Container>
    </Section>
  );
};

export { Security };
