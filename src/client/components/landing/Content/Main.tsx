import React from 'react';
import styled from 'styled-components';

import { mainColor } from '../../../global_style';
import backgroundPath from '../../../assets/landing/main_bg.jpg';
import commasLogoPath from '../../../assets/3commas_logo.svg';
import mockPath from '../../../assets/landing/main_mock.jpg';
import { Section, Container, Button } from '../styles';
import { Form, Input, Modal } from 'antd';
import { simpleBackendRequest } from '../../../utils/backend';
import { notify } from '../../../utils/notifications';
import { SOCIALS_MAP } from '../Footer';

const StyledSection = styled(Section)`
  background: url(${backgroundPath}) center/cover no-repeat;
  overflow: hidden;
`;

const Inner = styled.div`
  display: grid;
  gap: 48px;
  justify-items: center;
  align-self: flex-start;
  flex-grow: 1;
  flex-shrink: 0;
  color: #fff;

  @media (min-width: 500px) {
    gap: 56px;

    @media (min-width: 768px) {
      gap: 64px;
      justify-items: start;
    }
  }
`;

const Mock = styled.div`
  position: relative;
  right: -5%;
  display: none;
  flex-shrink: 0;
  width: 100%;
  max-width: 1009px;
  height: 520px;

  @media (min-width: 768px) {
    display: block;

    @media (min-width: 1024px) {
      right: -10%;
      height: 583px;

      @media (min-width: 1200px) {
        right: -20%;

        @media (min-width: 1440px) {
          right: -25%;
        }
      }
    }
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
    object-position: top left;
  }
`;

const Title = styled.h1`
  display: grid;
  gap: 18px;
  font-weight: 700;
  line-height: 1;
  text-align: center;
  text-transform: uppercase;

  @media (min-width: 500px) {
    gap: 22px;

    @media (min-width: 768px) {
      text-align: left;
    }
  }
`;

const FirstLine = styled.span`
  font-size: 27px;
  letter-spacing: 0.14em;

  @media (min-width: 500px) {
    font-size: 38px;

    @media (min-width: 768px) {
      font-size: 42px;
    }
  }
`;

const SecondLine = styled.span`
  font-size: 50px;
  color: ${mainColor};

  @media (min-width: 500px) {
    font-size: 78px;

    @media (min-width: 768px) {
      font-size: 90px;
    }
  }
`;

const ThirdLine = styled.span`
  font-weight: 500;
  font-size: 30px;
  letter-spacing: 0.05em;
  text-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);

  @media (min-width: 500px) {
    font-size: 52px;

    @media (min-width: 768px) {
      font-size: 64px;
    }
  }
`;

const Label = styled.div`
  display: flex;
  align-items: center;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  letter-spacing: 0.05em;

  img {
    width: 153px;
    height: 42px;
    margin-left: 18px;
    object-fit: contain;

    @media (min-width: 768px) {
      margin-left: 24px;
    }
  }
`;

const ButtonsGroup = styled.div`
  display: grid;
  gap: 24px;
  width: 100%;

  @media (min-width: 500px) {
    justify-content: center;

    @media (min-width: 768px) {
      justify-content: start;
    }
  }
`;

const SocialLine = styled.span`
  margin-bottom: 0;
  display: flex;
  align-items: center;

  img {
    height: 20px;
    margin-right: 8px;
    width: 20px;
    object-fit: contain;
  }
`;

const SocialsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const showWhiteList = false;

const Main = () => {
  const [valid, setValid] = React.useState(true);
  const [modalIsVisible, setModalIsVisible] = React.useState(false);
  const [email, setEmail] = React.useState('');
  const send = async () => {
    if (/^\S+@\S+\.\S+$/.test(email)) {
      await simpleBackendRequest('/landing/subscribe', 'post', { email });
      setValid(true);
      setEmail('');
      setModalIsVisible(false);
      notify({
        message: 'Thank you',
        description: (
          <div>
            <p>You are applied for whitelist. Increase your chances by following our socials.</p>
            <SocialsContainer>
              {SOCIALS_MAP.map((e) => (
                <SocialLine>
                  <a target="_blank" href={e.link} rel="noopener noreferrer">
                    <img src={e.logo} alt={e.name} />
                    {e.name}
                  </a>
                </SocialLine>
              ))}
            </SocialsContainer>
          </div>
        ),
        duration: 15,
        closeIcon: true,
      });
    } else {
      setValid(false);
    }
  };
  const onEmailChange = (e) => {
    setValid(true);
    setEmail(e.target.value);
  };

  return (
    <StyledSection>
      {
        showWhiteList && <Modal
          visible={modalIsVisible}
          title="Send your Email to apply for whitelist"
          okText="Send"
          onOk={send}
          onCancel={() => setModalIsVisible(false)}
        >
          <Form.Item validateStatus={valid ? 'success' : 'error'}>
            <Input type="email" value={email} onChange={onEmailChange} />
          </Form.Item>
        </Modal>
      }
      <Container wide>
        <Inner>
          <Title>
            <FirstLine>The first ever</FirstLine>
            <SecondLine>GRID BOT</SecondLine>
            <ThirdLine>Built on Solana</ThirdLine>
          </Title>
          <Label>
            Powered by
            <img src={commasLogoPath} alt="" />
          </Label>
          <ButtonsGroup>
            <Button href="/app/grid-bot" type="primary" size="large">
              Launch a GRID bot
            </Button>
            {
              showWhiteList && <Button size="large" onClick={() => setModalIsVisible(true)}>
                APPLY FOR WHITELIST
              </Button>
            }
          </ButtonsGroup>
        </Inner>

        <Mock>
          <img src={mockPath} alt="" />
        </Mock>
      </Container>
    </StyledSection>
  );
};

export { Main };
