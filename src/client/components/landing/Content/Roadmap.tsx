import React from 'react';
import styled, { css } from 'styled-components';

import { mainColor } from '../../../global_style';
import { Section, Container, SectionTitle, Button } from '../styles';

const ROADMAP_STEPS_MAP = [
  { date: 'Q4 2020', text: 'GRIDex Development Starts<span>GRID bot concept implementation on the Solana blockchain</span>', passed: true, current: false },
  { date: 'Q1 2021', text: 'GRIDex beta test launch', passed: true, current: true },
  {
    date: 'Q2 2021',
    text: 'Implementation of the dynamic & infinite GRID bots strategy',
    passed: false,
    current: false,
  },
  {
    date: 'Q3/Q4 2021',
    text: 'Derivatives trading on GRIDex',
    passed: false,
    current: false,
  },
  {
    date: '2022',
    text: 'GRIDex options trading<span>Adding technical indicators as signals for opening/closing trades within the bot, public API and strategies store</span>',
    passed: false,
    current: false,
  }
];

const List = styled.ul`
  display: grid;
  margin: 0 0 72px 16px;
  padding: 0;
  list-style: none;

  @media (min-width: 500px) {
    margin: 0 0 72px;
  }
`;

const Description = styled.p`
  margin: 8px 0 0;
  color: ${mainColor};
  display: flex;
  flex-direction: column;

  @media (min-width: 500px) {
    margin: 0;
  }

  span {
    width: 100%;
    color: #fff;
    font-size: 0.8em;
  }
`;

const Date = styled.span`
  color: #fff;
`;

const Step = styled.li<{ passed: boolean; current: boolean }>`
  position: relative;
  display: grid;
  align-items: center;
  padding: 20px 0 20px 48px;
  font-weight: 700;
  font-size: 16px;
  line-height: 20px;
  letter-spacing: 0.05em;

  ${(p) =>
    !p.current &&
    !p.passed &&
    css`
      ${Description} {
        opacity: 0.5;
      }

      ${Date} {
        opacity: 0.45;
      }
    `}

  @media (min-width: 500px) {
    gap: 100px;
    grid-template-columns: repeat(2, 1fr);
    padding: 16px 0;

    &:nth-child(odd) {
      direction: rtl;

      ${Description} {
        text-align: right;
      }
    }

    &:nth-child(even) {
      ${Date} {
        text-align: right;
      }
    }

    @media (min-width: 768px) {
      gap: 160px;
      padding: 24px 0;
      font-size: 20px;
      line-height: 24px;

      @media (min-width: 1024px) {
        gap: 210px;
      }
    }
  }

  &:before,
  &:after {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    margin: auto;
    background: #fff;

    @media (min-width: 500px) {
      right: 0;
    }
  }

  &:before {
    top: -4px;
    bottom: -4px;
    width: 1px;

    @media (max-width: 499px) {
      left: 7px;
    }
  }

  &:after {
    width: 15px;
    height: 15px;
    border-radius: 50%;

    ${(p) =>
      p.passed &&
      css`
        background: radial-gradient(circle closest-side, ${mainColor} 2px, #fff 3px);

        @media (min-width: 768px) {
          background: radial-gradient(circle closest-side, ${mainColor} 3px, #fff 4px);
        }
      `}

    ${(p) =>
      p.current &&
      css`
        background: radial-gradient(circle closest-side, ${mainColor} 3px, #fff 4px);
        box-shadow: 0 0 8px 2px rgba(255, 255, 255, 0.25);

        @media (min-width: 768px) {
          background: radial-gradient(circle closest-side, ${mainColor} 4px, #fff 5px);
        }
      `}

    @media (max-width: 499px) {
      margin: 0 auto;
      top: 23px;
    }

    @media (min-width: 768px) {
      width: 12px;
      height: 12px;
      ${(p) =>
        (p.passed) && css`
          width: 18px;
          height: 18px;
        `
      }

      ${(p) =>
        (p.current) && css`
          width: 22px;
          height: 22px;
        `
      }
    }
  }
`;

const Roadmap = () => {
  return (
    <Section id="roadmap-section">
      <Container column>
        <SectionTitle>Roadmap</SectionTitle>

        <List>
          {ROADMAP_STEPS_MAP.map(({ date, text, passed, current }) => (
            <Step key={date} passed={passed} current={current}>
              <Date>{date}</Date>
              <Description dangerouslySetInnerHTML={{ __html: text }} />
            </Step>
          ))}
        </List>

        <Button href="/app/grid-bot" type="primary" size="large">
          Try it now
        </Button>
      </Container>
    </Section>
  );
};

export { Roadmap };
