import React from 'react';
import styled from 'styled-components';

import walletFirstLogoPath from '../../../assets/landing/wallet_logo_1.png';
import walletSecondLogoPath from '../../../assets/landing/wallet_logo_2.png';
import walletThirdLogoPath from '../../../assets/landing/wallet_logo_3.svg';
import walletFourthLogoPath from '../../../assets/landing/wallet_logo_4.png';
import walletPhantomLogoPath from '../../../assets/landing/wallet_logo_phantom.png';
import walletLedgerLogoPath from '../../../assets/landing/wallet_logo_ledger.svg';

import { Section, SectionTitle, Container } from '../styles';

export const WALLETS_LOGO_MAP = [
  { logo: walletFirstLogoPath, name: 'Sollet' },
  { logo: walletSecondLogoPath, name: 'Solong' },
  { logo: walletThirdLogoPath, name: 'Bonfida' },
  { logo: walletFourthLogoPath, name: 'Math Wallet' },
  { logo: walletPhantomLogoPath, name: 'Phantom' },
  { logo: walletLedgerLogoPath, name: 'Ledger' },
];

const LogoList = styled.ul`
  display: grid;
  gap: 56px;
  grid-template-columns: repeat(2, min-content);
  justify-content: center;
  width: 100%;
  margin: 0;
  padding: 0;
  list-style: none;

  @media (min-width: 768px) {
    grid-template-columns: repeat(3, min-content);
    justify-content: space-around;

    @media (min-width: 1024px) {
      justify-content: space-between;
    }
  }
`;

const Logo = styled.li`
  width: 86px;
  height: 86px;
  margin-bottom: 6px;
  display: flex;
  flex-direction: column;
  justify-content: center;

  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
    margin-bottom: 16px;
    filter: grayscale(75%);
    transition: 0.3s filter ease;
  }

  &:hover {
    img {
      filter: grayscale(0);
    }
  }

  span {
    font-size: 16px;
    text-align: center;
  }
`;

const Wallets = () => {
  return (
    <Section id="wallets-section">
      <Container column>
        <SectionTitle>Supported wallets</SectionTitle>
        <LogoList>
          {WALLETS_LOGO_MAP.map((wallet) => (
            <Logo key={wallet.name}>
              <img src={wallet.logo} alt="" />
              <span>{wallet.name}</span>
            </Logo>
          ))}
        </LogoList>
      </Container>
    </Section>
  );
};

export { Wallets };
