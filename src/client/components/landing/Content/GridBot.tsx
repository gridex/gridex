import React from 'react';
import styled from 'styled-components';

import { Section, Container, SectionTitle, Text, Button } from '../styles';

const ButtonsGroup = styled.div`
  display: grid;
  gap: 24px;
  width: 100%;
  max-width: 788px;
  margin-top: 56px;

  @media (min-width: 500px) {
    justify-content: center;

    @media (min-width: 768px) {
      grid-template-columns: repeat(2, min-content);
      justify-content: space-between;
      margin-top: 72px;
    }
  }
`;

const GridBot = () => {
  return (
    <Section id="grid-bot-section" withBackground>
      <Container column>
        <SectionTitle>What are GRID bots?</SectionTitle>
        <Text>3Commas has developed a DeFi GRID bot based on mathematical trading data and built it into a single easy-to-use interface.</Text>
        <Text>GRIDex autonomously places limit orders at set price intervals, allowing you to obtain passive profits from asset movement.</Text>
        <Text>As GRIDex is autonomous, it will participate in the market 24/7 with minimal effort.</Text>
        <Text>As a user, you are in control of what range and parameters which the bot will operate. Then sit back, relax, and let the  GRIDex make the rest possible in a decentralized manner of the Solana blockchain.</Text>

        <ButtonsGroup>
          <Button href="/app/grid-bot" type="primary" size="large">
            Launch a GRID bot
          </Button>

          <Button size="large" href="/gridex.pdf" target="_blank">
            Download presentation
          </Button>
        </ButtonsGroup>
      </Container>
    </Section>
  );
};

export { GridBot };
