import React from 'react';
import styled from 'styled-components';
import { Anchor } from 'antd';

import { mainColor } from '../../global_style';
import logoPath from '../../assets/gridex.svg';
import { Header as LayoutHeader, Container, Button } from './styles';

const { Link } = Anchor;

const LINKS_MAP = [
  { path: '#grid-bot-section', text: 'GRID Bot' },
  { path: '#wallets-section', text: 'Wallets' },
  { path: '#roadmap-section', text: 'Roadmap' },
  { path: '#security-section', text: 'Security' },
];

const StyledHeader = styled(LayoutHeader)`
  height: auto;
  min-height: 114px;
  padding: 32px 0;
  background: none;

  @media (max-width: 991px) {
    padding: 24px 0 32px;

    @media (max-width: 499px) {
      min-height: 98px;
      padding: 24px 0;
    }
  }
`;

const Inner = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;
  margin-left: 16px;

  @media (max-width: 991px) {
    flex-direction: column;
    align-items: flex-end;
  }
`;

const Nav = styled.nav`
  flex-grow: 1;

  @media (max-width: 991px) {
    margin-bottom: 16px;

    @media (max-width: 499px) {
      display: none;
    }
  }

  .ant-anchor-wrapper {
    margin: 0;
    padding: 0;
    background-color: transparent;
    overflow: hidden;
  }

  .ant-anchor {
    display: flex;
    justify-content: center;
  }

  .ant-anchor-ink {
    display: none;
  }
`;

const StyledLink = styled(Link)`
  padding: 0;

  :not(:last-child) {
    margin-right: 40px;

    @media (max-width: 991px) {
      margin-right: 32px;
    }
  }

  a {
    font-weight: 700;
    font-size: 16px;
    line-height: 28px;
    letter-spacing: 0.05em;
    color: #fff;
    white-space: nowrap;

    :hover {
      color: ${mainColor};
    }
  }
`;

const Logo = styled.div`
  display: flex;
  align-items: center;
  flex-shrink: 0;
  width: 50px;
  height: 50px;

  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
`;

const StyledButton = styled(Button)`
  width: auto;
`;

const Header = () => {
  return (
    <StyledHeader>
      <Container wide>
        <Logo>
          <img src={logoPath} alt="" />
        </Logo>

        <Inner>
          <Nav affix={false}>
            <Anchor affix={false}>
              {LINKS_MAP.map(({ path, text }) => (
                <StyledLink key={path} href={path} title={text} />
              ))}
            </Anchor>
          </Nav>

          <StyledButton href="/app/grid-bot" type="primary" size="large" autoWidth>
            Launch a GRID bot
          </StyledButton>
        </Inner>
      </Container>
    </StyledHeader>
  );
};

export { Header };
