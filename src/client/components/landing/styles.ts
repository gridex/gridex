import { Layout, Row, Button as BaseButton } from 'antd';
import styled, { css } from 'styled-components';

import { mainColor } from '../../global_style';

const { Header, Content, Footer } = Layout;

const Section = styled.section<{ withBackground: boolean }>`
  padding: 56px 0;
  background-color: ${(p) => (p.withBackground ? '#1a2029;' : 'transparent')};

  @media (min-width: 768px) {
    padding: 72px 0;
  }
`;

const Container = styled(Row)<{ column: boolean; wide: boolean }>`
  display: flex;
  flex-direction: ${(p) => (p.column ? 'column' : 'row')};
  flex-wrap: nowrap;
  align-items: center;
  width: calc(100% - 16px * 2);
  max-width: ${(p) => (p.wide ? 1440 : 930)}px;
  margin: 0 auto;

  @media (min-width: 768px) {
    width: calc(100% - 32px * 2);
  }

  @media (min-width: 1440px) {
    width: calc(100% - 72px * 2);
  }
`;

const SectionTitle = styled.h2`
  width: 100%;
  margin-bottom: 56px;
  font-weight: 700;
  font-size: 28px;
  line-height: 1.2;
  text-align: center;
  letter-spacing: 0.05em;
  color: ${mainColor};

  @media (min-width: 768px) {
    margin-bottom: 72px;
    font-size: 42px;
  }
`;

const Button = styled(BaseButton)<{ autoWidth: boolean; type: 'primary' }>`
  width: 100%;
  font-size: 14px;
  font-weight: 700;

  text-transform: uppercase;

  ${(p) =>
    p.type !== 'primary' &&
    css`
      color: ${mainColor};
      border-color: ${mainColor};
    `}

  @media (min-width: 375px) {
    padding-left: 40px;
    padding-right: 40px;

    @media (min-width: 500px) {
      width: auto;
      min-width: ${(p) => (p.autoWidth ? 'auto' : '315px')};
    }
  }
`;

const Text = styled.p`
  width: 100%;
  margin: 0;
  font-weight: 500;
  font-size: 16px;
  line-height: 1.35;
  text-align: center;
  letter-spacing: 0.05em;
  color: #fff;

  @media (min-width: 768px) {
    font-size: 20px;
    line-height: 27px;
  }

  & + & {
    margin-top: 26px;
  }
`;

export { Container, Section, SectionTitle, Button, Text, Header, Content, Footer };
