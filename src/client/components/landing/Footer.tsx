import React from 'react';
import styled from 'styled-components';

import discordLogoPath from '../../assets/landing/discord_logo.svg';
import tgLogoPath from '../../assets/landing/tg_logo.svg';
import twitterLogoPath from '../../assets/landing/twiter_logo.svg';
import logoPath from '../../assets/gridex.svg';
import { Container, Footer as LayoutFooter } from './styles';
import { FeedbackForm } from '../FeedbackForm';

export const SOCIALS_MAP = [
  { link: 'https://discord.com/invite/FyENCJt8nY', logo: discordLogoPath, name: 'Discord' },
  { link: 'https://t.me/Gridex_bot', logo: tgLogoPath, name: 'Telegram' },
  { link: 'https://twitter.com/Gridex_bot', logo: twitterLogoPath, name: 'Twitter' },
];

interface JSSProps {
  isformainpage: boolean;
}

const StyledFooter = styled(LayoutFooter)`
  padding: ${({ isformainpage }: JSSProps) => (isformainpage ? '32px 0' : '24px 0')};
  background-color: #0c1117;
`;

const Inner = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;

const StyledContainer = styled(Container)`
  max-width: ${({ isformainpage }: JSSProps) => (isformainpage ? '1440px' : '100%')};
  @media (min-width: 1440px) {
    width: calc(100% - 30px * 2);
  }
`;

const Logo = styled.div`
  width: ${({ isformainpage }: JSSProps) => (isformainpage ? '50px' : '35px')};
  height: ${({ isformainpage }: JSSProps) => (isformainpage ? '50px' : '35px')};

  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
`;

const SocialList = styled.ul`
  display: flex;
  margin: 0;
  padding: 0;
  list-style: none;
`;

const SocialItem = styled.li`
  :not(:last-child) {
    margin-right: 20px;
  }

  a {
    display: flex;
    width: 28px;
    height: 28px;

    img {
      width: 100%;
      height: 100%;
      object-fit: contain;
      object-position: top right;
    }
  }
`;

interface Props {
  isForMainPage?: boolean;
}

const Footer = ({ isForMainPage = true }: Props) => {
  return (
    <StyledFooter $isformainpage={isForMainPage}>
      <StyledContainer $wide={true} $isformainpage={isForMainPage}>
        <Inner>
          <Logo $isformainpage={isForMainPage}>
            <img src={logoPath} alt="" />
          </Logo>
          {!isForMainPage && <FeedbackForm />}
          <SocialList>
            {SOCIALS_MAP.map(({ link, logo }) => (
              <SocialItem key={logo}>
                <a href={link} target="_blank" rel="noopener noreferrer">
                  <img src={logo} alt="" />
                </a>
              </SocialItem>
            ))}
          </SocialList>
        </Inner>
      </StyledContainer>
    </StyledFooter>
  );
};

export { Footer };
