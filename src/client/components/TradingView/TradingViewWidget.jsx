import { Component, Fragment } from 'react';
import React from 'react';

import tradingViewTimeZone from './tradingViewTimeZone';
import { Col, Row, Spin as Loader } from 'antd';
import debounce from 'lodash/debounce';

const getScript = (source, callback) => {
  let script = document.createElement('script');
  const prior = document.getElementsByTagName('script')[0];
  script.async = 1;

  script.onload = script.onreadystatechange = (_, isAbort) => {
    if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
      script.onload = script.onreadystatechange = null;
      script = undefined;
      if (!isAbort) {
        if (callback) {
          callback();
        }
      }
    }
  };

  script.src = source;
  prior.parentNode.insertBefore(script, prior);
};

const loadTradingViewScript = () =>
  new Promise((resolve, reject) => {
    if (window.TradingView && window.TradingView.widget) {
      resolve();
    } else {
      getScript('https://s3.tradingview.com/tv.js', resolve);
    }
  });

const STATIC_WIDGET_OPTIONS = {
  autosize: true,
  style: '1',
  locale: 'en',
  toolbar_bg: '#f1f3f6',
  enable_publishing: false,
  hide_top_toolbar: false,
  hide_side_toolbar: false,
  enabled_features: ['header_fullscreen_button', 'side_toolbar_in_fullscreen_mode'],
  hideideas: true,
  studies: ['BB@tv-basicstudies'],
};

export default class TradingViewWidget extends Component {
  constructor(props) {
    super(props);
    this.widget = null;
    this.errorCounter = 0;
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    if (!this.symbolIncludesUndefined()) {
      this.initWidget();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.symbolWasUndefined(prevProps) && !this.symbolIncludesUndefined()) {
      this.initWidget();
    } else if (!this.symbolWasUndefined(prevProps) && this.symbolIncludesUndefined()) {
      this.removeWidget();
    } else if (this.symbolChange(prevProps.exchange, prevProps.pair)) {
      this.setSymbol();
    }
  }

  symbolIncludesUndefined() {
    return this.widgetSymbolFromProps().toUpperCase().includes('UNDEFINED');
  }

  symbolWasUndefined(prevProps) {
    return prevProps.exchange == null || prevProps.pair == null;
  }

  setSymbol() {
    if (this.widget) {
      this.widget.options.symbol = this.widgetSymbolFromProps();
      this.widget.reload();
    }
  }

  initWidget = debounce(this._initWidget, 300);

  _initWidget() {
    loadTradingViewScript()
      .then(() =>
        this.setState({ loading: false }, () => {
          let dynamicWidgetOptions = {
            container_id: this.props.containerId,
            symbol: this.widgetSymbolFromProps(),
            interval: '60',
            timezone: tradingViewTimeZone(),
            theme: 'Dark',
          };
          this.widget = new window.TradingView.widget({
            ...STATIC_WIDGET_OPTIONS,
            ...dynamicWidgetOptions,
          });
          window.w = this.widget;
          this.widget.ready(() => {
            if (this.props.onChartRendered) {
              this.props.onChartRendered();
            }
          });
        }),
      )
      .catch((_) => {
        console.log(_);
        this.errorCounter = this.errorCounter + 1;
        console.error('Failed to load trading view');
        if (this.errorCounter < 5) {
          setTimeout(() => this._initWidget(), 1000 * this.errorCounter);
        }
      });
  }

  widgetSymbolFromProps() {
    if (this.props.pair && this.props.exchange) {
      let splittedPair = this.props.pair.split(/[_]/);
      if (this.props.exchange === 'BYBIT') {
        return `${this.props.exchange}:${this.props.pair}`;
      } else if (this.props.exchange === 'FTX') {
        if (splittedPair[1].includes('-')) {
          return `${this.props.exchange}:${splittedPair[1].replace('-', '')}`;
        } else {
          return `${this.props.exchange}:${splittedPair[1]}${splittedPair[0]}`;
        }
      } else {
        return `${this.props.exchange}:${splittedPair[1]}${splittedPair[0]}`;
      }
    } else {
      if (this.props.pair && this.props.use_fallback) {
        let splittedPair = this.props.pair.split(/[_]/);
        return `${splittedPair[1]}${splittedPair[0]}`;
      } else {
        return `UNDEFINED`;
      }
    }
  }

  symbolChange(prevExchange, prevPair) {
    return !(prevExchange === this.props.exchange && prevPair === this.props.pair);
  }

  removeWidget() {
    if (this.widget) {
      this.widget = null;
    }
    if (!this.state.loading) {
      this.setState({ loading: true });
    }
  }

  render() {
    return (
      <Fragment>
        {this.state.loading ? (
          <Row align="middle" justify="center" style={{ height: '100%' }}>
            <Col>
              <Loader />
            </Col>
          </Row>
        ) : (
          <Fragment>
            <div
              id={this.props.containerId}
              className={this.props.className}
              style={{ height: '100%', width: '100%' }}
            />
          </Fragment>
        )}
      </Fragment>
    );
  }
}

// TradingViewWidget.propTypes = {
//   containerId: PropTypes.string,
//   className: PropTypes.string,
//   pair: PropTypes.string,
//   exchange: PropTypes.string,
// };
