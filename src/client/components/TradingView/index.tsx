import * as React from 'react';
import { useEffect, useState } from 'react';
import { MARKETS } from '@project-serum/serum';
import './index.css';
import {
  ChartingLibraryWidgetOptions,
  IChartingLibraryWidget,
  IChartWidgetApi,
  widget,
} from '../../charting_library/charting_library'; // Make sure to follow step 1 of the README
import { useMarket } from '../../utils/markets';
import { findTVMarketFromAddress } from '../../utils/tradingview';
import { memoize } from 'lodash';
import { buyColor, sellColor, waitColor } from '../../global_style';
import { useBackend } from '../../utils/backend';
import TradingViewWidget from './TradingViewWidget';
import { Order } from '../../../common/models/grid-bot';

// This is a basic example of how to create a TV widget
// You can add more feature such as storing charts in localStorage

export interface ChartContainerProps {
  symbol: ChartingLibraryWidgetOptions['symbol'];
  interval: ChartingLibraryWidgetOptions['interval'];
  datafeedUrl: string;
  libraryPath: ChartingLibraryWidgetOptions['library_path'];
  chartsStorageUrl: ChartingLibraryWidgetOptions['charts_storage_url'];
  chartsStorageApiVersion: ChartingLibraryWidgetOptions['charts_storage_api_version'];
  clientId: ChartingLibraryWidgetOptions['client_id'];
  userId: ChartingLibraryWidgetOptions['user_id'];
  fullscreen: ChartingLibraryWidgetOptions['fullscreen'];
  autosize: ChartingLibraryWidgetOptions['autosize'];
  studiesOverrides: ChartingLibraryWidgetOptions['studies_overrides'];
  containerId: ChartingLibraryWidgetOptions['container_id'];
  theme: string;
}

const configurationData = {
  supported_resolutions: ['1', '5', '15', '30', '60', '240', '480', '1D', '1W', '1M'],
  exchanges: [
    {
      value: 'Serum',
      name: 'Serum',
      desc: 'Serum Solana Exchange',
    },
  ],
  symbols_types: [
    {
      name: 'crypto',
      // `symbolType` argument for the `searchSymbols` method, if a user selects this symbol type
      value: 'crypto',
    },
  ],
};

const getAllSymbols = memoize(() => {
  return MARKETS
    .filter(m => !m.deprecated)
    .map(m => ({
      symbol: m.name,
      full_name: m.name,
      description: m.address.toBase58(),
      exchange: 'Serum',
      type: 'crypto',
      address: m.address.toBase58(),
    }));
});

const RES_MUL = {
  H: 60,
  D: 60 * 24,
  M: 60 * 24 * 30,
  W: 60 * 24 * 7,
};

function resolutionToSec(res) {
  const mul = RES_MUL[res[res.length - 1]] || 1;
  return parseInt(res, 10) * mul;
}

async function loadKlines(symbolInfo, resolution, from, to) {
  const interval = resolutionToSec(resolution);

  const symbol = getAllSymbols().find((m) => m.symbol === symbolInfo.ticker)!;
  // http://3.8.240.68/api/klines
  const url = `/api/klines?symbol=${symbol.symbol
  }&startTime=${from
  }&endTime=${to
  }&limit=1000&interval=${interval}`;
  const req = await fetch(url);
  let klines = await req.json();
  klines.forEach((k, i) => {
    // k.high = k.low;
    k.open = i > 0 ? klines[i - 1].c : k.o;
    k.close = k.c;
    k.high = k.h;
    k.low = k.l;
    k.volume = k.v;
    k.time = k.t * 1000;

    if (k.h / k.o > 1.05) k.high = k.o * 1.05;
    if (k.l / k.o < 0.95) k.low = k.o * 0.95;
    // k.price = k.price / symbolInfo.pricescale;
  });

  return klines;
}

class DataFeed {
  onReady(callback) {
    setTimeout(() => callback(configurationData));
  }

  async resolveSymbol(symbolName, onSymbolResolvedCallback, onResolveErrorCallback) {
    console.log('[resolveSymbol]: Method call', symbolName);
    const symbolDesc = getAllSymbols().find((symbol) => symbol.symbol === symbolName);
    if (!symbolDesc) {
      console.log('[resolveSymbol]: Cannot resolve symbol', symbolName);
      onResolveErrorCallback('cannot resolve symbol');
      return;
    }
    // const market = await Market.load(
    //   new Connection(globalThis.SOLANA_NODE_URL),
    //   marketDesc.address,
    //   undefined,
    //   marketDesc.programId,
    // );
    const tickSize = 0.001;

    const precision = -Math.log10(tickSize);
    const symbolInfo = {
      ticker: symbolDesc.symbol,
      name: symbolDesc.symbol,
      description: `Serum ${symbolDesc.symbol} market`,
      type: 'crypto',
      session: '24x7',
      timezone: 'Etc/UTC',
      exchange: 'Serum',
      minmov: 1,
      pricescale: 1 / tickSize,
      has_intraday: true,
      has_no_volume: false,
      has_weekly_and_monthly: false,
      supported_resolutions: configurationData.supported_resolutions,
      volume_precision: precision,
      data_status: 'streaming',
    };

    console.log('[resolveSymbol]: Symbol resolved', symbolName);
    onSymbolResolvedCallback(symbolInfo);
  }

  async getBars(
    symbolInfo,
    resolution,
    from,
    to,
    onHistoryCallback,
    onErrorCallback,
  ) {
    try {
      const klines = await loadKlines(symbolInfo, resolution, from, to);

      onHistoryCallback(klines, { noData: false });
    } catch (err) {
      onErrorCallback(err);
    }
  }

  async searchSymbols(userInput, exchange, symbolType, onResultReadyCallback) {
    console.log('[searchSymbols]: Method call');
    userInput = userInput.toUpperCase();
    const newSymbols = getAllSymbols().filter((market) => {
      return market.symbol.includes(userInput);
    });
    onResultReadyCallback(newSymbols);
  }

  subs: { [uid: string]: any } = {};

  async subscribeBars(
    symbolInfo,
    resolution,
    onRealtimeCallback,
    subscribeUID,
  ) {
    this.subs[subscribeUID] = {
      isStop: false,
      lastAt: Date.now(),
      async run() {
        const klines = await loadKlines(symbolInfo, resolution, Math.floor(this.lastAt / 1000), '');
        this.lastAt = Date.now();
        onRealtimeCallback(klines);

        setTimeout(() => (!this.isStop && this.run()), 180_000);
      },
    };

    this.subs[subscribeUID].run();
  }

  unsubscribeBars(subscribeUID) {
    this.subs[subscribeUID].isStop = true;
    delete this.subs[subscribeUID];
  }
}

interface IChartAdapter {
  remove(): void;
}

let globalShapes: IChartAdapter[] = [];

export const TVChartNative = ({
  lines = [],
  backtest,
  symbol = "",
}: {
  lines: any[],
  backtest?: Order[],
  symbol?: string | null
}) => {
  const [chart, setChart] = useState<IChartWidgetApi>();
  // @ts-ignore
  const defaultProps: ChartContainerProps = {
    symbol: 'SOL/USDT',
    interval: '60' as ChartingLibraryWidgetOptions['interval'],
    theme: 'Dark',
    containerId: 'tv_chart_container',
    libraryPath: '/charting_library/',
    fullscreen: false,
    autosize: true,
    studiesOverrides: {},
  };

  const tvWidgetRef = React.useRef<IChartingLibraryWidget | null>(null);
  const { market } = useMarket();

  React.useEffect(() => {
    const dataFeed = new DataFeed();
    const widgetOptions: ChartingLibraryWidgetOptions = {
      symbol: symbol || findTVMarketFromAddress(market?.address.toBase58() || '') as string,
      // tslint:disable-next-line:no-any
      datafeed: dataFeed,
      interval: defaultProps.interval,
      container_id: defaultProps.containerId as ChartingLibraryWidgetOptions['container_id'],
      library_path: defaultProps.libraryPath as string,
      locale: 'en',
      disabled_features: ['use_localstorage_for_settings'],
      enabled_features: ['study_templates'],
      load_last_chart: true,
      client_id: defaultProps.clientId,
      user_id: defaultProps.userId,
      fullscreen: defaultProps.fullscreen,
      autosize: defaultProps.autosize,
      studies_overrides: defaultProps.studiesOverrides,
      theme: 'Dark',
      debug: true,
      timezone: 'Etc/UTC',
    };

    const tvWidget = new widget(widgetOptions);
    tvWidgetRef.current = tvWidget;

    tvWidget.onChartReady(() => {
      setChart(tvWidget.chart());
      tvWidget.headerReady().then(() => {
        const button = tvWidget.createButton();
        button.setAttribute('title', 'Click to show a notification popup');
        button.classList.add('apply-common-tooltip');
        button.addEventListener('click', () =>
          tvWidget.showNoticeDialog({
            title: 'Notification',
            body: 'TradingView Charting Library API works correctly',
            callback: () => {
              console.log('It works!!');
            },
          }),
        );
        button.innerHTML = 'Check API';
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [market, symbol]);

  useEffect(() => {
    if (chart) {
      chart.removeAllShapes();
      // chart.removeAllStudies();
      // chart.resetData();
      globalShapes.forEach(shape => shape.remove());
      globalShapes = [];

      try {
        backtest?.forEach((order) => {
          const arrow = chart.createExecutionShape({})
            .setPrice(order.price)
            .setDirection(order.side)
            .setTime(new Date(order.closedAt as any as string).getTime() / 1000);

          globalShapes.push(arrow);
        });

        lines.forEach((line) => {
          const { name, color } = {
            wait: {
              color: waitColor,
              name: 'Current'
            },
            buy: {
              color: buyColor,
              name: 'Buy'
            },
            sell: {
              color: sellColor,
              name: 'Sell'
            }
          }[line.side];
          const orderLine = chart.createOrderLine({})
            .setPrice(line.price)
            .setText(name)
            .setQuantity("")
            .setLineStyle(0)
            .setLineColor(color)
            .setBodyBorderColor(color)
            .setBodyBackgroundColor(color)
            .setBodyTextColor("#fff");
          globalShapes.push(orderLine);
        });
      } catch (e) {
        console.log(e);
      }
    }
  }, [chart, lines, backtest]);

  return <div id={defaultProps.containerId} className="tradingview-chart" />;
};

export const TVChartContainer = ({
  lines = [],
  backtest,
  marketAddress,
}: {
  lines: any[],
  backtest?: Order[],
  marketAddress?: string
}) => {
  const [symbol, setSymbol] = React.useState<null | string>(null);
  const [pair, setPair] = React.useState<null | string>(null);
  const [type, setType] = React.useState<'native' | 'widget' | null>(null);
  const { market: currentMarket } = useMarket();
  const { backendRequest, backEndConnected } = useBackend();

  const _marketAddress = marketAddress || currentMarket?.address?.toBase58();

  const checkKlines = async () => {
    setType(null);
    const symbol = findTVMarketFromAddress(_marketAddress || '');
    setSymbol(symbol);
    setPair(symbol ? symbol.split("/").reverse().join("_") : null);
    if (_marketAddress && backEndConnected) {
      const result = await backendRequest('/check-klines', 'post', { symbol });
      if (result.success) {
        setType('native');
      } else {
        setType('widget');
      }
    } else {
      setType('widget');
    }
  }

  useEffect(() => {
    checkKlines();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_marketAddress, backEndConnected]);

  switch (type) {
    case 'native':
      return <TVChartNative lines={lines} backtest={backtest} symbol={symbol} />;
    case 'widget':
      return <TradingViewWidget exchange="BINANCE" containerId="tv_container" pair={pair} />;
    default:
      return null;
  }
};
