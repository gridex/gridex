const tradingViewTimeZone = (
  current_timezone_offset = new Date().getTimezoneOffset(),
) => {
  let userTimeZoneOffset =
    (typeof current_timezone_offset === 'string'
      ? parseInt(current_timezone_offset)
      : current_timezone_offset) * 60;
  if (TradingViewZonesWithOffset.hasOwnProperty(userTimeZoneOffset)) {
    return TradingViewZonesWithOffset[userTimeZoneOffset];
  } else {
    return findClosestTimeZone(userTimeZoneOffset);
  }
};
const findClosestTimeZone = (currentUserTimeZoneOffSet) => {
  let bestTimeZone = 'Europe/London';
  let bestTimeZoneDifference = Math.abs(currentUserTimeZoneOffSet); // cause europe/london is zero
  for (let key in TradingViewZonesWithOffset) {
    let curDifference = Math.abs(currentUserTimeZoneOffSet - parseInt(key));
    if (curDifference < bestTimeZoneDifference) {
      bestTimeZone = TradingViewZonesWithOffset[key];
      bestTimeZoneDifference = curDifference;
    }
  }
  return bestTimeZone;
};
const TradingViewZonesWithOffset = {
  '-49500': 'Pacific/Chatham',
  '-46800': 'Pacific/Fakaofo',
  '-39600': 'Australia/ACT',
  '-37800': 'Australia/Adelaide',
  '-36000': 'Australia/Brisbane',
  '-32400': 'Asia/Seoul',
  '-28800': 'Asia/Chongqing',
  '-25200': 'Asia/Bangkok',
  '-21600': 'Asia/Almaty',
  '-20700': 'Asia/Kathmandu',
  '-19800': 'Asia/Kolkata',
  '-18000': 'Asia/Ashkhabad',
  '-14400': 'Asia/Muscat',
  '-12600': 'Asia/Tehran',
  '-10800': 'Asia/Riyadh',
  '-7200': 'Africa/Johannesburg',
  '-3600': 'Europe/Zurich',
  0: 'Europe/London',
  7200: 'America/Sao_Paulo',
  10800: 'America/Argentina/Buenos_Aires',
  14400: 'America/Caracas',
  18000: 'America/Bogota',
  21600: 'America/Mexico_City',
  25200: 'US/Mountain',
  28800: 'America/Vancouver',
  36000: 'Pacific/Honolulu',
};
export default tradingViewTimeZone;
