import React, { useState } from 'react';
import { Input, Modal } from 'antd';
import { useBackend } from '../utils/backend';
import { notify } from '../utils/notifications';
import Link from './Link';

const InviteModal = () => {
  const { setShowInviteModal, backendRequest, showInviteModal, checkInvited } = useBackend();
  const [code, setCode] = useState("");
  const [showInfo, setShowInfo] = useState(false);

  const deepLinkToTelegram = async () => {
    const { botAccountPublicKey } = await backendRequest('/bot/public_key', 'get');
    const link = `https://t.me/grid_ex_bot?start=${botAccountPublicKey}`;
    window.open(link);
    setShowInviteModal(false);
  };

  const activateCode = async () => {
    try {
      const result = await backendRequest('/activate_invite_code', 'post', {
        code
      });
      if (result && result.success) {
        notify({
          message: "Congratulations!",
          description: "You have been invited to GRIDex Testing Program!"
        });
        setCode("");
        checkInvited();
        setShowInviteModal(false);
        setShowInfo(true)
        return;
      }
    } catch (e) {
      // do nothing
    }

    setCode("");
    notify({
      type: "error",
      message: "Sorry!",
      description: "You sent wrong code!"
    });
  };

  return (
    <>
      <Modal
        title="Important!"
        visible={showInfo}
        onCancel={() => setShowInfo(false)}
        footer={null}
      >
        <p>GRIDex currently is in alpha testing stage.</p>
        <p>Protocol code has not been audited.</p>
        <p>Use at your own risk.</p>
      </Modal>
      <Modal
        title="How to get invited"
        onOk={deepLinkToTelegram}
        visible={showInviteModal}
        okText="Go to Telegram"
        cancelText="Cancel"
        onCancel={() => setShowInviteModal(false)}
      >
        <p>If you want to be invited to GRIDex Testng Program - tap "Start" in our Telegram bot <Link to="#" onClick={deepLinkToTelegram}>@grid_ex_bot</Link></p>
        <p>We will drop you access latter.</p>
        <p>If you have invite code - enter it there:</p>
        <Input.Search
          value={code}
          onChange={(e) => setCode(e.target.value)}
          prefix="Code:"
          enterButton="Activate"
          onSearch={activateCode}
        />
      </Modal>
    </>
  );
};

export default InviteModal;