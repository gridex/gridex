import React from 'react';
import { Button } from 'antd';
import { LinkOutlined } from '@ant-design/icons';
import { getExplorerUrl } from '../utils/utils';
import { EXPLORER_ADDRESS_KEY } from '../APP_SETTINGS';

export default function LinkAddress({
  title,
  address,
}: {
  title?: undefined | any;
  address: string;
}) {
  const currentUrl = getExplorerUrl(address, EXPLORER_ADDRESS_KEY);
  return (
    <div key={currentUrl}>
      {title && <p style={{ color: 'white' }}>{title}</p>}
      <Button
        type="link"
        icon={<LinkOutlined />}
        href={currentUrl}
        target="_blank"
        rel="noopener noreferrer"
      >
        {address}
      </Button>
    </div>
  );
}
