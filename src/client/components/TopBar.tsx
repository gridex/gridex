import { SettingOutlined } from '@ant-design/icons';
import { Button, Col, Menu, Popover, Row, Select } from 'antd';
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import logo from '../assets/gridex.svg';
import styled from 'styled-components';
import { useEndpoints, useWallet, useWalletProviders } from '../utils/wallet';
import { ENDPOINTS, useConnectionConfig } from '../utils/connection';
import Settings from './Settings';
import CustomClusterEndpointDialog from './CustomClusterEndpointDialog';
import { EndpointInfo } from '../utils/types';
import { notify } from '../utils/notifications';
import { Connection } from '@solana/web3.js';
import WalletConnect from './WalletConnect';
import { getGridBotPageUrl, getTradePageUrl } from '../utils/markets';
import { useBackend } from '../utils/backend';
import InviteModal from './InviteModal';
import BalancesBlock from './BalancesBlock';
import { EXPLORER_ADDRESS_KEY } from '../APP_SETTINGS';
import { getExplorerUrl } from '../utils/utils';

const Wrapper = styled.div`
  background-color: #1f1f1f;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  padding: 0px 30px;
  flex-wrap: wrap;
`;
const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
  color: #0080ff;
  font-weight: bold;
  cursor: pointer;
  img {
    height: 30px;
    margin-right: 8px;
  }
`;

const EXTERNAL_LINKS = {
  '/learn': 'https://serum-academy.com/en/serum-dex/',
  '/add-market': 'https://serum-academy.com/en/add-market/',
  '/wallet-support': 'https://serum-academy.com/en/wallet-support',
  '/dex-list': 'https://serum-academy.com/en/dex-list/',
  '/developer-resources': 'https://serum-academy.com/en/developer-resources/',
  '/explorer': getExplorerUrl('', EXPLORER_ADDRESS_KEY),
  '/srm-faq': 'https://projectserum.com/srm-faq',
  '/swap': 'https://swap.projectserum.com',
};

export default function TopBar() {
  const { WALLET_PROVIDERS } = useWalletProviders();
  const { connected, providerUrl, setProviderUrl } = useWallet();
  const {
    endpoint,
    endpointInfo,
    setEndpoint,
    availableEndpoints,
    setCustomEndpoints,
  } = useConnectionConfig();
  const { backEndConnected, invited, setShowInviteModal } = useBackend();
  const [addEndpointVisible, setAddEndpointVisible] = useState(false);
  const [testingConnection, setTestingConnection] = useState(false);
  const location = useLocation();
  const history = useHistory();

  const endpoints = useEndpoints(providerUrl);

  const handleClick = useCallback(
    (e) => {
      if (!(e.key in EXTERNAL_LINKS)) {
        history.push(e.key);
      }
    },
    [history],
  );

  const onAddCustomEndpoint = (info: EndpointInfo) => {
    const existingEndpoint = availableEndpoints.some((e) => e.endpoint === info.endpoint);
    if (existingEndpoint) {
      notify({
        message: `An endpoint with the given url already exists`,
        type: 'error',
      });
      return;
    }

    const handleError = (e) => {
      console.log(`Connection to ${info.endpoint} failed: ${e}`);
      notify({
        message: `Failed to connect to ${info.endpoint}`,
        type: 'error',
      });
    };

    try {
      const connection = new Connection(endpoint);
      connection
        .getEpochInfo()
        .then(() => {
          setTestingConnection(true);
          console.log(`testing connection to ${info.endpoint}`);
          const newCustomEndpoints = [...availableEndpoints.filter((e) => e.custom), info];
          setEndpoint(info.endpoint);
          setCustomEndpoints(newCustomEndpoints);
        })
        .catch(handleError);
    } catch (e) {
      handleError(e);
    } finally {
      setTestingConnection(false);
    }
  };

  const endpointInfoCustom = endpointInfo && endpointInfo.custom;
  useEffect(() => {
    const handler = () => {
      if (endpointInfoCustom) {
        setEndpoint(ENDPOINTS[0].endpoint);
      }
    };
    window.addEventListener('beforeunload', handler);
    return () => window.removeEventListener('beforeunload', handler);
  }, [endpointInfoCustom, setEndpoint]);

  const tradePageUrl = location.pathname.startsWith('/market/')
    ? location.pathname
    : getTradePageUrl();

  const gridBotPageUrl = location.pathname.startsWith('/grid-bot/')
    ? location.pathname
    : getGridBotPageUrl();

  return (
    <>
      <InviteModal />
      <CustomClusterEndpointDialog
        visible={addEndpointVisible}
        testingConnection={testingConnection}
        onAddCustomEndpoint={onAddCustomEndpoint}
        onClose={() => setAddEndpointVisible(false)}
      />
      <Wrapper>
        <LogoWrapper onClick={() => window.location.replace('/')}>
          <img src={logo} alt="" />
          {'Gridex'}
        </LogoWrapper>
        <Menu
          mode="horizontal"
          onClick={handleClick}
          selectedKeys={[location.pathname]}
          style={{
            borderBottom: 'none',
            backgroundColor: 'transparent',
            display: 'flex',
            alignItems: 'flex-end',
            flex: 1,
          }}
        >
          <Menu.Item key={gridBotPageUrl} style={{ margin: '0 10px' }}>
            GRID BOT
          </Menu.Item>
          {
            <Menu.SubMenu title="DEX" style={{ margin: '0 10px' }}>
              {connected && (
                <Menu.SubMenu title="Serum">
                  <Menu.Item key={tradePageUrl}>TRADE</Menu.Item>
                  <Menu.Item key="/balances">BALANCES</Menu.Item>
                  <Menu.Item key="/orders">ORDERS</Menu.Item>
                  <Menu.Item key="/convert">CONVERT</Menu.Item>
                </Menu.SubMenu>
              )}
              <Menu.Item key="/swap">
                <a href={EXTERNAL_LINKS['/swap']} target="_blank" rel="noopener noreferrer">
                  SWAP
                </a>
              </Menu.Item>
            </Menu.SubMenu>
          }
          <Menu.SubMenu
            title="LEARN"
            onTitleClick={() => window.open(EXTERNAL_LINKS['/learn'], '_blank')}
            style={{ margin: '0 0px 0 10px' }}
          >
            <Menu.Item key="/add-market">
              <a href={EXTERNAL_LINKS['/add-market']} target="_blank" rel="noopener noreferrer">
                Adding a market
              </a>
            </Menu.Item>
            <Menu.Item key="/wallet-support">
              <a href={EXTERNAL_LINKS['/wallet-support']} target="_blank" rel="noopener noreferrer">
                Supported wallets
              </a>
            </Menu.Item>
            <Menu.Item key="/dex-list">
              <a href={EXTERNAL_LINKS['/dex-list']} target="_blank" rel="noopener noreferrer">
                DEX list
              </a>
            </Menu.Item>
            <Menu.Item key="/developer-resources">
              <a
                href={EXTERNAL_LINKS['/developer-resources']}
                target="_blank"
                rel="noopener noreferrer"
              >
                Developer resources
              </a>
            </Menu.Item>
            <Menu.Item key="/explorer">
              <a href={EXTERNAL_LINKS['/explorer']} target="_blank" rel="noopener noreferrer">
                Solana block explorer
              </a>
            </Menu.Item>
            <Menu.Item key="/srm-faq">
              <a href={EXTERNAL_LINKS['/srm-faq']} target="_blank" rel="noopener noreferrer">
                SRM FAQ
              </a>
            </Menu.Item>
          </Menu.SubMenu>
        </Menu>
        <BalancesBlock />
        {connected && (
          <div style={{ marginRight: 8 }}>
            <Popover
              content={<Settings setAddEndpointVisible={setAddEndpointVisible} />}
              placement="bottomRight"
              title="Settings"
              trigger="click"
            >
              <Button>
                <SettingOutlined />
                Settings
              </Button>
            </Popover>
          </div>
        )}
        {backEndConnected && !invited && (
          <div style={{ marginRight: 8 }}>
            <Button onClick={() => setShowInviteModal(true)}>Get invite</Button>
          </div>
        )}
        {!connected && (
          <div style={{ marginRight: 8 }}>
            <Row align="middle" gutter={16}>
              <Col>
                <Select onSelect={setEndpoint} value={endpoint} style={{ width: '150px' }}>
                  {endpoints.map(({ name, endpoint: _endpoint }) => (
                    <Select.Option value={_endpoint} key={_endpoint}>
                      {name}
                    </Select.Option>
                  ))}
                </Select>
              </Col>
            </Row>
          </div>
        )}
        {!connected && (
          <div style={{ marginRight: 8 }}>
            <Select onSelect={setProviderUrl} value={providerUrl}>
              {WALLET_PROVIDERS.map(({ name, url }) => (
                <Select.Option value={url} key={url}>
                  {name}
                </Select.Option>
              ))}
            </Select>
          </div>
        )}
        <div>
          <WalletConnect />
        </div>
      </Wrapper>
    </>
  );
}
