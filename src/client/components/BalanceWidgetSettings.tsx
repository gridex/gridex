import React from 'react';
import styled from 'styled-components';
import { Select, Typography, Space } from 'antd';
import { useLocalStorageStringState } from '../utils/utils';
import { useWalletBalancesForAllMarkets } from '../utils/markets';
import { useMintToTickers } from '../utils/tokens';

const Container = styled.div`
  margin-top: 25px;
`;

const StyledSelect = styled(Select)`
  min-width: 150px;
`;

const options = [
  { value: 'biggestStable', name: 'Stable coin with biggest amount' },
  { value: 'specificCoin', name: 'Specific coin' },
  { value: 'none', name: 'Only SOL Balance' },
];

const BalanceWidgetSettings = () => {
  const [currentDisplayOption, setCurrentDisplayOption] = useLocalStorageStringState(
    'displayCoin',
    options[0].value,
  );
  const [coinToDisplay, setCoinToDisplay] = useLocalStorageStringState(
    'cointToDisplayInWalletWidget',
    '',
  );
  const walletBalances = useWalletBalancesForAllMarkets();
  const mintToTickers = useMintToTickers();

  const humanWalletBalancesAll = React.useMemo(() => {
    return walletBalances.map((balance) => {
      return {
        coin: mintToTickers[balance.mint],
        walletBalance: balance.balance,
        mint: balance.mint,
      };
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [walletBalances]);

  const coins = React.useMemo(
    () =>
      humanWalletBalancesAll.filter(
        (walletEntry) => walletEntry.coin && walletEntry.coin !== 'SOL',
      ),
    [humanWalletBalancesAll],
  );

  return (
    <Container>
      <h3>Balance Widget Settings</h3>
      <Space direction="vertical">
        <Typography.Text>Select what balances should wallet widget show</Typography.Text>
        <Space direction="horizontal">
          <StyledSelect
            value={currentDisplayOption || options[0].value}
            onChange={setCurrentDisplayOption}
          >
            {options.map((option) => (
              <Select.Option name={option.name} value={option.value} key={option.value}>
                {option.name}
              </Select.Option>
            ))}
          </StyledSelect>
          {currentDisplayOption === 'specificCoin' && (
            <StyledSelect
              placeholder="Select coin"
              name={coinToDisplay}
              value={coinToDisplay}
              key={coinToDisplay}
              onChange={setCoinToDisplay}
            >
              {coins.map(({ coin }) => (
                <Select.Option name={coin} value={coin} key={coin}>
                  {coin}
                </Select.Option>
              ))}
            </StyledSelect>
          )}
        </Space>
      </Space>
    </Container>
  );
};

export default BalanceWidgetSettings;
