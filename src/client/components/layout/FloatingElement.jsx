import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 4px;
  padding: 16px;
  background-color: #1a2029;
`;

const Wrapper2 = styled.div`
  width: 100%;
  background-color: #212834;
  padding: 20px;
  & + & {
    margin-top: 8px;
  }
`;

export const ComponentsWrapper = ({ children }) => (
  <Wrapper2>{children}</Wrapper2>
);

export default function FloatingElement({
  style = undefined,
  children,
  stretchVertical = false,
}) {
  return (
    <Wrapper
      style={{
        height: stretchVertical ? 'calc(100% - 10px)' : undefined,
        ...style,
      }}
    >
      {children}
    </Wrapper>
  );
}
