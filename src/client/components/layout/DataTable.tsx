import React from 'react';
import { ConfigProvider, Table } from 'antd';

export default function DataTable({
  dataSource,
  columns,
  emptyLabel = 'No data',
  pagination = false,
  loading = false,
  pageSize = 10,
  rowKey = 'coin',
  ...props
}: {
  [name: string]: any;
  rowKey?: string | ((any) => string);
}) {
  const customizeRenderEmpty = () => (
    <div
      style={{
        padding: '20px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      {emptyLabel}
    </div>
  );

  const paginationObject = pagination && typeof pagination === 'object' ? pagination : {};

  return (
    <ConfigProvider renderEmpty={customizeRenderEmpty}>
      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={pagination ? { pageSize, ...paginationObject } : false}
        loading={loading}
        rowKey={rowKey}
        {...props}
      />
    </ConfigProvider>
  );
}
