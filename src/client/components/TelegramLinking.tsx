import React, { useEffect, useState } from 'react';
import { useBackend } from '../utils/backend';
import { Button, Spin, Space } from 'antd';
import Text from 'antd/es/typography/Text';
import { useWallet } from '../utils/wallet';

interface TgConnectData {
  joinLink: string;
  botCode: string;
  expiresIn: Date;
}

let interval;

const TelegramLinking = () => {
  const { backEndConnected, backendRequest } = useBackend();
  const [tgConnectData, setTgConnectData] = useState({
    joinLink: '',
  } as TgConnectData);
  const [isNewLinking, setIsNewLinking] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [isExpired, setIsExpired] = useState(false);
  const { connected } = useWallet();

  const checkConnection = (rewrite = true) => {
    setIsLoading(rewrite);
    backendRequest('/telegram-connection-status', 'POST', { rewrite })
      .then((data) => {
        if (!data) {
          setIsLoading(false);
          setIsNewLinking(false);
          if (interval) {
            clearInterval(interval);
          }
        } else if (data.wait) {
          // do nothing
        } else if (data && data.joinLink) {
          setIsNewLinking(true);
          setTgConnectData(data);
          const estimatedTime = data.expiresIn - Date.now();
          setIsLoading(false);
          setIsExpired(false);
          setTimeout(() => {
            setIsExpired(true);
          }, estimatedTime);
          if (interval) {
            clearInterval(interval);
          }
          interval = setInterval(() => {
            checkConnection(false);
          }, 2000);
        } else {
          setIsExpired(true);
        }
      })
      .catch((err) => {
        console.error(err);
        setIsLoading(false);
      });
  };
  const { joinLink, botCode } = tgConnectData;

  const unlinkTelegram = () => {
    setIsLoading(true);
    backendRequest('/telegram-unlink', 'GET')
      .then(() => {
        setIsNewLinking(true);
        setIsLoading(false);
        checkConnection();
      })
      .catch((err) => {
        setIsLoading(false);
        console.error(err);
      });
  };

  useEffect(() => {
    if (backEndConnected && connected) {
      checkConnection();
    }
    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
    // eslint-disable-next-line
  }, [backEndConnected, connected]);

  const NewLinking = () =>
    !isExpired ? (
      <Space direction="vertical">
        <Text>
          If you already have not started our telegram bot @gridex_bot just start our bot by this{' '}
          <a href={joinLink} target="_blank" rel="noopener noreferrer">
            link
          </a>
        </Text>
        <Text>
          Otherwise send bot code: "<b>{botCode}</b>"
        </Text>
        <Button loading={isLoading} type="primary" onClick={() => checkConnection()}>
          Check connection
        </Button>
      </Space>
    ) : (
      <Space direction="vertical">
        <Text>Your telegram bot connection expired. You can refresh it!</Text>
        <Button loading={isLoading} type="primary" onClick={() => checkConnection()}>
          Refresh
        </Button>
      </Space>
    );

  const AlreadyLinked = () => (
    <Space direction="vertical">
      <Text>Telegram bot already connected! You can disconnect or reconnect it.</Text>
      <Button loading={isLoading} type="ghost" onClick={() => unlinkTelegram()}>
        Disconnect telegram
      </Button>
    </Space>
  );

  if (isLoading) {
    return <Spin />;
  }

  return (
    <>
      <h3>Telegram bot connection</h3>
      {isNewLinking ? <NewLinking /> : <AlreadyLinked />}
    </>
  );
};

export default TelegramLinking;
