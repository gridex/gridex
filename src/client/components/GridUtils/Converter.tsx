import { Connection, PublicKey } from '@solana/web3.js';
import { AccountSignAndSendTransaction, RELOAD_EVENT } from './utils';
import { Col, H3WithInfo } from './Components';
import React, { useEffect, useState, KeyboardEvent } from 'react';
import { TokenAccount } from '../../utils/types';
import { getTokenAccountInfo } from '../../../processor/utils/balance/getTokenAccountInfo';
import getAllBalances, { Balances } from '../../../processor/utils/balance/getAllBalances';
import { Row, Col as ACol, Select, Input, Button, Tag } from 'antd';
import { getAllMarketsData, MarketData } from '../../../processor/utils/markets/getAllMarketsData';
import { loadMarketPrice } from '../../utils/markets-calc';
import { notify } from '../../utils/notifications';
import setBlockHash from '../../../processor/utils/transactions/setBlockHash';
import { filterOption } from '../../utils/filterOption';
import { getValidCurrency } from './utils';
import { useHistory } from "react-router-dom";

const fromQuoteToBase = (quote, marketPrice) => quote * marketPrice;

export const Converter = ({ accountPublicKey, connection, accountType, signAndSendTransaction }: {
  accountPublicKey: PublicKey,
  connection: Connection,
  signAndSendTransaction: AccountSignAndSendTransaction,
  accountType: string,
}) => {
  const history = useHistory();
  const [market, setMarket] = React.useState<MarketData | null>(null);
  const [side, setSide] = React.useState<"buy" | "sell">("buy");
  const [value, setValue] = React.useState("");
  const [max, setMax] = React.useState(0);
  const [loading, setLoading] = useState(false);
  const [tokenAccounts, setTokenAccounts] = React.useState<TokenAccount[]>([]);
  const [toTokenAccounts, setToTokenAccounts] = React.useState<TokenAccount[]>([]);
  const [from, setFrom] = React.useState("");
  const [to, setTo] = React.useState("");
  const [balances, setBalances] = React.useState<{[key: string]: Balances}>({});
  const [markets, setMarkets] = React.useState<MarketData[]>([]);
  const [coin, setCoin] = React.useState<string>("");
  const [quoteCoin, setQuoteCoin] = React.useState<string>("");
  const [marketPrice, setMarketPrice] = React.useState<number>(0);
  const [withValidation, setWithValidation] = React.useState<boolean>(true);

  const loadTokens = async () => {
    setLoading(true);

    const _tokenAccounts = await getTokenAccountInfo(connection, accountPublicKey);
    setTokenAccounts(_tokenAccounts);
    const _balances = await getAllBalances(connection, accountPublicKey, true, _tokenAccounts);
    setBalances(_balances.reduce((acc, balance) => {
      acc[balance.mint] = balance;
      return acc;
    }, {}));
    const markets = await getAllMarketsData(connection);
    setMarkets(markets.filter(({ deprecated }) => !deprecated));
    setLoading(false);
  }

  useEffect(() => {
    if (from) {
      setTo("");
      setValue("0");
      const fromPubKey = new PublicKey(from);
      const token = tokenAccounts.find(({ pubkey }) => pubkey.equals(fromPubKey));
      if (token) {
        const _markets = markets.filter((market) =>
          market.market.quoteMintAddress.equals(token.effectiveMint) || market.market.baseMintAddress.equals(token.effectiveMint)
        );
        const marketsTokensPublicKeys = [...new Set(
          _markets.reduce((acc: string[], { market: { baseMintAddress, quoteMintAddress }}) => {
            if (!baseMintAddress.equals(token.effectiveMint)) {
              acc.push(baseMintAddress.toBase58());
            }
            if (!quoteMintAddress.equals(token.effectiveMint)) {
              acc.push(quoteMintAddress.toBase58());
            }
            return acc;
          }, [])
        )];
        const _tokenAccounts = marketsTokensPublicKeys
          .map((token) => tokenAccounts.find(t => t.effectiveMint.toBase58() === token))
          .filter(Boolean)
        ;
        // @ts-ignore
        setToTokenAccounts(_tokenAccounts)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [from, tokenAccounts]);

  const loadMarkerData = async () => {
    const fromPubKey = new PublicKey(from);
    const toPubKey = new PublicKey(to);
    const fromToken = tokenAccounts.find(({ pubkey }) => pubkey.equals(fromPubKey));
    const toToken = tokenAccounts.find(({ pubkey }) => pubkey.equals(toPubKey));
    if (fromToken && toToken) {
      const market = markets.find((market) =>
        (
          market.market.quoteMintAddress.equals(fromToken.effectiveMint) && market.market.baseMintAddress.equals(toToken.effectiveMint)
        ) || (
          market.market.quoteMintAddress.equals(toToken.effectiveMint) && market.market.baseMintAddress.equals(fromToken.effectiveMint)
        )
      );
      if (market) {
        setMarket(market);
        const _side = market.market.baseMintAddress.equals(fromToken.effectiveMint) ? 'sell' : 'buy';
        const token = _side === "sell" ? fromToken : toToken;
        const quoteToken = _side === "sell" ? toToken : fromToken;
        const _coin = balances[token.effectiveMint.toBase58()]?.coin;
        const _quoteCoin = balances[quoteToken.effectiveMint.toBase58()]?.coin;
        setCoin(_coin);
        setQuoteCoin(_quoteCoin);
        const balance = balances[fromToken.effectiveMint.toBase58()];
        setSide(_side);
        const price = await loadMarketPrice(connection, market.market);
        setMax(_side === "sell" ? balance.walletBalance : balance.walletBalance / (price || 1));
        setMarketPrice(price || 0);
      }
    }
  }

  React.useEffect(() => {
    if (to) {
      setValue("0");
      loadMarkerData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [to]);


  const valueChange = (rawValue: string) => {
    setValue(!withValidation ? rawValue : getValidCurrency(rawValue, max) || value)
  }

  const onKeyDownValueInputHandler = ({ key }: KeyboardEvent<HTMLInputElement>) => {
    setWithValidation(key !== "Backspace" && key !== "Delete");
  }

  const convert = async () => {
    if (market) {
      try {
        setLoading(true);
        const price = await loadMarketPrice(connection, market.market);
        if (price) {
          const toFixedValue = Math.abs(Math.log10(market.market.minOrderSize));

          const { transaction, signers } = await market.createPlaceOrderTransaction(
            accountPublicKey,
            {
              size: +((+value).toFixed(toFixedValue)),
              side,
              orderType: 'ioc',
              price: 0
            }
          );
          await setBlockHash(connection, transaction);
          const tx = await signAndSendTransaction(transaction, signers);
          console.log(tx);
          setFrom("");
          setValue("");
          setTo("");
          history.push(`/grid-bot/${accountType === "Bot account" ? "bot" : "wallet"}/balances`);
          window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
        } else {
          notify({
            message: "Can not load price"
          });
          setLoading(false);
        }
      } catch (e) {
        notify(e);
        setLoading(false);
      }
    }
  }

  useEffect(() => {
    loadTokens();
    window.addEventListener(RELOAD_EVENT, loadTokens);
    return () => window.removeEventListener(RELOAD_EVENT, loadTokens);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <Col span={24}>
    <H3WithInfo>Converter</H3WithInfo>
    <Row gutter={8}>
      <ACol span={6}>
        <label>From</label>
        <Select
          style={{ width: '100%' }}
          onChange={setFrom}
          value={from}
          showSearch
          filterOption={filterOption}
          placeholder={"Select token"}
          disabled={loading}
        >
          { tokenAccounts.filter(token => (balances[token.effectiveMint.toBase58()]?.walletBalance || 0) > 0).map(token =>
            <Select.Option
              name={balances[token.effectiveMint.toBase58()]?.coin}
              key={token.pubkey.toBase58()}
              value={token.pubkey.toBase58()}
            >
              { balances[token.effectiveMint.toBase58()]?.walletBalance } <Tag>{balances[token.effectiveMint.toBase58()]?.coin}</Tag>{token.pubkey.toBase58()}
            </Select.Option>
          )}
        </Select>
      </ACol>
      {
        from && <ACol span={6}>
          <label>To</label>
          <Select
            style={{ width: '100%' }}
            onChange={setTo}
            value={to}
            showSearch
            filterOption={filterOption}
            placeholder={"Select token"}
            disabled={loading}
          >
            { toTokenAccounts.map(token =>
              <Select.Option
                name={balances[token.effectiveMint.toBase58()]?.coin}
                key={token.pubkey.toBase58()}
                value={token.pubkey.toBase58()}
                filterOption={filterOption}
              >
                { balances[token.effectiveMint.toBase58()]?.walletBalance || 0 } <Tag>{balances[token.effectiveMint.toBase58()]?.coin}</Tag>{token.pubkey.toBase58()}
              </Select.Option>
            )}
          </Select>
        </ACol>
      }
      {
        from && to && <ACol span={6}>
          <label>Value</label>
          <Input
            value={value}
            onKeyDown={onKeyDownValueInputHandler}
            onChange={e => valueChange(e.target.value)}
            style={{ width: '100%' }}
            suffix={coin}
            disabled={loading}
          />
        </ACol>
      }
      {
        from && to && value && <ACol span={6}>
          <label>≈{fromQuoteToBase(value, marketPrice)} {quoteCoin}</label>
          <Button
            style={{ width: '100%' }}
            disabled={+value === 0 || loading}
            onClick={convert}
            loading={loading}
          >{side.toUpperCase()} {coin}</Button>
        </ACol>
      }
    </Row>
  </Col>
}
