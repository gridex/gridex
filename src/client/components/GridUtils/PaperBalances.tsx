import { Connection, PublicKey } from '@solana/web3.js';
import { RELOAD_EVENT } from './utils';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Col, H3WithInfo } from './Components';
import { Button, Checkbox, Form, Input, Modal, Table } from 'antd';
import { NumberInput, Selector } from '../form/Fields';
import TOKEN_MINTS from '../../../common/utils/tokenMints';
import useApi from '../../utils/useApi';
import { error, notify } from '../../utils/notifications';
import { SortOrder } from 'antd/es/table/interface';
import useInputValue from '../../utils/useInputValue';

const TOKENS = TOKEN_MINTS.slice().sort((a, b) => a.name.localeCompare(b.name));

type SetMint = (mint: string) => void;
const getTransferColumns = (initTransfer: SetMint, initExchange: SetMint, filter) => ([
    {
      title: <span>Coin <span style={{marginLeft: 8}}>{filter}</span></span>,
      dataIndex: 'name',
      width: '20%',
      sorter: (a, b) => a.name.localeCompare(b.name),
      sortDirections: ['ascend', 'descend'] as SortOrder[],
    },
    {
      title: 'Balance',
      dataIndex: 'amount',
      width: '20%',
      render: (amount) => <span style={{ fontFamily: 'monospace' }}>{amount.toFixed(6)}</span>,
      sorter: (a, b) => a.amount - b.amount,
      sortDirections: ['ascend', 'descend'] as SortOrder[],
    },
    {
      title: 'Action',
      width: '20%',
      render: (record) => (<>
        <Button onClick={() => initTransfer(record)}>TopUp</Button>
        <Button onClick={() => initExchange(record)}>Exchange</Button>
      </>),
    },
  ]
);

export type Balance = {
  name: string,
  mint: string,
  amount: number,
};

export default function({ accountPublicKey, connection }: {
  accountPublicKey: PublicKey,
  connection: Connection,
}) {
  const [transferQuantity, setTransferQuantity] = useState(0);
  // const [filterBalances, setFilterBalances] = useState<boolean>(true);
  const [loading, setLoading] = useState(false);
  const [toBalances, setToBalances] = useState<Balance[]>([]);
  // const [toTokenAccounts, setToTokenAccounts] = useState<TokenAccount[]>([]);
  const [transferBalance, setTransferBalance] = useState<Balance | null>(null);
  const [exchangeBalance, setExchangeBalance] = useState<Balance | null>(null);
  const [tokenToAdd, setTokenToAdd] = useState<typeof TOKENS[0] | null>(null);
  const [filterValue, changeFilter] = useInputValue<string>('');
  const [replaceTokenAmount, setReplceTokenAmount] = useInputValue<boolean>(false);

  const api = useApi();

  const loadBalances = useCallback(async () => {
    setLoading(true);

    const balances = await api.paperBot.getBalances();
    setToBalances(balances);

    setLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountPublicKey, connection]);

  const initTransfer = (coinBalance) => {
    setTransferQuantity(0);
    setTransferBalance(coinBalance);
  };

  const initExchange = (coinBalance) => {
    setTransferQuantity(0);
    setExchangeBalance(coinBalance);
  };

  const filter = useMemo(() =>
      <Input
        placeholder="filter..."
        style={{ width: 80 }}
        onChange={changeFilter}
        onClick={evt => evt.stopPropagation()}
      />,
    [changeFilter]);

  const transferColumns = getTransferColumns(initTransfer, initExchange, filter);

  const filteredBalances = useMemo(() => {
      if (!filterValue) return toBalances;

      const filter = filterValue.toUpperCase();
      return toBalances.filter(b => b.name.includes(filter));
    },
    [toBalances, filterValue]);
  // const toTokenAccount = transferMint ? toTokenAccounts.find(({ effectiveMint }) => effectiveMint.toBase58() === transferMint) : null;

  const transfer = async () => {
    const amount = (replaceTokenAmount ? -transferBalance!.amount : 0) + transferQuantity;
    await api.paperBot.changeBalance(transferBalance!.mint, amount);
    window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
    setTransferBalance(null);
  };

  const exchange = async () => {
    const fromMint = exchangeBalance!.mint;
    const fromAmount = transferQuantity;
    const toMint = tokenToAdd!.address;

    const res = await api.paperBot.exchange(fromMint, fromAmount, toMint.toBase58());
    if (!res.success) {
      return error('Paper Bot exchange', res.error);
    }
    window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
    setExchangeBalance(null);
  };

  const addToken = async () => {
    const { address, name } = tokenToAdd!;
    await api.paperBot.changeBalance(address.toBase58(), 0, name);
    window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
    notify({ message: 'Paper bot', description: `token ${name} added` });
  };


  useEffect(() => {
    loadBalances();
    window.addEventListener(RELOAD_EVENT, loadBalances);
    return () => window.removeEventListener(RELOAD_EVENT, loadBalances);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const reload = () => {
    window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
  };

  const tokensToAdd = useMemo(
    () => toBalances ? TOKENS.filter(t => !toBalances.find(b => b.mint === t.address.toBase58())) : TOKENS,
    [toBalances],
  );
  const tokenOptions = tokensToAdd.map((t, i) => ({ value: i, label: t.name }));

  return <Col span={24}>
    <Modal
      visible={Boolean(transferBalance)}
      title={`Top Up paper wallet with ${transferBalance?.name}`}
      okText="Top Up"
      onCancel={() => setTransferBalance(null)}
      onOk={transfer}
    >
      {<>
        <p>Token: {transferBalance?.name}</p>
        <NumberInput name="quantity" value={transferQuantity} onChange={val => setTransferQuantity(val)} />
        <Checkbox checked={replaceTokenAmount} onChange={setReplceTokenAmount} >Replace amount</Checkbox>
      </>}
    </Modal>
    <Modal
      visible={Boolean(exchangeBalance)}
      title={`Exchange ${exchangeBalance?.name}`}
      okText="Exchange"
      onCancel={() => setExchangeBalance(null)}
      onOk={exchange}
    >
      <p>Exchange: {transferBalance?.name}</p>
      <Form.Item label="Quantity">
        <NumberInput name="quantity" value={transferQuantity} onChange={val => setTransferQuantity(val)} />
      </Form.Item>
      <Form.Item label="Exchange for">
        <Selector
          search
          options={TOKENS.map((t, i) => ({ value: i, label: t.name }))}
          onChange={idx => setTokenToAdd(TOKENS[+idx])} style={{ width: 160 }}
        />
      </Form.Item>
    </Modal>
    <H3WithInfo>Transfer to paper account
      <span>
        <div style={{ marginRight: 16 }}>
          <Selector
            options={tokenOptions}
            defaultActiveFirstOption
            search
            onChange={idx => {
              setTokenToAdd(tokensToAdd[+idx])
            }}
            style={{ width: 160 }}
          />
          <Button disabled={loading} loading={loading} onClick={addToken}>Add token</Button>
        </div>
        {/*<div>Hide empty rows <Switch checked={filterBalances} onChange={setFilterBalances} size="small" style={{ marginRight: 8}} /></div>*/}
        <Button disabled={loading} loading={loading} onClick={reload}>Reload</Button>
      </span>
    </H3WithInfo>
    <Table
      // @ts-ignore
      dataSource={filteredBalances}
      columns={transferColumns}
      pagination={false}
      loading={loading}
      size="small"
      rowKey="name"
    />
  </Col>;
}
