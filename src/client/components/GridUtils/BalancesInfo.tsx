import getAllBalances, { Balances } from '../../../processor/utils/balance/getAllBalances';
import { Col, H3WithInfo, Row } from './Components';
import { abbreviateAddress, getExplorerUrl } from '../../utils/utils';
import { EXPLORER_ADDRESS_KEY } from '../../APP_SETTINGS';
import { Connection, PublicKey } from '@solana/web3.js';
import React, { useCallback, useEffect } from 'react';
import { Switch, Table, Button } from 'antd';
import StandaloneTokenAccountsSelect from '../StandaloneTokenAccountSelect';
import { getTokenAccountInfo } from '../../../processor/utils/balance/getTokenAccountInfo';
import { TokenAccount } from '../../utils/types';
import { createTokenAccount } from '../../../processor/utils/tokens/createTokenAccount';
import { AccountSignAndSendTransaction, RELOAD_EVENT } from './utils';
import { notify } from '../../utils/notifications';
import settleByMint from '../../../processor/utils/balance/settle';
import setBlockHash from '../../../processor/utils/transactions/setBlockHash';
import sortBy from 'lodash/sortBy';
import TOKEN_MINTS from '../../../common/utils/tokenMints';

const getMintsTableColumns = (addMint: (address: string) => void, loadings) => [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    width: '15%',
  },
  {
    title: 'Public Key',
    dataIndex: 'address',
    key: 'address',
    width: '15%',
  },
  {
    title: 'Actions',
    key: 'actions',
    width: '65%',
    render: (item) => (
      <Button
        loading={loadings === item.address}
        disabled={loadings !== ''}
        onClick={() => addMint(item.address)}
      >
        Add mint
      </Button>
    ),
  },
];

const getBalancesTableColumns = (
  tokenAccounts: TokenAccount[],
  settle: (mint: string) => Promise<void>,
  accountPublicKeyString: string,
) => [
  {
    title: 'Coin',
    key: 'coin',
    width: '10%',
    render: (walletBalance: Balances) => (
      <Row align="middle">
        <a
          href={getExplorerUrl(walletBalance.mint, EXPLORER_ADDRESS_KEY)}
          target={'_blank'}
          rel="noopener noreferrer"
          style={{ whiteSpace: 'pre' }}
        >
          {walletBalance.coin || abbreviateAddress(new PublicKey(walletBalance.mint))}
        </a>
      </Row>
    ),
  },
  {
    title: 'Wallet Balance',
    dataIndex: 'walletBalance',
    key: 'walletBalance',
    width: '15%',
  },
  {
    title: 'Open orders total balances',
    dataIndex: 'openOrdersTotal',
    key: 'openOrdersTotal',
    width: '15%',
  },
  {
    title: 'Unsettled balances',
    dataIndex: 'openOrdersFree',
    key: 'openOrdersFree',
    width: '15%',
  },
  {
    title: 'Settle',
    key: 'settle',
    width: '10%',
    render: (walletBalance) => (
      <Button
        disabled={walletBalance.openOrdersFree === 0}
        onClick={() => settle(walletBalance.mint)}
      >
        Settle
      </Button>
    ),
  },
  {
    title: 'Selected token account',
    key: 'selectTokenAccount',
    width: '20%',
    render: (walletBalance) => (
      <Row align="middle" style={{ width: '430px' }}>
        <StandaloneTokenAccountsSelect
          key={`${walletBalance.mint}`}
          accounts={tokenAccounts?.filter((t) => t.effectiveMint.toBase58() === walletBalance.mint)}
          mint={walletBalance.mint}
        />
      </Row>
    ),
  },
];

const balancesLoading: {
  [key: string]: boolean;
} = {};

export const BalancesInfo = ({
  accountPublicKey,
  connection,
  reloadData,
  signAndSendTransaction,
}: {
  accountPublicKey: PublicKey;
  connection: Connection;
  reloadData: boolean;
  signAndSendTransaction: AccountSignAndSendTransaction;
}) => {
  const [loading, setLoading] = React.useState(false);
  const [balances, setBalances] = React.useState<Balances[]>([]);
  const [filterBalances, setFilterBalances] = React.useState<boolean>(false);
  const [tokenAccounts, setTokenAccounts] = React.useState<TokenAccount[]>([]);
  const [unusedMints, setUnusedMints] = React.useState<
    {
      address: string;
      name: string;
    }[]
  >([]);
  const [unusedMintsLoading, setUnusedMintsLoading] = React.useState(false);
  const [unusedMintsLoadings, setUnusedMintsLoadings] = React.useState<string>('');
  const [unusedMintsShow, setUnusedMintsShow] = React.useState(false);

  const loadBalances = useCallback(async () => {
    if (!balancesLoading[accountPublicKey.toBase58()]) {
      balancesLoading[accountPublicKey.toBase58()] = true;
      setUnusedMintsLoading(true);
      setLoading(true);
      const _tokenAccounts = await getTokenAccountInfo(connection, accountPublicKey);
      setTokenAccounts(_tokenAccounts);
      const balances = await getAllBalances(connection, accountPublicKey, true, _tokenAccounts);
      setBalances(balances);
      setLoading(false);
      balancesLoading[accountPublicKey.toBase58()] = false;
      setUnusedMintsLoading(false);
    }
  }, [accountPublicKey, connection]);

  React.useEffect(() => {
    const _unusedTokens = TOKEN_MINTS.filter(
      ({ address, name }) => !balances.find(({ mint }) => address.toBase58() === mint),
    ).map(({ address, name }) => ({ address: address.toBase58(), name }));
    setUnusedMints(sortBy(_unusedTokens, ['name']));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [balances.map(({ coin }) => coin).join('')]);

  React.useEffect(() => {
    loadBalances();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadBalances]);

  React.useEffect(() => {
    if (reloadData) {
      loadBalances();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reloadData, loadBalances]);

  const filteredBalances = sortBy(
    filterBalances
      ? balances.filter(
          ({ walletBalance, openOrdersFree, openOrdersTotal }) =>
            walletBalance > 0 || openOrdersFree > 0 || openOrdersTotal > 0,
        )
      : balances,
    ({ walletBalance = 0 }) => -walletBalance,
  );

  const addMint = async (address: string) => {
    setUnusedMintsLoadings(address);
    try {
      const { transaction, signers } = await createTokenAccount(
        connection,
        accountPublicKey,
        new PublicKey(address),
      );
      await signAndSendTransaction(transaction, signers);
      window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
    } catch (e) {
      notify(e);
    }
    setUnusedMintsLoadings('');
  };

  const settle = async (mint: string) => {
    setLoading(true);
    try {
      const transactions = await settleByMint(connection, accountPublicKey, new PublicKey(mint));
      Promise.all(
        transactions.map(async (tx) => {
          const { transaction, signers } = tx;
          await setBlockHash(connection, transaction);
          await signAndSendTransaction(transaction, signers);
        }),
      ).finally((...data) => {
        setLoading(false);
        window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
      });
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  };

  const balancesTableColumns = React.useMemo(
    () => getBalancesTableColumns(tokenAccounts, settle, accountPublicKey.toBase58()),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [tokenAccounts],
  );
  const mintsTableColumns = React.useMemo(
    () => getMintsTableColumns(addMint, unusedMintsLoadings),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [unusedMintsLoadings, unusedMints.map(({ address }) => address).join('')],
  );

  useEffect(() => {
    window.addEventListener(RELOAD_EVENT, loadBalances);
    return () => window.removeEventListener(RELOAD_EVENT, loadBalances);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Col span={24}>
      <H3WithInfo>
        Balances table{' '}
        <span>
          Hide empty rows{' '}
          <Switch
            checked={filterBalances}
            onChange={setFilterBalances}
            size="small"
            style={{ marginRight: 8 }}
          />
          Show unused mints{' '}
          <Switch
            checked={unusedMintsShow}
            onChange={setUnusedMintsShow}
            size="small"
            style={{ marginRight: 8 }}
          />
          <Button disabled={loading} loading={loading} onClick={loadBalances}>
            Reload
          </Button>
        </span>
      </H3WithInfo>
      <Table
        dataSource={filteredBalances}
        columns={balancesTableColumns}
        pagination={false}
        loading={loading}
        size="small"
        rowKey="coin"
      />
      {unusedMintsShow && (
        <Table
          dataSource={unusedMints}
          columns={mintsTableColumns}
          pagination={false}
          loading={unusedMintsLoading}
          size="small"
          rowKey="address"
        />
      )}
    </Col>
  );
};
