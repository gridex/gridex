import React, { useEffect } from 'react';
import { AccountSignAndSendTransaction } from './utils';
import { notify } from '../../utils/notifications';
import { Row } from './Components';
import { AccountInfo } from './AccountInfo';
import { useConnection } from '../../utils/connection';
import { useWallet } from '../../utils/wallet';
import { PublicKey } from '@solana/web3.js';
import { SEND_OPTIONS } from '../../../processor/utils/send-options';
import { waitTransactionConfirmation } from '../../../processor/utils/transactions/waitTransactionConfirmation';

export function TabWallet({ activeTab, botAccountPublicKey }: { activeTab: string, botAccountPublicKey: PublicKey }) {
  const { wallet } = useWallet();
  const connection = useConnection();

  const accountSignAndSendTransaction: AccountSignAndSendTransaction = async (transaction, signers= []) => {
    notify({
      message: 'Sign transaction',
    });
    try {
      if (!transaction.feePayer) {
        transaction.feePayer = wallet.publicKey;
      }
      if (signers.length > 0) {
        transaction.partialSign(...signers);
      }
      const tx = await wallet.signTransaction(transaction);
      notify({
        message: 'Send transaction',
      });
      const signature = await connection.sendRawTransaction(tx.serialize(), SEND_OPTIONS);
      try {
        await waitTransactionConfirmation(connection, signature, 60 * 1000);
        notify({
          message: 'Success',
          txid: signature,
        });
      } catch (e) {
        notify({
          type: "warning",
          message: 'Transaction has not been confirmed!',
          txid: signature
        });
      }
      return signature;
    } catch (e) {
      notify(e)
      return '';
    }
  };

  useEffect(() => {
    document.title = 'Account wallet info';
  }, []);

  return (
    <Row>
      <AccountInfo
        accountPublicKey={wallet.publicKey}
        connection={connection}
        accountType="Main account"
        otherAccountType="Bot"
        reloadData={activeTab === "wallet"}
        signAndSendTransaction={accountSignAndSendTransaction}
        otherAccountPublicKey={botAccountPublicKey}
      />
    </Row>
  );
}
