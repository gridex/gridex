import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import { Connection, PublicKey } from '@solana/web3.js';
import { MarketData } from '../../../processor/utils/markets/getAllMarketsData';
import { Order } from '@project-serum/serum/lib/market';
import { Col } from './Components';
import { Table, Tag } from 'antd';
import { MakeTransactionResult } from '../../../processor/utils/commonTypes';
import { notify } from '../../utils/notifications';
// import sortBy from 'lodash/sortBy';
import { useBackendRequest } from '../../utils/useBackend';
import { buyColor, sellColor } from '../../global_style';
import Icon from 'react-jdenticon';
import { flatMap } from 'lodash';

// const Select = styled(ASelect)`
//   width: 180px;
//   margin-left: 8px;
// `;

const openOrdersLoading: {
  [key: string]: boolean
} = {};

type OpenOrderTableElement = {
  marketData: MarketData,
  marketAddress: string,
  openOrder: Order,
  orderId: string,
  createCancelTransaction: (orders: Order[]) => MakeTransactionResult
}

// const CancelPopConfirm = ({ canceledOrder, order, cancelOrder }) => {
//   const [confirmIsVisible, setConfirmIsVisible] = useState(false);
//   const confirm = async () => {
//     await cancelOrder(order);
//     setConfirmIsVisible(false);
//   }
//
//   return <Popconfirm
//     title="You want to cancel order. Are you sure?"
//     visible={confirmIsVisible}
//     onConfirm={confirm}
//     onCancel={() => setConfirmIsVisible(false)}
//     okButtonProps={{
//       loading: canceledOrder === order.orderId,
//       disabled: canceledOrder === order.orderId,
//     }}
//     cancelButtonProps={{
//       disabled: canceledOrder === order.orderId
//     }}
//   >
//     <Button
//       loading={canceledOrder === order.orderId}
//       onClick={() => setConfirmIsVisible(true)}
//     >Cancel order</Button>
//   </Popconfirm>
// }

const IconContainer = styled.div`
  display: flex;
  align-items: center;

  > div {
    margin-right: 8px;
    display: flex;
  }
`;

const getOpenOrdersTableColumns = (
  // cancelOrder: (order: OpenOrderTableElement) => void,
  // canceledOrder: string,
  // canEdit: boolean,
  ordersByBots: {
    [key: string]: string
  }
) => ([
  {
    title: 'Market',
    key: 'name',
    width: '20%',
    render: (order) => order.marketAddress,
  },
  {
    title: 'Side',
    key: 'openOrder.side',
    width: '20%',
    render: (order) => <Tag
      color={order.openOrder.side === 'buy' ? buyColor : sellColor}
      style={{ fontWeight: 700 }}
    >{order.openOrder.side}</Tag>
  },
  {
    title: 'Price',
    key: 'openOrder.price',
    width: '20%',
    render: (order) => order.openOrder.price
  },
  {
    title: 'Size',
    key: 'openOrder.size',
    width: '20%',
    render: (order) => order.openOrder.size
  },
  {
    title: 'Bot',
    key: 'bot',
    width: '20%',
    render: (order) => <IconContainer>
      <Icon size="16" value={ordersByBots[order.openOrder?.clientId] || "N/A"} />{ordersByBots[order.openOrder?.clientId] || "N/A"}
    </IconContainer>
  }/*,
  {
    title: 'Actions',
    key: 'actions',
    width: '20%',
    render: (order) => <CancelPopConfirm canceledOrder={canceledOrder} order={order} cancelOrder={cancelOrder} />
  },*/
]);

const OpenOrdersInfo = ({ accountPublicKey, connection }: {
  accountPublicKey: PublicKey,
  connection: Connection,
  // reloadData: boolean
  // signAndSendTransaction: AccountSignAndSendTransaction,
  // canEdit?: boolean
}) => {
  const [filterByPair, /*setFilterByPair*/] = useState<string>('');
  const [filterByBot, /*setFilterByBot*/] = useState<string>('');
  // const [cancelAllOrdersConfirmIsVisible, setCancelAllOrdersConfirmIsVisible] = useState(false);
  // const [cancelAllOrdersLoading, setCancelAllOrdersLoading] = useState(false);
  // eslint-disable-next-line
  const [bots, botsLoading, botsUpdate] = useBackendRequest('/grid-bot?account=paper');
  // const [canceledOrder, setCanceledOrder] = React.useState("");
  const [ordersList, setOrdersList] = React.useState<OpenOrderTableElement[]>([]);
  const [loading, setLoading] = React.useState(false);
  const accountPublicKeyStr = accountPublicKey.toBase58();

  const loadOrders = useCallback(async () => {
    if (!openOrdersLoading[accountPublicKeyStr]) {
      setLoading(true);
      botsUpdate();
      try {
        const ordersList = flatMap(bots, bot => bot.orders.map(openOrder => {
          return {
            openOrder,
            marketAddress: bot.marketAddress,
            orderId: openOrder.id,
          };
        }));

        setOrdersList(ordersList);
      } catch (e) {
        notify(e);
      }
      setLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountPublicKey, accountPublicKeyStr, connection, bots]);

  // const cancelOrder = async (order: OpenOrderTableElement) => {
  //   setCanceledOrder(order.orderId);
  //   try {
  //     const { transaction, signers } = await order.createCancelTransaction([order.openOrder]);
  //     await setBlockHash(connection, transaction);
  //     await signAndSendTransaction(transaction, signers);
  //     await loadOrders();
  //   } catch(e) {
  //     notify(e);
  //   }
  //   setCanceledOrder("");
  // }

  // const cancelAllOrders = async (orders: OpenOrderTableElement[]) => {
  //   setCancelAllOrdersLoading(true);
  //   const transactions = orders.map(async (order) => {
  //     const { transaction, signers } = await order.createCancelTransaction([order.openOrder]);
  //     await setBlockHash(connection, transaction);
  //     await signAndSendTransaction(transaction, signers);
  //   });
  //
  //   Promise.all(transactions).then(() => {
  //     notify({
  //       message: "Success"
  //     });
  //   }).catch(e => {
  //     console.log(e);
  //     notify(e);
  //   }).finally(() => {
  //     setCancelAllOrdersLoading(false);
  //     setCancelAllOrdersConfirmIsVisible(false);
  //   });
  // }

  React.useEffect(() => {
    loadOrders();
    botsUpdate();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountPublicKeyStr, loadOrders]);

  // React.useEffect(() => {
  //   if (reloadData) {
  //     loadOrders();
  //     botsUpdate();
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [reloadData, loadOrders]);

  const ordersByBots = bots.reduce((acc, obj) => {
    obj.orders.forEach(order => {
      if (order.clientId) {
        acc[order.clientId] = obj.botName;
      }
    });
    return acc;
  }, {});

  const openOrdersTableColumns = getOpenOrdersTableColumns(/*canEdit, */ordersByBots);
  // const pairs = [...new Set(ordersList.map(order => order.marketData.marketName))];
  let filteredOrdersList = filterByPair === "" ? ordersList : ordersList.filter((order) => order.marketData.marketName === filterByPair);
  filteredOrdersList = filterByBot === "" ? filteredOrdersList : filteredOrdersList.filter(
    (order) => {
      const orderId = order.openOrder?.clientId?.toJSON();
      if (orderId) {
        const bot = ordersByBots[orderId] || "N/A";
        return filterByBot === bot;
      }
      return false;
    });

  // filteredOrdersList = sortBy(filteredOrdersList, (order) => ordersByBots[order.openOrder?.clientId?.toJSON() || ""] || "N/A");

  return (
    <Col span={24}>
      {/*<H3WithInfo>Orders table <span>*/}
      {/*  <ButtonGroup>*/}
      {/*    {*/}
      {/*      filteredOrdersList.length > 0 && <Popconfirm*/}
      {/*        title="Cancel all orders?"*/}
      {/*        visible={cancelAllOrdersConfirmIsVisible}*/}
      {/*        onConfirm={() => cancelAllOrders(filteredOrdersList)}*/}
      {/*        onCancel={() => setCancelAllOrdersConfirmIsVisible(false)}*/}
      {/*        okButtonProps={{*/}
      {/*          loading: cancelAllOrdersLoading*/}
      {/*        }}*/}
      {/*        cancelButtonProps={{*/}
      {/*          disabled: cancelAllOrdersLoading*/}
      {/*        }}*/}
      {/*      >*/}
      {/*        <Button onClick={() => setCancelAllOrdersConfirmIsVisible(true)} disabled={cancelAllOrdersLoading}>Cancel all orders</Button>*/}
      {/*      </Popconfirm>*/}
      {/*    }*/}
      {/*    <Button disabled={loading} loading={loading} onClick={loadOrders}>Reload</Button>*/}
      {/*  </ButtonGroup>*/}
      {/*  <Select*/}
      {/*    value={filterByPair}*/}
      {/*    onChange={setFilterByPair}*/}
      {/*    filterOption={filterOption}*/}
      {/*    showSearch*/}
      {/*  >*/}
      {/*    <Select.Option value={''} name="all">All pairs</Select.Option>*/}
      {/*    {*/}
      {/*      pairs.map(pair => (<Select.Option name={pair} value={pair} key={pair}>{ pair }</Select.Option> ))*/}
      {/*    }*/}
      {/*  </Select>*/}
      {/*  <Select*/}
      {/*    style={{ width: 180 }}*/}
      {/*    value={filterByBot}*/}
      {/*    onChange={setFilterByBot}*/}
      {/*    filterOption={filterOption}*/}
      {/*    showSearch*/}
      {/*  >*/}
      {/*    <Select.Option value={''} name={"all"}>All bots</Select.Option>*/}
      {/*    <Select.Option value="N/A" name={"n/a"}>N/A</Select.Option>*/}
      {/*    {*/}
      {/*      bots.map(bot => (<Select.Option*/}
      {/*        name={`${bot.name}${bot.botName}`}*/}
      {/*        value={bot.botName}*/}
      {/*        key={bot.botName}*/}
      {/*      >*/}
      {/*        [{ bot.name }]{ bot.botName }*/}
      {/*      </Select.Option> ))*/}
      {/*    }*/}
      {/*  </Select>*/}
      {/*</span></H3WithInfo>*/}
      <Table
        dataSource={filteredOrdersList}
        columns={openOrdersTableColumns}
        pagination={false}
        loading={loading}
        size="small"
        rowKey="orderId"
      />
    </Col>
  );
};

export default OpenOrdersInfo;
