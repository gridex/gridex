import { getValidCurrency } from './utils';

describe('getValidCurrency', () => {
  it('should return correct value', () => {
    expect(getValidCurrency('0001', '1')).toStrictEqual('0.001');
    expect(getValidCurrency('0.', '1')).toStrictEqual('0.');
    expect(getValidCurrency('sdfsdf', '1')).toStrictEqual(null);
    expect(getValidCurrency('0.324324s', '1')).toStrictEqual(null);
    expect(getValidCurrency('0.324324', '1')).toStrictEqual('0.324324');
    expect(getValidCurrency('1.00000000001', '1')).toStrictEqual('1');
    expect(getValidCurrency('NaN', '1')).toStrictEqual(null);
    expect(getValidCurrency(1.023223, '1')).toStrictEqual('1');
    expect(getValidCurrency('1.23423434', 1)).toStrictEqual('1');
    expect(getValidCurrency(Infinity, 1)).toStrictEqual(null);
    expect(getValidCurrency('Infinity', Infinity)).toStrictEqual(null);
    expect(getValidCurrency(Infinity, Infinity)).toStrictEqual(null);
    expect(getValidCurrency(NaN, NaN)).toStrictEqual(null);
    expect(getValidCurrency(NaN, Infinity)).toStrictEqual(null);
    expect(getValidCurrency('-1.22323', Infinity)).toStrictEqual(null);
    expect(getValidCurrency('0', NaN)).toStrictEqual('0');
    expect(getValidCurrency('-0', '-0')).toStrictEqual(null);
    expect(getValidCurrency('-1,234', '-1.233')).toStrictEqual(null);
    expect(getValidCurrency('1,2339999999999999', '1.234')).toStrictEqual('1.2339999999999999');
    expect(getValidCurrency('1,,,,,3434', '1.234')).toStrictEqual(null);
  });
});
