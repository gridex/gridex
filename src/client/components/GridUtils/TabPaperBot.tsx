import React, { useCallback, useEffect } from 'react';
import { PublicKey } from '@solana/web3.js';
import { useConnection } from '../../utils/connection';
import PaperBalances from './PaperBalances';
import PaperOpenOrdersInfo from './PaperOpenOrdersInfo';
import { Tabs } from 'antd';
import { useHistory, useRouteMatch } from 'react-router-dom';

const { TabPane } = Tabs;

export function TabPaperBot({
  botAccountPublicKey,
  activeTab
}: {
  botAccountPublicKey: PublicKey,
  activeTab: string
}) {
  // const { wallet } = useWallet();
  // const { backendRequest } = useBackend();
  const { params: { subTab = "balances", tab } } = useRouteMatch();
  const history = useHistory();

  const setTabKey = useCallback((key: string) => {
    history.push(`/grid-bot/${tab}/${key}`);
  }, [history, tab]);


  const connection = useConnection();

  useEffect(() => {
    document.title = 'Bot Paper wallet info';
  }, []);

  return (<>
    <h2>Paper {/*{accountPublicKey.toBase58()}*/}</h2>
    <Tabs style={{ width: '100%' }} activeKey={subTab} onTabClick={setTabKey} >
      <TabPane tab="Balances" key="balances">
        <PaperBalances accountPublicKey={botAccountPublicKey} connection={connection} />
      </TabPane>
      <TabPane tab="Orders" key="orders">
        <PaperOpenOrdersInfo accountPublicKey={botAccountPublicKey} connection={connection} />
      </TabPane>
    </Tabs>
  </>);
}
