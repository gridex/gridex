import React, { useEffect } from 'react';
import { PublicKey } from '@solana/web3.js';
import { AccountSignAndSendTransaction } from './utils';
import { notify } from '../../utils/notifications';
import { AccountInfo } from './AccountInfo';
import { useConnection } from '../../utils/connection';
import { useBackend } from '../../utils/backend';
import { useWallet } from '../../utils/wallet';

export function TabBot({
  botAccountPublicKey,
  activeTab,
}: {
  botAccountPublicKey: PublicKey;
  activeTab: string;
}) {
  const { wallet } = useWallet();
  const { backendRequest } = useBackend();
  const connection = useConnection();

  // @ts-ignore
  const botSignAndSendTransaction: AccountSignAndSendTransaction = async (
    transaction,
    signers = [],
  ) => {
    try {
      notify({
        message: 'Sign and send transaction',
      });
      transaction.setSigners(botAccountPublicKey, ...signers.map((s) => s.publicKey));
      if (signers.length) {
        transaction.partialSign(...signers);
      }
      transaction.feePayer = botAccountPublicKey;
      const data = await backendRequest('/bot/send_transaction', 'post', {
        transaction: transaction.serialize({
          requireAllSignatures: false,
          verifySignatures: false,
        }),
      });
      if (data.success) {
        const { signature } = data;
        notify({
          message: 'Success',
          txid: signature,
        });
        if (data.confirmationError) {
          notify({
            message: 'Error while confirmation',
            description: data.confirmationError,
          });
        }
        return signature;
      } else {
        notify({
          message: data.error,
        });
        return '';
      }
    } catch (e) {
      console.log(e);
      notify({
        message: e.toString(),
      });
      return '';
    }
  };

  useEffect(() => {
    document.title = 'Bot wallet info';
  }, []);

  return (
    <AccountInfo
      canEdit={false}
      accountPublicKey={botAccountPublicKey}
      connection={connection}
      accountType="Bot account"
      otherAccountType="Wallet"
      reloadData={activeTab === 'bot'}
      signAndSendTransaction={botSignAndSendTransaction}
      otherAccountPublicKey={wallet.publicKey}
    />
  );
}
