import { Connection, PublicKey } from '@solana/web3.js';
import { BalancesInfo } from './BalancesInfo';
import React, { useCallback } from 'react';
import OpenOrdersInfo from './OpenOrdersInfo';
import { AccountSignAndSendTransaction } from './utils';
import Transfer from './Transfer';
import { Tabs } from 'antd';
import { Converter } from './Converter';
import { useHistory, useRouteMatch } from 'react-router-dom';

const { TabPane } = Tabs;

export const AccountInfo = ({
  accountPublicKey,
  connection,
  accountType,
  reloadData,
  signAndSendTransaction,
  canEdit = true,
  otherAccountPublicKey,
  otherAccountType,
}: {
  accountPublicKey: PublicKey;
  connection: Connection;
  accountType: string;
  reloadData: boolean;
  signAndSendTransaction: AccountSignAndSendTransaction;
  canEdit?: boolean;
  otherAccountPublicKey: PublicKey;
  otherAccountType: string;
}) => {
  const {
    params: { subTab = 'balances', tab },
  } = useRouteMatch();
  const history = useHistory();

  const setTabKey = useCallback(
    (key: string) => {
      history.push(`/grid-bot/${tab}/${key}`);
    },
    [history, tab],
  );

  return (
    <>
      <h2>
        {accountType} {accountPublicKey.toBase58()}
      </h2>
      <Tabs style={{ width: '100%' }} activeKey={subTab} onTabClick={setTabKey}>
        <TabPane tab="Balances" key="balances">
          {subTab === 'balances' && (
            <BalancesInfo
              accountPublicKey={accountPublicKey}
              connection={connection}
              reloadData={reloadData}
              signAndSendTransaction={signAndSendTransaction}
            />
          )}
        </TabPane>
        <TabPane tab="Transfer" key="transfer">
          {subTab === 'transfer' && (
            <Transfer
              from={accountPublicKey}
              to={otherAccountPublicKey}
              signAndSendTransaction={signAndSendTransaction}
              connection={connection}
              otherAccountType={otherAccountType}
            />
          )}
        </TabPane>
        <TabPane tab="Converter" key="converter">
          {subTab === 'converter' && (
            <Converter
              accountType={accountType}
              accountPublicKey={accountPublicKey}
              connection={connection}
              signAndSendTransaction={signAndSendTransaction}
            />
          )}
        </TabPane>
        <TabPane tab="Orders" key="orders">
          {subTab === 'orders' && (
            <OpenOrdersInfo
              accountPublicKey={accountPublicKey}
              connection={connection}
              reloadData={reloadData}
              signAndSendTransaction={signAndSendTransaction}
              canEdit={canEdit}
            />
          )}
        </TabPane>
      </Tabs>
    </>
  );
};
