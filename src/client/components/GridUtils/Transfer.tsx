import { Connection, PublicKey, Transaction, SystemProgram } from '@solana/web3.js';
import { AccountSignAndSendTransaction, RELOAD_EVENT } from './utils';
import React, { useCallback, useEffect } from 'react';
import { Col, H3WithInfo } from './Components';
import getAllBalances, { Balances } from '../../../processor/utils/balance/getAllBalances';
import { TokenAccount } from '../../utils/types';
import { getTokenAccountInfo } from '../../../processor/utils/balance/getTokenAccountInfo';
import { Button, Modal, Switch, Table, Input, Select } from 'antd';
import { createTokenTransferInstruction } from '../../../processor/utils/tokens/transferTokens';
import setBlockHash from '../../../processor/utils/transactions/setBlockHash';
import { parseTokenMintData } from '../../utils/tokens';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';

type Pair = {
  coin: string;
  from: Balances;
  to: Balances;
  mint: string;
};

const getTransferColumns = (initTransfer: (record: Pair) => void) => [
  {
    title: 'Coin',
    dataIndex: 'coin',
    key: 'coin',
    width: '20%',
  },
  {
    title: 'From balance',
    dataIndex: 'from',
    key: 'from',
    width: '20%',
    render: (record) => record.walletBalance,
  },
  {
    title: 'To balance',
    dataIndex: 'to',
    key: 'to',
    width: '20%',
    render: (record) => record.walletBalance,
  },
  {
    title: 'Action',
    key: 'to',
    width: '20%',
    render: (record) => <Button onClick={() => initTransfer(record)}>Transfer</Button>,
  },
];

const MaxButton = ({ onClick }) => (
  <Button type="link" onClick={onClick} style={{ marginRight: 5 }}>
    MAX
  </Button>
);

export default function Transfer({
  from,
  to,
  signAndSendTransaction,
  connection,
  otherAccountType,
}: {
  from: PublicKey;
  to: PublicKey;
  signAndSendTransaction: AccountSignAndSendTransaction;
  connection: Connection;
  otherAccountType: string;
}) {
  const [search, setSearch] = React.useState('');
  const [transferQuantity, setTransferQuantity] = React.useState('');
  const [filterBalances, setFilterBalances] = React.useState<boolean>(true);
  const [loading, setLoading] = React.useState(false);
  const [transferLoading, setTransferLoading] = React.useState(false);
  const [fromBalances, setFromBalances] = React.useState<Balances[]>([]);
  const [fromTokenAccounts, setFromTokenAccounts] = React.useState<TokenAccount[]>([]);
  const [toBalances, setToBalances] = React.useState<Balances[]>([]);
  const [toTokenAccounts, setToTokenAccounts] = React.useState<TokenAccount[]>([]);
  const [transferPair, setTransferPair] = React.useState<Pair | null>(null);
  const [fromTokenAccount, setFromTokenAccount] = React.useState<string>('');
  const [toTokenAccount, setToTokenAccount] = React.useState<string>('');

  const loadBalances = useCallback(async () => {
    setLoading(true);

    const _fromTokenAccounts = await getTokenAccountInfo(connection, from);
    setFromTokenAccounts(_fromTokenAccounts);
    const _fromBalances = await getAllBalances(connection, from, true, _fromTokenAccounts);
    setFromBalances(_fromBalances);

    const toTokenAccounts = await getTokenAccountInfo(connection, to);
    setToTokenAccounts(toTokenAccounts);
    const _toBalances = await getAllBalances(connection, to, true, toTokenAccounts);
    setToBalances(_toBalances);

    setLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [from, connection]);

  // @ts-ignore
  const tokenPairs: Pair[] = toBalances
    .map((toBalance): Pair | null => {
      const fromBalance = fromBalances.find(({ mint: fMint }) => fMint === toBalance.mint);
      if (fromBalance) {
        return {
          from: fromBalance,
          to: toBalance,
          coin: toBalance.coin,
          mint: toBalance.mint,
        };
      }
      return null;
    })
    .filter(Boolean);

  // @ts-ignore
  const filteredPairs = filterBalances
    ? tokenPairs.filter(({ from: { walletBalance } }) => walletBalance > 0)
    : tokenPairs;

  const initTransfer = (pair) => {
    setTransferQuantity('');
    setFromTokenAccount('');
    setToTokenAccount('');
    setTransferPair(pair);
  };

  const transferColumns = getTransferColumns(initTransfer);

  const _fromTokenAccounts = transferPair
    ? fromTokenAccounts.filter(
        ({ effectiveMint }) => effectiveMint.toBase58() === transferPair.from.mint,
      )
    : [];
  const _toTokenAccounts = transferPair
    ? toTokenAccounts.filter(
        ({ effectiveMint }) => effectiveMint.toBase58() === transferPair.to.mint,
      )
    : [];

  const changeTransferQuantity = ({ target: { value } }) => {
    if (transferPair) {
      value = value.replace(/,/, '.');
      value = value.replace(/\D\./, '');
      const parsedValue = value.endsWith('.') ? `${value}0` : value;
      if (/^(-)?[0-9]{1,6}(\.?\d*)?$/.test(parsedValue)) {
        if (value > +transferPair?.from?.walletBalance) {
          setTransferQuantity(`${transferPair?.from?.walletBalance}`);
        } else {
          setTransferQuantity(value);
        }
      }
    }
  };

  const transfer = async () => {
    setTransferLoading(true);
    const _toTokenAccount =
      search === toTokenAccount
        ? { pubkey: new PublicKey(search) }
        : toTokenAccounts.find((acc) => acc.pubkey.toBase58() === toTokenAccount);
    const _fromTokenAccount = fromTokenAccounts.find(
      (acc) => acc.pubkey.toBase58() === fromTokenAccount,
    );
    if (transferPair && _fromTokenAccount && _toTokenAccount) {
      try {
        const mintAccount = (await connection.getAccountInfo(_fromTokenAccount.effectiveMint))!;
        const mintData = parseTokenMintData(mintAccount.data);

        const instruction =
          transferPair.mint === WRAPPED_SOL_MINT.toBase58()
            ? SystemProgram.transfer({
                fromPubkey: _fromTokenAccount.pubkey,
                toPubkey: _toTokenAccount.pubkey,
                lamports: +transferQuantity * Math.pow(10, mintData.decimals),
              })
            : createTokenTransferInstruction(
                _fromTokenAccount.pubkey,
                _toTokenAccount.pubkey,
                +transferQuantity * Math.pow(10, mintData.decimals),
                from,
              );

        const transaction = new Transaction();
        transaction.add(instruction);
        await setBlockHash(connection, transaction);
        await signAndSendTransaction(transaction, []);
        setTransferPair(null);
        window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
      } catch (e) {
        // do nothing
      }
    }
    setTransferLoading(false);
  };

  useEffect(() => {
    loadBalances();
    window.addEventListener(RELOAD_EVENT, loadBalances);
    return () => window.removeEventListener(RELOAD_EVENT, loadBalances);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const reload = () => {
    window.dispatchEvent(new CustomEvent(RELOAD_EVENT));
  };

  return (
    <Col span={24}>
      <Modal
        visible={Boolean(transferPair)}
        title={`${transferPair?.coin} transfer`}
        okText="Transfer"
        onCancel={() => setTransferPair(null)}
        onOk={transfer}
        okButtonProps={{
          disabled: !(fromTokenAccount && toTokenAccount) || transferLoading,
          loading: transferLoading,
        }}
      >
        {_fromTokenAccounts.length > 0 && _toTokenAccounts.length > 0 && (
          <>
            <label>From</label>
            <Select
              style={{ width: '100%' }}
              value={fromTokenAccount}
              onChange={setFromTokenAccount}
              placeholder={'From'}
            >
              {_fromTokenAccounts.map((acc) => {
                const pubkey = acc.pubkey.toBase58();
                return <Select.Option value={pubkey}>{pubkey}</Select.Option>;
              })}
            </Select>
            <p>&nbsp;</p>
            <label>To</label>
            <Select
              style={{ width: '100%' }}
              value={toTokenAccount}
              onChange={setToTokenAccount}
              placeholder={'To'}
              showSearch
              onSearch={setSearch}
            >
              {_toTokenAccounts.map((acc) => {
                const pubkey = acc.pubkey.toBase58();
                return <Select.Option value={pubkey}>{pubkey}</Select.Option>;
              })}
              {search && <Select.Option value={search}>{search}</Select.Option>}
            </Select>
            <p>&nbsp;</p>
            <Input
              name="quantity"
              value={transferQuantity}
              onChange={changeTransferQuantity}
              suffix={
                <>
                  <MaxButton
                    onClick={() =>
                      setTransferQuantity((transferPair?.from?.walletBalance || 0).toString())
                    }
                  />
                  {transferPair?.coin}
                </>
              }
            />
          </>
        )}
      </Modal>
      <H3WithInfo>
        Transfer to {otherAccountType} account{' '}
        <span>
          Hide empty rows
          <Switch
            checked={filterBalances}
            onChange={setFilterBalances}
            size="small"
            style={{ marginRight: 8 }}
          />
          <Button disabled={loading} loading={loading} onClick={reload}>
            Reload
          </Button>
        </span>
      </H3WithInfo>
      <Table
        // @ts-ignore
        dataSource={filteredPairs}
        columns={transferColumns}
        pagination={false}
        loading={loading}
        size="small"
        rowKey="coin"
      />
    </Col>
  );
}
