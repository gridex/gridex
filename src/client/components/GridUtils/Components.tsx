import styled from 'styled-components';

import { Row as aRow, Col as aCol } from 'antd';
import { mainColor } from '../../global_style';

export const Row = styled(aRow)`
  & + & {
    margin-top: 16px;
  }

  &.with_margin {
    margin-top: 48px!important;
  }

  &.without_margin {
    margin-top: 0!important;
  }
`;

export const H3WithInfo = styled.h3`
  display: flex;
  justify-content: space-between;
  margin-bottom: 16px;

  i {
    color: ${mainColor};
  }
  
  > span {
    display: flex;
    align-items: center;
  } 
`;

export const Col = styled(aCol)`
  & + & {
    margin-top: 16px;
  }
`
