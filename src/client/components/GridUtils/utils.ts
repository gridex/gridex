import { loadMarket, loadMarketPrice } from '../../utils/markets-calc';
import { error } from '../../utils/notifications';
import { Account, Transaction } from '@solana/web3.js';

export const loadMarketInfo = async (connection, market) => {
  const _market = await loadMarket(connection, market.address.toBase58(), market.programId.toBase58());
  if (!_market) {
    error('Enable bot', "Can't enable bot, market not found");
    return;
  }

  const marketPrice = await loadMarketPrice(connection, _market);
  if (!marketPrice) {
    error('Enable bot', "Can't enable bot, market not found");
    return;
  }

  return {
    _market, marketPrice
  }
}

export type AccountSignAndSendTransaction = (transaction: Transaction, signers: Account[], botAccountIsFirstSigner?: boolean) => Promise<string>;

export const RELOAD_EVENT = "RELOAD_EVENT";

const getReplacedValue = (rawValue: string): string => {
  const actions: [RegExp, string][] = [
    [/^0([0-9]+)/, '0.$1'],
    [/,/, '.'],
    [/\D\./, '']
  ]
  return actions.reduce((value, [searchValue, replaceString]) =>
    value.replace(searchValue, replaceString), String(rawValue))
}

export const getValidCurrency = (rawValue: string, maxValue: string | number): string | null => {
  const value = getReplacedValue(rawValue);
  const checkedValue = value.endsWith(".") ? `${value}0` : value;

  if (/^[0-9]{1,6}(\.?\d{1,20})?$/.test(checkedValue)) {
    return Number(value) > Number(maxValue) ? String(maxValue) : value;
  }
  return null;
}
