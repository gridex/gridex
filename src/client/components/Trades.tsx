import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Button, Col, Popover, Row, Select, Typography } from 'antd';
import Orderbook from '../components/Orderbook';
import UserInfoTable from '../components/UserInfoTable/index';
import StandaloneBalancesDisplay from '../components/StandaloneBalancesDisplay';
import {
  useMarket,
  useMarketsList,
  useUnmigratedDeprecatedMarkets,
  useMarketInfos,
} from '../utils/markets';
import TradeForm from '../components/TradeForm';
import TradesTable from '../components/TradesTable';
import LinkAddress from '../components/LinkAddress';
import DeprecatedMarketsInstructions from '../components/DeprecatedMarketsInstructions';
import {
  InfoCircleOutlined,
  PlusCircleOutlined,
} from '@ant-design/icons';
import CustomMarketDialog from '../components/CustomMarketDialog';
import { notify } from '../utils/notifications';
import { ComponentsWrapper } from '../components/layout/FloatingElement';
import { getMarketInfos } from '../utils/markets-calc';
import { useConnection, useConnectionConfig } from '../utils/connection';
import { MarketInfo } from '../utils/types';
import { getAllMarketsData } from '../../processor/utils/markets/getAllMarketsData';
import sortBy from "lodash/sortBy";

const { Option, OptGroup } = Select;

export default function TradePage() {
  const { marketName } = useMarket();
  const [handleDeprecated, setHandleDeprecated] = useState(false);
  const deprecatedMarkets = useUnmigratedDeprecatedMarkets();
  const [, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    document.title = marketName ? `${marketName} — Gridex` : 'Gridex';
  }, [marketName]);

  const changeOrderRef = useRef<
    ({ size, price }: { size?: number; price?: number }) => void
  >();

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth,
      });
    };

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  const componentProps = {
    onChangeOrderRef: (ref) => (changeOrderRef.current = ref),
    onPrice: useCallback(
      (price) => changeOrderRef.current && changeOrderRef.current({ price }),
      [],
    ),
    onSize: useCallback(
      (size) => changeOrderRef.current && changeOrderRef.current({ size }),
      [],
    ),
  };
  const component = (() => {
    if (handleDeprecated) {
      return (
        <DeprecatedMarketsPage
          switchToLiveMarkets={() => setHandleDeprecated(false)}
        />
      );
    } else {
      return <RenderNormal {...componentProps} />;
    }
  })();

  return (
    <>
      {deprecatedMarkets && deprecatedMarkets.length > 0 && (
        <Row
          align="middle"
          style={{ paddingLeft: 5, paddingRight: 5 }}
          gutter={16}
        >
          <Col>
            <Typography>
              You have unsettled funds on old markets! Please go through them to
              claim your funds.
            </Typography>
          </Col>
          <Col>
            <Button onClick={() => setHandleDeprecated(!handleDeprecated)}>
              {handleDeprecated ? 'View new markets' : 'Handle old markets'}
            </Button>
          </Col>
        </Row>
      )}
      {component}
    </>
  );
}

export function MarketSelector({
  placeholder,
  setHandleDeprecated,
  customMarkets,
  width = 200,
}: {
  markets?: any;
  placeholder: any;
  setHandleDeprecated: any;
  customMarkets: any;
  onDeleteCustomMarket?: any;
  width?: number | string;
}) {
  const connection = useConnection();
  const [loadedMarkets, setLoadedMarkets] = React.useState<MarketInfo[]>([]);
  const { market, setMarketAddress } = useMarket();

  const onSetMarketAddress = (marketAddress) => {
    setHandleDeprecated(false);
    setMarketAddress(marketAddress);
  };

  const selectedMarket = useMarketInfos(customMarkets)
    .find(
      (proposedMarket) =>
        market?.address && proposedMarket.address.equals(market.address),
    )
    ?.address?.toBase58();

  const loadMarkets = async () => {
    const markets = await getAllMarketsData(connection, false);
    setLoadedMarkets(markets.map(({ address, deprecated, programId, baseName, quoteName }) => ({
      address,
      deprecated,
      programId,
      name: `${baseName}/${quoteName}`,
    })))
  }

  useEffect(() => {
    loadMarkets();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Select
      showSearch
      size={'large'}
      style={{ width }}
      placeholder={placeholder || 'Select a market'}
      optionFilterProp="name"
      onSelect={onSetMarketAddress}
      listHeight={400}
      value={selectedMarket}
      filterOption={(input, option) =>
        option?.name?.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      <OptGroup label="Markets">
        {sortBy(loadedMarkets, "name")
          .map(({ address, name, deprecated }, i) => (
            <Option
              value={address.toBase58()}
              key={address.toBase58()}
              name={name}
              style={{
                padding: '10px',
                // @ts-ignore
                backgroundColor: i % 2 === 0 ? 'rgb(39, 44, 61)' : null,
              }}
            >
              {name} {deprecated ? ' (Deprecated)' : null}
            </Option>
          ))}
      </OptGroup>
    </Select>
  );
}

const MarketsSelector = () => {
  const {
    market,
    marketName,
    customMarkets,
    setCustomMarkets,
    setMarketAddress,
  } = useMarket();
  const markets = useMarketsList();
  const [, setHandleDeprecated] = useState(false);
  const [addMarketVisible, setAddMarketVisible] = useState(false);
  const { endpoint } = useConnectionConfig();

  useEffect(() => {
    document.title = marketName ? `${marketName} — Gridex` : 'Gridex';
  }, [marketName]);

  const onDeleteCustomMarket = (address) => {
    const newCustomMarkets = customMarkets.filter((m) => m.address !== address);
    setCustomMarkets(newCustomMarkets);
  };

  const onAddCustomMarket = (customMarket) => {
    const marketInfo = getMarketInfos(customMarkets, endpoint).some(
      (m) => m.address.toBase58() === customMarket.address,
    );
    if (marketInfo) {
      notify({
        message: `A market with the given ID already exists`,
        type: 'error',
      });
      return;
    }
    const newCustomMarkets = [...customMarkets, customMarket];
    setCustomMarkets(newCustomMarkets);
    setMarketAddress(customMarket.address);
  };

  return (
    <>
      <CustomMarketDialog
        visible={addMarketVisible}
        onClose={() => setAddMarketVisible(false)}
        onAddCustomMarket={onAddCustomMarket}
      />
      <ComponentsWrapper>
        <Row align="middle" gutter={16}>
          <Col span={24} style={{ display: 'flex', alignItems: 'center' }}>
            <MarketSelector
              width="100%"
              markets={markets}
              setHandleDeprecated={setHandleDeprecated}
              placeholder={'Select market'}
              customMarkets={customMarkets}
              onDeleteCustomMarket={onDeleteCustomMarket}
            />
            {market ? (
              <Popover
                content={<LinkAddress address={market.publicKey.toBase58()} />}
                placement="bottomRight"
                title="Market address"
                trigger="click"
              >
                <InfoCircleOutlined
                  style={{ color: '#0080FF', marginLeft: 8 }}
                />
              </Popover>
            ) : null}
            <PlusCircleOutlined
              style={{ color: '#0080FF', marginLeft: 8 }}
              onClick={() => setAddMarketVisible(true)}
            />
          </Col>
        </Row>
      </ComponentsWrapper>
    </>
  );
};

const DeprecatedMarketsPage = ({ switchToLiveMarkets }) => {
  return (
    <>
      <Row>
        <Col flex="auto">
          <DeprecatedMarketsInstructions
            switchToLiveMarkets={switchToLiveMarkets}
          />
        </Col>
      </Row>
    </>
  );
};

const RenderNormal = ({ onChangeOrderRef, onPrice, onSize }) => {
  return (
    <>
      <Row
        style={{
          minHeight: '900px',
          flexWrap: 'nowrap',
        }}
        gutter={8}
      >
        <Col span={12} style={{ height: '100%' }}>
          <Orderbook smallScreen={false} onPrice={onPrice} onSize={onSize} />
          <TradesTable smallScreen={false} />
        </Col>
        <Col
          span={12}
          style={{ height: '100%', display: 'flex', flexDirection: 'column' }}
        >
          <MarketsSelector />
          <TradeForm setChangeOrderRef={onChangeOrderRef} />
          <StandaloneBalancesDisplay />
        </Col>
      </Row>
      <Row gutter={8}>
        <Col flex="auto" style={{ height: '100%', display: 'flex' }}>
          <UserInfoTable />
        </Col>
      </Row>
    </>
  );
};
