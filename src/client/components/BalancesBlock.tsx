import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import { PublicKey } from '@solana/web3.js';
import { useWallet } from '../utils/wallet';
import { Popover, Tag } from 'antd';
import sortBy from 'lodash/sortBy';
import { useWalletBalancesForAllMarkets } from '../utils/markets';
import { useMintToTickers } from '../utils/tokens';
import { WALLETS_LOGO_MAP } from './landing/Content/Wallets';
import { getTokenAccountInfo } from '../../processor/utils/balance/getTokenAccountInfo';
import getAllBalances, { Balances } from '../../processor/utils/balance/getAllBalances';
import gridexLogo from '../assets/gridex.svg';
import { useConnection } from '../utils/connection';
import { useBackend } from '../utils/backend';
import { RELOAD_EVENT } from './GridUtils/utils';
import { useLocalStorageStringState } from '../utils/utils';

const Logo = styled.img`
  position: relative;
  top: -2px;
  left: -2px;
  margin-right: 3px;
  width: 12px;
  height: 12px;
`;

const logoMapper = (providerName) => {
  if (/Sollet/.test(providerName)) {
    return WALLETS_LOGO_MAP.find(({ name }) => name === 'Sollet');
  }
  if (/Solong/.test(providerName)) {
    return WALLETS_LOGO_MAP.find(({ name }) => name === 'Solong');
  }
  if (/Bonfida/.test(providerName)) {
    return WALLETS_LOGO_MAP.find(({ name }) => name === 'Bonfida');
  }
  if (/Math/.test(providerName)) {
    return WALLETS_LOGO_MAP.find(({ name }) => name === 'Math Wallet');
  }
};

const filterSOLAndSelectedCoinBalances = (
  balances: Array<{ coin: string; walletBalance: number }>,
  displayMode: string,
  selectedCoin: string,
) => {
  const filteredBalance: { SOL: string; coin?: string } = { SOL: '0 SOL' };
  const solBalance = balances.find(({ coin }) => coin === 'SOL') || {
    coin: 'SOL',
    walletBalance: 0,
  };
  let coinBalance: { coin: string; walletBalance: number } | undefined = balances.find(
    ({ coin }) => coin === 'USDC (USD Coin)',
  ) || {
    coin: 'USDC (USD Coin)',
    walletBalance: 0,
  };
  if (solBalance) {
    filteredBalance.SOL = `${(+solBalance.walletBalance).toFixed(4)} ${solBalance.coin}`;
  }

  switch (displayMode) {
    case 'none':
      coinBalance = undefined;
      break;
    case 'biggestStable':
      coinBalance = balances.reduce((biggestStable, walletEntry) => {
        if (
          walletEntry.coin?.includes('USD') &&
          walletEntry.walletBalance > biggestStable.walletBalance
        ) {
          return walletEntry;
        }
        return biggestStable;
      }, coinBalance);
      break;
    case 'specificCoin':
      coinBalance = balances.find(({ coin }) => coin === selectedCoin) || {
        coin: selectedCoin,
        walletBalance: 0,
      };
      break;
  }
  filteredBalance.SOL = `${solBalance?.walletBalance.toFixed(3) || '-'} SOL`;
  if (coinBalance) {
    filteredBalance.coin = `${coinBalance?.walletBalance.toFixed(3) || '-'} ${
      coinBalance.coin.includes('USDC') ? 'USDC' : coinBalance.coin
    }`;
  }
  return filteredBalance;
};

const formatBalanceString = (balanceToDisplay) => {
  const coinBalance = balanceToDisplay.coin ? ` / ${balanceToDisplay.coin}` : '';
  return `${balanceToDisplay.SOL}${coinBalance}`;
};

const balancesLoading: {
  [key: string]: boolean;
} = {};

const BalanceItem = ({
  loading,
  balances,
  logo,
  title,
  titleBalances,
}: {
  loading?: boolean;
  balances: Array<{ coin?: string; walletBalance: number }>;
  logo?: string;
  title: string;
  titleBalances: { coin?: string; SOL: string };
}) => (
  <Popover
    content={
      loading ? (
        <>...</>
      ) : (
        <div style={{ minWidth: 280 }}>
          {sortBy(balances, 'coin').map(({ coin, walletBalance }) => (
            <p key={coin} style={{ margin: 0, display: 'flex', justifyContent: 'space-between' }}>
              <span>{coin}</span><span style={{ fontFamily: 'monospace' }}>{walletBalance ? walletBalance.toFixed(6) : '0'}</span>
            </p>
          ))}
        </div>
      )
    }
    placement="bottomRight"
    title={title}
    trigger="hover"
  >
    <Tag>
      <Logo src={logo} alt="" />
      {loading ? '...' : <span>{formatBalanceString(titleBalances)}</span>}
    </Tag>
  </Popover>
);

const HumanWalletBalance = () => {
  const walletBalances = useWalletBalancesForAllMarkets();
  const mintToTickers = useMintToTickers();
  const { providerName } = useWallet();
  const logo = logoMapper(providerName);
  const [currentDisplayOption] = useLocalStorageStringState('displayCoin', 'biggestStable');
  const [coinToDisplay] = useLocalStorageStringState('cointToDisplayInWalletWidget', '');

  const humanWalletBalancesAll = React.useMemo(() => {
    return walletBalances.map((balance) => {
      return {
        coin: mintToTickers[balance.mint],
        walletBalance: balance.balance,
        mint: balance.mint,
      };
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [walletBalances]);

  const humanWalletSOLAndSelectedCoinBalance = React.useMemo(
    () =>
      filterSOLAndSelectedCoinBalances(
        humanWalletBalancesAll,
        currentDisplayOption as string,
        coinToDisplay as string,
      ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [humanWalletBalancesAll],
  );

  return (
    <BalanceItem
      balances={humanWalletBalancesAll}
      logo={logo?.logo}
      title="Balances"
      titleBalances={humanWalletSOLAndSelectedCoinBalance}
    />
  );
};

const BotBalance = () => {
  const [botAccountPublicKey, setBotAccountPublicKey] = React.useState<null | PublicKey>(null);
  const [botBalances, setBotBalances] = React.useState<Balances[]>([]);
  const { backEndConnected, backendRequest } = useBackend();
  const { connected } = useWallet();
  const connection = useConnection();
  const [loading, setLoading] = useState(true);
  const [currentDisplayOption] = useLocalStorageStringState('displayCoin', 'biggestStable');
  const [coinToDisplay] = useLocalStorageStringState('cointToDisplayInWalletWidget', '');

  React.useEffect(() => {
    if (backEndConnected && connected) {
      backendRequest('/bot/public_key', 'get').then((data) => {
        if (data.success && data.botAccountPublicKey) {
          setBotAccountPublicKey(new PublicKey(data.botAccountPublicKey));
        }
      });
    } else {
      setBotAccountPublicKey(null);
    }
  }, [connected, backEndConnected, backendRequest]);

  const loadBotBalances = useCallback(async () => {
    setLoading(true);
    if (botAccountPublicKey) {
      if (!balancesLoading[botAccountPublicKey.toBase58()]) {
        balancesLoading[botAccountPublicKey.toBase58()] = true;
        const _tokenAccounts = await getTokenAccountInfo(connection, botAccountPublicKey);
        const balances = await getAllBalances(
          connection,
          botAccountPublicKey,
          true,
          _tokenAccounts,
        );
        setBotBalances(balances);
        balancesLoading[botAccountPublicKey.toBase58()] = false;
        setLoading(false);
      }
    }
  }, [botAccountPublicKey, connection]);

  React.useEffect(() => {
    loadBotBalances();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadBotBalances]);

  useEffect(() => {
    window.addEventListener(RELOAD_EVENT, loadBotBalances);
    return () => {
      window.removeEventListener(RELOAD_EVENT, loadBotBalances);
    };
  });

  const botWalletSOLAndSelectedCoinBalance = React.useMemo(
    () =>
      filterSOLAndSelectedCoinBalances(
        botBalances,
        currentDisplayOption as string,
        coinToDisplay as string,
      ),
    [botBalances, currentDisplayOption, coinToDisplay],
  );

  return backEndConnected ? (
    <BalanceItem
      loading={loading}
      balances={botBalances}
      logo={gridexLogo}
      title="Bot balances"
      titleBalances={botWalletSOLAndSelectedCoinBalance}
    />
  ) : null;
};

export default function BalancesBlock() {
  const { connected } = useWallet();
  return connected ? (
    <div style={{ marginRight: 8 }}>
      <HumanWalletBalance />
      <BotBalance />
    </div>
  ) : null;
}
