import React, { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.less';
import { ConnectionProvider } from './utils/connection';
import { WalletProvider } from './utils/wallet';
import { GlobalStyle } from './global_style';
import { Spin } from 'antd';
import ErrorBoundary from './components/ErrorBoundary';
import { Routes } from './routes';
import { PreferencesProvider } from './utils/preferences';
import { BackendProvider } from './utils/backend';
import MainPage from './pages/MainPage';
import { ServicesProvider } from './utils/services';

export default function App() {
  return (
    <Suspense fallback={() => <Spin size="large" />}>
      <GlobalStyle />
      <ErrorBoundary>
        <BrowserRouter basename={'/'}>
          <Switch>
            <Route exact path="/">
              <MainPage />
            </Route>
            <Route exact path="/app/*">
              <PreferencesProvider>
                <ConnectionProvider>
                  <WalletProvider>
                    <BackendProvider>
                      <ServicesProvider>
                      <Suspense fallback={() => <Spin size="large" />}>
                        <Routes />
                      </Suspense>
                      </ServicesProvider>
                    </BackendProvider>
                  </WalletProvider>
                </ConnectionProvider>
              </PreferencesProvider>
            </Route>
          </Switch>
        </BrowserRouter>
      </ErrorBoundary>
    </Suspense>
  );
}
