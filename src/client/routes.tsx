import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import TradePage from './pages/TradePage';
import OpenOrdersPage from './pages/OpenOrdersPage';
import React from 'react';
import BalancesPage from './pages/BalancesPage';
import ConvertPage from './pages/ConvertPage';
import BasicLayout from './components/BasicLayout';
import ListNewMarketPage from './pages/ListNewMarketPage';
import NewPoolPage from './pages/pools/NewPoolPage';
import PoolPage from './pages/pools/PoolPage/index';
import PoolListPage from './pages/pools/PoolListPage';
import GridBotPage from './pages/GridBotPage';
import { getGridBotPageUrl, getTradePageUrl } from './utils/markets';
import { useWallet } from './utils/wallet';
import Dependent from './components/Dependent';

export function Routes() {
  const { connected } = useWallet();

  return (
    <>
      <BrowserRouter basename={'/app'}>
        <BasicLayout>
          <Switch>
            <Route exact path="/market">
              <Redirect to={getTradePageUrl()} />
            </Route>
            <Route exact path="/market/:marketAddress">
              <TradePage />
            </Route>
            <Route exact path="/orders" component={OpenOrdersPage} />
            <Route exact path="/balances" component={BalancesPage} />
            <Route exact path="/convert" component={ConvertPage} />
            <Route exact path="/list-new-market" component={ListNewMarketPage} />
            <Route exact path="/pools">
              <PoolListPage />
            </Route>
            <Route exact path="/pools/new">
              <NewPoolPage />
            </Route>
            <Route exact path="/pools/:poolAddress">
              <PoolPage />
            </Route>
            {[
              '/grid-bot/:tab(paper-bot|bots|tokens|bot|wallet|invites|settings)',
              '/grid-bot/:tab(bot|wallet|paper-bot)/:subTab',
            ].map((path) => (
              <Route
                key="tab"
                exact
                path={path}
                render={({ match }) => {
                  const {
                    params: { tab },
                  } = match;
                  return <Dependent dependencies={[connected]}>
                    <GridBotPage tab={tab} />
                  </Dependent>;
                }}
              />
            ))}
            <Route exact path="/grid-bot">
              <Redirect to={getGridBotPageUrl()} />
            </Route>
            <Route exact path="/grid-bot/:marketAddress">
              <GridBotPage tab="create_bot" />
            </Route>
          </Switch>
        </BasicLayout>
      </BrowserRouter>
    </>
  );
}
