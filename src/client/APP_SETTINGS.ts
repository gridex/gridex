export const EXPLORER_ADDRESS_KEY = 'EXPLORER_ADDRESS_KEY';
export const EXPLORER_TX_KEY = 'EXPLORER_TX_KEY';

export interface WalletExplorer {
  name: string;
  displayName: string;
  isDefault: boolean;
  urlBase: string;
  [EXPLORER_ADDRESS_KEY]: string;
  [EXPLORER_TX_KEY]: string;
}

export type ExplorerKey = typeof EXPLORER_ADDRESS_KEY | typeof EXPLORER_TX_KEY;

interface AppSettings {
  walletExplorers: WalletExplorer[];
}

export default {
  walletExplorers: [
    {
      name: 'solanaBeach',
      displayName: 'Solana Beach',
      isDefault: true,
      urlBase: 'https://solanabeach.io/',
      [EXPLORER_ADDRESS_KEY]: 'address/',
      [EXPLORER_TX_KEY]: 'transaction/',
    },
    {
      name: 'solana',
      displayName: 'Solana',
      isDefault: false,
      urlBase: 'https://explorer.solana.com/',
      [EXPLORER_ADDRESS_KEY]: 'address/',
      [EXPLORER_TX_KEY]: 'tx/',
    }
  ]
} as AppSettings;
