import { Connection, PublicKey } from '@solana/web3.js';
import { getTokenAccountInfo } from '../balance/getTokenAccountInfo';
import { MintInfo, parseTokenMintData } from '../../../client/utils/tokens';
import { getMultipleSolanaAccounts } from './getMultipleSolanaAccounts';
import { MarketData } from '../markets/getAllMarketsData';
import { TokenAccount } from '../../../client/utils/types';

export type MintInfos = {
  [key: string]: MintInfo
}

export default async function getAllMintInfo(
  connection: Connection,
  accountPublicKey: PublicKey,
  allMarkets: MarketData[],
  tokenAccounts?: TokenAccount[]
): Promise<MintInfos> {
  if (!tokenAccounts) {
    tokenAccounts = await getTokenAccountInfo(connection, accountPublicKey);
  }

  const allMints = (tokenAccounts || [])
    .map((account) => account.effectiveMint)
    .concat(
      (allMarkets || []).map((marketInfo) => marketInfo.market.baseMintAddress),
    )
    .concat(
      (allMarkets || []).map(
        (marketInfo) => marketInfo.market.quoteMintAddress,
      ),
    );
  const uniqueMints = [...new Set(allMints.map((mint) => mint.toBase58()))].map(
    (stringMint) => new PublicKey(stringMint),
  );

  const mintInfos = await getMultipleSolanaAccounts(connection, uniqueMints);

  return Object.fromEntries(
    Object.entries(mintInfos.value).map(([key, accountInfo]) => [
      key,
      accountInfo && parseTokenMintData(accountInfo.data),
    ]).filter(([, accountInfo]) => accountInfo),
  );
}