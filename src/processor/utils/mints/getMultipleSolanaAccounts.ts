import { AccountInfo, Connection, PublicKey, RpcResponseAndContext } from '@solana/web3.js';
import { Buffer } from 'buffer';
import { GetMultipleAccountsAndContextRpcResult } from '../../../client/utils/send';
import { MessageError } from '../../../common/utils/error';

export async function getMultipleSolanaAccounts(
  connection: Connection,
  publicKeys: PublicKey[],
): Promise<RpcResponseAndContext<{ [key: string]: AccountInfo<Buffer> | null }>> {
  const args = [publicKeys.map((k) => k.toBase58()), { commitment: 'recent' }];
  // @ts-ignore
  const unsafeRes = await connection._rpcRequest('getMultipleAccounts', args);
  const res = GetMultipleAccountsAndContextRpcResult(unsafeRes);
  if (res.error || typeof res.result === 'undefined') {
    throw new MessageError({
      message: `failed to get info about accounts, ${publicKeys.map((k) => k.toBase58()).join(', ')}`,
      details: {
        error: res.error.message,
        result: res.result
      },
    });
  }

  const accounts: ({
    executable: any;
    owner: PublicKey;
    lamports: any;
    data: Buffer;
  } | null)[] = [];
  for (const account of res.result.value) {
    let value: {
      executable: any;
      owner: PublicKey;
      lamports: any;
      data: Buffer;
    } | null = null;
    if (res.result.value) {
      const { executable, owner, lamports, data } = account;
      if (data[1] !== 'base64') {
        throw new MessageError({
          message: `error parsing accounts data[1] is not "base64"`,
          details: {
            data
          }
        })
      }
      value = {
        executable,
        owner: new PublicKey(owner),
        lamports,
        data: Buffer.from(data[0], 'base64'),
      };
    }
    accounts.push(value);
  }
  return {
    context: {
      slot: res.result.context.slot,
    },
    value: Object.fromEntries(
      accounts.map((account, i) => [publicKeys[i].toBase58(), account]),
    ),
  };
}
