import { Market } from '@project-serum/serum';
import { Connection, PublicKey } from '@solana/web3.js';
import { Order } from '@project-serum/serum/lib/market';
import { MakeTransactionResult } from '../commonTypes';

export function createCancelOrdersTransaction(
  connection: Connection,
  accountPublicKey: PublicKey,
  market: Market,
  orders: Order[],
): MakeTransactionResult {
  const result = new MakeTransactionResult(connection);

  result.addTransaction(market.makeMatchOrdersTransaction(5));
  orders.forEach((order) => {
    result.addTransaction(
        market.makeCancelOrderInstruction(connection, accountPublicKey, order),
    );
  });
  result.addTransaction(market.makeMatchOrdersTransaction(5));

  return result;
}
