import getAllOpenOrdersForAccountGroupedByMarkets from './getAllOpenOrdersForAccountGroupedByMarkets';
import testsBase from '../testsBase';

describe('test', () => {
  let connection;
  let accountPublicKey;
  beforeEach(() => {
    const data = testsBase();
    connection = data.connection;
    accountPublicKey = data.accountPublicKey;
  });

  describe('load all open orders for account', function () {
    let openOrders;

    beforeEach(async (done) => {
      openOrders = await getAllOpenOrdersForAccountGroupedByMarkets(
        connection,
        accountPublicKey,
      );
      done();
    });

    it('check orders', () => {
      expect(Array.isArray(openOrders)).toBeTruthy();
      if (openOrders.length) {
        const [openOrder] = openOrders;
        expect(Array.isArray(openOrder.orders)).toBeTruthy();
        expect(openOrder.marketData.hasOwnProperty('market')).toBeTruthy();
        expect(openOrder.marketData.hasOwnProperty('marketName')).toBeTruthy();
        expect(openOrder.marketData.hasOwnProperty('programId')).toBeTruthy();
        expect(openOrder.marketData.hasOwnProperty('address')).toBeTruthy();
        expect(typeof openOrder.marketAddress).toEqual('string');
      }
    });
  });
});
