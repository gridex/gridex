import { MARKETS, OpenOrders } from '@project-serum/serum';
import { Connection, PublicKey } from '@solana/web3.js';
import { memoPromise } from '../../../common/utils/memo-promise';

async function getAllOpenOrdersAccountsForOwnerFunc(connection: Connection, ownerPublicKey: PublicKey, programIds: PublicKey[]): Promise<OpenOrders[]> {
  const result = await Promise.all(programIds.map(async (programId) => {
    const orders = await OpenOrders.findForOwner(connection, ownerPublicKey, programId);
    return orders;
  }));
  return result.flat();
}

const getAllOpenOrdersAccountsForOwner = memoPromise(
  getAllOpenOrdersAccountsForOwnerFunc,
  (connection: Connection, ownerPublicKey: PublicKey, programIds: PublicKey[]) => [ownerPublicKey.toBase58(), ...programIds.map(p => p.toBase58())].join('_'),
);

async function loadAllOrdersForAccountFunc(connection: Connection, accountPublicKey: PublicKey): Promise<OpenOrders[]> {
    const programIds = [
      ...new Set(MARKETS.map((info) => info.programId.toBase58())),
    ].map((stringProgramId) => new PublicKey(stringProgramId));

    return await getAllOpenOrdersAccountsForOwner(connection, accountPublicKey, programIds);
}

const loadAllOrdersForAccount = memoPromise(
  loadAllOrdersForAccountFunc,
  (connection: Connection, accountPublicKey: PublicKey) => accountPublicKey.toBase58(),
);

export default loadAllOrdersForAccount;
