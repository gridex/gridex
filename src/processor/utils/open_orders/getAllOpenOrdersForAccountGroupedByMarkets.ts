import { Connection, PublicKey } from '@solana/web3.js';
import loadAllOrdersForAccount from './loadAllOrdersForAccount';
import { OpenOrders } from '@project-serum/serum';
import { getAllMarketsData, MarketData } from '../markets/getAllMarketsData';
import { Order } from '@project-serum/serum/lib/market';
import { createCancelOrdersTransaction } from './createCancelOrdersTransaction';
import { MakeTransactionResult } from '../commonTypes';

type OpenOrdersByAddress = {
  [key: string]: OpenOrders[]
}

function groupOrdersAccountsByAddress(openOrdersAccounts: OpenOrders[]): OpenOrdersByAddress {
  const openOrdersAccountsByAddress: OpenOrdersByAddress = {};

  for (const account of openOrdersAccounts || []) {
    const marketsAddr = account.market.toBase58();
    if (!(marketsAddr in openOrdersAccountsByAddress)) {
      openOrdersAccountsByAddress[marketsAddr] = [];
    }
    openOrdersAccountsByAddress[marketsAddr].push(account);
  }

  return openOrdersAccountsByAddress;
}

type MarketsByAddress = {
  [key: string]: MarketData
}


function getMarketsByAddress(markets: MarketData[] = []): MarketsByAddress {
  return Object.fromEntries(
    markets.map((marketData) => [marketData.market.address.toBase58(), marketData]),
  );
}

export type OpenOrdersForAccountGroupedByMarkets = {
  orders: Order[],
  marketData: MarketData,
  marketAddress: string,
  createCancelTransaction: (orders: Order[]) => MakeTransactionResult
};

export default async function getAllOpenOrdersForAccountGroupedByMarkets(
  connection: Connection,
  accountPublicKey: PublicKey,
  markets?: MarketData[]
): Promise<OpenOrdersForAccountGroupedByMarkets[]>
{
  const skipOtherMarkets = markets && markets.length;
  const openOrdersAccounts = await loadAllOrdersForAccount(connection, accountPublicKey);
  const openOrdersAccountsByAddress = groupOrdersAccountsByAddress(openOrdersAccounts);

  if (!skipOtherMarkets) {
    markets = await getAllMarketsData(connection);
  }
  const marketsByAddress = getMarketsByAddress(markets);

  const filteredOpenOrdersAccountsByAddress: OpenOrdersByAddress = {};
  if (skipOtherMarkets) {
    Object.keys(openOrdersAccountsByAddress).forEach((marketAddr) => {
      if (marketsByAddress[marketAddr]) {
        filteredOpenOrdersAccountsByAddress[marketAddr] = openOrdersAccountsByAddress[marketAddr];
      }
    });
  }
  const ordersAccounts = Object.keys(filteredOpenOrdersAccountsByAddress).length ? filteredOpenOrdersAccountsByAddress : openOrdersAccountsByAddress;
  return await Promise.all(
    Object.keys(ordersAccounts).map(async (marketAddr) => {
      const market = marketsByAddress[marketAddr].market;
      const [bids, asks] = await Promise.all([
        market.loadBids(connection),
        market.loadAsks(connection),
      ]);
      return {
        orders: market.filterForOpenOrders(
          bids,
          asks,
          ordersAccounts[marketAddr],
        ),
        marketData: marketsByAddress[marketAddr],
        marketAddress: marketAddr,
        createCancelTransaction(orders: Order[]) {
          return createCancelOrdersTransaction(connection, accountPublicKey, market, orders);
        }
      };
    }),
  );
}
