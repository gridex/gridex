import { getTokenAccountInfo } from '../balance/getTokenAccountInfo';
import { getSelectedTokenAccountForMint } from '../../../client/utils/markets-calc';
import { Market, Orderbook } from '@project-serum/serum';
import { getDecimalCount } from '../../../client/utils/utils';
import { getMarketOrderPrice } from '../../../client/utils/markets';
import { createPlaceOrderTransaction } from '../../../common/createPlaceOrderTransaction';
import { Connection, PublicKey } from '@solana/web3.js';
import { MakeTransactionResult } from '../commonTypes';
import { MessageError } from '../../../common/utils/error';

export type CreatePlaceOrderTransactionParams = {
  size: number,
  side: 'buy' | 'sell',
  orderType: 'limit' | 'ioc' | 'postOnly',
  price: number | null,
}

export type OrderPlacer = (accountPublicKey: PublicKey, { side, size, price, orderType }: CreatePlaceOrderTransactionParams) => Promise<MakeTransactionResult>;

export const createPlaceOrderTransactionForMarket =
  (connection: Connection, market: Market): OrderPlacer =>
    async (accountPublicKey: PublicKey, { side, size, price, orderType }) => {
      const accounts = await getTokenAccountInfo(connection, accountPublicKey);
      const baseCurrencyAccount = getSelectedTokenAccountForMint(
        accounts,
        market.baseMintAddress,
      );
      const quoteCurrencyAccount = getSelectedTokenAccountForMint(
        accounts,
        market.quoteMintAddress,
      );
      const sidedOrderbookAccount = side === 'buy' ? market.asksAddress : market.bidsAddress;
      const orderbookData = await connection.getAccountInfo(
        sidedOrderbookAccount,
      );

      if (!(orderbookData && baseCurrencyAccount && quoteCurrencyAccount)) {
        throw new MessageError({
          message: "No orderbookData | baseCurrencyAccount | quoteCurrencyAccount",
          details: {
            orderbookData,
            baseCurrencyAccount,
            quoteCurrencyAccount,
          }
        });
      } else {
        const decodedOrderbookData = Orderbook.decode(market, orderbookData.data);

        const tickSizeDecimals = getDecimalCount(market.tickSize);
        let parsedPrice = getMarketOrderPrice(
          decodedOrderbookData,
          size,
          tickSizeDecimals,
        );

        if (side === 'buy') {
          parsedPrice = +parsedPrice.toFixed(Math.log10(market.minOrderSize) * -1);
        }

        const result = await createPlaceOrderTransaction({
          side,
          price: price || parsedPrice,
          size,
          orderType,
          market,
          connection,
          accountPublicKey,
          baseCurrencyAccount: baseCurrencyAccount.pubkey,
          quoteCurrencyAccount: quoteCurrencyAccount.pubkey,
          feeDiscountPubkey: undefined,
        });

        if (result) {
          return result;
        } else {
          console.log("Error creating order transaction", result);
          throw new MessageError({
            message: "Error creating order transaction",
            details: {
              result,
            }
          });
        }
      }
    }
