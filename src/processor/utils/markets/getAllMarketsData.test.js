import { Account, PublicKey, Transaction } from '@solana/web3.js';
import { Market } from '@project-serum/serum';
import testsBase from '../testsBase';
import accounts from '../../accounts.json';
import { getAllMarketsData } from './getAllMarketsData';
import setBlockHash from '../transactions/setBlockHash';

const account = new Account(accounts['8YmMhex5Vd5JPsyNhCwFPDx5vqeedpCuyFE2W7VtRXQT'].bot_account);

describe('test getting all markets', () => {
  let connection;
  let accountPublicKey;

  beforeEach(() => {
    const data = testsBase();
    connection = data.connection;
    accountPublicKey = account.publicKey;
  });

  describe('load all balances for account', function () {
    let markets;

    beforeEach(async (done) => {
      markets = await getAllMarketsData(connection);
      done();
    });

    it('check markets', function () {
      expect(Array.isArray(markets)).toBeTruthy();

      if (markets.length) {
        const [market] = markets;

        expect(market.market instanceof Market).toBeTruthy();
        expect(market.programId instanceof PublicKey).toBeTruthy();
        expect(market.address instanceof PublicKey).toBeTruthy();
        expect(typeof market.createPlaceOrderTransaction).toEqual('function');
      }
    });

    describe('make order transaction', () => {
      let result;

      beforeEach(async (done) => {
        const market = markets.find(({ marketName }) => marketName === 'SOL/USDT');
        result = await market.createPlaceOrderTransaction(accountPublicKey, {
          side: 'sell',
          size: 1,
          price: 20,
          orderType: 'limit',
        });
        done();
      });

      it('test result', function () {
        expect(result.transaction instanceof Transaction).toBeTruthy();
        expect(Array.isArray(result.signers)).toBeTruthy();
        for (const signer of result.signers) {
          expect(signer instanceof Account).toBeTruthy();
        }
      });

      describe('pace order', () => {
        let tx;

        beforeEach(async (done) => {
          await setBlockHash(connection, result.transaction);
          result.transaction.partialSign(account, ...result.signers);

          tx = await connection.sendRawTransaction(result.transaction.serialize());
          console.log(tx);
          done();
        });

        it('check tx', () => {
          expect(1).toEqual(1);
        });
      });
    });
  });
});
