import { MARKETS } from '@project-serum/serum';
import { MarketInfo } from '../../../client/utils/types';

export default function getNotDeprecatedMarkets(): MarketInfo[] {
  return MARKETS.filter(({ deprecated }) => !deprecated);
}