import { memoize } from 'lodash';
import { Market, MARKETS } from '@project-serum/serum';
import { Connection, PublicKey } from '@solana/web3.js';
import { MarketInfo } from '../../../client/utils/types';
import { MakeTransactionResult } from '../commonTypes';
import { MessageError } from '../../../common/utils/error';
import { createPlaceOrderTransactionForMarket, CreatePlaceOrderTransactionParams } from './createPlaceOrderTransaction';
import TOKEN_MINTS from '../../../common/utils/tokenMints';

const _MARKETS = MARKETS.filter((m) => !(m.name === "OXY/USDT" && m.address.toBase58() === "HdBhZrnrxpje39ggXnTb6WuTWVvj5YKcSHwYGQCRsVj"));

export type MarketData = {
  baseName: string;
  quoteName: string;
  market: Market;
  marketName: string;
  programId: PublicKey;
  address: PublicKey;
  deprecated: boolean;
  createPlaceOrderTransaction: (accountPublicKey: PublicKey, params: CreatePlaceOrderTransactionParams) => Promise<MakeTransactionResult>
}

async function getAllMarketsDataFunc(connection: Connection, allMarkets = true): Promise<MarketData[]> {
  const marketInfos: MarketInfo[] = allMarkets ? _MARKETS : _MARKETS.filter(({ deprecated }) => !deprecated);

  const markets: (MarketData | null)[] = await Promise.all(
    marketInfos.map(async (marketInfo) => {
      try {
        const market = await Market.load(
          connection,
          marketInfo.address,
          {},
          marketInfo.programId,
        );

        const base = TOKEN_MINTS.find(({ address }) => address.equals(market.baseMintAddress));
        const quote = TOKEN_MINTS.find(({ address }) => address.equals(market.quoteMintAddress));
        const [baseName = "", quoteName = ""] = marketInfo.name.split("/");

        return {
          baseName: base ? base.name : baseName,
          quoteName: quote ? quote.name : quoteName,
          deprecated: Boolean(marketInfo.deprecated),
          market,
          marketName: marketInfo.name,
          programId: marketInfo.programId,
          address: marketInfo.address,
          createPlaceOrderTransaction: createPlaceOrderTransactionForMarket(connection, market)
        };
      } catch (e) {
        throw new MessageError({
          message: 'Error loading all market',
          description: e.message,
          type: 'error',
        });
      }
    }),
  );

  return markets.filter((m): m is MarketData => !!m);
}

export const getAllMarketsData = memoize(getAllMarketsDataFunc, (a, b) => b);
