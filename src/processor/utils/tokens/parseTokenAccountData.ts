import { PublicKey } from '@solana/web3.js';
import { ACCOUNT_LAYOUT } from '../layouts';

export type TokenAccountData = {
  mint: PublicKey;
  owner: PublicKey;
  amount: number;
}

export function parseTokenAccountData(
  data: Buffer,
): TokenAccountData {
  const { mint, owner, amount } = ACCOUNT_LAYOUT.decode(data);
  return {
    mint: new PublicKey(mint),
    owner: new PublicKey(owner),
    amount,
  };
}