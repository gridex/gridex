import { Account, Connection, PublicKey, SystemProgram, Transaction } from '@solana/web3.js';
import { TokenInstructions } from '@project-serum/serum';
import { MakeTransactionResult } from '../commonTypes';
import setBlockHash from '../transactions/setBlockHash';

export async function createTokenAccount(
  connection: Connection,
  accountPublicKey: PublicKey,
  mintPublicKey: PublicKey
): Promise<MakeTransactionResult> {
  const newAccount = new Account();
  const transaction = new Transaction();
  const instruction = SystemProgram.createAccount({
    fromPubkey: accountPublicKey,
    newAccountPubkey: newAccount.publicKey,
    lamports: await connection.getMinimumBalanceForRentExemption(165),
    space: 165,
    programId: TokenInstructions.TOKEN_PROGRAM_ID,
  });
  transaction.add(instruction);

  transaction.add(
    TokenInstructions.initializeAccount({
      account: newAccount.publicKey,
      mint: mintPublicKey,
      owner: accountPublicKey,
    }),
  );

  transaction.feePayer = accountPublicKey;
  await setBlockHash(connection, transaction);

  return new MakeTransactionResult(connection, transaction, [newAccount]);
}