import { Token, TOKEN_PROGRAM_ID } from '@solana/spl-token';
import { Connection, PublicKey, Account } from '@solana/web3.js';

export default async function transferTokens(
  source: PublicKey,
  destination: PublicKey,
  amount: number,
  connection: Connection,
  mint: PublicKey,
  accountPublicKey: Account,
) {
  const token = new Token(connection, mint, TOKEN_PROGRAM_ID, accountPublicKey);
  return await token.transfer(source, destination, accountPublicKey, [], amount);
}

export function createTokenTransferInstruction(
  source: PublicKey,
  destination: PublicKey,
  amount: number,
  accountPublicKey: PublicKey,
) {
  return Token.createTransferInstruction(
    TOKEN_PROGRAM_ID,
    source,
    destination,
    accountPublicKey,
    [],
    amount,
  );
}
