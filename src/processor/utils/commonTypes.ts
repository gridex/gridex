import { Account, Transaction, Connection, PublicKey } from '@solana/web3.js';
import sendTransaction from './transactions/sendTransaction';
import { uniq } from 'lodash';

export class MakeTransactionResult {
  public transaction: Transaction;
  public signers: Account[];
  public connection?: Connection;

  constructor(connection?: Connection, transaction?: Transaction, signers?: Account[]) {
    this.transaction = transaction || new Transaction();
    this.signers = signers || [];
    this.connection = connection;
  }

  add({
    transaction,
    signers,
    connection,
  }: {
    transaction: Transaction;
    signers: Account[];
    connection?: Connection;
  }): this {
    this.addTransaction(transaction);
    this.signers.push(...signers);
    if (!this.connection) {
      this.connection = connection;
    }
    return this;
  }

  addTransaction(transaction): this {
    this.transaction.add(transaction);
    this.transaction.feePayer = this.transaction.feePayer || transaction.feePayer;
    return this;
  }

  addSigners(signers): this {
    this.signers.push(...signers);
    return this;
  }

  setSigners(...signer: Array<PublicKey>): this {
    this.transaction.setSigners(...signer);
    return this;
  }

  setFeePayer(payer): this {
    if (payer.publicKey) {
      this.signers.push(payer);
      this.transaction.feePayer = payer.publicKey;
    } else if (payer instanceof PublicKey) {
      this.transaction.feePayer = payer;
    } else {
      throw new Error('unknown payer type');
    }

    return this;
  }

  async send(connection?: Connection) {
    connection = connection || this.connection;
    if (!connection) {
      throw new Error('connection not set for transaction result');
    }

    return await sendTransaction(connection, this.transaction, uniq(this.signers));
  }
}
