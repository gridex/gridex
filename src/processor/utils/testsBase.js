import { Connection, PublicKey, clusterApiUrl } from '@solana/web3.js';
import { cluster } from '../../api/routes/test/util/url';

if (!Array.prototype.flat)
  Array.prototype.flat = function (depth = 1) {
    depth = isNaN(depth) ? 0 : Math.floor(depth);
    if (depth < 1) return this.slice();
    return [].concat(
      ...(depth < 2 ? this : this.map((v) => (Array.isArray(v) ? v.flat(depth - 1) : v))),
    );
  };

if (!Object.fromEntries)
  Object.fromEntries = function (iterable) {
    return [...iterable].reduce((obj, [key, val]) => {
      obj[key] = val;
      return obj;
    }, {});
  };

export default function testsBase(cluster = 'mainnet-beta') {
  const url = clusterApiUrl(cluster, false);
  const connection = new Connection(url);

  const accountPublicKey = new PublicKey('ERdVo71ghER4zPz86kLPfWaZDTa8fGLYt4UWszXm7t5h');

  return { connection, accountPublicKey };
}
