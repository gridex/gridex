import { Market } from '@project-serum/serum';
import { loadMarketPrice } from '../../../client/utils/markets-calc';
import { Connection, PublicKey } from '@solana/web3.js';
import getAllBalances from './getAllBalances';
import { createRoundToDecimal } from '../../../client/utils/utils';
import { MessageError } from '../../../common/utils/error';

type BotParams = {
  upperLimitPrice: number;
  lowerLimitPrice: number;
  gridQuantity: number;
  quantityPerGrid: number;
};

type GridBotCalc = {
  balance: number;
  lacking: number;
  need: number;
}

export function calcGridAmounts(bot: BotParams, currentPrice: number, round: (number) => number) {
  const { upperLimitPrice, lowerLimitPrice, gridQuantity, quantityPerGrid } = bot;

  let baseCurrencySum = 0;
  let quoteCurrencySum = 0;
  const step = (upperLimitPrice - lowerLimitPrice) / (gridQuantity - 1);

  for (let i = 0; i < (gridQuantity - 1); i++) {
    const price = round(lowerLimitPrice + step * i);
    if (price < currentPrice) {
      quoteCurrencySum += price * quantityPerGrid;
    } else {
      baseCurrencySum += quantityPerGrid;
    }
  }
  return { baseCurrencySum, quoteCurrencySum };
}

export default async function calculateBalanceForGridBot(
  connection: Connection,
  accountPublicKey: PublicKey,
  market: Market,
  botParams: BotParams
): Promise<{
  base: GridBotCalc;
  quote: GridBotCalc;
}> {
  const currentPrice = await loadMarketPrice(connection, market);
  if (!currentPrice) {
    throw new MessageError({
      message: "Failed to load current market price"
    });
  }
  const toFixedValue = Math.abs(Math.log10(market.tickSize));
  const round = createRoundToDecimal(toFixedValue);

  let {
    baseCurrencySum,
    quoteCurrencySum,
  } = calcGridAmounts(botParams, currentPrice, round);

  const { baseMintAddress, quoteMintAddress } = market;

  const balances = await getAllBalances(connection, accountPublicKey, true);
  const baseCurrencyBalanceData = balances.find(({ mint }) => baseMintAddress.toBase58() === mint);
  const quoteCurrencyBalanceData = balances.find(({ mint }) => quoteMintAddress.toBase58() === mint);

  if (!baseCurrencyBalanceData) {
    throw new MessageError({ message: "Can not load balance for base currency" });
  }

  if (!quoteCurrencyBalanceData) {
    throw new MessageError({ message: "Can not load balance for quote currency" });
  }

  const baseCurrencyBalance = baseCurrencyBalanceData.walletBalance + baseCurrencyBalanceData.openOrdersFree;
  const quoteCurrencyBalance = quoteCurrencyBalanceData.walletBalance + quoteCurrencyBalanceData.openOrdersFree;

  const baseCurrencyDifference = baseCurrencyBalance - baseCurrencySum;
  const quoteCurrencyDifference = quoteCurrencyBalance - quoteCurrencySum;

  return {
    base: {
      lacking: round(baseCurrencyDifference < 0 ? -baseCurrencyDifference : 0),
      balance: round(baseCurrencyBalance),
      need: round(baseCurrencySum),
    },
    quote: {
      lacking: round(quoteCurrencyDifference < 0 ? -quoteCurrencyDifference : 0),
      balance: round(quoteCurrencyBalance),
      need: round(quoteCurrencySum),
    },
  };
}
