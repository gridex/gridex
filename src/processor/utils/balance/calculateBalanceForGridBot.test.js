import { Market, MARKETS } from '@project-serum/serum';
import { createRoundToDecimal } from '../../../client/utils/utils';
import testsBase from '../testsBase';
import calculateBalanceForGridBot, { calcGridAmounts } from './calculateBalanceForGridBot';

const { connection, accountPublicKey } = testsBase();

jest.setTimeout(10000);

describe('test calculateBalanceForGridBot', () => {
  let result;
  let market;
  beforeEach(async (done) => {
    const MARKET = MARKETS.find(({ name, deprecated }) => !deprecated && name === 'SOL/USDT');
    market = await Market.load(connection, MARKET.address, {}, MARKET.programId);

    const toFixedValue = Math.abs(Math.log10(market.tickSize));
    const round = createRoundToDecimal(toFixedValue);

    result = await calcGridAmounts(
      {
        upperLimitPrice: 19,
        lowerLimitPrice: 10,
        gridQuantity: 10,
        quantityPerGrid: 0.1,
      },
      14.5,
      round,
    );
    done();
  });

  it('test result types', () => {
    expect(result.baseCurrencySum).toEqual(0.4);
    expect(result.quoteCurrencySum).toEqual('number');
  });

  describe('test if anyway we will need more money', () => {
    beforeEach(async (done) => {
      result = await calculateBalanceForGridBot(connection, accountPublicKey, market, {
        upperLimitPrice: 1000,
        lowerLimitPrice: 1,
        gridQuantity: 10,
        quantityPerGrid: 1,
      });
      done();
    });

    it('test results are more than 0', () => {
      expect(result.baseCurrencyNotEnoughSum > 0).toBeTruthy();
      expect(result.quoteCurrencyNotEnoughSum > 0).toBeTruthy();
    });
  });

  describe("test if anyway we won't more money", () => {
    beforeEach(async (done) => {
      result = await calculateBalanceForGridBot(connection, accountPublicKey, market, {
        upperLimitPrice: 20,
        lowerLimitPrice: 10,
        gridQuantity: 3,
        quantityPerGrid: 0.001,
      });
      done();
    });

    it('test results are equal 0', () => {
      expect(result.baseCurrencyNotEnoughSum).toEqual(0);
      expect(result.quoteCurrencyNotEnoughSum).toEqual(0);
    });
  });
});
