import testsBase from '../testsBase';
import { PublicKey } from '@solana/web3.js';
import settleByMint from './settle';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';

const mint = WRAPPED_SOL_MINT.toBase58();

describe('test settle by mint', () => {
  let connection;
  let accountPublicKey;
  let txData;

  beforeEach(async (done) => {
    const data = testsBase();
    connection = data.connection;
    accountPublicKey = data.accountPublicKey;

    txData = await settleByMint(connection, accountPublicKey, new PublicKey(mint));
    done();
  });

  it('expect', function () {
    expect(1).toEqual(1);
  });

  it('transactions is array', function () {
    expect(Array.isArray(txData.transactions)).toBeTruthy();
  });

  it('signers is array', function () {
    expect(Array.isArray(txData.signers)).toBeTruthy();
  });
});
