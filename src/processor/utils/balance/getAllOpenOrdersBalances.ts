import { Connection, PublicKey } from '@solana/web3.js';
import loadAllOrdersForAccount from '../open_orders/loadAllOrdersForAccount';
import getAllMintInfo, { MintInfos } from '../mints/getAllMintInfo';
import { getAllMarketsData, MarketData } from '../markets/getAllMarketsData';
import { divideBnToNumber, getTokenMultiplierFromDecimals } from '../../../client/utils/utils';
import BN from 'bn.js';

export type OpenOrdersBalances = {
  [mint: string]: {
    market: PublicKey;
    free: number;
    total: number;
  }[];
}

export default async function getAllOpenOrdersBalances(
  connection: Connection,
  accountPublicKey: PublicKey,
  allMarkets?: MarketData[],
  mintInfos?: MintInfos
): Promise<OpenOrdersBalances> {
  const openOrdersAccounts = await loadAllOrdersForAccount(connection, accountPublicKey);

  if (!allMarkets) {
    allMarkets = await getAllMarketsData(connection);
  }

  if (!mintInfos) {
    mintInfos = await getAllMintInfo(connection, accountPublicKey, allMarkets);
  }

  const marketsByAddress = Object.fromEntries(
    (allMarkets || []).map((m) => [m.market.address.toBase58(), m]),
  );

  const openOrdersBalances: OpenOrdersBalances = {};

  for (const account of openOrdersAccounts) {
    const marketInfo = marketsByAddress[account.market.toBase58()];
    const baseMint = marketInfo?.market.baseMintAddress.toBase58();
    const quoteMint = marketInfo?.market.quoteMintAddress.toBase58();
    if (!(baseMint in openOrdersBalances)) {
      openOrdersBalances[baseMint] = [];
    }
    if (!(quoteMint in openOrdersBalances)) {
      openOrdersBalances[quoteMint] = [];
    }

    const baseMintInfo = mintInfos && mintInfos[baseMint];
    const baseFree = divideBnToNumber(
      new BN(account.baseTokenFree),
      getTokenMultiplierFromDecimals(baseMintInfo?.decimals || 0),
    );
    const baseTotal = divideBnToNumber(
      new BN(account.baseTokenTotal),
      getTokenMultiplierFromDecimals(baseMintInfo?.decimals || 0),
    );
    const quoteMintInfo = mintInfos && mintInfos[quoteMint];
    const quoteFree = divideBnToNumber(
      new BN(account.quoteTokenFree),
      getTokenMultiplierFromDecimals(quoteMintInfo?.decimals || 0),
    );
    const quoteTotal = divideBnToNumber(
      new BN(account.quoteTokenTotal),
      getTokenMultiplierFromDecimals(quoteMintInfo?.decimals || 0),
    );

    openOrdersBalances[baseMint].push({
      market: account.market,
      free: baseFree,
      total: baseTotal,
    });
    openOrdersBalances[quoteMint].push({
      market: account.market,
      free: quoteFree,
      total: quoteTotal,
    });
  }
  return openOrdersBalances;
}