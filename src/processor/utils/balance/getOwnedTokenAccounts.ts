import bs58 from 'bs58';
import { AccountInfo, Connection, PublicKey } from '@solana/web3.js';
import { getOwnedAccountsFilters, TOKEN_PROGRAM_ID } from '../../../client/utils/tokens';
import { MessageError } from '../../../common/utils/error';
import { memoPromise } from '../../../common/utils/memo-promise';

export type TokenAccount = {
  publicKey: PublicKey;
  accountInfo: AccountInfo<Buffer>
};

async function getOwnedTokenAccountsFunc(
  connection: Connection,
  accountPublicKey: PublicKey,
): Promise<TokenAccount[]> {
  const filters = getOwnedAccountsFilters(accountPublicKey);
  // @ts-ignore
  const resp = await connection._rpcRequest('getProgramAccounts', [
    TOKEN_PROGRAM_ID.toBase58(),
    {
      commitment: connection.commitment,
      filters,
    },
  ]);
  if (resp.error) {
    throw new MessageError({
      message: `failed to get token accounts owned by ${accountPublicKey.toBase58()}`,
      details: resp.error.message,
    });
  }
  const result = resp.result
    .map(({ pubkey, account: { data, executable, owner, lamports } }) => ({
      publicKey: new PublicKey(pubkey),
      accountInfo: {
        data: bs58.decode(data),
        executable,
        owner: new PublicKey(owner),
        lamports,
      },
    }))
    .filter(({ accountInfo }) => {
      // TODO: remove this check once mainnet is updated
      return filters.every((filter) => {
        if (filter.dataSize) {
          return accountInfo.data.length === filter.dataSize;
        } else if (filter.memcmp) {
          const filterBytes = bs58.decode(filter.memcmp.bytes);
          return accountInfo.data
            .slice(
              filter.memcmp.offset,
              filter.memcmp.offset + filterBytes.length,
            )
            .equals(filterBytes);
        }
        return false;
      });
    });

  return result;
}

export const getOwnedTokenAccounts = memoPromise(
  getOwnedTokenAccountsFunc,
  (connection: Connection, accountPublicKey: PublicKey) => accountPublicKey.toBase58(),
);
