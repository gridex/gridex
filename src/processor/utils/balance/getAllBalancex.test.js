import { Connection, PublicKey } from '@solana/web3.js';
import getAllBalances from './getAllBalances';
import testsBase from '../testsBase';

describe('test getting all balances', () => {
  let connection;
  let accountPublicKey;

  beforeEach(() => {
    const data = testsBase();
    connection = data.connection;
    accountPublicKey = data.accountPublicKey;
  });

  describe('load all balances for account', function () {
    let balances;

    beforeEach(async (done) => {
      balances = await getAllBalances(connection, accountPublicKey);

      console.log(balances);
      done();
    });

    it('check balances', () => {
      expect(Array.isArray(balances)).toBeTruthy();
      if (balances.length) {
        const [balanceEx] = balances;
        expect(typeof balanceEx.mint).toEqual('string');
        expect(typeof balanceEx.coin).toEqual('string');
        expect(typeof balanceEx.walletBalance).toEqual('number');
        expect(typeof balanceEx.openOrdersFree).toEqual('number');
        expect(typeof balanceEx.openOrdersTotal).toEqual('number');
      }
    });
  });
});
