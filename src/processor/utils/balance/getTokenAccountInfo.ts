import { Connection, PublicKey } from '@solana/web3.js';
import { TokenAccount } from '../../../client/utils/types';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';
import { getOwnedTokenAccounts } from './getOwnedTokenAccounts';
import { parseTokenAccountData } from '../tokens/parseTokenAccountData';
import { memoPromise } from '../../../common/utils/memo-promise';

async function getTokenAccountInfoFunc(
  connection: Connection,
  accountPublicKey: PublicKey,
): Promise<TokenAccount[]> {
  const [splAccounts, account] = await Promise.all([
    getOwnedTokenAccounts(connection, accountPublicKey),
    connection.getAccountInfo(accountPublicKey),
  ]);

  const parsedSplAccounts: TokenAccount[] = splAccounts.map(
    ({ publicKey, accountInfo }) => {
      return {
        pubkey: publicKey,
        account: accountInfo,
        effectiveMint: parseTokenAccountData(accountInfo.data).mint,
      };
    },
  );

  return parsedSplAccounts.concat({
    pubkey: accountPublicKey,
    account,
    effectiveMint: WRAPPED_SOL_MINT,
  });
}

export const getTokenAccountInfo = memoPromise(
  getTokenAccountInfoFunc,
  (conn, accKey) => accKey.toBase58(),
);
