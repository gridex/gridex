import { Connection, PublicKey } from '@solana/web3.js';
import { getAllMarketsData } from '../markets/getAllMarketsData';
import { getTokenAccountInfo } from './getTokenAccountInfo';
import { getSelectedTokenAccountForMint } from '../../../client/utils/markets-calc';
import { Market, OpenOrders } from '@project-serum/serum';
import { MakeTransactionResult } from '../commonTypes';
import TOKEN_MINTS from '../../../common/utils/tokenMints';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';
import { USDC, USDT } from '../mints/mintsNames';

function getReferrerQuoteWallet(market: Market): PublicKey | null {
  let referrerQuoteWallet: PublicKey | null = null;

  if (market.supportsReferralFees) {
    const usdt = TOKEN_MINTS.find(({ name }) => name === USDT);
    const usdc = TOKEN_MINTS.find(({ name }) => name === USDC);
    const sol = WRAPPED_SOL_MINT;

    if (
      process.env.REACT_APP_USDT_REFERRAL_FEES_ADDRESS &&
      usdt &&
      market.quoteMintAddress.equals(usdt.address)
    ) {
      referrerQuoteWallet = new PublicKey(
        process.env.REACT_APP_USDT_REFERRAL_FEES_ADDRESS,
      );
    } else if (
      process.env.REACT_APP_USDC_REFERRAL_FEES_ADDRESS &&
      usdc &&
      market.quoteMintAddress.equals(usdc.address)
    ) {
      referrerQuoteWallet = new PublicKey(
        process.env.REACT_APP_USDC_REFERRAL_FEES_ADDRESS,
      );
    } else if (
      process.env.REACT_APP_SOL_REFERRAL_FEES_ADDRESS &&
      market.quoteMintAddress.equals(sol)
    ) {
      referrerQuoteWallet = new PublicKey(
        process.env.REACT_APP_SOL_REFERRAL_FEES_ADDRESS,
      );
    }
  }

  return referrerQuoteWallet;
}

export default async function settleByMint(
  connection: Connection,
  accountPublicKey: PublicKey,
  mint: PublicKey,
): Promise<MakeTransactionResult[]> {
  const markets = await getAllMarketsData(connection);
  const marketsToSettle = markets.filter(({ market }) => market.baseMintAddress.equals(mint) || market.quoteMintAddress.equals(mint));
  const tokenAccounts = await getTokenAccountInfo(connection, accountPublicKey);

  const transactions: MakeTransactionResult[] = [];

  for (const _market of marketsToSettle) {
    const referrerQuoteWallet = getReferrerQuoteWallet(_market.market);

    const baseCurrencyAccount = getSelectedTokenAccountForMint(
      tokenAccounts,
      _market.market.baseMintAddress,
    );
    const quoteCurrencyAccount = getSelectedTokenAccountForMint(
      tokenAccounts,
      _market.market.quoteMintAddress,
    );
    if (baseCurrencyAccount && quoteCurrencyAccount) {
      const openOrders = await OpenOrders.findForMarketAndOwner(connection, _market.market.address, accountPublicKey, _market.market.programId);
      for (const openOrder of openOrders) {
        const {
          transaction: settleFundsTransaction,
          signers: settleFundsSigners,
        } = await _market.market.makeSettleFundsTransaction(
          connection,
          openOrder,
          baseCurrencyAccount?.pubkey,
          quoteCurrencyAccount?.pubkey,
          referrerQuoteWallet,
        );
        transactions.push(new MakeTransactionResult(connection, settleFundsTransaction, settleFundsSigners));
      }
    }
  }

 return transactions;
}