import { SendOptions } from '@solana/web3.js';

/**
 * Опции отправки
 * Проверяем все перед тем как отправить транзу
 */
export const SEND_OPTIONS: SendOptions = {
  skipPreflight: false,
  preflightCommitment: 'singleGossip',
};
/**
 * Плата за трансфер
 */
export const TRANSFER_TX_FEE = 5000;
