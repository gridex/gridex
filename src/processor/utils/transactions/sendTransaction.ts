import { Connection, sendAndConfirmTransaction, Transaction, Account, TransactionSignature } from '@solana/web3.js';
import { IWallet } from '../../../common/models/wallet';

async function sendTransaction(
  connection: Connection,
  transaction: Transaction,
  signers: Account[],
): Promise<TransactionSignature>  {
  const { blockhash } = await connection.getRecentBlockhash('max');
  transaction.recentBlockhash = blockhash;
  transaction.feePayer = transaction.feePayer || signers[0].publicKey;

  const accountSigners = signers.filter(signer => signer instanceof Account);
  if (accountSigners.length) {
    transaction.partialSign(...signers);
  }

  const walletSigner = signers.find(signer => !(signer instanceof Account));
  if (walletSigner && (walletSigner as IWallet).signTransaction) {
    transaction = await (walletSigner as IWallet).signTransaction(transaction);
  }

  return await sendAndConfirmTransaction(connection, transaction, signers, {
    commitment: 'max',
    preflightCommitment: 'singleGossip',
  });
}

export default sendTransaction;
