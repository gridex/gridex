import { Account, Transaction, SystemProgram, sendAndConfirmRawTransaction } from '@solana/web3.js';
import { waitTransactionConfirmation } from './waitTransactionConfirmation';
import testsBase from '../testsBase';
import setBlockHash from './setBlockHash';
jest.setTimeout(21000);

describe('check that transaction was send', () => {
  const { connection } = testsBase('devnet');
  let confirmationData;

  describe('check old tx', () => {
    beforeEach(async (done) => {
      const account = new Account();
      await connection.requestAirdrop(account.publicKey, 10000000);
      const transaction = new Transaction();
      transaction.add(
        SystemProgram.transfer({
          fromPubkey: account.publicKey,
          toPubkey: account.publicKey,
          lamports: 10,
        }),
      );
      await setBlockHash(connection, transaction);
      transaction.feePayer = account.publicKey;
      transaction.sign(account);

      const tx = await sendAndConfirmRawTransaction(connection, transaction.serialize(), {
        skipPreflight: true,
      });

      confirmationData = await waitTransactionConfirmation(connection, tx, 20000);
      done();
    });

    it('check confirmationData', () => {
      expect(confirmationData.err).toEqual(null);
    });
  });
});

describe('check existed transaction', () => {
  const { connection } = testsBase('mainnet-beta');
  let confirmationData;

  describe('check old tx', () => {
    beforeEach(async (done) => {
      confirmationData = await waitTransactionConfirmation(
        connection,
        '3ngdbEQ1CHx5VuPWHQr4twSQhw3NZnahWBosxvSSoZP5GLLiB7ijdZu8bTgWWgUuA3VQ5f8k43BAz6RXg5ug29QK',
        20000,
      );
      console.log(confirmationData);
      done();
    });

    it('check confirmationData', () => {
      expect(confirmationData.err).toEqual(null);
    });
  });
});
