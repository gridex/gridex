import { Connection, Transaction } from '@solana/web3.js';

export default async function setBlockHash(connection: Connection, tx: Transaction): Promise<void> {
  const { blockhash } = await connection.getRecentBlockhash('max');
  tx.recentBlockhash = blockhash;
}
