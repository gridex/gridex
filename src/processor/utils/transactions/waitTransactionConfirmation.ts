import { Connection, TransactionSignature } from '@solana/web3.js';
import { sleep } from '../../../client/utils/utils';

export async function waitTransactionConfirmation(
  connection: Connection,
  txId: TransactionSignature,
  timeout: number = 10 * 1000,
): Promise<any> {
  let done = false;
  const result = await new Promise((resolve, reject) => {
    (async () => {
      setTimeout(() => {
        if (done) {
          return;
        }
        done = true;
        console.log('Timed out for txId', txId);
        reject({ timeout: true });
      }, timeout);
      while (!done) {
        // eslint-disable-next-line no-loop-func
        await (async () => {
          try {
            const signatureStatuses = await connection.getSignatureStatuses([txId], { searchTransactionHistory: true });
            const result = signatureStatuses && signatureStatuses.value[0];
            if (!done) {
              if (!result) {
                // console.log('REST null result for', txId, result);
              } else if (result.err) {
                console.log('REST error for', txId, result);
                done = true;
                reject(result.err);
              }
              // @ts-ignore
              else if (result.confirmations === null && result.err === null && result.slot > 0) {
                console.log('REST confirmed', txId, result);
                done = true;
                resolve(result);
              }
            }
          } catch (e) {
            if (!done) {
              console.log('REST connection error: txId', txId, e);
            }
          }
        })();
        await sleep(300);
      }
    })();
  });
  done = true;
  return result;
}