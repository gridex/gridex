import { calculateGrid } from './grids';
import { GridBot } from '../models/grid-bot';


const bot = {
  lowerLimitPrice: 11,
  upperLimitPrice: 35,
  gridQuantity: 10,
  quantityPerGrid: 1,
  marketTickSize: 0.001,
}

describe('grids creation test', () => {
  it('under grid', () => {
    const grid = calculateGrid(bot as GridBot, 5);
    expect(grid.length).toBe(10);
    expect(grid[4].side).toBe('sell');
  });
  it('in grid', () => {
    const grid = calculateGrid(bot as GridBot, 22);
    expect(grid.length).toBe(10);
    expect(grid[3].side).toBe('buy');
    expect(grid[4].side).toBe('wait');
    expect(grid[5].side).toBe('sell');
  });
  it('above grid', () => {
    const grid = calculateGrid(bot as GridBot, 100);
    expect(grid.length).toBe(10);
    expect(grid[4].side).toBe('buy');
  });
});
