import { GridBot } from '../models/grid-bot';
import { createRoundToDecimal } from '../../client/utils/utils';
import { Side } from '../../api/grid-bot/serum-exchange';

export function calculateCoinAmounts(
  bot: GridBot,
  currentPrice: number,
): {
  base: number;
  quote: number;
} {
  const round = createRoundToDecimal(-Math.log10(bot.marketTickSize));

  const step =
    (bot.upperLimitPrice - bot.lowerLimitPrice) / (bot.gridQuantity - 1);
  let base = 0;
  let quote = 0;

  for (let line = 0; line < bot.gridQuantity; line++) {
    let price = round(bot.lowerLimitPrice + step * line);
    // if (line === nearestPrice) continue;

    if (price < currentPrice) {
      quote += bot.quantityPerGrid * price;
    } else {
      base += bot.quantityPerGrid;
    }
  }

  return { base: round(base), quote: round(quote) };
}

export interface Line {
  side: 'buy' | 'sell' | 'wait';
  price: number;
}

export function calculateLines(upperLimitPrice, lowerLimitPrice, gridQuantity, currentPrice, marketTickSize, waitPrice?: number): Line[] {
  const round = createRoundToDecimal(-Math.log10(marketTickSize));
  waitPrice = waitPrice && round(waitPrice);
  const lines: Line[] = [];
  const step = (upperLimitPrice - lowerLimitPrice) / (gridQuantity - 1);

  const nearestPriceId = (currentPrice > upperLimitPrice || lowerLimitPrice > currentPrice)
    ? -1
    : Math.round((currentPrice - lowerLimitPrice) / step);

  for (let i = 0; i < gridQuantity; i++) {
    let price = round(lowerLimitPrice + step * i);
    let side: Side | 'wait' = price < currentPrice ? 'buy' : 'sell';

    if (
      waitPrice === price ||
      (waitPrice === 0 && i === nearestPriceId)
    ) side = 'wait';

    lines.push({ side, price });
  }

  return lines;
}

export function calculateGrid(bot: GridBot, currentPrice: number, waitPrice?: number): Line[] {
  return calculateLines(
    bot.upperLimitPrice, bot.lowerLimitPrice, bot.gridQuantity, currentPrice, bot.marketTickSize, waitPrice
  );
}

export function calculateGridNoWait(bot: GridBot, currentPrice: number): Line[] {
  return calculateLines(
    bot.upperLimitPrice, bot.lowerLimitPrice, bot.gridQuantity, currentPrice, bot.marketTickSize, undefined
  );
}
