import calcProfit, { calcProfitByCurrentPrice } from './calcProfit';

const data = {
  orders: [
    {
      size: 1,
      price: 10,
      side: 'buy',
    },
    {
      size: 1,
      price: 11,
      side: 'buy',
    },
    {
      size: 1,
      price: 12,
      side: 'buy',
    },
    {
      size: 1,
      price: 14,
      side: 'sell',
    },
    {
      size: 1,
      price: 15,
      side: 'sell',
    },
  ],
  closedOrders: [
    {
      size: 1,
      price: 11,
      side: 'buy',
    },
    {
      size: 1,
      price: 10,
      side: 'buy',
    },
    {
      size: 1,
      price: 11,
      side: 'sell',
    },
    {
      size: 1,
      price: 12,
      side: 'sell',
    },
    {
      size: 1,
      price: 11,
      side: 'buy',
    },
    {
      size: 1,
      price: 12,
      side: 'sell',
    },
    {
      size: 1,
      price: 13,
      side: 'sell',
    },
  ],
};

describe('test calc bot profit', function () {
  const { closedOrders } = data;
  let profit;

  beforeEach(() => {
    profit = calcProfit(closedOrders);
  });

  it('check profit', () => {
    expect(profit).toEqual({ base: -1, quote: 16 });
  });

  describe('calc profit for current price', () => {
    it('current price is 13.5', () => {
      const profitByCurrentPrice = calcProfitByCurrentPrice(profit, 13.5);
      expect(profitByCurrentPrice).toEqual({ base: 0.18518518518518512, quote: 2.5 });
    });

    it('current price is 12.5', () => {
      const profitByCurrentPrice = calcProfitByCurrentPrice(profit, 12.5);
      expect(profitByCurrentPrice).toEqual({ base: 0.28, quote: 3.5 });
    });
  });
});
