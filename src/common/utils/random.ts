import { customAlphabet } from 'nanoid';
// Base58 Bitcoin alphabet without lookalikes
const alphabet = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';
export const randomId = customAlphabet(alphabet, 20);
