import wait from './wait';

const retry = async (fn, sleepBetween, times = 3, error = 'Retry times exceeded') => {
  if (times < 1) {
    throw new Error(`Can't try less than 1 time: ${times}`);
  }

  for (let i = 1; i <= times; i++) {
    try {
      return await fn();
    } catch (err) {
      if (i === times) throw err;
      // console.log
    }
    await wait(sleepBetween * i);
  }
  throw new Error(error);
};

export default retry;

const WITH_RETRY = Symbol('with-retry');
export function withRetry(
  prototype: any,
  target: any,
  iteratoree: (field: any, name: string) => boolean,
) {
  if (target[WITH_RETRY]) {
    console.warn('already patched');
    return target;
  }

  for (let name of Object.getOwnPropertyNames(prototype)) {
    const field = prototype[name];
    if (iteratoree(field, name) && typeof field === 'function') {
      target[name] = function (...args) {
        const self = this;
        return retry(() => field.apply(self, args), 3000, 10);
      };
    }
  }
  target[WITH_RETRY] = true;
  return target;
}
