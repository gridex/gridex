import { TOKEN_MINTS } from '@project-serum/serum';
import { PublicKey } from '@solana/web3.js';
import { USDC, USDT, WUSDC, WUSDT } from '../../processor/utils/mints/mintsNames';

const mints: {address: PublicKey, name: string}[] = TOKEN_MINTS.map((mint) => {
  switch (mint.address.toBase58()) {
    case "Es9vMFrzaCERmJfrF4H2FYD4KCoNkY11McCe8BenwNYB":
      return {
        ...mint,
        name: USDT,
      };
    case "BQcdHdAQW1hczDbBi9hiegXAR7A98Q9jx3X3iBBBDiq4":
      return {
        ...mint,
        name: WUSDT,
      };
    case "EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v":
      return {
        ...mint,
        name: USDC,
      };
    case "BXXkv6z8ykpG1yuvUDPgh732wzVHB69RnB9YgSYh3itW":
      return {
        ...mint,
        name: WUSDC,
      };
    default: return mint;
  }
});

export default mints;
