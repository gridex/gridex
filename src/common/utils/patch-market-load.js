import { Market } from '@project-serum/serum';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';
import { struct, u8, blob } from 'buffer-layout';
import LOADED_MARKETS from './loaded_markets.json';

const MARKETS = {};

function saveMarket(key, marketData) {
  if (process.env.NODE_ENV !== 'development') return;

  LOADED_MARKETS.push(marketData);

  const fs = require('fs');
  fs.writeFileSync(__dirname + '/../loaded_markets.json', JSON.stringify(LOADED_MARKETS, null, 2));
}

// const marketLoad = Market.load;
// Market.load = async function loadMarket(connection, address, options, programId) {
//   const marketKey = `${address.toBase58()}_${programId.toBase58()}`;
//   let market = MARKETS[marketKey];
//   if (!market) {
//     market = await marketLoad.call(Market, connection, address, options, programId);
//     saveMarket(marketKey, market);
//   }
//
//   return market;
// };

function throwIfNull(value, message = 'account not found') {
  if (value === null) {
    throw new Error(message);
  }
  return value;
}

const MINT_LAYOUT = struct([blob(44), u8('decimals'), blob(37)]);

export async function getMintDecimals(connection, mint) {
  if (mint.equals(WRAPPED_SOL_MINT)) {
    return 9;
  }
  const { data } = throwIfNull(await connection.getAccountInfo(mint), 'mint not found');
  const { decimals } = MINT_LAYOUT.decode(data);
  return decimals;
}

Market.load = async function load(connection, address, options = {}, programId) {
  const marketKey = address.toBase58();
  let market = MARKETS[marketKey];
  if (market) return market;

  let marketData = LOADED_MARKETS.find((d) => d[0] === marketKey);
  let decoded;

  if (!marketData) {
    const { owner, data } = throwIfNull(
      await connection.getAccountInfo(address),
      'Market not found',
    );
    if (!owner.equals(programId)) {
      throw new Error('Address not owned by program: ' + owner.toBase58());
    }

    const decoded = this.getLayout(programId).decode(data);
    if (
      !decoded.accountFlags.initialized ||
      !decoded.accountFlags.market ||
      !decoded.ownAddress.equals(address)
    ) {
      throw new Error('Invalid market');
    }
    const [baseMintDecimals, quoteMintDecimals] = await Promise.all([
      getMintDecimals(connection, decoded.baseMint),
      getMintDecimals(connection, decoded.quoteMint),
    ]);

    marketData = [marketKey, data.toString('base64'), baseMintDecimals, quoteMintDecimals];
    saveMarket(marketKey, marketData);
  }

  decoded = decoded || this.getLayout(programId).decode(Buffer.from(marketData[1], 'base64'));
  market = new Market(decoded, marketData[2], marketData[3], options, programId);
  MARKETS[marketKey] = market;

  return market;
};
