import { Order } from '../models/grid-bot';

export type BotProfit = {
  base: number;
  quote: number;
}

export function calcProfitByCurrentPrice(profit: BotProfit, price: number): BotProfit {
  return {
    base: profit.base + profit.quote / price,
    quote: profit.quote + profit.base * price,
  }
}

export default function calcProfit(closedOrders: Order[]): BotProfit {
  // calc profit by closed orders
  return closedOrders.reduce((acc, el) => {
    if (el.side === 'buy') {
      acc.base += el.size;
      acc.quote -= el.size * el.price;
    } else {
      acc.base -= el.size;
      acc.quote += el.size * el.price;
    }

    return acc;
  }, {
    base: 0,
    quote: 0
  })
};
