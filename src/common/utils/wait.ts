const wait = (ms: number): Promise<void> => new Promise((r) => setTimeout(r, ms));

export class TimeoutError extends Error {};

export const timeout = (ms: number, msg: string) => wait(ms).then(() => {
  throw new TimeoutError(`timeout(${ms}): ` + msg);
});

export default wait;
