import { Market } from '@project-serum/serum';
import { PublicKey } from '@solana/web3.js';
import { MarketInfo } from '../../client/utils/types';

function isRejected(item: PromiseSettledResult<any>): item is PromiseRejectedResult {
  return item.status === 'rejected';
}

export type MarketEx = {
  market: Market,
  marketName: string,
  programId: PublicKey,
};


export const getAllMarkets = async (connection, marketInfos: MarketInfo[]): Promise<[MarketEx[], any[]]> => {
  const markets: MarketEx[] = [];
  const settled = await Promise.allSettled(
    marketInfos.map(async (marketInfo) => {
      const market = await Market.load(
        connection,
        marketInfo.address,
        {},
        marketInfo.programId,
      );
      markets.push({
        market,
        marketName: marketInfo.name,
        programId: marketInfo.programId,
      });
    }),
  );
  const errors: any[] = settled
    .filter(isRejected)
    .map(s => s.reason);

  return [markets, errors];
};
