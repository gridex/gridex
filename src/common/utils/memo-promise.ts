export function memoPromise<T extends (...args: any) => Promise<any>>(func: T, resolver: (...args: Parameters<T>) => string): T {
  const requests: { [key: string]: Promise<any> } = {};

  // @ts-ignore
  return async (...args) => {
    const key = resolver(...args);
    if (requests[key]) {
      return requests[key];
    }

    return requests[key] = func(...args).finally(() => {
      delete requests[key];
    });
  };
}
