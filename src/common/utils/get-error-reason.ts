import serumErrorsList from './serum_errors_list.json';

export const getErrorReason = (message = ''): string => {
  if (/0x(\d){1,2}/.test(message)) {
    const data = /0x(\d){1,2}/.exec(message);
    if (data) {
      const [code] = data;
      const id = Number(code);
      if (id && serumErrorsList[id]) {
        return serumErrorsList[id];
      }
    }
  } else if (message.includes('Node is behind by')) {
    return 'Mainnet-beta node error';
  }
  return 'Unknown error';
};
