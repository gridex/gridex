export class MessageError extends Error {
  data: any = {};

  constructor(data: any) {
    super(data.message);
    Object.assign(this.data, data);
  }
}

export class UserError extends Error {
}
