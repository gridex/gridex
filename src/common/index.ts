import { withRetry } from './utils/retry';
import './utils/patch-market-load';
import { Connection } from '@solana/web3.js';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';


globalThis.SOLANA_NODE_URL = 'https://vip-api.mainnet-beta.solana.com/';
globalThis.SOLANA_AWS_NODE_URL = 'http://3.8.240.68:8899/'; // AWS without VPN

// globalThis.SOLANA_NODE_URL = 'https://api.rpcpool.com';
// globalThis.SOLANA_NODE_URL = 'https://solana-api.projectserum.com/';
// globalThis.SOLANA_NODE_URL = 'https://api.mainnet-beta.solana.com';
// globalThis.SOLANA_NODE_URL = 'http://95.216.24.251:8899';
// globalThis.SOLANA_NODE_URL = 'http://178.154.215.182:8899';
// globalThis.SOLANA_NODE_URL = 'http://95.216.24.251:8899';
// globalThis.SOLANA_NODE_URL = 'http://51.89.218.54:8899';
// globalThis.SOLANA_NODE_URL = 'http://3.8.240.68:8899';
// globalThis.SOLANA_NODE_URL = 'http://157.90.91.168:8899';



// XXX patch connection with retried functions to make it more stable
withRetry(Connection.prototype, Connection.prototype, (_, name) => name.startsWith('get'));

// dayjs utc
dayjs.extend(utc);
