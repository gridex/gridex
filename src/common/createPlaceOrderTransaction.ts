import { Market } from '@project-serum/serum';
import { Connection, PublicKey } from '@solana/web3.js';
import { getDecimalCount } from '../client/utils/utils';
import { MakeTransactionResult } from '../processor/utils/commonTypes';
import { MessageError } from './utils/error';
import BN from 'bn.js';

export async function createPlaceOrderTransaction({
  side,
  price,
  size,
  orderType,
  market,
  connection,
  accountPublicKey,
  baseCurrencyAccount,
  quoteCurrencyAccount,
  feeDiscountPubkey,
  clientId,
}: {
  side: 'buy' | 'sell';
  price: number;
  size: number;
  orderType: 'ioc' | 'postOnly' | 'limit';
  market: Market | undefined | null;
  connection: Connection;
  accountPublicKey: PublicKey;  // Account | Wallet;
  baseCurrencyAccount: PublicKey;
  quoteCurrencyAccount: PublicKey;
  feeDiscountPubkey: PublicKey | undefined;
  clientId?: BN;
}): Promise<MakeTransactionResult> {
  clientId = clientId || new BN(Date.now()).imuln(1_000_000).iaddn(Math.round(Math.random() * 1000000));

  const formattedMinOrderSize =
    market?.minOrderSize?.toFixed(getDecimalCount(market.minOrderSize)) ||
    market?.minOrderSize;
  const formattedTickSize =
    market?.tickSize?.toFixed(getDecimalCount(market.tickSize)) ||
    market?.tickSize;
  const isIncrement = (num, step) =>
    Math.abs((num / step) % 1) < 1e-5 ||
    Math.abs(((num / step) % 1) - 1) < 1e-5;
  if (isNaN(price)) {
    throw new MessageError({ message: 'Invalid price', type: 'error' });
  }
  if (isNaN(size)) {
    throw new MessageError({ message: 'Invalid size', type: 'error' });
  }
  if (!accountPublicKey) {
    throw new MessageError({ message: 'Connect account', type: 'error' });
  }
  if (!market) {
    throw new MessageError({ message: 'Invalid  market', type: 'error' });
  }
  if (!isIncrement(size, market.minOrderSize)) {
    throw new MessageError({
      message: `Size must be an increment of ${formattedMinOrderSize}`,
      meta: {
        size,
        minOrderSize: market.minOrderSize
      },
      type: 'error',
    });
  }
  if (size < market.minOrderSize) {
    throw new MessageError({ message: 'Size too small', type: 'error' });
  }
  if (!isIncrement(price, market.tickSize)) {
    throw new MessageError({
      message: `Price must be an increment of ${formattedTickSize}`,
      type: 'error',
    });
  }
  if (price === 0) {
    price = market.tickSize;
  }
  if (price < market.tickSize) {
    throw new MessageError({ message: 'Price under tick size', type: 'error', market, price });
  }
  const owner = accountPublicKey;

  const payer = side === 'sell' ? baseCurrencyAccount : quoteCurrencyAccount;

  if (!payer) {
    throw new MessageError({
      message: 'Need an SPL token account for cost currency',
      type: 'error',
    });
  }
  const params = {
    owner,
    payer,
    side,
    price,
    size,
    orderType,
    clientId,
    feeDiscountPubkey: feeDiscountPubkey || null,
  };

  const result = new MakeTransactionResult(connection);

  result.addTransaction(market.makeMatchOrdersTransaction(5));
  result.add(await market.makePlaceOrderTransaction(
    connection,
    params,
    120_000,
    120_000,
  ));
  result.addTransaction(market.makeMatchOrdersTransaction(5));

  return result;
}
