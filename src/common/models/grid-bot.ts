import { a, getSchemaByType } from './utils';
import { Line } from '../grid-bot/grids';

export enum BotState {
  BALANCING = 'balancing',
  BALANCING_DONE = 'balancing_done',
  STOPPING = 'stopping',
  CLOSING = 'closing',
  STOPPED = 'stopped',
  READY_TO_RUN = 'ready-to-run',
  RUNNING = 'running',
  WAITING = 'waiting', // bot still running after timeout
  COOLDOWN_1 = 'cooldown-1',
  COOLDOWN_2 = 'cooldown-2',
  COOLDOWN_3 = 'cooldown-3',
  ERROR = 'error',
}

export enum BotStateNames {
  stopping = 'Stopping',
  closing = 'Closing',
  stopped = 'Stopped',
  running = 'Running',
  'ready-to-run' = 'Ready to run',
  'cooldown-1' = 'Cooldown',
  'cooldown-2' = 'Cooldown',
  'cooldown-3' = 'Cooldown',
  'error' = 'Error',
}

export type OrderSide = 'buy' | 'sell';

@a.model
export class BotError {
  @a.date
  public date: Date = new Date();

  @a.date
  public lastDate: Date = new Date();

  @a.string
  public message: string = "";

  @a.string
  public bot: string = "";

  @a.number
  public newErrors: number = 0;

  @a.number
  public count: number = 0;
}

// tslint:disable-next-line:max-classes-per-file
@a.model
export class Order implements Line {
  @a.number
  size: number = 0;
  @a.number
  price: number = 0;
  @a.enum(['sell', 'buy'])
  side: OrderSide = 'buy';

  @a.enum(['starting', 'running'])
  state?: 'starting' | 'running' = 'starting';

  @a.string
  clientId: string = '';

  @a.string
  createdAt: Date = new Date();
  @a.string
  closedAt: Date | null = null;

  constructor(values: Partial<Order>) {
    Object.assign(this, values);
  }
}

// tslint:disable-next-line:max-classes-per-file
@a.model
export class GridBot {
  public _id?: any;

  @a.string
  public account: string = '';

  @a.date
  public createdAt: Date = new Date();

  @a.date
  public updatedAt: Date = new Date();

  @a.string // client public key
  public publicKey: string = '';

  // @a.string
  // public profit: number = 0;

  @a.string
  public name: string = '';
  @a.string
  public botName: string = '';
  @a.string
  public marketAddress: string = '';
  @a.string
  public marketProgramId: string = '';
  @a.number
  public marketTickSize: number = 0;

  @a.number
  public upperLimitPrice: number = 0;
  @a.number
  public lowerLimitPrice: number = 0;

  @a.number
  public gridQuantity: number = 0;
  @a.number
  public quantityPerGrid: number = 0;

  @a.string
  public baseCurrency: string = '';
  @a.string
  public baseMintKey: string = '';
  @a.string
  public quoteCurrency: string = '';
  @a.string
  public quoteMintKey: string = '';

  @a.enum(BotState)
  public state: BotState = BotState.STOPPED;

  @a.date
  public startedAt?: Date;

  @a.array(Order)
  public orders: Order[] = [];

  @a.array(Order)
  public closedOrders: Order[] = [];

  @a.number
  public lastOrderPrice: number = 0;
}

export const GridBotSchema = getSchemaByType(GridBot);
