import { an, getSchemaByType, schema } from 'yup-decorator';

@schema(an.object().required().noUnknown().strict())
class User {}
export { User };

export const UserSchema = getSchemaByType(User);
