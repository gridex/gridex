import { mixed, date } from 'yup';
import { a as A, an, getSchemaByType, is, schema } from 'yup-decorator/dist/index';
export { getSchemaByType, an };

export const a = {
  model: schema(an.object().required().noUnknown().strict()),
  string: is(A.string().required()),
  number: is(A.number().required()),
  boolean: is(A.boolean().required()),
  enum: <E>(en: E) => is(mixed<E>().oneOf(Object.values(en))),
  array: <E>(en: E) => is(an.array().of(getSchemaByType(en))),
  date: is(date),
};
