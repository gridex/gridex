import { Transaction, PublicKey, Account } from '@solana/web3.js';

interface IEventEmitter {
  on(event: string, func: (...args: Array<any>) => any);
  off(event: string, func: (...args: Array<any>) => any);
}

export class IWallet extends Account implements IEventEmitter {
  publicKey: PublicKey;
  // eslint-disable-next-line no-undef
  secretKey: Buffer = Buffer.from('');
  autoApprove?: boolean;
  connected: boolean;
  signTransaction(transaction: Transaction): Promise<Transaction>;
  connect(): void;
  disconnect(): void;
  on(event: string, func: (...args: Array<any>) => any);
  off(event: string, func: (...args: Array<any>) => any);
}
