import { a } from './utils';

@a.model
export class TelegramBotMessage {
  public _id?: any;

  @a.date
  public date: Date = new Date();

  @a.date
  public sendDate: Date = new Date();

  @a.string
  public text: string = "";

  @a.number
  public to: number = 0;
}