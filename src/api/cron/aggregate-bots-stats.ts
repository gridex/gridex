import { gridBotsModel } from '../routes/grid-bot';

export const aggregateBotsStats = () => {
  gridBotsModel.aggregate([
    {
      $facet: {
        stats: [
          {
            $group: {
              _id: '$state',
              count: {
                $sum: 1,
              },
            },
          },
        ],
      },
    },
    {
      $addFields: {
        created_at: new Date(),
      },
    },
    {
      $merge: { into: 'bots-stats' },
    },
  ]);
};
