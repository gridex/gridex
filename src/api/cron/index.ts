import cron from 'node-cron';
import { checkCreateInvites } from './check-create-invites';
import { aggregateBotsStats } from './aggregate-bots-stats';

export default function() {
  // create invites for bots running 1+ days every day at 00:00 UTC
  cron.schedule('0 0 * * *', checkCreateInvites, { scheduled: true, timezone: 'UTC' });
  // create bots stats every minute
  cron.schedule('* * * * *', aggregateBotsStats, { scheduled: true, });
}
