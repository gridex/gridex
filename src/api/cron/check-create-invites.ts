import { BotState, GridBot } from '../../common/models/grid-bot';
import { gridBotsModel } from '../routes/grid-bot';
import moment from 'moment';
import { botKeysModel } from '../routes/bot';
import { createNewInviteCode } from '../invites/index';

export const checkCreateInvites = async () => {
  const bots: GridBot[] = await gridBotsModel.find({
    state: BotState.RUNNING, account: 'default',
  });

  bots
    .filter(bot => moment().diff(bot.startedAt) >= 1)
    .forEach(async (bot) => {
        const account = await botKeysModel.findOne({ publicKey: bot.publicKey });
        if (account) {
          await createNewInviteCode(account.botAccountPublicKey, 'bot_start');
        }
    });
};
