import model from '../utils/model';
import { BotState, GridBot } from '../../common/models/grid-bot';

export default async function _003_LastOrderPrice() {
  const botsModel = await model('grid-bots');
  const running = await botsModel.find({ state: BotState.RUNNING });
  for (let i = 0; i < running.length; i++) {
    const bot: GridBot = running[i];
    await botsModel.updateOne(bot._id, { lastOrderPrice: bot.closedOrders[0]?.price || 0 });
  }
  await botsModel.collection.updateMany({ state: { $ne: BotState.RUNNING } }, { $set: { lastOrderPrice: 0 } });
  console.log('migration3LastOrderPrice:', running.modifiedCount, 'accounts updated');
}
