import model from '../utils/model';

export default async function _002_AddPaperAccountType() {
  const keysModel = await model('bot_keys');
  const res = await keysModel.collection.updateMany({ account: { $exists: false } }, { $set: { account: 'default' } });
  console.log('migration2AddPaperAccountType:', res.modifiedCount, 'accounts updated');
}
