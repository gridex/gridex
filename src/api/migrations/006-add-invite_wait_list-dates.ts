import model from '../utils/model';

export default async function _006_add_invite_wait_list_dates() {
  const invitesModel = model("invite_wait_list").collection;
  invitesModel.updateMany({}, { $set: { createdAt: new Date() } });
}
