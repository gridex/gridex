import { gridBotsModel } from '../routes/grid-bot';
import { BotState } from '../../common/models/grid-bot';

export default async function _004_updateStartBotDate() {
  const bots = await gridBotsModel.find({
    state: BotState.RUNNING,
  });
  bots.forEach((bot) => {
    if (!bot.startedAt) {
      gridBotsModel.updateOne(bot._id, {
        startedAt: bot.updatedAt,
      });
    }
  });
}
