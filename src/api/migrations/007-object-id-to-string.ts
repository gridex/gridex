import { ObjectId } from 'mongodb';

import model from '../utils/model';
import { randomId } from '../../common/utils/random';

async function updateObject(col, object) {
  const _id = object._id;
  delete object._id;

  const res = await col.insert(object);
  await col.remove({ _id });
  return res.insertedIds[0];
}

export default async function _007_object_id_to_string() {

  const collections = [
    'auth',
    'auth-req',
    'bot_keys',
    'errors',
    'grid-bots',
    'errors',
    'invite_wait_list',
    'invites',
    'subscribers',
    'telegram_messages',
    'tg_bot_join_pending',
  ];

  const errors = model('errors').collection;


  for (let i = 0; i < collections.length; i++) {
    const colName = collections[i];
    const col = model(colName).collection;

    const objects = await col.find({}).toArray();

    for (let j = 0; j < objects.length; j++) {
      const object = objects[j];
      if (!(object._id instanceof ObjectId)) {
        const newId = await updateObject(col, object);
        if (colName === 'grid-bots') {
          await errors.updateOne({ bot: ObjectId(object._id) }, { $set: { bot: newId } });
        }
      }
    }
  }

  // change migration config id
  const config = model('config').collection;
  const migration = await config.findOne({ _id: ObjectId('migration___') });
  const oldMigration = await config.findOne({ _id: 'migration' });
  if (!migration && oldMigration) {
    await config.insert({ ...migration, _id: ObjectId('migration___') });
    await config.deleteOne({ _id: 'migration' });
  }
}
