import { gridBotsModel } from '../routes/grid-bot';
import { BotState } from '../../common/models/grid-bot';
import model from '../utils/model';

export default async function _005_addBotKyesStatus() {
  const botKeysModel = model('bot_keys').collection;
  await botKeysModel.updateMany({ activator: 'system' }, { $set: {
    createdAt: new Date('2021-01-01Z+0'),
  } });
  await botKeysModel.updateMany({ activator: { $ne: 'system' } }, { $set: {
    createdAt: new Date('2021-04-08Z+0'),
  } });
  await botKeysModel.updateMany({ activator: { $exists: true } }, { $set: {
    status: 'alpha',
  } });
}
