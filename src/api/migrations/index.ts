// migrations.
// add migrations sequentally to the end of this array
// dont change name after migration done
const migrations = [
  require('./001-set-bot-names'),
  require('./002-add-paper-account'),
  require('./003-last-order-price'),
  require('./004-update-start-bot-date'),
  require('./005-add-bot-keys-date-status'),
  require('./006-add-invite_wait_list-dates'),
  require('./006-add-bot-startedAt'),
  require('./007-paper-add-balances'),
  require('./007-object-id-to-string'),
]
export default migrations.map(r => ([r.default.name, r.default]));
