import { gridBotsModel } from '../routes/grid-bot';
import { BotState } from '../../common/models/grid-bot';
import model from '../utils/model';

export default async function _006_addBotStartedAt() {
  // day ago
  const startedAt = new Date;
  startedAt.setDate(startedAt.getDate() - 1);

  const botKeysModel = model('grid_bots').collection;
  await botKeysModel.updateMany({ status: 'running' }, { $set: { startedAt } });
}
