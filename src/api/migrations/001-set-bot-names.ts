import nameGenerator from 'name-creator';
import capitalize from 'lodash/capitalize';
import model from '../utils/model';

export default async function _001_setBotNames() {
  const botsModel = await model('grid-bots');
  const bots = await botsModel.find({});
  for (const bot of bots) {
    if (!bot.botName) {
      bot.botName = nameGenerator().raw.map(capitalize).join(" ");
      const { _id, ... rest } = bot;
      await botsModel.updateOne(_id, rest);
    }
  }
}
