import model from '../utils/model';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';

export default async function _007_AddPaperBalances() {
  const keysModel = await model('bot_keys');
  const accounts = await keysModel.collection.find({ account: 'default' }, { $set: { account: 'default' } }).toArray();
  for (let i = 0; i < accounts.length; i++) {
    const { publicKey } = accounts[i];
    if (!await keysModel.findOne({ publicKey, account: 'paper' })) {
      await keysModel.add({
        createdAt: new Date(),
        account: 'paper',
        botAccountPublicKey: 'paper',
        publicKey,
        balances: [{ mint: WRAPPED_SOL_MINT.toBase58(), name: 'SOL', amount: 100 }],
      });
    }
  }
}
