import model from "../utils/model";

export default async (req, res) => {
  const subscribersModel = await model("subscribers");
  const { email } = req.body;

  const subscribedEmails = await subscribersModel.find({ email });

  if (!subscribedEmails.length) {
    await subscribersModel.add({
      email,
      date: +new Date()
    })
  }

  return res.json({
    success: true
  });
}