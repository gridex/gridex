/* eslint-disable curly */
import { produce } from 'immer';
import { BotState, BotState as BS, GridBot } from '../../common/models/grid-bot';
import wait, { timeout, TimeoutError } from '../../common/utils/wait';
import { gridBotsModel } from '../routes/grid-bot';
import { onChangeMarketPrice } from '../routes/paper-bot';

import { createSerumExchangeFactory } from './serum-exchange';
import processGrid, { stopGrid } from './bot-function';
import { Exchange } from './exchange';
import { createPaperSerumExchangeFactory } from './serum-paper-exchange';

export async function updateBotFunc(bot: GridBot, exchange: Exchange) {
  try {
    const bs = bot.state;
    if (bs === 'closing') {
      await stopGrid(bot, exchange);
      bot.state = BS.STOPPED;
    } else if (bs === 'ready-to-run') {
      await processGrid(bot, exchange);

      bot.startedAt = new Date();
      bot.state = BS.RUNNING;
    } else if (bs === BS.STOPPING) {
      bot.state = BS.COOLDOWN_1;
    } else if (bs === BS.COOLDOWN_1) {
      bot.state = BS.COOLDOWN_2;
    } else if (bs === BS.COOLDOWN_2) {
      bot.state = BS.CLOSING;
    } else {
      await processGrid(bot, exchange);
    }

  } catch (err) {
    console.error(`Bot error: ${bot._id}`, err.message);
  }
}

export async function updateBot(bot: GridBot, exchange: Exchange): Promise<boolean> {
  const lastState = bot.state;
  const lastClosedLength = bot.closedOrders.length;
  const lastOpenLength = bot.orders.length;

  await updateBotFunc(bot, exchange);

  const needSave = !bot.updatedAt
    || lastState !== bot.state
    || lastClosedLength !== bot.closedOrders.length
    || lastOpenLength !== bot.orders.length;

  if (needSave) {
    bot.updatedAt = new Date();
  }

  return needSave;
}

const waitingBots: { [id: string]: boolean } = {};

export async function botStep(exchangeFactory, botModel) {

  async function setBotState(bot: GridBot, state: BotState) {
    bot.state = state;
    await botModel.updateOne(bot._id, { state });
  }

  const allRunningBots: GridBot[] = await botModel.find({
    state: { $exists: true, $nin: [BS.STOPPED, BS.ERROR] },
  });

  await Promise.allSettled(allRunningBots.map(async (bot: GridBot) => {
    if (waitingBots[bot._id]) return;
    try {
      const exchange = await exchangeFactory(bot.account, bot.publicKey, bot.marketAddress);
      const update = async () => {
        const needSave = await updateBot(bot, exchange);
        if (needSave === true) await botModel.updateOne(bot._id, bot);
      }
      const promise = update();
      promise
        .catch(err => {
          if (bot.state === BS.WAITING) console.log(bot._id, 'bot error while waiting', err);
          throw err;
        })
        .finally(async () => {
          if (waitingBots[bot._id]) {
            console.log(bot._id, 'recovering bot waiting -> running');
            console.timeEnd(`${bot._id}_timeout`);
            delete waitingBots[bot._id];
          }
        });
      const needSave = await Promise.race([
        promise,
        timeout(10 * 1000, 'update bot'),
      ]);
    } catch (err) {
      // timeout occured, bot still running
      if (err instanceof TimeoutError) {
        console.log(bot._id, 'bot timeout, waiting');
        console.time(`${bot._id}_timeout`);
        waitingBots[bot._id] = true;
        return;
      }
      console.warn('bot update error, continuing: ', err);
    }
  }));
}

export async function mainGridCycle() {
  // eslint-disable-next-line no-constant-condition
  const serumExchangeFactory = await createSerumExchangeFactory();
  const paperExchangeFactory = await createPaperSerumExchangeFactory();

  onChangeMarketPrice(async (clientPublicKey, marketAddress, price) => {
    // @ts-ignore
    const exchange = await paperExchangeFactory.cache.get(`${clientPublicKey}_${marketAddress}`);
    exchange?.setPrice(price);
    return !!exchange;
  });

  function exchangeFactory(account: string, publicKey, marketAddress) {
    return account === 'paper'
      ? paperExchangeFactory(publicKey, marketAddress)
      : serumExchangeFactory(publicKey, marketAddress);
  }

  let waitMs = 1000;
  // eslint-disable-next-line no-constant-condition
  while (true) {
    try {
      // @ts-ignore
      serumExchangeFactory.resetCaches();
      // @ts-ignore
      paperExchangeFactory.resetCaches();
      await botStep(exchangeFactory, gridBotsModel);
      waitMs = 1000;
    } catch (err) {
      waitMs += 1000;
      console.error('mainCycle', err);
    }
    // console.log('wait', waitMs);
    await wait(waitMs);
  }
}
