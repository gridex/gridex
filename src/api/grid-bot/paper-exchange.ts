import { Order } from '../../common/models/grid-bot';
import { Exchange } from './exchange';
import EventEmitter from 'events';
import { groupBy, without } from 'lodash';

export const getOrderId = (order) => `${order.side}_${order.size}_${order.price.toFixed(3)}`;

export type ChangeBalance = (isBase: boolean, amount: number) => void;

export class PaperExchange extends EventEmitter implements Exchange {
  market: string;
  orders: Order[] = [];
  price: number = 0;
  time: Date = new Date;
  prices: { [addr: string]: number } = {};
  makerFee: number = -0.0003; // -0.03%
  takerFee: number = +0.0022; // +0.22%
  changeBalance: ChangeBalance;

  constructor(orders, clientPublicKey: string, market: string, prices, changeBalance: ChangeBalance) {
    super();
    this.market = market;
    this.prices = prices;
    this.orders = orders || [];
    this.changeBalance = changeBalance;
  }

  async buy(size: number, price: number): Promise<Order> {
    console.log('paper buy', this.market, size, price);
    const order = new Order({ size, price, side: 'buy' });
    this.orders.push(order);
    return order;
  }

  async sell(size: number, price: number): Promise<Order> {
    console.log('paper sell', this.market, size, price);
    const order = new Order({ size, price, side: 'sell' });
    this.orders.push(order);
    return order;
  }

  async cancelOrders(orders: Order[]) {
    orders.forEach(order => {
      const orderId = getOrderId(order);
      const found = this.orders.find(o => getOrderId(o) === orderId);
      if (found) {
        this.orders = without(this.orders, found);
      }
    });
  }

  async getPrice() {
    const price = this.prices[this.market];
    if (!price) {
      throw new Error(`price not set for ${this.market}`);
    }
    return price;
  }

  async trade(orders: Order[]) {
    this.orders.push(...orders);
  }

  async getOpenOrders() {
    return this.orders;
  }

  getDate() {
    return this.time;
  }

  setKline(low: number, high: number, open: number, close: number, curTime: Date = new Date) {
    this.time = curTime;

    const closedAll = this.orders.filter(o => o.side === 'sell' ? o.price < high : low < o.price);
    closedAll.sort((a, b) => a.side === 'sell' ? a.price - b.price : b.price - a.price);

    this.prices[this.market] = close;
    this.price = close;

    for (let j = 0; j < closedAll.length; j++) {
      const closed = closedAll[j];

      this.orders = without(this.orders, closed);

      const dir = closed.side === 'sell' ? -1 : 1;
      this.changeBalance(true, (dir - this.makerFee) * closed.size);
      this.changeBalance(false, -dir * closed.size * closed.price);

      this.prices[this.market] = closed.price;
      this.price = closed.price;

      break;
    }
  }

  setPrice(price: number, curTime: Date = new Date) {
    const { true: open = [], false: closed = [] } = groupBy(
      this.orders,
      (o) => o.state !== 'running' || (o.side === 'sell' ? o.price > price : price > o.price)); // drop orders which become inside price pange
    this.time = curTime;
    this.orders = open;
    closed.forEach(o => {
      const dir = o.side === 'sell' ? -1 : 1;
      this.changeBalance(true, dir * o.size);
      this.changeBalance(true, -o.size * this.makerFee);
      this.changeBalance(false, -dir * o.size * o.price);
    });
    this.prices[this.market] = price;
    this.price = price;
  }
}

