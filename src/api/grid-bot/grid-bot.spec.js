import { BotState } from '../../common/models/grid-bot';
import model from '../utils/model';

// import { chooseCluster } from '../routes/test/util/url';
import { updateBot } from './main-cycle';
import { PaperExchange } from './paper-exchange';

jest.setTimeout(50000000);

const exchanges = {};

const SOL_USDT = 'G3uhFg2rBFunHUXtCera13vyQ5KCS8Hx3d4HohLoZbT5';
const prices = {
  [SOL_USDT]: 10.5,
};

function exchangeFactory(clientPublicKey, marketAddress) {
  const id = `${clientPublicKey}_${marketAddress}`;
  return (
    exchanges[id] || (exchanges[id] = new PaperExchange({}, clientPublicKey, marketAddress, prices))
  );
}

describe('grid-bot', () => {
  describe('test main cycle', () => {
    it('create bot', async () => {
      const botModel = await model('grid-bots');
      const bot = await botModel.findOne({ state: { $ne: BotState.STOPPED } });
      const exchange = new PaperExchange(bot, bot.publicKey, bot.marketAddress, prices);

      const p = [10, 13, 10, 25, 3];
      const data = {};
      for (let price of p) {
        prices[SOL_USDT] = price;
        await updateBot(bot, exchange, data);
      }
    });
  });
});
