import { Order } from '../../common/models/grid-bot';

export interface Exchange {
  buy(size: number, price: number): Promise<Order>;

  sell(size: number, price: number): Promise<Order>;

  trade(orders: Order[]): Promise<any>;

  cancelOrders(orders: Order[]): Promise<void>;

  getPrice(): Promise<number>;

  // current exchange date
  getDate(): Date;

  getOpenOrders(): Promise<Order[]>;

  resetCaches?(): void;
}

