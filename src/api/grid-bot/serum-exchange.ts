import { Account, Connection, PublicKey } from '@solana/web3.js';
import { getAllMarketsData, MarketData } from '../../processor/utils/markets/getAllMarketsData';
import { memoize, chunk } from 'lodash';
import getAllOpenOrdersForAccountGroupedByMarkets, {
  OpenOrdersForAccountGroupedByMarkets,
} from '../../processor/utils/open_orders/getAllOpenOrdersForAccountGroupedByMarkets';
import { loadMarketPrice } from '../../client/utils/markets-calc';
import { Exchange } from './exchange';
import {
  createPlaceOrderTransactionForMarket,
  OrderPlacer,
} from '../../processor/utils/markets/createPlaceOrderTransaction';
import { Order } from '../../common/models/grid-bot';
import sendTransaction from '../../processor/utils/transactions/sendTransaction';
import model from '../utils/model';
import BN from 'bn.js';
import wait from '../../common/utils/wait';
import { getOrderId } from './paper-exchange';
import { MakeTransactionResult } from '../../processor/utils/commonTypes';

export type Side = 'buy' | 'sell';

function serializeOrder(order) {
  return {
    ...order,
    orderId: order.orderId.toJSON(),
    priceLots: order.priceLots.toJSON(),
    sizeLots: order.sizeLots.toJSON(),
    clientId: order.clientId?.toJSON(),
    openOrdersAddress: order.openOrdersAddress.toBase58(),
  };
}
function deserializeOrder(order) {
  return {
    ...order,
    orderId: new BN(order.orderId, 'hex'),
    priceLots: new BN(order.priceLots, 'hex'),
    sizeLots: new BN(order.sizeLots, 'hex'),
    clientId: new BN(order.clientId, 'hex'),
    openOrdersAddress: new PublicKey(order.openOrdersAddress),
  };
}

export async function createSerumExchangeFactory() {
  const connection = new Connection(globalThis.SOLANA_NODE_URL);

  const allMarketsData = await getAllMarketsData(connection);

  // const ordersByMarkets = (publicKey) => getAllOpenOrdersForAccountGroupedByMarkets(connection, publicKey)
  const ordersByMarkets = memoize(
    (publicKey) => getAllOpenOrdersForAccountGroupedByMarkets(connection, publicKey),
    (publicKey) => publicKey.toBase58(),
  );
  const memoMarketPrice = memoize(
    (market) => loadMarketPrice(connection, market),
    (market) => market.address.toBase58(),
  );

  class SerumExchange implements Exchange {
    connection: Connection;
    marketData: MarketData;
    orderPlacer: OrderPlacer;
    clientPublicKey: PublicKey;
    signerAccount: Account;

    constructor(connection: Connection, clientPublicKey: string, marketAddress: string) {
      const marketData = allMarketsData.find((m) => m.address.toBase58() === marketAddress)!;

      this.orderPlacer = createPlaceOrderTransactionForMarket(connection, marketData.market);
      this.marketData = marketData;
      this.clientPublicKey = new PublicKey(clientPublicKey);
      this.connection = connection;
      this.signerAccount = new Account();

      (async () => {
        const botKeysModel = await model('bot_keys');
        const botKeys = await botKeysModel.findOne({ publicKey: clientPublicKey });
        const botAccount = new Account(
          // @ts-ignore
          Object.entries(botKeys.botAccountSecretKey).map(([, value]) => {
            return value;
          }),
        );

        this.signerAccount = botAccount;
      })();
    }

    async getPrice(): Promise<number> {
      return (await memoMarketPrice(this.marketData.market))!;
    }

    async _loadOrders(): Promise<OpenOrdersForAccountGroupedByMarkets | null> {
      let byMarkets;
      let counter = 0;
      do {
        byMarkets = await ordersByMarkets(this.signerAccount.publicKey);
        await wait(1000);
        counter++;
      } while (!byMarkets.length && counter < 5);

      const found = byMarkets.find((o) => o.marketAddress === this.marketData.address.toBase58());
      return found;
    }

    async getOpenOrders(): Promise<Order[]> {
      const orders = await this._loadOrders();
      return orders?.orders.map(serializeOrder) || [];
    }

    async cancelOrders(orders): Promise<void> {
      orders = orders.map(deserializeOrder);
      const openOrders = await this._loadOrders();
      if (!openOrders) {
        // no open orders found
        return;
      }
      const orderIds = orders.map(getOrderId);
      const foundOrders = openOrders.orders.filter((order) => orderIds.includes(getOrderId(order)));

      const { transaction, signers } = openOrders.createCancelTransaction(foundOrders);

      await sendTransaction(connection, transaction, [this.signerAccount, ...signers]);
    }

    async _trade(side: Side, size: number, price: number) {
      const result = await this.orderPlacer(this.signerAccount.publicKey, {
        side,
        size,
        price,
        orderType: 'limit',
      });
      // console.log('sending trade', side, size, price);
      try {
        result.setFeePayer(this.signerAccount);
        const signature = await result.send();
        console.log('done trade', side, size, price, signature);
      } catch (err) {
        // console.error('error', side, size, price, err);
        throw err;
      }
    }

    async trade(orders: Order[]): Promise<void> {
      const results = await Promise.all(
        orders.map(async ({ side, size, price }) => {
          // console.log('sending trade', side, size, price);
          return await this.orderPlacer(this.signerAccount.publicKey, {
            side,
            size,
            price,
            orderType: 'limit',
          });
        }),
      );

      for (const res of results) {
        try {
          await res.setFeePayer(this.signerAccount).send();
        } catch (err) {
          // console.error('error', orders, err);
          throw new Error(`${orders.map(({ side, price, size }) =>
            `Side: ${side}, price: ${price}, size: ${size}`).join('; ')} ${err.toString()}`,
          );
        }
      }
    }

    async buy(size: number, price: number): Promise<any> {
      return this._trade('buy', size, price);
    }

    async sell(size: number, price: number): Promise<any> {
      return this._trade('sell', size, price);
    }

    getDate(): Date {
      return new Date;
    }
  }

  const exchangeFactory = memoize((clientPublicKey, marketAddress: string): Exchange => {
    return new SerumExchange(connection, clientPublicKey, marketAddress);
  }, (clientPublicKey, marketAddress) => `${clientPublicKey}_${marketAddress}`);

  (exchangeFactory as any).resetCaches = () => {
    memoMarketPrice.cache.clear?.();
    ordersByMarkets.cache.clear?.();
  };

  return exchangeFactory;
}
