import { Exchange } from './exchange';
import { Connection } from '@solana/web3.js';
import { withRetry } from '../../common/utils/retry';
import { getAllMarketsData, MarketData } from '../../processor/utils/markets/getAllMarketsData';
import { memoize } from 'lodash';
import { loadMarketPrice } from '../../client/utils/markets-calc';
import model from '../utils/model';
import { gridBotsModel } from '../routes/grid-bot';
import { ChangeBalance, PaperExchange } from './paper-exchange';

export async function createPaperSerumExchangeFactory(): Promise<(clientPublicKey: string, marketAddress: string) => Promise<Exchange>> {
  // wrap all connect methods, starting with 'get' into retry function
  let connection = new Connection(globalThis.SOLANA_NODE_URL);
  connection = withRetry(Connection.prototype, connection, (_, name) => name.startsWith('get'));

  const allMarketsData = await getAllMarketsData(connection);

  const memoMarketPrice = memoize(
    (market) => loadMarketPrice(connection, market),
    (market) => market.address.toBase58(),
  );

  class SerumPaperExchange extends PaperExchange {
    // connection: Connection;
    marketData: MarketData;
    orderPlacer: any;
    clientPublicKey: string;

    constructor(connection: Connection, clientPublicKey: string, marketAddress: string, changeBalance: ChangeBalance, orders) {
      const marketData = allMarketsData.find((m) => m.address.toBase58() === marketAddress)!;
      super(orders, clientPublicKey, marketAddress, {}, changeBalance);

      this.marketData = marketData;
      this.clientPublicKey = clientPublicKey;

      this.getPrice();
    }

    async getPrice(): Promise<number> {
      if (!this.marketData) {
        return 0;
      } // no constructor yet run
      const price = (await memoMarketPrice(this.marketData.market))!;

      if (!price) {
        throw new Error(`price not set for ${this.market}`);
      }
      this.setPrice(price);
      return price;
    }

    getDate(): Date {
      return new Date;
    }
  }


  const botKeysModel = await model('bot_keys');
  const balances = {};

  async function createChangeBalance(clientPublicKey, marketAddress): Promise<ChangeBalance> {

    let botBalances = balances[clientPublicKey];
    if (!botBalances) {
      const botKeys = await botKeysModel.findOne({ publicKey: clientPublicKey, account: 'paper' });
      balances[clientPublicKey] = botBalances = botKeys.balances;
    }

    const { market } = allMarketsData.find((m) => m.address.toBase58() === marketAddress)!;

    return (isBase: boolean, amount: number) => {
      const mintAddress = (isBase ? market.baseMintAddress : market.quoteMintAddress).toBase58();
      let balance = botBalances.find(b => b.mint === mintAddress);
      if (!balance) {
        balance = { mint: mintAddress, name: mintAddress, amount: 0 };
        botBalances.push(balance);
      }
      if (balance.amount + amount < 0) {
        throw new Error('wrong balance');
      }
      balance.amount += amount;

      botKeysModel.update({ publicKey: clientPublicKey, account: 'paper' }, { balances: botBalances });
    };
  }

  const exchangeFactory = memoize(
    async (clientPublicKey, marketAddress): Promise<Exchange> => {
      const bots = await gridBotsModel.find({ publicKey: clientPublicKey, marketAddress, account: 'paper' });
      const orders = bots.flatMap(b => b.orders);
      const changeBalance = await createChangeBalance(clientPublicKey, marketAddress);
      return new SerumPaperExchange(connection, clientPublicKey, marketAddress, changeBalance, orders);
    },
    (clientPublicKey, marketAddress) => `${clientPublicKey}_${marketAddress}`,
  );

  (exchangeFactory as any).resetCaches = () => {
    memoMarketPrice.cache.clear?.();
  };

  return exchangeFactory;
}
