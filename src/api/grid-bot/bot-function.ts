import { BotState, GridBot, Order } from '../../common/models/grid-bot';
import { calculateGrid, calculateGridNoWait } from '../../common/grid-bot/grids';
import { Exchange } from './exchange';
import { without, last, memoize } from 'lodash';
import model from '../utils/model';
import { errorsModel } from '../routes/grid-bot';
import { getErrorReason } from '../../common/utils/get-error-reason';

const orderToIdFormatter = memoize((quantityPerGrid, marketTickSize) => memoize((order, short = true) =>
  short
    ? `${order.size || quantityPerGrid}_${order.price.toFixed(-Math.log10(marketTickSize))}`
    : `${order.side}_${order.size || quantityPerGrid}_${order.price.toFixed(-Math.log10(marketTickSize))}`,
), (quantityPerGrid, marketTickSize) => quantityPerGrid * 100000 + marketTickSize);

function skipTimedOut(promise: Promise<any>) {
  return promise.catch((err) => {
    if (!err.message.startsWith('Transaction was not confirmed')) {
      throw err;
    } else {
      console.warn(err.message);
    }
  });
}

async function analyzeBotError(bot: GridBot, e) {
  logBotError(bot, e);
  const reason = getErrorReason(e.message);
  if (reason === 'InsufficientFunds') { // 34 Insufficient funds - stop bot
    console.warn('stopping bot', bot._id, 'due to error', reason);
    bot.state = BotState.ERROR;
  }
}

async function logBotError(bot: GridBot, e) {
  const message = e.toString();
  const error = await errorsModel.findOne(
    { bot: bot._id, message },
    { sort: { lastError: -1 } },
  );

  if (!error) {
    const errorObj = {
      bot: bot._id,
      message,
      date: new Date(),
      count: 1,
      lastError: new Date(),
    };
    await errorsModel.add(errorObj);
  } else {
    await errorsModel.collection.updateOne({ _id: error._id }, {
      $set: { lastError: new Date() },
      $inc: {
        count: 1,
        newErrors: 1,
      },
    });
  }
}

export default async function updateGrid(bot: GridBot, exchange: Exchange): Promise<void> {
  const openOrders = await exchange.getOpenOrders();
  const currentPrice = await exchange.getPrice();
  // check all current orders: grid and bot.
  const getOrderId = orderToIdFormatter(bot.quantityPerGrid, bot.marketTickSize);
  const ordersToCreate: Order[] = [];

  function logBot(...params) {
    if (bot.account !== 'backtest') console.log(bot.name, ...params);
  }

  function checkOrders(grid) {
    for (const line of grid) {
      const lineId = getOrderId(line);

      const exchangeOrder = openOrders.find((o) => getOrderId(o) === lineId);
      const botOrder = bot.orders.find((o) => getOrderId(o) === lineId);

      if (exchangeOrder) {
        // we received exchange order on line, it is ok, just check it
        if (botOrder?.state === 'starting') {
          logBot('order appeared', getOrderId(botOrder));
          (Object as any).assign(botOrder, exchangeOrder);
          botOrder.state = 'running';
          delete bot.updatedAt;
        } else if (!botOrder) {
          bot.orders.push({ ...exchangeOrder, state: 'running' });
        }
      } else {
        if (botOrder) {
          if (botOrder.state === 'starting') {
            const timeout = 2 * 60 * 1000; // check timeout 2 min
            if (+exchange.getDate() - +botOrder.createdAt < timeout) {
              continue;
            } else {
              logBot('not appeared in', timeout);
            }
          }
          bot.orders = without(bot.orders, botOrder);
          botOrder.closedAt = exchange.getDate();
          bot.closedOrders.push(botOrder);
          bot.lastOrderPrice = botOrder.price;
          logBot('order closed', getOrderId(botOrder));
        } else if (line.side !== 'wait') {
          // start new
          const order = new Order({
            side: line.side,
            price: line.price,
            size: bot.quantityPerGrid,
            state: 'starting',
            createdAt: exchange.getDate(),
          });
          ordersToCreate.push(order);
        }
      }
    }
  }

  // firstly check closed orders
  checkOrders(bot.orders);

  // and then
  const grid = calculateGrid(bot, currentPrice, bot.lastOrderPrice);
  checkOrders(grid);

  if (ordersToCreate.length > 0) {
    logBot('creating', ordersToCreate);
  }

  await Promise.allSettled(ordersToCreate.map(async (order) => {
    try {
      logBot('starting order', getOrderId(order));
      await skipTimedOut(exchange.trade([order]));
      bot.orders.push(order);
    } catch (err) {
      await analyzeBotError(bot, err);
    }
  }));
}

export async function stopGrid(bot: GridBot, exchange: Exchange) {
  if (bot.orders.length) {
    console.log('cancelling bot orders', bot._id);
    await skipTimedOut(exchange.cancelOrders(bot.orders));
    bot.orders.forEach(order => {
      order.closedAt = exchange.getDate();
    });
    bot.closedOrders.push(...bot.orders);
    bot.orders = [];
    bot.lastOrderPrice = 0;
    delete bot.startedAt;
    console.log('done cancelling bot orders', bot._id);
  }
}
