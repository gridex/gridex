import './utils/source-map';
import './utils/unhandled-promise-rejection';
import '../common';
import * as express from 'express';

import session from './middlewares/session';
import bodyParser from './middlewares/bodyParser';
import auth from './middlewares/auth';
import router from './router';

export default (app: express.Application) => {
  bodyParser(app);
  session(app);
  app.use(auth);
  router(app);
};
