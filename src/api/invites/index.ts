import { botKeysModel } from '../routes/bot';
import model from '../utils/model';
import nameGenerator from 'name-creator';
import lowerCase from 'lodash/lowerCase';

export const invitesModel = model('invites');

export async function getInvitedStatus(publicKey: string): Promise<boolean> {
  const account = 'default';
  const data = await botKeysModel.findOne({ publicKey, account });

  return Boolean(data.activator);
}

export const checkInviteCode = async (publicKey: string, code: string): Promise<boolean> => {
  const alreadyInvited = await getInvitedStatus(publicKey);
  if (alreadyInvited) {
    return true;
  }

  const invite = await invitesModel.findOne({
    code,
    activation_date: null,
    activated_account: null,
  });
  if (invite) {
    await botKeysModel.update({
      publicKey,
      account: 'default',
    }, {
      activator: invite.activator,
      status: 'alpha',
    });
    await invitesModel.updateOne(invite._id, {
      activation_date: new Date(),
      activated_account: publicKey,
    });

    return true;
  }

  return false;
};

export const generateInviteCode = async (activator: string): Promise<string> => {
  const randomNames = nameGenerator().raw.map(lowerCase);
  const part1 = randomNames[0].split('').reverse().join('').substring(0, 3);
  const part2 = randomNames[1].split('').reverse().join('').substring(0, 3);

  return `${activator.substring(0, 3)}_${part1}${part2}`.toLowerCase();
};

export const createNewInviteCode = async (activator: string, reason: string): Promise<string> => {
  let code;
  do {
    code = await generateInviteCode(activator);
  } while (!(await invitesModel.find({ code })));
  await invitesModel.add({
    createdAt: new Date(),
    activator,
    code,
    reason,
  });
  return code;
};

export const getActivatorInvites = async (activator: string): Promise<any[]> => {
  return await invitesModel.find({
    activator,
  });
};
