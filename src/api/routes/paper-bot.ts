import { Market } from '@project-serum/serum';

import model from '../utils/model';
import { botKeysModel } from './bot';
import { PaperExchange } from '../grid-bot/paper-exchange';
import { GridBot } from '../../common/models/grid-bot';
import { fetchKlines } from './klines';
import { createRoundToDecimal } from '../../client/utils/utils';
import { calculateCoinAmounts } from '../../common/grid-bot/grids';
import { botStep, updateBot } from '../grid-bot/main-cycle';
import { clusterApiUrl, PublicKey, Connection } from '@solana/web3.js';
import TOKEN_MINTS from '../../common/utils/tokenMints';
import { getMarketInfos, loadMarketPrice, USE_MARKETS } from '../../client/utils/markets-calc';
import { useAllMarkets } from '../../client/utils/markets';
import { getAllMarketsData } from '../../processor/utils/markets/getAllMarketsData';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';

function error(res, error: string) {
  res.json({ success: false, error });
  throw new Error(error);
}

export const gridBotsModel = model('grid-bots');


type ChangePriceCallback = (publicKey: string, marketAddress: string, price: number) => any;

export function setPaperBotPrice(req, res) {
  const { marketAddress, price } = req.body;
  const { publicKey } = req.session;

  changePriceHandlers.forEach(cb => cb(publicKey, marketAddress, price));

  res.json({ success: true });
}

const changePriceHandlers: ChangePriceCallback[] = [];

export function onChangeMarketPrice(fn: ChangePriceCallback) {
  changePriceHandlers.push(fn);
}

export async function changePaperBalance(req, res) {
  const { mintAddress, name, amount } = req.body;
  const { publicKey } = req.session;

  const account = await botKeysModel.findOne({ publicKey, account: 'paper' });
  let balance = account.balances.find(b => b.mint === mintAddress);
  if (balance) {
    balance.amount += amount;
  } else {
    balance = { mint: mintAddress, name, amount };
    account.balances.push(balance);
  }

  await botKeysModel.updateOne(account._id, { balances: account.balances });

  return res.json({ success: true });
}


async function findMarket(connection, mint1, mint2) {
  const markets = await getAllMarketsData(connection, false);
  return markets.find(m => (
    m.market.baseMintAddress.equals(mint1) && m.market.quoteMintAddress.equals(mint2) ||
    m.market.baseMintAddress.equals(mint2) && m.market.quoteMintAddress.equals(mint1)
  ));
}

export async function exchangePaperBalance(req, res) {
  const { fromMint, fromAmount, toMint } = req.body;
  const { publicKey } = req.session;

  const account = await botKeysModel.findOne({ publicKey, account: 'paper' });
  const fromAcc = account.balances.find(b => b.mint === fromMint);
  if (!fromAcc) {
    return error(res, 'balance not found');
  }

  let toAcc = account.balances.find(b => b.mint === toMint);
  if (!toAcc) {
    const token = TOKEN_MINTS.find(t => t.address.toBase58() === toMint);
    if (!token) {
      return error(res, `Token ${toMint} not found`);
    }
    toAcc = { mint: token.address.toBase58(), name: token.name, amount: 0 };
    account.balances.push(toAcc);
  }
  if (!(fromAmount <= fromAcc.amount)) {
    return error(res, 'insufficient funds');
  }

  const connection = new Connection(clusterApiUrl('mainnet-beta'));
  /////////////////////////////
  const fromPub = new PublicKey(fromMint);
  const toPub = new PublicKey(toMint);
  const market = await findMarket(connection, fromPub, toPub);
  if (!market) {
    return error(res, 'market not found');
  }
  const marketPrice = await loadMarketPrice(connection, market.market) || 0;
  const price = market.market.baseMintAddress.equals(fromPub) ? marketPrice : 1 / marketPrice;

  fromAcc.amount -= fromAmount;
  toAcc.amount += fromAmount * price * 0.995; // emulate loss on pool change

  await botKeysModel.updateOne(account._id, { balances: account.balances });

  res.json({ success: true });
}

export async function paperBacktest(bot: GridBot, publicKey, klines) {
  const { baseCurrency: base, quoteCurrency: quote } = bot;

  const amounts = calculateCoinAmounts(bot, klines[0].o);
  const balances = {
    [base]: amounts.base,
    [quote]: amounts.quote,
  };
  const counts = {
    buy: 0,
    sell: 0,
  };



  function changeBalance(isBase, amount) {
    const mint = isBase ? base : quote;
    balances[mint] += amount;
  }

  const exchange = new PaperExchange([], publicKey, bot.marketAddress, {}, changeBalance);


  for (let i = 0; i < klines.length; i++) {
    const kline = klines[i];
    const date = new Date(kline.t * 1000);
    exchange.setKline(kline.l, kline.h, kline.o, kline.c, date);
    await updateBot(bot, exchange);
  }

  const gains = {
    base: balances[base] - amounts.base,
    quote: balances[quote] - amounts.quote,
  };

  return {
    trades: bot.closedOrders,
    amounts,
    balances,
    gains,
  };
}

export async function paperBacktestRequest(req, res) {
  const { bot, startTime, endTime }: { bot: GridBot, startTime?: number, endTime?: number } = req.body;
  const { publicKey } = req.session;

  const baseCurrency = /USDT/.test(bot.baseCurrency) ? "USDT" : (/USDC/.test(bot.baseCurrency) ? "USDC" : bot.baseCurrency);
  const quoteCurrency = /USDT/.test(bot.quoteCurrency) ? "USDT" : (/USDC/.test(bot.quoteCurrency) ? "USDC" : bot.quoteCurrency);

  const klines = await fetchKlines({
    interval: 1,
    endTime: endTime || Math.trunc(Date.now() / 1000),
    startTime,
    limit: 20000, // 5 days
    symbol: `${baseCurrency}/${quoteCurrency}`,
  });

  const result = await paperBacktest(bot, publicKey, klines);

  res.json({ success: true, ...result });
}
