import { sync as uid } from 'uid-safe';
import model from '../utils/model';

export default async function (req, res) {
  const { accountPublicKey } = req.body;

  const authReqModel = await model('auth-req');

  const sessionId = req.session.id;
  const secret = uid(24);

  await authReqModel.upsert(
    { sessionId, accountPublicKey },
    {
      createdAt: new Date(),
      secret,
    },
  );

  return res.json({ success: true, secret });
}
