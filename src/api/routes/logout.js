module.exports = async function (req, res) {
  const { publicKey } = req.session;
  const { save_session } = req.body;

  if (publicKey) {
    if (save_session) {
      req.session.lastAuthorizedPublicKey = publicKey;
    }
    req.session.publicKey = null;
    req.session.save();
  }

  res.json({
    success: true,
  });
};
