import { paperBacktest } from './paper-bot';
import { BotState, GridBot } from '../../common/models/grid-bot';
import { fetchKlines } from './klines';
import { calculateGrid } from '../../common/grid-bot/grids';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';

import { CustomConsole, LogType, LogMessage } from '@jest/console';

jest.setTimeout(60000);


function simpleFormatter(type: LogType, message: LogMessage): string {
  const TITLE_INDENT = '    ';
  const CONSOLE_INDENT = TITLE_INDENT + '  ';

  return message
    .split(/\n/)
    .map(line => CONSOLE_INDENT + line)
    .join('\n');
}

global.console = new CustomConsole(process.stdout, process.stderr, simpleFormatter);

const publicKey = 'some-key';
const BOT1: GridBot = {
  _id: 'bot1',
  publicKey,
  createdAt: new Date,
  updatedAt: new Date,
  botName: 'vasya',
  "account": "backtest",
  "marketAddress": "9wFFyRfZBsuAha4YcuxcXLKwMxJR43S7fPfQLusDBzvT",
  "marketProgramId": "9xQeWvG816bUx9EPjHmaT23yvVM2ZWbrrpZb9PusVFin",
  "marketTickSize": 0.001,
  "orders": [],
  "closedOrders": [],
  "state": BotState.READY_TO_RUN,
  "name": "SOL/USDC",
  "baseCurrency": "SOL",
  "baseMintKey": WRAPPED_SOL_MINT.toBase58(),
  "quoteCurrency": "USDC",
  "quoteMintKey": "EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v",
  "upperLimitPrice": 16,
  "lowerLimitPrice": 14,
  "gridQuantity": 2,
  "quantityPerGrid": 3,
  "lastOrderPrice": 0,
}

const BOT2 = {
  ...BOT1,
  "gridQuantity": 21,
  "quantityPerGrid": 0.3
}

describe('grid-bot', () => {
  it('calc-grid', () => {
    const grid = calculateGrid(BOT1, 14.5, undefined);
    expect(grid[0]).toEqual({ price: 14, side: 'buy' });
    expect(grid[1]).toEqual({ price: 16, side: 'sell' })
  })
  it('calc-grid with wait', () => {
    const grid = calculateGrid(BOT1, 14.5, 0);
    expect(grid[0]).toEqual({ price: 14, side: 'wait' });
    expect(grid[1]).toEqual({ price: 16, side: 'sell' })
  })
  it('calc-grid with price wait 14', () => {
    const grid = calculateGrid(BOT1, 14.5, 14);
    expect(grid[0]).toEqual({ price: 14, side: 'wait' });
    expect(grid[1]).toEqual({ price: 16, side: 'sell' })
  })
  it('calc-grid with price wait 16', () => {
    const grid = calculateGrid(BOT1, 14.5, 16);
    expect(grid[0]).toEqual({ price: 14, side: 'buy' });
    expect(grid[1]).toEqual({ price: 16, side: 'wait' })
  })
  it('calc-grid with price wait line other', () => {
    const bot = {
      ...BOT1,
      upperLimitPrice: 16.001,
    }
    const grid = calculateGrid(bot, 14.5, 16.0010001);
    expect(grid[0]).toEqual({ price: 14, side: 'buy' });
    expect(grid[1]).toEqual({ price: 16.001, side: 'wait' })
  })
})

describe('paper-bot', () => {
  it('trade SOL/USDC from 15 to 30 mar 2021', async () => {
    const bot = BOT2;
    const startTime = new Date('03/15/2021');
    const endTime = new Date('03/30/2021');

    const klines = await fetchKlines({
      interval: 1,
      endTime: Math.ceil(endTime.getTime() / 1000),
      startTime: startTime && Math.ceil(startTime.getTime() / 1000),
      limit: 50000, // 5 days
      symbol: `${bot.baseCurrency}/${bot.quoteCurrency}`,
    });

    console.time('backtest');
    const result = await paperBacktest(bot, publicKey, klines);
    console.timeEnd('backtest');
    expect(result.gains).not.toBeUndefined();
    expect(result.trades).not.toBeUndefined();

    for (let i = 1; i < result.trades.length; i++) {
      const trade = result.trades[i];
      // we cant trade next order the same price, have to wait
      // if (trade.price === result.trades[i-1].price) debugger;
      expect(trade.price).not.toEqual(result.trades[i-1].price);
    }

    console.log(result.balances);
  });
})
