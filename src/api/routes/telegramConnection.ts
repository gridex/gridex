import model from '../utils/model';
import nameGenerator from "name-creator";
import lowerCase from 'lodash/lowerCase';

export const generateTgJoinCode = async () => {
  const randomNames = nameGenerator().raw.map(lowerCase);
  const part1 = randomNames[0].substring(0, 3);
  const part2 = randomNames[1].substring(0, 3);
  const botCode = `join_${part1}-${part2}`.toLowerCase();
  const tgBotJoinPending = model('tg_bot_join_pending');
  const tgBotPendingData = await tgBotJoinPending.findOne({
    botCode,
    expiresIn: { $gt: Date.now() },
  });
  if (tgBotPendingData) {
    generateTgJoinCode();
  } else {
    return botCode;
  }
}

export const checkTelegramConnection = async (req, res) => {
  const { TELEGRAM_BOT_NAME, TELEGRAM_JOIN_LINK_LIFETIME = '60000' } = process.env;
  try {
    const { publicKey } = req.session;
    const { rewrite } = req.body;
    const botKeys = model('bot_keys');
    const tgBotJoinPending = model('tg_bot_join_pending');
    const botKeysData = await botKeys.findOne({ publicKey });

    if (!rewrite) {
      if (botKeysData && botKeysData.telegramId) {
        return res.status(200).send();
      } else {
        return res.status(201).json({
          wait: true,
        }).send();
      }
    }

    const joinLink = `https://t.me/${TELEGRAM_BOT_NAME}?start=join_${publicKey}`;

    if (botKeysData && botKeysData.telegramId) {
      return res.status(200).send();
    } else {
      const botCode = await generateTgJoinCode();
      const tgBotPendingData = await tgBotJoinPending.findOne({
        publicKey,
        expiresIn: { $gt: Date.now() },
      });

      if (tgBotPendingData) {
        res.json(tgBotPendingData).status(201).send();

      } else {
        const newPendingData = await tgBotJoinPending.add({
          joinLink,
          publicKey,
          expiresIn: Date.now() + parseInt(TELEGRAM_JOIN_LINK_LIFETIME, 10),
          botCode,
        });
        res.status(201).json(newPendingData.ops[0]).send();
      }
    }
  } catch (err) {
    res.status(500).json(err).send();
  }
}

export const unlinkTelegram = async (req, res) => {
  try {
    const { publicKey } = req.session;
    const botKeys = model('bot_keys');
    const botKeysData = await botKeys.findOne({ publicKey });

    if (botKeysData && botKeysData.telegramId) {
      await botKeys.updateOne(botKeysData._id,{telegramId: ''});
      res.status(200).send();
    } else {
      res.status(404).send()
    }
  } catch (err) {
    res.json(err).status(500).send();
  }
}
