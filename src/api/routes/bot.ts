import model from '../utils/model';
import { Account, Connection, PublicKey, Transaction } from '@solana/web3.js';
import setBlockHash from '../../processor/utils/transactions/setBlockHash';
import { createTokenAccountTransaction } from '../../client/utils/send';
import { getTokenAccountInfo } from '../../processor/utils/balance/getTokenAccountInfo';
import { GridBot } from '../../common/models/grid-bot';
import { gridBotsModel } from './grid-bot';
import { SEND_OPTIONS } from '../../processor/utils/send-options';
import { waitTransactionConfirmation } from '../../processor/utils/transactions/waitTransactionConfirmation';
import { checkInviteCode, getActivatorInvites, getInvitedStatus } from '../invites';

export const botKeysModel = model("bot_keys");

export const getBotDataByAccountPublicKey = async (publicKey, account) => {
  return await botKeysModel.findOne({ publicKey, account });
}

export const getBotPublicKey = async (req, res) => {
  const { publicKey } = req.session;
  const { account = 'default' } = req.query;

  const data = await getBotDataByAccountPublicKey(publicKey, account);

  if (!data) {
    return res.json({
      success: false,
    });
  }

  const { botAccountPublicKey, balances, activator, isAdmin } = data;

  return res.json({
    success: true,
    botAccountPublicKey,
    isAdmin,
    balances,
    invited: Boolean(activator),
  });
}

export const checkInvitedStatus = async (req, res) => {
  const { publicKey } = req.session;

  return res.json({
    invited: await getInvitedStatus(publicKey)
  })
}

export const activateInviteCode = async (req, res) => {
  const { publicKey } = req.session;
  const { code } = req.body;

  return res.json({
    success: await checkInviteCode(publicKey, code)
  });
}

export const getAccountInvites = async (req, res) => {
  const { publicKey } = req.session;
  const data = await getBotDataByAccountPublicKey(publicKey, "default");
  let invites: any[] = [];

  if (data) {
    invites = await getActivatorInvites(data.botAccountPublicKey);
  }

  return res.json({
    invites,
  })
}

export const sendBotTransaction = async (req, res) => {
  const { publicKey } = req.session;
  const data = await botKeysModel.find({ publicKey });
  if (!data || !data.length) {
    return res.json({
      success: false,
    });
  }

  try {
    const [{ botAccountSecretKey }] = data.filter((bot) => bot.botAccountSecretKey);
    const { transaction: serializedTransaction } = req.body;

    // @ts-ignore
    const botAccount = new Account(Object.entries(botAccountSecretKey).map(([, value]) => value));
    const transaction = Transaction.from(serializedTransaction.data);
    const connection = new Connection(globalThis.SOLANA_NODE_URL);
    await setBlockHash(connection, transaction);
    transaction.feePayer = botAccount.publicKey;
    transaction.partialSign(botAccount);
    transaction.signatures.forEach(s => {
      console.log(s.publicKey.toBase58());
    })

    const signature = await connection.sendRawTransaction(transaction.serialize(), SEND_OPTIONS);
    try {
      const confirmation = await waitTransactionConfirmation(connection, signature, 30000);
      return res.json({
        confirmationError: false,
        confirmation,
        success: true,
        signature
      });
    } catch(confirmationError) {
      return res.json({
        confirmationError,
        confirmation: false,
        success: true,
        signature
      });
    }

  } catch (error) {
    return res.json({
      success: false,
      error: error?.toString()
    });
  }
}

async function loadBotAccount(publicKey: string) {
  const botKeys = await botKeysModel.findOne({ publicKey });
  if (!botKeys) {
    throw new Error('bot keys not found');
  }

  // @ts-ignore
  botKeys.account = new Account(Object.entries(botKeys.botAccountSecretKey).map(([, value]) => value));

  return botKeys;
}

export const createBotTokenAccounts = async (req, res) => {
  const { publicKey } = req.session;

  try {
    const botAccount = await loadBotAccount(publicKey);
    const { botId } = req.body;
    const bot: GridBot = await gridBotsModel.findOne({ _id: botId, publicKey });
    if (!bot) throw new Error('404');

    // @ts-ignore
    const connection = new Connection(globalThis.SOLANA_NODE_URL);
    const tokenAccounts = await getTokenAccountInfo(connection, botAccount.account.publicKey);
    const transaction = new Transaction();

    async function checkCreateAccount(mintKey): Promise<[Transaction|null, Account|null]> {
      if (!tokenAccounts.find(token => token.effectiveMint.toBase58() === mintKey)) {
        const { transaction, signer, newAccountPubkey } = await createTokenAccountTransaction({
          connection,
          wallet: botAccount.account,
          mintPublicKey: new PublicKey(mintKey)
        });
        return [transaction, signer];
      }
      console.log('account', mintKey, 'exists');
      return [null, null];
    }

    const [t1, s1] = await checkCreateAccount(bot.baseMintKey);
    const [t2, s2] = await checkCreateAccount(bot.quoteMintKey);
    if (t1 || t2) {
      const transaction = new Transaction();
      if (t1) transaction.add(t1);
      if (t2) transaction.add(t2);
      const signers = [botAccount.account, s1, s2].filter(Boolean);

      await setBlockHash(connection, transaction);
      transaction.feePayer = botAccount.account.publicKey;
      await connection.sendTransaction(transaction, signers, SEND_OPTIONS);
    }

    return res.json({
      success: true,
    });

  } catch (error) {
    console.log(error);
    return res.json({
      success: false,
      error
    });
  }
}
