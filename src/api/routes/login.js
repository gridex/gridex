import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';

const solanaWeb3 = require('@solana/web3.js');
import model from '../utils/model';

const connections = {
  gridex: new solanaWeb3.Connection(globalThis.SOLANA_AWS_NODE_URL),
  ['mainnet-beta']: new solanaWeb3.Connection(globalThis.SOLANA_NODE_URL),
  devnet: new solanaWeb3.Connection('https://devnet.solana.com'),
};

async function getAuthSecret(accountPublicKey, sessionId) {
  const authReqModel = await model('auth-req');
  const authReq = await authReqModel.findOne({
    sessionId,
    accountPublicKey,
  });
  if (authReq) {
    await authReqModel.delete(authReq._id);
    return authReq.secret;
  }
  return null;
}

async function ensureBotKeys(publicKey) {
  const botKeysModel = await model('bot_keys');
  const keyExists = await botKeysModel.findOne({
    publicKey,
    account: 'default',
  });

  if (keyExists) {
    return keyExists;
  }

  const account = new solanaWeb3.Account();
  const botAccountPublicKey = account.publicKey.toBase58();
  const botAccountSecretKey = account.secretKey;
  await botKeysModel.add({
    createdAt: new Date(),
    account: 'default',
    botAccountPublicKey,
    botAccountSecretKey,
    publicKey,
    tokens: [],
  });
  await botKeysModel.add({
    createdAt: new Date(),
    account: 'paper',
    botAccountPublicKey: 'paper',
    publicKey,
    balances: [{ mint: WRAPPED_SOL_MINT.toBase58(), name: 'SOL', amount: 100 }],
  });

  return await botKeysModel.findOne({ publicKey, account: 'default' });
}

module.exports = async function (req, res) {
  const { transaction, endpoint, accountPublicKey, impersonatePublicKey } = req.body;
  const connection = connections[endpoint] || connections['mainnet-beta'];

  if (!connection) {
    res.json({ success: false, error: 'wrong endpoint' });
    return;
  }

  const trans = solanaWeb3.Transaction.from(transaction.data);
  if (!trans.verifySignatures()) {
    res.json({ success: false });
    return;
  }

  let publicKey;
  try {
    publicKey = trans.signatures[0].publicKey.toBase58();
  } catch (e) {
    res.json({ success: false, error: 'Can not parse transaction signature' });
    return;
  }

  if (publicKey !== accountPublicKey) {
    res.json({ success: false, error: 'Wrong public key' });
    return;
  }

  // check assingWithSeed transaction seed
  const instruction = solanaWeb3.SystemInstruction.decodeAssignWithSeed(trans.instructions[0]);

  const secret = await getAuthSecret(accountPublicKey, req.session.id);
  if (secret !== instruction.seed) {
    res.json({ success: false, error: 'Wrong secret' });
    return;
  }

  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const authModel = await model('auth');

  const botKeys = await ensureBotKeys(publicKey);

  // try to authorize different key as current
  if (impersonatePublicKey) {
    if (!botKeys.isAdmin) {
      res.json({ success: false, error: 'Wrong permissions' });
      return;
    } else {
      publicKey = impersonatePublicKey;
    }
  }

  const authorizations = await authModel.find({
    ip,
    publicKey,
  });

  if (authorizations.length) {
    await authModel.deleteMany(authorizations.map(({ _id }) => _id));
  }

  await authModel.add({
    ip,
    publicKey,
    active: true,
  });

  req.session.publicKey = publicKey;
  req.session.save();

  res.json({
    success: true,
  });
};
