import {
  Account,
  BPF_LOADER_PROGRAM_ID,
  BpfLoader,
  Connection,
  PublicKey,
  SystemProgram,
  Transaction,
  Version
} from '@solana/web3.js';
import fs from "fs";
import BufferLayout from 'buffer-layout';

import { urlTls } from './util/url';
import { Store } from './util/store';
import { newAccountWithLamports } from './util/new-account-with-lamports';

const pathToProgram = 'program/solana_gridex.so';

export class GridexApp {
  static instance: GridexApp;
  static loading: Promise<GridexApp>|undefined;
  static store: Store = new Store();

  newAccountDataLayout = BufferLayout.struct([
    BufferLayout.u32('numGreets'),
  ]);
  connection: Connection;
  gridexAccount: Account;
  programId: PublicKey;

  version: Version | null = null;
  url: string;

  private constructor(url: string, connection: Connection, gridexAccount: Account, programId: PublicKey) {
    this.connection = connection;
    this.url = url;
    this.gridexAccount = gridexAccount;
    this.programId = programId;
  }

  static async getInstance(url: string): Promise<GridexApp> {
    if (!GridexApp.instance) {
      if (this.loading) return this.loading;
      this.loading = (async () => {
        const connection = new Connection(globalThis.SOLANA_NODE_URL);
        const gridexAccount = await GridexApp.getGridexAccount(connection);
        const programId = await GridexApp.loadProgram(connection, gridexAccount);
        GridexApp.instance = new GridexApp(url, connection, gridexAccount, programId);

        await GridexApp.instance.establishConnection();
        this.loading = undefined;
        return GridexApp.instance;
      })();
      return this.loading;
    }
    return GridexApp.instance;
  }

  async establishConnection(): Promise<void> {
    if (!this.version) {
      this.version = await this.connection.getVersion();
    }
    console.log('Connection to cluster established:', this.url, this.version);
  }

  static async getGridexAccount(connection: Connection, storageName: string = 'account.json'): Promise<Account> {
    let account;
    try {
      const config = await GridexApp.store.load(storageName);
      account = new Account(JSON.parse(`[${config.secret_key}]`));
    } catch {
      account = await newAccountWithLamports(connection, 10_000_000_000);
      await GridexApp.store.save(storageName, {
        secret_key: account.secretKey.toString(),
        public_key: account.publicKey.toString(),
      });
    }
    return account;
  }

  /**
   * Load the BPF program if not already loaded
   */
  static async loadProgram(connection: Connection, gridexAccount: Account): Promise<PublicKey> {
    let programId: PublicKey;
    try {
      const config = await GridexApp.store.load('config.json');
      programId = new PublicKey(config.programId);
      await connection.getAccountInfo(programId);
    } catch (err) {
      // Load the program
      console.log('Loading program...');
      const data = await fs.readFileSync(pathToProgram);
      const programAccount = new Account();
      await BpfLoader.load(
        connection,
        gridexAccount,
        programAccount,
        data,
        BPF_LOADER_PROGRAM_ID,
      );
      programId = programAccount.publicKey;
      console.log('Program loaded to account', programId.toBase58());

      // Save this info for next time
      await GridexApp.store.save('config.json', {
        url: urlTls,
        programId: programId.toBase58(),
      });
    }

    return programId;
  }

  async serializeAssignTransaction(pubkey: PublicKey): Promise<Buffer> {
    if (!this.connection) {
      throw new Error("Please set connection");
    }
    if (!this.gridexAccount) {
      throw new Error("Please load gridex account");
    }
    if (!this.programId) {
      throw new Error("Please load program");
    }

    const space = this.newAccountDataLayout.span;
    const lamports = await this.connection.getMinimumBalanceForRentExemption(
      this.newAccountDataLayout.span,
    );

    const transaction = new Transaction().add(
      // SystemProgram.createAccount({
      //   fromPubkey: this.gridexAccount.publicKey,
      //   newAccountPubkey: pubkey,
      //   lamports,
      //   space,
      //   programId: this.programId,
      // }),
      SystemProgram.createAccount({
        fromPubkey: this.gridexAccount.publicKey,
        newAccountPubkey: pubkey,
        lamports,
        space,
        programId: SystemProgram.programId,
      }),
    );
    const { blockhash } = await this.connection.getRecentBlockhash();
    transaction.recentBlockhash = blockhash;
    transaction.feePayer = this.gridexAccount.publicKey;
    transaction.partialSign(this.gridexAccount);

    return transaction.serialize({
      requireAllSignatures: false,
      verifySignatures: false,
    });
  }
}
