import { Account, Transaction, PublicKey } from '@solana/web3.js';
import { dappAssign, testDrop } from './dapp_test';
import dotenv from 'dotenv';
import { url } from './util/url';
import { sleep } from './util/sleep';
import { GridexApp } from './GridexApp';

dotenv.config();

jest.setTimeout(60000);

describe('Request to test dapp', function () {
  let connection;
  let wallet;
  let req;
  let transaction;
  let app;

  beforeEach(async function (done) {
    app = await GridexApp.getInstance(url);

    connection = app.connection;
    wallet = new Account();

    req = {
      session: {
        publicKey: wallet.publicKey.toBase58(),
        account: wallet,
      },
    };
    done();
  });

  describe('create local wallet', function () {
    let response;
    let programId;
    let res = {
      json: (data) => {
        response = JSON.parse(JSON.stringify(data));
      },
    };

    beforeEach(async function (done) {
      await dappAssign(req, res);
      const { tx, programId: pid } = response;
      transaction = Transaction.from(tx.data, []);
      transaction.partialSign(wallet);
      programId = pid;
      done();
    });

    describe('sign transaction', function () {
      let tx;
      let programAccountsCount;

      beforeEach(async function (done) {
        const serialized_transaction = transaction.serialize();
        let { length } = await connection.getProgramAccounts(
          new PublicKey(programId),
          'singleGossip',
        );
        programAccountsCount = length;
        tx = await connection.sendRawTransaction(serialized_transaction, {
          commitment: 'singleGossip',
          preflightCommitment: 'singleGossip',
        });

        done();
      });

      describe('test tx', function () {
        it('check transaction hash', function () {
          expect(typeof tx).toEqual('string');
          console.log(
            `tx: https://explorer.solana.com/tx/${tx}?cluster=devnet`,
            '\n',
            `account: https://explorer.solana.com/address/${wallet.publicKey.toBase58()}?cluster=devnet`,
          );
        });
      });

      describe('test dropping lamports', () => {
        let lamports;
        beforeEach(async function (done) {
          await connection.requestAirdrop(wallet.publicKey, 1000000000);
          while (!lamports || lamports < 1000000000) {
            lamports = await connection.getBalance(wallet.publicKey);
          }

          done();
        });

        it('move 100 lamports from account', async function (done) {
          await testDrop(
            {
              ...req,
              body: {
                lamports: 100,
              },
            },
            res,
          );
          let new_lamports = lamports;
          let i = 0;
          while (!new_lamports || new_lamports === lamports || i++ < 20) {
            new_lamports = await connection.getBalance(wallet.publicKey);
          }
          expect(new_lamports).toEqual(lamports - 100);

          done();
        });
      });

      describe('test program accounts', function () {
        let programAccounts;
        beforeEach(async function (done) {
          let accountsCount = programAccountsCount;
          while (accountsCount === programAccountsCount) {
            programAccounts = await connection.getProgramAccounts(
              new PublicKey(programId),
              'singleGossip',
            );
            accountsCount = programAccounts.length;
            await sleep(300);
          }
          done();
        });

        it('Account is assigned to program', function () {
          const hasAccount = programAccounts.find(({ pubkey }) => {
            return pubkey.toBase58() === wallet.publicKey.toBase58();
          });
          expect(!!hasAccount).toBeTruthy();
        });
      });
    });
  });
});
