import {
  PublicKey, sendAndConfirmTransaction,
  Transaction, TransactionInstruction,
  SystemProgram,
} from '@solana/web3.js';
import BufferLayout from 'buffer-layout';
import { url } from './util/url';
import { GridexApp } from "./GridexApp";

export async function dappGetProgramKey(req, res) {
  const app = await GridexApp.getInstance(url);

  res.json({
    programId: app.programId.toBase58()
  })
}

export async function dappAssign(req, res) {
  const app = await GridexApp.getInstance(url);
  const { publicKey } = req.session;
  const tx = await app.serializeAssignTransaction(new PublicKey(publicKey));

  res.json({
    tx,
    programId: app.programId.toBase58()
  });
}

export async function testDrop(req, res) {
  const app = await GridexApp.getInstance(url);
  const { publicKey } = req.session;
  const { lamports } = req.body;

  const accountPublicKey = new PublicKey(publicKey);
  // const inst = withdrawInstruction(app, new PublicKey(publicKey), lamports);
  const instruction = SystemProgram.transfer({
    fromPubkey: accountPublicKey,
    toPubkey: accountPublicKey,
    lamports,
  });
  instruction.keys.forEach(k => { k.isSigner = false; });
  // instruction.keys.unshift({
  //   pubkey: instruction.programId,
  //   isSigner: false,
  //   isWritable: false,
  // });
  // instruction.keys.unshift({
  //   pubkey: app.gridexAccount.publicKey,
  //   isSigner: true,
  //   isWritable: false,
  // });
  // instruction.programId = app.programId;

  const transaction = new Transaction().add(instruction);
  transaction.recentBlockhash = (await app.connection.getRecentBlockhash()).blockhash;
  transaction.feePayer = app.gridexAccount.publicKey;
  transaction.partialSign(app.gridexAccount);
  transaction.serialize();

  const tx = await sendAndConfirmTransaction(
    app.connection,
    transaction,
    [app.gridexAccount],
    {
      commitment: 'singleGossip',
      preflightCommitment: 'singleGossip',
    },
  );

  res.json({
    tx
  })
}

function withdrawInstruction(app, accountPubkey, lamports) {
  const { gridexAccount, programId } = app;

  const dataLayout = BufferLayout.struct([
    BufferLayout.u8('instruction'),
    BufferLayout.u8('lamports'),
  ]);

  const data = Buffer.alloc(dataLayout.span);

  dataLayout.encode({
    instruction: 0,
    lamports,
  }, data);

  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: accountPubkey, isSigner: false, isWritable: true},
      {pubkey: gridexAccount.publicKey, isSigner: true, isWritable: true},
    ],
    programId,
    data
  });

  return instruction;
}
