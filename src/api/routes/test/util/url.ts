// To connect to a public cluster, set `export LIVE=1` in your
// environment. By default, `LIVE=1` will connect to the devnet cluster.

import { clusterApiUrl, Cluster } from '@solana/web3.js';
import dotenv from 'dotenv';

export function chooseCluster(): Cluster | undefined {
  dotenv.config();
  switch (process.env.CLUSTER) {
    case 'devnet':
    case 'testnet':
    case 'mainnet-beta': {
      return process.env.CLUSTER;
    }
  }
  throw new Error(
    `Unknown cluster "${process.env.CLUSTER}", check the .env file`,
  );
}

export const cluster = chooseCluster();

export const url = process.env.CLUSTER
  ? clusterApiUrl(cluster, false)
  : 'http://localhost:8899';

export const urlTls = process.env.CLUSTER
  ? clusterApiUrl(cluster, true)
  : 'http://localhost:8899';

export const walletUrl =
  process.env.WALLET_URL || 'https://solana-example-webwallet.herokuapp.com/';
