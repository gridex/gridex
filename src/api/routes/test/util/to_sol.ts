import { LAMPORTS_PER_SOL } from '@solana/web3.js';

export const toSol = (lamports: number) => lamports / LAMPORTS_PER_SOL;
