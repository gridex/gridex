/**
 * Simple file-based datastore
 */

import path from 'path';
import fs from 'fs';
import mkdirp from 'mkdirp';

type Config = {[key: string]: any};

export class Store {
  rootDir: string;
  constructor(rootDir: string = __dirname) {
    this.rootDir = rootDir;
    const dir = this.getDir();
    fs.exists(dir, async (ok) => {
      if (!ok) {
        fs.mkdirSync(dir);
      }
    })
  }
  getDir(uri = ''): string {
    return path.join(this.rootDir, 'store', uri);
  }

  async load(uri: string): Promise<Config> {
    const filename = this.getDir(uri);
    const data = await fs.readFileSync(filename, 'utf8');
    return JSON.parse(data) as Config;
  }

  async save(uri: string, config: Config): Promise<void> {
    await mkdirp(this.getDir());
    const filename = this.getDir(uri);
    await fs.writeFileSync(filename, JSON.stringify(config, null, 2), 'utf8');
  }
}
