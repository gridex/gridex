import {
  Transaction,
  sendAndConfirmTransaction,
  TransactionInstruction,
  PublicKey,
  SystemProgram,
} from '@solana/web3.js';
import BL from 'buffer-layout';
import dotenv from 'dotenv';
import { GridexApp } from './GridexApp';
import { url } from './util/url';

dotenv.config();

jest.setTimeout(60000);

const SEED = 'balance';

function encode(layout, data) {
  const bl = BL.struct(layout);
  const buffer = Buffer.alloc(bl.span);
  bl.encode(data, buffer);

  return buffer;
}

const ProxyProgram = {
  programId: '',
  async deposit({ fromPubkey, lamports }) {
    const data = encode([BL.nu64('lamports'), BL.u8('instruction')], {
      lamports,
      instruction: 0x1,
    });

    const [derivedKey] = await this._derivedKey(fromPubkey);
    const keys = [
      { pubkey: fromPubkey, isSigner: true, isWritable: true },
      { pubkey: derivedKey, isSigner: false, isWritable: true },
      { pubkey: SystemProgram.programId, isSigner: false, isWritable: false },
    ];
    return new TransactionInstruction({
      programId: this.programId,
      keys,
      data,
    });
  },
  async withdraw({ toPubkey, lamports }) {
    const data = encode([BL.nu64('lamports'), BL.u8('instruction')], {
      lamports,
      instruction: 0x2,
    });

    const [derivedKey] = await this._derivedKey(toPubkey);
    const keys = [
      { pubkey: derivedKey, isSigner: false, isWritable: true },
      { pubkey: toPubkey, isSigner: true, isWritable: true },
      { pubkey: SystemProgram.programId, isSigner: false, isWritable: false },
      { pubkey: this.programId, isSigner: false, isWritable: false },
    ];
    return new TransactionInstruction({
      programId: this.programId,
      keys,
      data,
    });
  },

  async _derivedKey(fromKey) {
    return await PublicKey.findProgramAddress(
      [fromKey.toBuffer(), Buffer.from(SEED, 'utf8')],
      this.programId,
    );
  },
};

// function rawStringToArray( str ) {
//   var idx, len = str.length, arr = new Array( len );
//   for ( idx = 0 ; idx < len ; ++idx ) {
//     arr[ idx ] = str.charCodeAt(idx) & 0xFF;
//   }
//   // You may create an ArrayBuffer from a standard array (of values) as follows:
//   return new Uint8Array( arr );
// }

async function createAndCallProgram() {
  const app = await GridexApp.getInstance(url);
  ProxyProgram.programId = app.programId;

  const connection = app.connection;

  async function sendTransaction(transaction, signer) {
    const { blockhash } = await app.connection.getRecentBlockhash();
    transaction.recentBlockhash = blockhash;
    transaction.feePayer = signer.publicKey;
    transaction.sign(signer);

    const tx = await sendAndConfirmTransaction(
      app.connection,
      transaction,
      [signer],
      {
        commitment: 'singleGossip',
        preflightCommitment: 'singleGossip',
      },
    );

    return tx;
  }

  const wallet = await GridexApp.getGridexAccount(
    connection,
    'test-wallet.json',
  );

  const walletBalance = await connection.getBalance(wallet.publicKey);
  console.log('wallet balance', walletBalance);
  console.log(
    'gridex balance',
    await connection.getBalance(app.gridexAccount.publicKey),
  );

  {
    // deposit
    const lamports = 100000;
    await sendTransaction(
      new Transaction().add(
        await ProxyProgram.deposit({
          fromPubkey: wallet.publicKey,
          lamports,
        }),
      ),
      wallet,
    );

    let depositBalance = walletBalance;
    let i = 0;
    while (!depositBalance || depositBalance === walletBalance || i++ < 20) {
      depositBalance = await connection.getBalance(wallet.publicKey);
    }
    expect(depositBalance).toEqual(walletBalance - (lamports + 5000) /* fee */);

    // withdraw
    await sendTransaction(
      new Transaction().add(
        await ProxyProgram.withdraw({
          toPubkey: wallet.publicKey,
          lamports,
        }),
      ),
      wallet,
    );

    let withdrawBalance = depositBalance;
    i = 0;
    while (!withdrawBalance || withdrawBalance === depositBalance || i++ < 20) {
      withdrawBalance = await connection.getBalance(wallet.publicKey);
    }
    expect(withdrawBalance).toEqual(walletBalance - 10000);
  }

  // const tx = await app.serializeAssignTransaction(wallet.publicKey);
  // if (1) {
  //   console.log('creating program account');
  //
  //   const space = app.newAccountDataLayout.span;
  //   const minLamports = await app.connection.getMinimumBalanceForRentExemption(
  //     app.newAccountDataLayout.span,
  //   );
  //
  //   // const program_id = app.program_id;
  //   // const transaction = new Transaction().add(
  // SystemProgram.createAccount({});
  //   //     fromPubkey: app.gridexAccount.publicKey,
  //   //     newAccountPubkey: pubkey,
  //   //     minLamports,
  //   //     space,
  //   //     programId: app.programId,
  //   //   }),
  //     //
  //     // SystemProgram.createAccountWithSeed({
  //     //   fromPubkey: app.gridexAccount.publicKey,
  //     //   newAccountPubkey: wallet.publicKey,
  //     //
  //     //   basePubkey: wallet.publicKey,
  //     //   seed: 'some-seed',
  //     //
  //     //   lamports: minLamports,
  //     //   space,
  //     //   programId,
  //     // }),
  //   // );
  //   const { blockhash } = await app.connection.getRecentBlockhash();
  //   transaction.recentBlockhash = blockhash;
  //   transaction.feePayer = app.gridexAccount.publicKey;
  //   transaction.sign(wallet, app.gridexAccount);
  //   // transaction.partialSign(wallet);
  //
  //   // let { length } = await connection.getProgramAccounts(
  //   //   new PublicKey(programId),
  //   //   'singleGossip',
  //   // );
  //   const transactionResult = await connection.sendRawTransaction(
  //     transaction.serialize(),
  //     {
  //       commitment: 'singleGossip',
  //       preflightCommitment: 'singleGossip',
  //     },
  //   );
  //
  //   console.log('creating program account', 'DONE', transactionResult);
  // } else {
  //   const accounts = await connection.getProgramAccounts(app.programId);
  //   console.log(accounts);
  // }

  //// test drop
  {
    // instruction.keys.unshift({
    //   pubkey: instruction.programId,
    //   isSigner: false,
    //   isWritable: false,
    // });
    // instruction.keys.unshift({
    //   pubkey: app.gridexAccount.publicKey,
    //   isSigner: true,
    //   isWritable: false,
    // });
    // instruction.programId = app.programId;
    // const transaction = new Transaction().add(
    //   await ProxyProgram.deposit({
    //     fromPubkey: wallet.publicKey,
    //     lamports: 100000,
    //   }),
    // );
    // await sendTransaction(transaction, app.gridexAccount);
  }
}

describe('test program-call', function () {
  beforeAll(() => {});

  it('program-call', createAndCallProgram);
});
