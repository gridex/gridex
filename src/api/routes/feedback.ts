import { addMessage } from '../../telegram/functions';

export default async function (req, res) {
  const chatId = process.env.TELEGRAM_SUPPORT_CHAT_ID;
  try {
    if (chatId) {
      await addMessage({
        to: parseInt(chatId, 10),
        text: `Новая заявка.\n\n${req.body.title}: ${req.body.message}`,
      });
      res.status(201).send();
    } else {
      const message = process.env.NODE_ENV === 'development'
        ? 'No telegram chat id in env variables'
        : 'Something wrong, try again later.';
      throw new Error(message);
    }
  } catch (err) {
    res.status(500).json(err).send();
  }
}
