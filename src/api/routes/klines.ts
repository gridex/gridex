import axios from 'axios';
import { map } from 'lodash';

const requests: { [key: string]: Promise<any> } = {};

export function fetchKlines(params: string | { [param: string]: any }) {
  if (typeof params !== 'string') {
    params = map(params, (v, k) => `${encodeURIComponent(k)}=${encodeURIComponent(v)}`).join('&');
  }
  return axios(`${process.env.CHART_DATA_URL}?${params}`)
    .then(response => response.data)
    .then(klines => {
      klines.forEach((k, i) => {
        // k.high = k.low;
        k.open = i > 0 ? klines[i - 1].c : k.o;
        k.close = k.c;
        k.high = k.h;
        k.low = k.l;
        k.volume = k.v;

        if (k.h / k.o > 1.05) k.high = k.o * 1.05;
        if (k.l / k.o < 0.95) k.low = k.o * 0.95;
        // k.price = k.price / symbolInfo.pricescale;
      });

      return klines;
    });
}

export const getKlines = async (req, res) => {
  const [, params] = req.url.split('/api/klines?');
  if (params) {
    if (!requests[params]) {
      requests[params] = fetchKlines(params)
        .catch((e) => {
          console.log(e);
          return [];
        });
    }
    const data = await requests[params];
    return res.json(data);
  }
  return res.json([]);
};

export const checkKlines = async (req, res) => {
  const { symbol } = req.body;
  axios(`${process.env.CHART_DATA_URL}?symbol=${symbol}&startTime=1615199449&endTime=1615199600&limit=1000&interval=60`)
    .then(response => {
      res.json({
        success: response.data.length > 0,
      });
    })
    .catch((e) => {
      console.log(e);
      res.json({
        success: false,
      });
    });
};
