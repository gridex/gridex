import { ValidationError } from 'yup';
import dayjs from 'dayjs';
import { ObjectId } from 'mongodb';
import nameGenerator from 'name-creator';

import { BotState, GridBot, GridBotSchema } from '../../common/models/grid-bot';
import model from '../utils/model';
import { calculateGrid } from '../../common/grid-bot/grids';
import capitalize from 'lodash/capitalize';
import { getInvitedStatus } from '../invites';

function error(res, error: string) {
  res.status(400).json({ success: false, error });
}

export const gridBotsModel = model('grid-bots');
export const errorsModel = model('errors');
export const getErrorsModel = () => errorsModel;

export async function getGridBots(req, res) {
  const { publicKey } = req.session;
  const { account = { $in: ['paper', 'default'] } } = req.query;

  const bots = await gridBotsModel.find({ publicKey, account });

  for (const bot of bots) {
    bot.errorsCount = await errorsModel.size({
      bot: bot._id,
    });
    bot.newErrorsCount = 0;
    if (bot.errorsCount > 0) {
      const [error] = await errorsModel.find({
        bot: bot._id,
      // @ts-ignore
      }, 1, 1);
      bot.newErrorsCount = error.newErrors || 0;
    }
  }
  res.json(bots);
}

export async function getBotErrors(req, res) {
  const { botId } = req.params;
  const { page, pageSize } = req.body;
  const errorsModel = await getErrorsModel();

  const errors = await errorsModel.find({
    bot: new ObjectId(botId),
  }, pageSize, page);

  const total = await errorsModel.size({
    bot: new ObjectId(botId),
  });

  res.json({ errors, total });
}

export async function markErrorsAsOld(req, res) {
  const { botId } = req.params;
  const errorsModel = await getErrorsModel();
  const errors = await errorsModel.find({
    bot: botId,
  }, 1, 1);

  if (errors.length) {
    const [error] = errors;
    await errorsModel.updateOne(error._id, {
      newErrors: 0
    });
  }

  res.json({ success: true });
}

export async function createGridBot(req, res) {
  const data = req.body;
  const { publicKey } = req.session;
  const invited = getInvitedStatus(publicKey);

  if (data.account === "default" && !invited) {
    return error(res, 'You are not invited');
  }

  try {
    const nowAt = dayjs().utc().format();
    data.createdAt = nowAt;
    data.updatedAt = nowAt;
    data.lastOrderPrice = 0;
    data.botName = nameGenerator().raw.map(capitalize).join(" ");
    const bot = GridBotSchema.validateSync({ ...data, publicKey });

    const result = await gridBotsModel.add(bot);

    res.json({ success: true, botId: result.insertedId });
  } catch (err) {
    if (err instanceof ValidationError) {
      console.error('wrong bot format', err);
      error(res, 'wrong bot format');
      return;
    }
    console.error('createGridBot', err);
    error(res, 'internal error');
  }
}

export async function deleteGridBot(req, res) {
  const { botId } = req.params;
  const { publicKey } = req.session;

  const result = await gridBotsModel.delete(botId, { publicKey });

  if (result.deletedCount === 1) {
    res.json({ success: true });
  } else {
    res.status(404).json({ success: false });
  }
}

export async function enableGridBot(req, res) {
  const { botId } = req.params;
  const { enabled: enabling } = req.body;
  const { publicKey } = req.session;

  const bot: GridBot = await gridBotsModel.findOne({ _id: botId });
  if (!bot) {
    throw new Error('404');
  }

  const enablingStates = {
    true: [BotState.STOPPED, BotState.ERROR],
    false: [BotState.RUNNING],
  }

  if (!enablingStates[enabling].includes(bot.state)) {
    throw new Error('wrong state');
  }

  // check if we have bot with same lines and sizes, to distinguish between orders
  const grid = calculateGrid(bot, 0);
  let sameLines;
  do {
    const size = bot.quantityPerGrid;
    const query = { $or: grid.map(({ price }) => ({ order: { $elemMatch: { price, size } } })) };
    sameLines = await gridBotsModel.find({ _id: { $ne: botId }, publicKey, ...query });
    if (sameLines.length) {
      console.warn('found same lines for different bot, changing quantity', sameLines);
      bot.quantityPerGrid -= bot.marketTickSize;
    }
  } while (sameLines.length);

  const result = await gridBotsModel.update(
    { _id: botId, publicKey },
    { state: enabling ? BotState.READY_TO_RUN : BotState.STOPPING },
  );

  if (result.modifiedCount === 1) {
    res.json({ success: true });
  } else {
    res.status(404).json({ success: false });
  }
}
