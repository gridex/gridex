import express from 'express';
import * as dotenv from 'dotenv';

dotenv.config();

import api from "./index";
import { mainGridCycle } from './grid-bot/main-cycle';
import { telegramBotLoop } from '../telegram/loop';
import { connectMongo } from './utils/mongo';
import cron from './cron';
import migrate from './migrator';

async function main() {
  await connectMongo();

  await migrate();

  const app: express.Application = express();

  api(app);

  app.listen(9000, () => {
    console.log(`Server started on port ${9000}`);
  });

  cron();

  if (process.env.TELEGRAM_BOT_ENABLED) {
    telegramBotLoop().then(console.log, console.error);
  }

  mainGridCycle().catch(console.error);
}

main().then(console.log, console.error);
