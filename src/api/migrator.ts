import { MigrationSet } from 'migrate';
import model from './utils/model';
import { promisify } from 'util';

import migrations from './migrations';

const MIGRATION_ID = 'migration___';
const config = model('config');
const mongoStore = {
  save(data, cb) {
    const { lastRun, migrations } = data;
    return config.upsert({ _id: MIGRATION_ID }, { lastRun, migrations }).then(res => (cb?.(null, data),res), cb);
  },
  load(cb?: any) {
    return config.findOne({ _id: MIGRATION_ID });
  }
};

export default async function doMigrate() {
  const set = new MigrationSet(mongoStore);
  // load and restore config from mongo
  const config = await mongoStore.load();
  set.lastRun = config?.lastRun;

  migrations.forEach(m => {
    set.addMigration(...m);
  });

  if (config?.migrations) {
    set.migrations.forEach(m => {
      m.timestamp = config.migrations.find(cm => cm.title === m.title)?.timestamp;
    });
  }

  // run up migrations
  await promisify(cb => set.up(cb))();
  await promisify(cb => set.save(cb))();

  console.log('migrations successfully ran');
}
