import login from './routes/login';
import loginRequest from './routes/login-request';
import logout from './routes/logout';
import ping from './routes/ping';
import feedback from './routes/feedback';
import {
  setPaperBotPrice,
  changePaperBalance,
  paperBacktestRequest,
  exchangePaperBalance,
} from './routes/paper-bot';
import {
  getGridBots,
  createGridBot,
  deleteGridBot,
  enableGridBot,
  getBotErrors,
  markErrorsAsOld,
} from './routes/grid-bot';
import { dappGetProgramKey, dappAssign, testDrop } from './routes/test/dapp_test';
import {
  getBotPublicKey,
  sendBotTransaction,
  createBotTokenAccounts,
  checkInvitedStatus,
  activateInviteCode,
  getAccountInvites,
} from './routes/bot';
import subscribe from './landing/subscribe';
import { checkTelegramConnection, unlinkTelegram } from './routes/telegramConnection';
import { checkKlines, getKlines } from './routes/klines';

function createMethod(app, method) {
  return function (url, func) {
    app[method](url, function (req, res) {
      return func(req, res)
        .then((res) => {
          // console.error('route result', url, res);
        })
        .catch((err) => {
          // if (err instanceof UserError) {
          //   res.json({ success: false, error: err.message });
          // }
          console.error('route error', url, err);
          throw err;
        });
    });
  };
}

export default function (app) {
  const post = createMethod(app, 'post');
  const get = createMethod(app, 'get');
  const del = createMethod(app, 'delete');

  post('/api/login', login);
  post('/api/login-req', loginRequest);
  post('/api/logout', logout);
  post('/api/ping', ping);

  post('/api/feedback', feedback);

  get('/api/invited_status', checkInvitedStatus);
  post('/api/activate_invite_code', activateInviteCode);
  get('/api/invites', getAccountInvites);

  get('/api/grid-bot', getGridBots);
  post('/api/grid-bot/:botId/enable', enableGridBot);
  post('/api/grid-bot/:botId/errors', getBotErrors);
  del('/api/grid-bot/:botId/errors', markErrorsAsOld);
  post('/api/grid-bot', createGridBot);
  del('/api/grid-bot/:botId', deleteGridBot);

  post('/api/paper-bot/set-price', setPaperBotPrice);
  post('/api/paper-bot/change-balance', changePaperBalance);
  post('/api/paper-bot/exchange', exchangePaperBalance);
  post('/api/paper-bot/backtest', paperBacktestRequest);

  get('/api/dapp-program', dappGetProgramKey);
  post('/api/dapp-assign', dappAssign);
  post('/api/dapp-drop', testDrop);

  get('/api/bot/public_key', getBotPublicKey);
  post('/api/bot/send_transaction', sendBotTransaction);
  post('/api/bot/create_token_accounts', createBotTokenAccounts);

  post('/api/landing/subscribe', subscribe);

  get('/api/klines', getKlines);
  post('/api/check-klines', checkKlines);

  post('/api/telegram-connection-status', checkTelegramConnection);
  get('/api/telegram-unlink', unlinkTelegram);
}
