import { isEqual } from 'lodash';
import { clusterApiUrl, Connection } from '@solana/web3.js';
import { INSTRUCTION_LAYOUT } from '@project-serum/serum/lib/instructions';
import {
  Market,
  MARKETS,
  TOKEN_MINTS,
  Orderbook,
  TokenInstructions,
} from '@project-serum/serum';
import { chooseCluster } from '../routes/test/util/url';
import { sleep } from '../../client/utils/utils';

export async function startFetchingBlocks() {
  const connection = new Connection(globalThis.SOLANA_NODE_URL);

  console.log('first block', await connection.getFirstAvailableBlock());
  console.log(await connection.getConfirmedBlock(1));

  const startSlot = await connection.getSlot('max');
  const waitTime = 450;
  let slot = startSlot;
  // eslint-disable-next-line no-constant-condition
  while (true) {
    let block;
    try {
      block = await connection.getConfirmedBlock(slot);
    } catch (err) {
      if (err.message.includes('was skipped')) { // block skipped is ok
        slot++;
        continue;
      }
      // if (err.message.includes('502 Bad Gateway')) continue; // not ok, retry this block
      console.warn('block', err.message.slice(0, 100));
      sleep(waitTime);

      continue;
    }

    block.transactions.forEach(({ transaction, meta }) => {
      let found = false;
      transaction.instructions.forEach((instruction) => {
        const [first, command] = instruction.data;
        if (first !== 0 || command > 8) return; // we search only market orders

        const programId = instruction.programId;
        const address = instruction.keys[0]?.pubkey;

        if (!address) return;

        const foundMarket = MARKETS.find((market) =>
          programId.equals(market.programId) && address.equals(market.address)
        );
        if (foundMarket) {
          found = true;
          const data = INSTRUCTION_LAYOUT.decode(instruction.data);
          // console.log('market instruction', command, foundMarket.name, data);
          if (command === 1 && data.newOrder.orderType !== 'postOnly') {
            meta.inner
          }
          // found market transaction
        }
      });

      if (found) {
        if (!isEqual(meta.preBalances, meta.postBalances) && meta.postBalances[0] - meta.preBalances[0] !== -5000) {
          // console.log(
          //   'was trade',
          //   meta.postBalances.map((bal, i) => bal - meta.preBalances[i])
          // );
        }
      }
    });
    // console.log('found block', slot, block.blockhash);

    slot++;
    // await sleep(waitTime);
  }
}
