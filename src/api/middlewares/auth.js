import model from '../utils/model';

export default async (req, res, next) => {
  let _next = true;
  if (
    ![
      '/api/login-req',
      '/api/login',
      '/api/logout',
      '/api/landing/subscribe',
      '/api/check-klines',
    ].includes(req.originalUrl)
  ) {
    const { publicKey, lastAuthorizedPublicKey } = req.session;
    const { publicKey: clientPublicKey } = req.body;

    if (lastAuthorizedPublicKey && clientPublicKey && clientPublicKey === lastAuthorizedPublicKey) {
      req.session.publicKey = lastAuthorizedPublicKey;
      req.session.lastAuthorizedPublicKey = null;
      next();
      return;
    } else if (clientPublicKey && publicKey && clientPublicKey !== publicKey) {
      _next = false;
      req.session.publicKey = null;
    }

    if (publicKey && _next) {
      // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      const authModel = await model('auth');
      // log('ip', ip);

      const authorizationsCount = await authModel.size({
        // ip,
        publicKey,
        active: true,
      });

      _next = authorizationsCount > 0;
    } else {
      _next = false;
    }
  }

  if (_next) {
    next();
  } else {
    res.status(401).send('Unauthorized');
  }
};
