import session from 'express-session';
import cookieParser from 'cookie-parser';
import fileStore from 'session-file-store';

const sessionOptions = {
  secret: 'uind*237b78c7*B@b7d8137xqw',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 24 * 60 * 60 * 1000, // 24h
  },
};

if (process.env.NODE_ENV === 'development') {
  const FileStore = fileStore(session);
  sessionOptions.store = new FileStore({
    path: './run/sessions',
    reapInterval: -1, // dont clear sessions, dev only
  });
}

export default (app) => {
  app.use(cookieParser());
  app.set('trust proxy', 1);
  app.use(session(sessionOptions));
};
