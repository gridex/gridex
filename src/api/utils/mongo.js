const MongoClient = require('mongodb').MongoClient;
const dotenv = require('dotenv');
dotenv.config();

const { MONGO_HOST, MONGO_PORT, MONGO_DB /*, MONGO_DB_TEST*/ } = process.env;

const connect_string = `mongodb://${MONGO_HOST}:${MONGO_PORT}/`;
let dbo_;
let resolveDbo;
let dboProm = new Promise((res) => (resolveDbo = res));

export async function connectMongo() {
  const db = await MongoClient.connect(connect_string, { useUnifiedTopology: true });
  dbo_ = db.db(
    // eslint-disable-next-line no-undef
    MONGO_DB,
  );
  resolveDbo(dbo_);

  console.info('connected to mongodb', connect_string, MONGO_DB);

  await dbo_.collection('auth');
}

export const waitMongo = () => dboProm;

export const getMongo = () => dbo_;
