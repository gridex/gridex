import { waitMongo } from './mongo';

const ObjectId = require('mongodb').ObjectId;
const { getMongo } = require('./mongo');

export default function model(collectionName) {
  let collection;

  const model = {
    async get(pageSize = 999, current = 1) {
      return await collection
        .find({})
        .skip(current > 1 ? (current - 1) * pageSize : 0)
        .limit(pageSize)
        .sort({ _id: -1 })
        .toArray();
    },
    async size(data = {}) {
      if (data._id) {
        data._id = ObjectId(data._id);
      }
      return collection.find(data).count();
    },
    async add(data) {
      return await collection.insertOne(data);
    },
    async delete(_id, query) {
      return collection.deleteOne({
        _id: ObjectId(_id),
        ...query,
      });
    },

    async deleteMany(_ids) {
      return await collection.deleteMany({
        _id: {
          $in: _ids.map(ObjectId),
        },
      });
    },
    async updateOne(_id, data) {
      return collection.updateOne(
        { _id: ObjectId(_id) },
        {
          $set: data,
        },
      );
    },
    async update(selector, data) {
      if (selector._id) selector._id = ObjectId(selector._id);
      return collection.updateOne(selector, {
        $set: data,
      });
    },
    async upsert(selector, data) {
      if (selector._id) selector._id = ObjectId(selector._id);
      return collection.updateOne(selector, { $set: data }, { upsert: true });
    },
    async find(data, pageSize = 999, current = 1, sort = { _id: -1 }) {
      if (typeof data._id === 'string') {
        data._id = ObjectId(data._id);
      }
      return collection
        .find(data)
        .skip(current > 1 ? (current - 1) * pageSize : 0)
        .limit(pageSize)
        .sort(sort)
        .toArray();
    },
    async findOne(query, params) {
      if (query._id) {
        query._id = ObjectId(query._id);
      }
      return await collection.findOne(query, params);
    },
    async distinct(path, find = {}) {
      return collection.distinct(path, find);
    },
    async aggregate(query) {
      return collection.aggregate(query).toArray();
    },
  };

  const dbo = getMongo();
  if (!dbo) {
    waitMongo().then((dbo) => {
      model.collection = collection = dbo.collection(collectionName);
    });
  } else {
    model.collection = collection = dbo.collection(collectionName);
  }

  return model;
}
