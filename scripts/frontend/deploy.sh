#/usr/bin/env bash

# cleanup on error
function cleanup() {
  echo "cleanup"
}
#set 'cleanup; exit 1' ERR
set -e

if [ -z "$SSH_USER" -o -z "$SSH_HOST" -o -z "$DOMAIN" -o -z "$SSH_PRIVATE" ]; then
  echo "Please set SSH_USER, SSH_HOST, SSH_PRIVATE & DOMAIN env variables"
  exit 1;
fi

SSH_STR=${SSH_USER}@${SSH_HOST}
SSH="ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $SSH_PRIVATE"
BUILD_DIR=./build
WWW_DIR=/var/www/$DOMAIN


rsync -avzh \
  --no-perms --no-owner --no-group \
  -e "$SSH" --rsync-path="sudo rsync" \
  --delete --progress $BUILD_DIR/ $SSH_STR:$WWW_DIR
#$SSH $SSH_STR "sudo ..."

echo "deployment done"
