#!/bin/sh

# script to create nginx-undersandable passwd files

FILE=$1
USER=$2

if [ "X$USER" = "X" -o "X$FILE" = "X" ]; then
  echo "usage: $0 <file> <user>"
  exit 1
fi

echo -n "$USER:" >> $FILE
openssl passwd -apr1 >> $FILE
