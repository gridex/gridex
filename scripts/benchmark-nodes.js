const fs = require('fs/promises');
const { Connection, PublicKey } = require('@solana/web3.js');
const axios = require('axios');

const account = 'CsZ5LZkDS7h9TDKjrbL7VAwQZ9nsRu8vJLhRYfmGaN8K';
const accountKey = new PublicKey(account);

const wait = (ms) =>
  new Promise((res, rej) => setTimeout(() => rej(new Error('timeout ' + ms)), ms));

async function bench(address) {
  const conn = new Connection(address);

  const start = Date.now();
  try {
    await Promise.race([
      wait(1000),
      (async () => {
        if (!(await conn.getAccountInfo(accountKey))) throw new Error('null account');
        await conn.getAccountInfo(accountKey);
        await conn.getAccountInfo(accountKey);
        await conn.getAccountInfo(accountKey);
        await conn.getAccountInfo(accountKey);
      })(),
    ]);
    console.log(Date.now() - start, 'ms', address);
  } catch (err) {
    console.log(err.message.slice(0, 64), address);
  }
}

async function benchLong(address, i) {
  // const conn = new Connection(address);

  const start = Date.now();
  try {
    await Promise.race([
      wait(100000),
      (async () => {
        const ownerAddress = new PublicKey('4bepTKQsPpzSNTiR9XV96KgM5fuhBzjjjZGtWwvanZdf');

        const mint = new PublicKey('TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA');

        const response = await axios.post(
          address,
          {
            method: 'getProgramAccounts',
            jsonrpc: '2.0',
            params: [
              'TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA',
              {
                filters: [
                  { memcmp: { offset: 32, bytes: '4bepTKQsPpzSNTiR9XV96KgM5fuhBzjjjZGtWwvanZdf' } },
                  { dataSize: 165 },
                ],
              },
            ],
            id: '55944f9c-6e1f-493c-aba2-880a8a57ff41',
          },
          {
            headers: {
              'Content-Type': 'application/json',
            },
          },
        );
        const json = response.data;

        if (!json) throw new Error('null account');
      })(),
    ]);
    console.log(i, Date.now() - start, 'ms', address);
  } catch (err) {
    console.log(i, err.message.slice(0, 64), address);
  }
}

async function mainBenchShort() {
  const nodeContent = await fs.readFile('../nodes.txt');
  const nodes = nodeContent
    .toString()
    .split('\n')
    .map((l) => {
      let [ip, port] = l.split('|');
      if (!ip) return null;
      return `http://${ip.trim()}:${port.trim()}`;
    })
    .filter(Boolean);

  // await Promise.allSettled(nodes.map(addr => bench(addr)));
  for (let i = 0; i < nodes.length; i++) {
    const address = nodes[i];
    await bench(address);
  }
}

async function mainBenchLong() {
  const nodeContent = await fs.readFile('../nodes-bench-sort.txt');
  const nodes = nodeContent
    .toString()
    .split('\n')
    .map((l) => {
      let [num, ms, url] = l.split(' ');
      if (parseInt(num)) return url;
    })
    .filter(Boolean);

  await Promise.allSettled(nodes.map(benchLong));
  // for (let i = 0; i < nodes.length; i++) {
  //   const address = nodes[i];
  //   await benchLong(address);
  // }
}

// mainBenchShort().catch(console.error);
mainBenchLong().catch(console.error);
