#/usr/bin/env bash

# cleanup on error
function cleanup() {
  echo "cleanup"
}
#set 'cleanup; exit 1' ERR
set -e

if [ -z "$SSH_USER" -o -z "$SSH_HOST" -o -z "$SSH_PRIVATE" -o -z "$DOMAIN" ]; then
  echo "Please set SSH_USER, SSH_HOST, SSH_PRIVATE & DOMAIN env variables"
  exit 1;
fi

SSH_STR=${SSH_USER}@${SSH_HOST}
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $SSH_PRIVATE"
SSH="ssh $SSH_OPTS"
SCP="scp $SSH_OPTS"

BUILD_DIR="~/backend/"
WWW_DIR=/var/www/$DOMAIN

BUNDLE="
  build_api/out.js
  build_api/out.js.map
  scripts/backend/Dockerfile
  scripts/backend/run.sh
"

rsync -avzh \
  --no-perms --no-owner --no-group \
  -e "$SSH" --rsync-path="sudo rsync" \
  --delete --progress $BUNDLE $SSH_STR:$BUILD_DIR

$SSH $SSH_STR "cd $BUILD_DIR && DOCKER_PORT=$DOCKER_PORT DOCKER_NAME=$DOCKER_NAME CHART_DATA_URL=$CHART_DATA_URL TELEGRAM_BOT_TOKEN=$TELEGRAM_BOT_TOKEN TELEGRAM_BOT_ENABLED=$TELEGRAM_BOT_ENABLED TELEGRAM_SUPPORT_CHAT_ID=$TELEGRAM_SUPPORT_CHAT_ID TELEGRAM_BOT_NAME=$TELEGRAM_BOT_NAME REACT_APP_USDT_REFERRAL_FEES_ADDRESS=$REACT_APP_USDT_REFERRAL_FEES_ADDRESS REACT_APP_USDC_REFERRAL_FEES_ADDRESS=$REACT_APP_USDC_REFERRAL_FEES_ADDRESS REACT_APP_SOL_REFERRAL_FEES_ADDRESS=$REACT_APP_SOL_REFERRAL_FEES_ADDRESS ./run.sh"

echo "deployment done"
