const express = require('express');
const path = require('path');
const app = express();
const api = require('./build_api/out');

app.use(express.static(path.join(__dirname, 'build')));

api.default(app);

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(9000);
