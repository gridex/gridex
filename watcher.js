const { exec } = require('child_process');

const chokidar = require('chokidar');
const debounсe = require('lodash/debounce');

function handler() {
  const start = Date.now();
  exec('yarn backend:build:dev', (err, stdout, stderr) => {
    if (err) {
      console.error('Error while building', err);
      return;
    }

    const milliseconds = Date.now() - start;
    console.log(`Build is ready. Building time: ${milliseconds} milliseconds`);
  });
}

chokidar
  .watch(['./src/api', './src/common', './src/processor', './src/telegram'])
  .on('all', debounсe(handler, 300));
