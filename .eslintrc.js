module.exports = {
  parser: '@typescript-eslint/parser',
  env: {
    commonjs: true,
    browser: true,
    jest: true,
    es6: true,
  },
  globals: {
    process: true,
    globalThis: true,
  },
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  ignorePatterns: ['*.min.js'],
  plugins: ['react', 'prettier', '@typescript-eslint'],
  rules: {
    'no-async-promise-executor': 0,
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.js', '.jsx', '.tsx'],
      },
    ],
    curly: 'error',
    'no-empty': ['error', { allowEmptyCatch: true }],
    'prettier/prettier': 'warn',
    // note you must disable the base rule as it can report incorrect errors
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['error'],
  },
};
